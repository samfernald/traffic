//
//  MorningCommuteViewController.h
//  TrafficViewer
//
//  Created by sfernald on 10/29/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EveningCommuteViewController.h"
#import "TouchXML.h"
#import "Commute.h"
#import "Congestion.h"
#import "Segment.h"
#import "User.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "CommuteCell.h"
#import "SoundEffect.h"
#import "CommuteType.h"
#import "DejalActivityView.h"


@interface CommuteViewController : UITableViewController <UIAccelerometerDelegate> {
	IBOutlet UIBarButtonItem *altButton;
	IBOutlet UIBarButtonItem *eveningButton;
	IBOutlet UIBarButtonItem *refreshButton;
	
	
	
	AppDelegate *appDelegate;
	NSMutableData * receivedData;
	NSURLConnection * conn;
	UIAlertView *notifier;
	UIAlertView *noCommuteNotifier;
	
	NSMutableArray *congestionInfo;
	NSMutableArray *userInfo;
	NSMutableArray *morningCommuteInfo;
	NSMutableArray *eveningCommuteInfo;
	NSMutableArray *altMorningCommuteInfo;
	NSMutableArray *altEveningCommuteInfo;
	NSMutableArray *segmentInfo;
	NSMutableArray *segmentsCounter;
	NSMutableArray *commuteCells;
	NSMutableArray *incidentInfo;
	CommuteCell *commuteCellForImage;
	BOOL histeresisExcited;
	BOOL priAlt;
	BOOL loadMorning;
    BOOL refreshing;
	UIAcceleration* lastAcceleration;
	UISegmentedControl * segmentedControl;
	NSString *filePath;

	
	int morningCommuteSegments,altMorningCommuteSegments,eveningCommuteSegments,altEveningCommuteSegments,segmentsTracker;
	int congestionIndex,segmentsRemaining,segmentsDone;
	
	//User contains user info
	User * user;
	Commute * tempMorning;
	Commute * tempAltMorning;
	Commute * tempEvening;
	Commute * tempAltEvening;
	Congestion * tempCong;
	Segment * tempSeg;
	
	SoundEffect * hornSound;
	
}

@property (nonatomic,retain) UIBarButtonItem * altButton;
@property (nonatomic,retain) UIBarButtonItem * eveningButton;
@property (nonatomic,retain) UIBarButtonItem * refreshButton;
@property (nonatomic,retain) CommuteCell * commuteCellForImage;
@property (nonatomic,retain) UIAcceleration * lastAcceleration;
@property (nonatomic, retain) UISegmentedControl *segmentedControl;
@property (nonatomic,retain) SoundEffect * hornSound;
@property (nonatomic,retain) NSString * filePath;




-(void) createCommuteInfo;

-(void) prepareCommuteCells:(Commute *)c;

-(BOOL) L0AccelerationIsShaking:(UIAcceleration*)last current:(UIAcceleration*)current threshold:(double) threshold;

- (void) segmentAction:(id)sender;

@end
