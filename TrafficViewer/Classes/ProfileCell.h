//
//  ProfileCell.h
//  TrafficViewer
//
//  Created by Sam Fernald on 9/12/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProfileCell : NSObject {
    int displayType;
    NSString *displayText;
    id container;
}

@property int displayType;
@property (nonatomic, retain) NSString * displayText;
@property (assign) id container;

-(id) initWithDisplay:(NSString*)d type:(int)t connection:(id)o;

@end
