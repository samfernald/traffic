//
//  DailyReport.h
//  TrafficViewer
//
//  Created by Sam Fernald on 9/10/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DailyReport : NSObject{
    int reportID;
    int commuteType;
    int hour;
    int minute;
    int ampm;
    int notify;
    int day;
}

@property int reportID;
@property int commuteType;
@property int hour;
@property int minute;
@property int ampm;
@property int notify;
@property int day;

-(id) initWithID:(int)rid
     commuteType:(int)t 
            hour:(int)h 
          minute:(int)m 
            ampm:(int)a 
          notify:(int)n 
             day:(int)d;

@end
