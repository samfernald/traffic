//
//  SDHighwaysViewController.h
//  TrafficViewer
//
//  Created by sfernald on 10/29/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HighwayFlowViewController.h"

@interface SDHighwaysViewController : UIViewController {
	HighwayFlowViewController *highwayFlowViewController; 
}

@property (nonatomic, retain) HighwayFlowViewController *highwayFlowViewController;

-(IBAction) highwaySelected:(id) sender;

@end
