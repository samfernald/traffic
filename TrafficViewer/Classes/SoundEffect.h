//
//  SoundEffect.h
//  TrafficViewer
//
//  Created by sfernald on 4/10/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//



#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioServices.h>

@interface SoundEffect : NSObject {
    SystemSoundID _soundID;
}

+ (id)soundEffectWithContentsOfFile:(NSString *)aPath;
- (id)initWithContentsOfFile:(NSString *)path;
- (void)play;

@end
