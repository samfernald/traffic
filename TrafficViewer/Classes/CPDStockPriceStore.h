
//CURRENTLY USING THIS AS THE DATA SOURCE DELEGATE

#import "AppDelegate.h"

@interface CPDStockPriceStore : NSObject {

}
+ (CPDStockPriceStore *)sharedInstance;



- (NSArray *)datesInWeek;
//- (NSArray *)weeklyPrices:(NSString *)tickerSymbol;

- (NSArray *)xAxisLabels;
- (NSArray *)dailyTimes:(NSString *)tickerSymbol;
+ (NSMutableArray *)generateData;

@end
