//
//  pointInfo.h
//  mapLines
//
//  Created by sfernald on 2/17/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class mapLinesViewController;

@interface mapLinesAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    mapLinesViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet mapLinesViewController *viewController;

@end

