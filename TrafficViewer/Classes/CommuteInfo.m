//
//  CommuteInfo.m
//  TrafficViewer
//
//  Created by sfernald on 11/5/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "CommuteInfo.h"


@implementation CommuteInfo

@synthesize currentTime;
@synthesize averageTime;
@synthesize otherTime;
@synthesize difference;
@synthesize distance;
@synthesize commuteType;


-(id) initWithCurrent:(int)currTime averageTime:(int)ave otherTime:(int)other difference:(float)diff distance:(float)dist commuteType:(BOOL)type{
	currentTime = currTime;
	averageTime = ave;
	otherTime = other;
	difference = diff;
	distance = dist;
	commuteType = type;
	return self;
}

@end
