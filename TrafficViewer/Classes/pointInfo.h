//
//  pointInfo.h
//  mapLines
//
//  Created by sfernald on 2/17/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface pointInfo : NSObject {
	int highway;
	int mph;

}

@property int highway;
@property int mph;

-(id) initWithHwy:(int)hwy
			speed:(int)milesperhour;
@end
