//
//  DailyReportViewController.h
//  TrafficViewer
//
//  Created by Sam Fernald on 8/31/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "DailyReport.h"

@interface DailyReportViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate, UIAlertViewDelegate>{
    AppDelegate *appDelegate;
    IBOutlet UITableView *reportProfile;
    NSMutableArray *reportParts;
    NSMutableArray *addParts;
    
    UIPickerView * typePicker;
    UIPickerView * weekdayPicker;
    UIPickerView * timePicker;
    UIPickerView * notificationPicker;
    
    UIActionSheet *actionSheetType;
    UIActionSheet *actionSheetWeekday;
    UIActionSheet *actionSheetTime;
    UIActionSheet *actionSheetNotification;
    
    NSMutableArray *typeList;
    NSMutableArray *notificationTypeList;
    NSMutableArray *hourList;
    NSMutableArray *minList;
    NSMutableArray *ampmList;
    NSMutableArray *weekdayList;
    
    
    NSURLConnection *conn;
    NSMutableData *receivedData;
    NSMutableData *receivedIDData;
    NSMutableData *receivedNewReportData;
    NSMutableData *receivedEditedReportData;
    NSMutableArray *reportList;
    //UISegmentedControl *segmentedControl;
    
    DailyReport *morningReport;
    DailyReport *eveningReport;
    
    DailyReport *selectedReport;
    
    NSString * amORpm;
    
    BOOL isEditingReport;
    BOOL isAdding;
    BOOL editsDisplayed;
    BOOL typeSelected;
    BOOL weekdaySelected;
    BOOL timeSelected;
    BOOL notificationSelected;
    BOOL reportTypeSet;
    
    int commuteType;
    
    int hour;
    int min;
    int ampm;
    int weekday;
    int notificationType;
    
    int numMorning;
    int numEvening;
    int numAltMorning;
    int numAltEvening;
    
    int selectedReportID;
    
    int connectionType;
    
}

-(void) editReport;
-(void) deleteReport;

-(void) selectType;
-(void) selectWeekday;
-(void) selectTime;
-(void) selectNotification;

-(void) addReport;

-(void) selectedType:(id)sender;
-(void) selectedTime:(id)sender;
-(void) selectedCongestion:(id)sender;
-(void) selectedNotification:(id)sender;


-(void) removePartIn:(NSMutableArray*)parts forType:(int) t;

@end
