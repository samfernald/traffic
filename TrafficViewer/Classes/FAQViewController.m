//
//  FAQViewController.m
//  TrafficViewer
//
//  Created by sfernald on 1/14/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "FAQViewController.h"
#import "AppDelegate.h"
#import "GAi.h"


@implementation FAQViewController

@synthesize faqView;


/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[self setTitle:@"FAQ"];
    [super viewDidLoad];
    //self.trackedViewName = @"FAQ view";
    //id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-33122525-1"];
    //[tracker sendView:@"FAQ view"];
	
	
	UIScrollView * scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 1500)];//(0,0,320,160)
	//scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	
	[scrollView setContentSize:CGSizeMake(320,1500)];
	
	scrollView.bounces = NO;
	scrollView.scrollEnabled = YES;
	[scrollView setBackgroundColor:[UIColor whiteColor]];
	
    //UIImage *img = [UIImage imageNamed:@"Traffic_App_FAQ.png"];
	
	//UIImageView *viewForImage = [[UIImageView alloc] initWithImage:img];
	//self.view = viewForImage;

    
    UILabel *question1 = [[UILabel alloc] initWithFrame:CGRectMake(8, 10, 300, 40)];
    question1.font = [UIFont boldSystemFontOfSize:16];
    question1.text = @"Why do certain regions and highways not have traffic coverage?";
    question1.lineBreakMode = NSLineBreakByWordWrapping;
    question1.numberOfLines = 2;
    [scrollView addSubview:question1];
    [question1 release];

	UILabel *answer1 = [[UILabel alloc] initWithFrame:CGRectMake(25, 50, 300, 90)];
    answer1.font = [UIFont systemFontOfSize:14];
    answer1.text = @"We get the traffic data from Caltrans and they have gaps in their coverage. We depend on Caltrans for traffic data and we cannot extend coverage on our own. Google provides the traffic data for the Google Maps.";
    answer1.lineBreakMode = NSLineBreakByWordWrapping;
    answer1.numberOfLines = 5;
    [scrollView addSubview:answer1];
    [answer1 release];
    
    
    UILabel *question2 = [[UILabel alloc] initWithFrame:CGRectMake(8, 160, 300, 20)];
    question2.font = [UIFont boldSystemFontOfSize:16];
    question2.text = @"How often are the speeds updated?";
    question2.lineBreakMode = NSLineBreakByWordWrapping;
    question2.numberOfLines = 1;
    [scrollView addSubview:question2];
    [question2 release];

	UILabel *answer2 = [[UILabel alloc] initWithFrame:CGRectMake(25, 180, 300, 35)];
    answer2.font = [UIFont systemFontOfSize:14];
    answer2.text = @"Every 5 minutes for LA and every minute for San Diego and the Bay Area.";
    answer2.lineBreakMode = NSLineBreakByWordWrapping;
    answer2.numberOfLines = 2;
    [scrollView addSubview:answer2];
    [answer2 release];
    
    
    UILabel *question3 = [[UILabel alloc] initWithFrame:CGRectMake(8, 235, 300, 20)];
    question3.font = [UIFont boldSystemFontOfSize:16];
    question3.text = @"How do I get personal traffic reports?";
    question3.lineBreakMode = NSLineBreakByWordWrapping;
    question3.numberOfLines = 1;
    [scrollView addSubview:question3];
    [question3 release];
    
	UILabel *answer3 = [[UILabel alloc] initWithFrame:CGRectMake(25, 255, 300, 105)];
    answer3.font = [UIFont systemFontOfSize:14];
    answer3.text = @"Sign up for free at http://traffic.calit2.net and add your commute information. Then login to the iPhone app with the ID you created. We recommend you signup using a Mac or a PC instead of using your phone, but it's not mandatory.";
    answer3.lineBreakMode = NSLineBreakByWordWrapping;
    answer3.numberOfLines = 6;
    [scrollView addSubview:answer3];
    [answer3 release];
    
    
    UILabel *question4 = [[UILabel alloc] initWithFrame:CGRectMake(8, 380, 300, 40)];
    question4.font = [UIFont boldSystemFontOfSize:16];
    question4.text = @"Can I get traffic reports without signing up?";
    question4.lineBreakMode = NSLineBreakByWordWrapping;
    question4.numberOfLines = 2;
    [scrollView addSubview:question4];
    [question4 release];
    
	UILabel *answer4 = [[UILabel alloc] initWithFrame:CGRectMake(25, 420, 300, 40)];
    answer4.font = [UIFont systemFontOfSize:14];
    answer4.text = @"Yes. Click on the \"Guest\" button on the login screen.";
    answer4.lineBreakMode = NSLineBreakByWordWrapping;
    answer4.numberOfLines = 2;
    [scrollView addSubview:answer4];
    [answer4 release];

    
    UILabel *question5 = [[UILabel alloc] initWithFrame:CGRectMake(8, 480, 300, 60)];
    question5.font = [UIFont boldSystemFontOfSize:16];
    question5.text = @"What do the numbers next to Commute Time, Typical, Diff and Distance in my traffic report mean?";
    question5.lineBreakMode = NSLineBreakByWordWrapping;
    question5.numberOfLines = 3;
    [scrollView addSubview:question5];
    [question5 release];
    
    UILabel *answer5h = [[UILabel alloc] initWithFrame:CGRectMake(25, 542, 290, 15)];
    answer5h.font = [UIFont systemFontOfSize:14];
    answer5h.textColor = [UIColor blueColor];
    answer5h.text = @"Commute Time:";
    answer5h.lineBreakMode = NSLineBreakByWordWrapping;
    answer5h.numberOfLines = 1;
    [scrollView addSubview:answer5h];
    [answer5h release];
    
	UILabel *answer5a = [[UILabel alloc] initWithFrame:CGRectMake(25, 540, 290, 90)];
    answer5a.backgroundColor = [UIColor clearColor];
    answer5a.font = [UIFont systemFontOfSize:14];
    answer5a.text = @"                           Your highway travel time in minutes, based on current traffic conditions. It does not account for the time it takes to drice side streets. This number will be less than your actual door to door commute time.";
    answer5a.lineBreakMode = NSLineBreakByWordWrapping;
    answer5a.numberOfLines = 5;
    [scrollView addSubview:answer5a];
    [answer5a release];

    UILabel *answer5h2 = [[UILabel alloc] initWithFrame:CGRectMake(25, 651, 290, 15)];
    answer5h2.font = [UIFont systemFontOfSize:14];
    answer5h2.textColor = [UIColor blueColor];
    answer5h2.text = @"Typical:";
    answer5h2.lineBreakMode = NSLineBreakByWordWrapping;
    answer5h2.numberOfLines = 1;
    [scrollView addSubview:answer5h2];
    [answer5h2 release];
    
	UILabel *answer5b = [[UILabel alloc] initWithFrame:CGRectMake(25, 650, 290, 35)];
    answer5b.backgroundColor = [UIColor clearColor];
    answer5b.font = [UIFont systemFontOfSize:14];
    answer5b.text = @"              The time it usually takes to drive this route at this time.";
    answer5b.lineBreakMode = NSLineBreakByWordWrapping;
    answer5b.numberOfLines = 2;
    [scrollView addSubview:answer5b];
    [answer5b release];
    
    UILabel *answer5h3 = [[UILabel alloc] initWithFrame:CGRectMake(25, 702, 290, 15)];
    answer5h3.font = [UIFont systemFontOfSize:14];
    answer5h3.textColor = [UIColor blueColor];
    answer5h3.text = @"Diff:";
    answer5h3.lineBreakMode = NSLineBreakByWordWrapping;
    answer5h3.numberOfLines = 1;
    [scrollView addSubview:answer5h3];
    [answer5h3 release];
    
	UILabel *answer5c = [[UILabel alloc] initWithFrame:CGRectMake(25, 700, 290, 90)];
    answer5c.backgroundColor = [UIColor clearColor];
    answer5c.font = [UIFont systemFontOfSize:14];
    answer5c.text = @"       The percentage difference between Commute Time and Typical Time. If the number is positive, your commute is worse than usual and if it is negative it is better than usual.";
    answer5c.lineBreakMode = NSLineBreakByWordWrapping;
    answer5c.numberOfLines = 5;
    [scrollView addSubview:answer5c];
    [answer5c release];
    
    UILabel *answer5h4 = [[UILabel alloc] initWithFrame:CGRectMake(25, 807, 290, 15)];
    answer5h4.font = [UIFont systemFontOfSize:14];
    answer5h4.textColor = [UIColor blueColor];
    answer5h4.text = @"Distance:";
    answer5h4.lineBreakMode = NSLineBreakByWordWrapping;
    answer5h4.numberOfLines = 1;
    [scrollView addSubview:answer5h4];
    [answer5h4 release];
    
	UILabel *answer5d = [[UILabel alloc] initWithFrame:CGRectMake(25, 805, 290, 35)];
    answer5d.backgroundColor = [UIColor clearColor];
    answer5d.font = [UIFont systemFontOfSize:14];
    answer5d.text = @"                The total highway distance for your commute.";
    answer5d.lineBreakMode = NSLineBreakByWordWrapping;
    answer5d.numberOfLines = 2;
    [scrollView addSubview:answer5d];
    [answer5d release];

    
    UILabel *question6 = [[UILabel alloc] initWithFrame:CGRectMake(8, 860, 300, 20)];
    question6.font = [UIFont boldSystemFontOfSize:16];
    question6.text = @"Is the service free?";
    question6.lineBreakMode = NSLineBreakByWordWrapping;
    question6.numberOfLines = 1;
    [scrollView addSubview:question6];
    [question6 release];
    
	UILabel *answer6 = [[UILabel alloc] initWithFrame:CGRectMake(25, 880, 300, 20)];
    answer6.font = [UIFont systemFontOfSize:14];
    answer6.text = @"Yes, the service is 100% free.";
    answer6.lineBreakMode = NSLineBreakByWordWrapping;
    answer6.numberOfLines = 5;
    [scrollView addSubview:answer6];
    [answer6 release];
    
    
    UILabel *question7 = [[UILabel alloc] initWithFrame:CGRectMake(8, 920, 300, 40)];
    question7.font = [UIFont boldSystemFontOfSize:16];
    question7.text = @"How do I get traffic alerts and daily traffic reports via SMS and email?";
    question7.lineBreakMode = NSLineBreakByWordWrapping;
    question7.numberOfLines = 2;
    [scrollView addSubview:question7];
    [question7 release];
    
	UILabel *answer7 = [[UILabel alloc] initWithFrame:CGRectMake(25, 960, 300, 35)];
    answer7.font = [UIFont systemFontOfSize:14];
    answer7.text = @"You can do this by logging into http://traffic.calit2.net";
    answer7.lineBreakMode = NSLineBreakByWordWrapping;
    answer7.numberOfLines = 2;
    [scrollView addSubview:answer7];
    [answer7 release];
    
    
    UILabel *question8 = [[UILabel alloc] initWithFrame:CGRectMake(8, 1020, 300, 40)];
    question8.font = [UIFont boldSystemFontOfSize:16];
    question8.text = @"Can I get traffic reports by making a phone call?";
    question8.lineBreakMode = NSLineBreakByWordWrapping;
    question8.numberOfLines = 2;
    [scrollView addSubview:question8];
    [question8 release];
    
	UILabel *answer8a = [[UILabel alloc] initWithFrame:CGRectMake(25, 1060, 300, 20)];
    answer8a.font = [UIFont systemFontOfSize:14];
    answer8a.text = @"Yes. You can call toll free the numbers below:";
    answer8a.lineBreakMode = NSLineBreakByWordWrapping;
    answer8a.numberOfLines = 1;
    [scrollView addSubview:answer8a];
    [answer8a release];
    
    UILabel *answer8b = [[UILabel alloc] initWithFrame:CGRectMake(25, 1090, 300, 35)];
    answer8b.font = [UIFont systemFontOfSize:14];
    answer8b.text = @"Los Angeles, Orange County & Insland Empire: (888) 922-5482";
    answer8b.lineBreakMode = NSLineBreakByWordWrapping;
    answer8b.numberOfLines = 2;
    [scrollView addSubview:answer8b];
    [answer8b release];
    
    UILabel *answer8c = [[UILabel alloc] initWithFrame:CGRectMake(25, 1125, 300, 20)];
    answer8c.font = [UIFont systemFontOfSize:14];
    answer8c.text = @"San Diego: (866) 500-0977";
    answer8c.lineBreakMode = NSLineBreakByWordWrapping;
    answer8c.numberOfLines = 1;
    [scrollView addSubview:answer8c];
    [answer8c release];
    
    UILabel *answer8d = [[UILabel alloc] initWithFrame:CGRectMake(25, 1145, 300, 20)];
    answer8d.font = [UIFont systemFontOfSize:14];
    answer8d.text = @"Bay Area: (888) 422-5482";
    answer8d.lineBreakMode = NSLineBreakByWordWrapping;
    answer8d.numberOfLines = 1;
    [scrollView addSubview:answer8d];
    [answer8d release];
    
    
    UILabel *question9 = [[UILabel alloc] initWithFrame:CGRectMake(8, 1185, 300, 40)];
    question9.font = [UIFont boldSystemFontOfSize:16];
    question9.text = @"How can I determine the best time to commute?";
    question9.lineBreakMode = NSLineBreakByWordWrapping;
    question9.numberOfLines = 2;
    [scrollView addSubview:question9];
    [question9 release];
    
	UILabel *answer9 = [[UILabel alloc] initWithFrame:CGRectMake(25, 1225, 300, 35)];
    answer9.font = [UIFont systemFontOfSize:14];
    answer9.text = @"Log in to http://traffic.calit2.net for that information.";
    answer9.lineBreakMode = NSLineBreakByWordWrapping;
    answer9.numberOfLines = 2;
    [scrollView addSubview:answer9];
    [answer9 release];

    
    UILabel *question10 = [[UILabel alloc] initWithFrame:CGRectMake(8, 1280, 300, 20)];
    question10.font = [UIFont boldSystemFontOfSize:16];
    question10.text = @"Where can I find more information?";
    question10.lineBreakMode = NSLineBreakByWordWrapping;
    question10.numberOfLines = 1;
    [scrollView addSubview:question10];
    [question10 release];
    
	UILabel *answer10a = [[UILabel alloc] initWithFrame:CGRectMake(25, 1300, 300, 20)];
    answer10a.font = [UIFont systemFontOfSize:14];
    answer10a.text = @"Los Angeles: http://traffic.calit2.net/la/faq.jsp";
    answer10a.lineBreakMode = NSLineBreakByWordWrapping;
    answer10a.numberOfLines = 1;
    [scrollView addSubview:answer10a];
    [answer10a release];
    
    UILabel *answer10b = [[UILabel alloc] initWithFrame:CGRectMake(25, 1320, 300, 20)];
    answer10b.font = [UIFont systemFontOfSize:14];
    answer10b.text = @"San Diego: http://traffic.calit2.net/sd/faq.jsp";
    answer10b.lineBreakMode = NSLineBreakByWordWrapping;
    answer10b.numberOfLines = 1;
    [scrollView addSubview:answer10b];
    [answer10b release];
    
    UILabel *answer10c = [[UILabel alloc] initWithFrame:CGRectMake(25, 1340, 300, 20)];
    answer10c.font = [UIFont systemFontOfSize:14];
    answer10c.text = @"Bay Area: http://traffic.calit2.net/bayarea/faq.jsp";
    answer10c.lineBreakMode = NSLineBreakByWordWrapping;
    answer10c.numberOfLines = 1;
    [scrollView addSubview:answer10c];
    [answer10c release];
    /*
    UILabel *question = [[UILabel alloc] initWithFrame:CGRectMake(8, , 300, 20)];
    question.font = [UIFont boldSystemFontOfSize:16];
    question.text = @"";
    question.lineBreakMode = NSLineBreakByWordWrapping;
    question.numberOfLines = 1;
    [scrollView addSubview:question];
    [question release];
    
	UILabel *answer = [[UILabel alloc] initWithFrame:CGRectMake(25, , 300, 90)];
    answer.font = [UIFont systemFontOfSize:14];
    answer.text = @"";
    answer.lineBreakMode = NSLineBreakByWordWrapping;
    answer.numberOfLines = 5;
    [scrollView addSubview:answer];
    [answer release];
     */
    
	self.view = scrollView;
	[scrollView retain];
}

-(void)viewDidAppear:(BOOL) animated {
	[super viewDidAppear:animated];
	//AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	//[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:NO];
}

- (void)viewDidDisappear:(BOOL)animated {
	
	//AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	//[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:YES];
}




/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; 
	// Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


- (void)dealloc {
    [super dealloc];
}


@end
