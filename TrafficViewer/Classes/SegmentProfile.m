//
//  SegmentProfile.m
//  TrafficViewer
//
//  Created by Sam Fernald on 9/10/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import "SegmentProfile.h"

@implementation SegmentProfile

@synthesize segmentID, onRamp, offRamp, commuteType, sequence, highway, onRampName, offRampName;

-(id) initWithID:(int)sid 
          onRamp:(int)on 
         offRamp:(int)off
     commuteType:(int)t 
        sequence:(int)s 
         highway:(NSString *)h 
      onRampName:(NSString*)onrn 
     offRampName:(NSString*)offrn{
    self.segmentID = sid;
    self.onRamp = on;
    self.offRamp = off;
    self.commuteType = t;
    self.sequence = s;
    self.highway = h;
    self.onRampName = onrn;
    self.offRampName = offrn;
    return self;
}

@end
