//
//  SegmentCreationViewController.m
//  TrafficViewer
//
//  Created by Sam Fernald on 8/31/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import "SegmentCreationViewController.h"

@interface SegmentCreationViewController ()

@end

@implementation SegmentCreationViewController

@synthesize typeList, highwayList, exitList, exitOffList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    onRampButton.enabled = FALSE;
    offRampButton.enabled = FALSE;
    onRampSelected = FALSE;
    offRampSelected = FALSE;
    //--------------TYPE ACTION SHEET
    [actionSheetType dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetType=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarType = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarType sizeToFit];
    pickerToolbarType.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsType = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnType = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedType:)];
    [barItemsType addObject:doneBtnType];
    [doneBtnType release];
    [pickerToolbarType setItems:barItemsType animated:YES];
    [actionSheetType addSubview:pickerToolbarType];
    [barItemsType release];
    [pickerToolbarType release];
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    typePicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    typePicker.showsSelectionIndicator = YES;
    typePicker.dataSource = self;
    typePicker.delegate = self;
    typePicker.tag = 0;
    typePicker.showsSelectionIndicator = YES;
    [actionSheetType addSubview:typePicker];
    
    //--------------Highway ACTION SHEET
    [actionSheetHighway dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetHighway=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarHighway = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarHighway sizeToFit];
    pickerToolbarHighway.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsHighway = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnHighway = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedHighway:)];
    [barItemsHighway addObject:doneBtnHighway];
    [doneBtnHighway release];
    [pickerToolbarHighway setItems:barItemsHighway animated:YES];
    [actionSheetHighway addSubview:pickerToolbarHighway];
    [barItemsHighway release];
    [pickerToolbarHighway release];
    highwayPicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    highwayPicker.showsSelectionIndicator = YES;
    highwayPicker.dataSource = self;
    highwayPicker.delegate = self;
    highwayPicker.tag = 1;
    highwayPicker.showsSelectionIndicator = YES;
    [actionSheetHighway addSubview:highwayPicker];
    
    //--------------onRamp ACTION SHEET
    [actionSheetOnRamp dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetOnRamp = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarOnRamp = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarOnRamp sizeToFit];
    pickerToolbarOnRamp.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsOnRamp = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnOnRamp = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedOnRamp:)];
    [barItemsOnRamp addObject:doneBtnOnRamp];
    [doneBtnOnRamp release];
    [pickerToolbarOnRamp setItems:barItemsOnRamp animated:YES];
    [actionSheetOnRamp addSubview:pickerToolbarOnRamp];
    [barItemsOnRamp release];
    [pickerToolbarOnRamp release];
    onRampPicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    onRampPicker.showsSelectionIndicator = YES;
    onRampPicker.dataSource = self;
    onRampPicker.delegate = self;
    onRampPicker.tag = 2;
    onRampPicker.showsSelectionIndicator = YES;
    [actionSheetOnRamp addSubview:onRampPicker];
    
    //--------------offRamp ACTION SHEET
    [actionSheetOffRamp dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetOffRamp=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarOffRamp = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarOffRamp sizeToFit];
    pickerToolbarOffRamp.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsOffRamp = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnOffRamp = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedOffRamp:)];
    [barItemsOffRamp addObject:doneBtnOffRamp];
    [doneBtnOffRamp release];
    [pickerToolbarOffRamp setItems:barItemsOffRamp animated:YES];
    [actionSheetOffRamp addSubview:pickerToolbarOffRamp];
    [barItemsOffRamp release];
    [pickerToolbarOffRamp release];
    offRampPicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    offRampPicker.showsSelectionIndicator = YES;
    offRampPicker.dataSource = self;
    offRampPicker.delegate = self;
    offRampPicker.tag = 3;
    offRampPicker.showsSelectionIndicator = YES;
    [actionSheetOffRamp addSubview:offRampPicker];
    
    
    
    
    typeList = [[NSMutableArray alloc] init];
    highwayList = [[NSMutableArray alloc] init];
    exitList = [[NSMutableArray alloc] init];
    exitOffList = [[NSMutableArray alloc] init];
    
    [typeList addObject:[[ProfileCell alloc] initWithDisplay:@"Morning" type:0 connection:NULL]];
    [typeList addObject:[[ProfileCell alloc] initWithDisplay:@"Evening" type:1 connection:NULL]];
    [typeList addObject:[[ProfileCell alloc] initWithDisplay:@"Alternate Morning" type:2 connection:NULL]];
    [typeList addObject:[[ProfileCell alloc] initWithDisplay:@"Alternate Evening" type:3 connection:NULL]];
    
    if(appDelegate.selectedCity == 1){
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"5 North" type:1 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"5 South" type:2 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"8 East" type:3 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"8 West" type:4 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"15 North" type:5 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"15 South" type:6 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"52 East" type:7 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"52 West" type:8 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"54 East" type:9 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"54 West" type:10 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"56 East" type:11 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"56 West" type:12 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"78 East" type:13 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"78 West" type:14 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"94 East" type:15 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"94 West" type:16 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"125 North" type:17 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"125 South" type:18 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"163 North" type:19 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"163 South" type:20 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"805 North" type:21 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"805 South" type:22 connection:NULL]];
        
    } else if(appDelegate.selectedCity == 2){
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"2 North" type:1 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"2 South" type:2 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"5 North" type:3 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"5 South" type:4 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"10 East" type:5 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"10 West" type:6 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"14 North" type:7 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"14 South" type:8 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"15 North" type:49 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"15 South" type:50 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"22 East" type:9 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"22 West" type:10 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"55 North" type:13 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"55 South" type:14 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"57 North" type:15 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"57 South" type:16 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"60 East" type:17 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"60 West" type:18 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"71 North" type:51 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"71 South" type:51 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"73 North" type:19 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"73 South" type:20 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"91 East" type:21 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"91 West" type:21 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"101 North" type:23 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"101 South" type:24 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"105 East" type:25 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"105 West" type:26 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"110 North" type:27 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"110 South" type:28 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"118 East" type:29 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"118 West" type:30 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"133 North" type:31 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"133 South" type:32 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"134 East" type:33 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"134 West" type:34 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"170 North" type:35 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"170 South" type:36 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"210 East" type:37 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"210 West" type:38 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"215 North" type:53 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"215 South" type:54 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"241 North" type:39 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"241 South" type:39 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"261 North" type:41 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"261 South" type:41 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"405 North" type:43 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"405 South" type:44 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"605 North" type:45 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"605 South" type:46 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"710 North" type:47 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"710 South" type:48 connection:NULL]];
        
    } else if(appDelegate.selectedCity == 3){
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"1 North" type:41 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"1 South" type:42 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"4 East" type:1 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"4 West" type:2 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"17 North" type:5 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"17 South" type:6 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"24 East" type:7 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"24 West" type:8 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"37 East" type:9 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"37 West" type:10 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"80 East" type:11 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"80 West" type:12 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"84 East" type:13 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"84 West" type:14 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"85 North" type:15 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"85 South" type:16 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"87 North" type:45 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"87 South" type:46 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"92 East" type:17 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"92 West" type:18 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"101 North" type:19 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"101 South" type:20 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"205 East" type:43 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"205 West" type:44 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"237 East" type:21 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"237 West" type:22 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"238 North" type:23 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"238 South" type:24 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"242 North" type:25 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"242 South" type:26 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"280 North" type:27 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"280 South" type:27 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"505 North" type:47 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"505 South" type:48 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"580 East" type:30 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"580 West" type:31 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"680 North" type:33 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"680 South" type:33 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"880 North" type:37 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"880 South" type:37 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"980 East" type:39 connection:NULL]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"980 West" type:40 connection:NULL]];
    }
    
    NSString* filePath;
    if(appDelegate.selectedCity == 1) {
        filePath = [[NSBundle mainBundle] pathForResource:@"SanDiegoMap" ofType:@"csv"];
    } else if(appDelegate.selectedCity == 2){
        filePath = [[NSBundle mainBundle] pathForResource:@"LAMap" ofType:@"csv"];
    } else {
        filePath = [[NSBundle mainBundle] pathForResource:@"BayAreaMap" ofType:@"csv"];
    }
    NSString* fileContents = [NSString stringWithContentsOfFile:filePath];
    NSArray* exitStrings = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    NSLog(@"exits = %d",exitStrings.count);
    exits = [[NSMutableArray alloc] initWithCapacity:exitStrings.count];

        
    for(int idx = 0; idx < exitStrings.count; idx++) {
        NSString* currentExit = [exitStrings objectAtIndex:idx];
        NSArray* exitData = [currentExit componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
        [exits addObject:[[ExitListing alloc] initWithName:[exitData objectAtIndex:0] exitId:[[exitData objectAtIndex:1] intValue] highwayId:[[exitData objectAtIndex:3] intValue]]];
    }
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    int tag = pickerView.tag;
    if(tag == 0) {
        return [typeList count];
    } else if(tag == 1) {
        return [highwayList count];
    } else if(tag == 2) {
        return [exitList count]-1;
    } else if(tag == 3) {
        return [exitOffList count];
    }
    return 0;
}

-(NSString *)pickerView:(UIPickerView *)pickerView
            titleForRow:(NSInteger)row
           forComponent:(NSInteger)component 
{
    int tag = pickerView.tag;
    if(tag == 0) {
        return [[typeList objectAtIndex:row] displayText];
    } else if(tag == 1) {
        return [[highwayList objectAtIndex:row] displayText];
    } else if(tag == 2){
        return [[exitList objectAtIndex:row] displayText];
    } else if(tag == 3){
        return [[exitOffList objectAtIndex:row] displayText];
    }
    return 0;
}

-(IBAction) selectType:(id)sender {
    [actionSheetType showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetType setBounds:CGRectMake(0, 0, 320, 485)];
}

-(IBAction) selectHighway:(id)sender {
    [actionSheetHighway showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetHighway setBounds:CGRectMake(0, 0, 320, 485)];
}

-(IBAction) selectOnRamp:(id)sender {
    [actionSheetOnRamp showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetOnRamp setBounds:CGRectMake(0, 0, 320, 485)];
}

-(IBAction) selectOffRamp:(id)sender {
    [actionSheetOffRamp showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetOffRamp setBounds:CGRectMake(0, 0, 320, 485)];
}

-(void) selectedType:(id)sender {
    [actionSheetType dismissWithClickedButtonIndex:0 animated:YES];
    [typeButton setTitle:[[typeList objectAtIndex:[typePicker selectedRowInComponent:0]] displayText] forState:UIControlStateNormal];
    commuteType = [[typeList objectAtIndex:[typePicker selectedRowInComponent:0]] displayType];
    highwayButton.hidden = NO;
}

-(void) selectedHighway:(id)sender{
    [actionSheetHighway dismissWithClickedButtonIndex:0 animated:YES];
    [highwayButton setTitle:[[highwayList objectAtIndex:[highwayPicker selectedRowInComponent:0]] displayText] forState:UIControlStateNormal];
    highway = [[highwayList objectAtIndex:[highwayPicker selectedRowInComponent:0]] displayType];
    [self setOnRamps:highway];
    onRampButton.enabled = TRUE;
    offRampButton.enabled = TRUE;
    onRampButton.hidden = NO;
}

-(void) selectedOnRamp:(id)sender{
    onRampSelected = TRUE;
    [actionSheetOnRamp dismissWithClickedButtonIndex:0 animated:YES];
    [onRampButton setTitle:[[exitList objectAtIndex:[onRampPicker selectedRowInComponent:0]] displayText] forState:UIControlStateNormal];
    onRampID = [[exitList objectAtIndex:[onRampPicker selectedRowInComponent:0]] displayType];
    onRampIndex = [onRampPicker selectedRowInComponent:0];
    [exitOffList removeAllObjects];
    [self setOffRamps:highway onRampSEQ:[onRampPicker selectedRowInComponent:0]];
    offRampButton.hidden = NO;
}

-(void) selectedOffRamp:(id)sender{
    offRampSelected = TRUE;
    [actionSheetOffRamp dismissWithClickedButtonIndex:0 animated:YES];
    [offRampButton setTitle:[[exitOffList objectAtIndex:[offRampPicker selectedRowInComponent:0]] displayText] forState: UIControlStateNormal];
    offRampID = [[exitOffList objectAtIndex:[offRampPicker selectedRowInComponent:0]] displayType];
    offRampIndex = onRampIndex + [offRampPicker selectedRowInComponent:0];
    addSegment.hidden = NO;
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	// the user clicked one of the OK/Cancel buttons
    NSLog(@"button %d", buttonIndex);
    [notifier dismissWithClickedButtonIndex:0 animated:YES];
    
}


-(void) setOnRamps:(int) highwaySelected {
    for(int i = 0; i < [exits count]; i++){
        if([[exits objectAtIndex:i] highwayID] == highwaySelected){
            [exitList addObject:[[ProfileCell alloc] initWithDisplay:[[exits objectAtIndex:i] exitName] type:[[exits objectAtIndex:i] exitID] connection:NULL]];
        }
    }
    [onRampPicker reloadAllComponents];
}

-(void) setOffRamps:(int) highwaySelected onRampSEQ:(int) selectedOnRampID {
    for(int i = selectedOnRampID+1; i < [exitList count]; i++){
        [exitOffList addObject:[exitList objectAtIndex:i]];
    }
    [offRampPicker reloadAllComponents];
}

-(IBAction)addSegment:(id)sender {
    NSLog(@"ON RAMP = %d and OFF RAMP = %d", onRampIndex, offRampIndex);
    if(onRampIndex > offRampIndex){
        //alert
    } else {
        NSString *URL;
        if(appDelegate.selectedCity == 1){
            URL = [NSString stringWithFormat: @"http://%@/sd/servlet/segmentservlet?type=%d&user=%d&onramp=%d&offramp=%d&ctype=%d&seq=%d",appDelegate.cpuUrl, 1, appDelegate.userID, onRampID, offRampID, commuteType, 1];
            NSLog(@"URL = %@",URL);
        } else if(appDelegate.selectedCity == 2){
            URL = [NSString stringWithFormat: @"http://%@/la/servlet/segmentservlet?type=%d&user=%d",appDelegate.cpuUrl, 1, appDelegate.user.userId];
        } else {
            URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/segmentservlet?type=%d&user=%d",appDelegate.cpuUrl, 1, appDelegate.user.userId];
        }
        
        
               
        NSError *error = nil;
        @try{

            
            NSString *post = @"";
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
            NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:URL]];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSArray *cookies = [scs cookies];
            NSEnumerator* e = [cookies objectEnumerator];
            NSHTTPCookie* cookie;
            while ((cookie = [e nextObject])) {
                NSString* name = [cookie name];
                if ([name isEqualToString:@"userId"]) {
                    [scs deleteCookie:cookie];
                }
            }
            
            conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (conn)
            {
                NSLog(@"received Data");
                receivedData = [[NSMutableData data] retain];
            }
            else
            {
                NSLog(@"no conn");
            }
        } @catch (NSException *e) {
            NSLog(@"Failed to Login");
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSLog(@"append text data");
    [receivedData appendData:data];    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"conn failed with error: %@", error);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
        NSLog(@"returned data = %@",outputString);
        
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
