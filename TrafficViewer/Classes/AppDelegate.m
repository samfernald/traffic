//
//  AppDelegate.m
//  TrafficViewer
//
//  Created by sfernald on 10/29/08.
//  Copyright __MyCompanyName__ 2008. All rights reserved.
//

#import "AppDelegate.h"
#import "RootViewController.h"
#import "MapViewController.h"
#import "CommuteViewController.h"
#import <Crashlytics/Crashlytics.h>
#import <mach/mach.h>
#import <mach/mach_host.h>
#import "ProbeReading.h"
#import "TestFlight.h"
#import "GAI.h"

#define GOOGLEMAPSAPIKEY AIzaSyAyC541IDExfrRMQj7_Po3ZhjEweGYdCr0
static NSString * const kUUID = @"UUID";

//#define NSLog TFLog

@implementation AppDelegate

@synthesize window;
@synthesize navigationController, menuNavController;
@synthesize notifier, mapNotifier, gMapNotifier;
@synthesize user;
@synthesize screenToLoad;
@synthesize selectedHighway, selectedCity;
@synthesize tabBarController;
@synthesize selectedHighwayName, selectedHighwayImagePath;
@synthesize userName, password;
@synthesize urlForCommute, urlForCommuteJson, firstTimeLoadingSpeed;
@synthesize userCreated, testingCounter;
@synthesize homeCity, speedsViewLoaded;
@synthesize map1, map2, map3;
@synthesize autoLoadedGuest;
@synthesize hasAnAccount;
@synthesize morningExists, eveningExists, altMorningExists, altEveningExists;
@synthesize sdMapUrl, baMapUrl, la1MapUrl, la2MapUrl, la3MapUrl, cpuUrl;
@synthesize lastRefresh,lastLocationUpdate;
@synthesize initMapLoad, calTransMapLoaded, gMapLoaded, usingLocation;
@synthesize refreshingCaltrans;
@synthesize tempCommute, priorLoads, firstReset;
@synthesize alertToEdit, reportToEdit, segmentToEdit;
@synthesize commuteToLoad;
@synthesize googleAnalytics;
@synthesize askToRate;
@synthesize userID, userLat, userLong;
@synthesize timer;
@synthesize mapToLoad;
@synthesize mapViewLoaded;
@synthesize storyBoard;

/*
+ (natural_t) get_free_memory {
	mach_port_t host_port;
	mach_msg_type_number_t host_size;
	vm_size_t pagesize;
	host_port = mach_host_self();
	host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
	host_page_size(host_port, &pagesize);
	vm_statistics_data_t vm_stat;
	if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS) {
		NSLog(@"Failed to fetch vm statistics");
		return 0;
	}
	
	natural_t mem_used = (vm_stat.inactive_count) * pagesize;
	natural_t mem_free = vm_stat.free_count * pagesize;
	natural_t mem_purgable = vm_stat.purgeable_count * pagesize;
	//NSLog(@"mem used = %u",mem_used);
	//NSLog(@"mem purgable = %u",mem_purgable);
	return mem_free;
}
*/
 
//- (void)applicationDidFinishLaunching:(UIApplication *)application {
- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
	firstTimeLoadingSpeed = TRUE;
	userCreated = FALSE;
	autoLoadedGuest = FALSE;
	testingCounter = 0;
	initMapLoad = TRUE;
	calTransMapLoaded = FALSE;
	gMapLoaded = FALSE;
	refreshingCaltrans = FALSE;
	firstReset = TRUE;
    askToRate = FALSE;
    priorLoads = 0;
    userID = -1;
    sentProbeData = FALSE;
    mapToLoad = 1;
    mapViewLoaded = FALSE;
    morningExists = FALSE;
    altMorningExists = FALSE;
    eveningExists = FALSE;
    altEveningExists = FALSE;
    
    application.idleTimerDisabled = YES;
	//UINavigationController *localNavController;
    //UINavigationController *rootNavController;
	userName = (NSString*)CFPreferencesCopyAppValue((CFStringRef) @"userName", kCFPreferencesCurrentApplication);
	password = (NSString*)CFPreferencesCopyAppValue((CFStringRef) @"password", kCFPreferencesCurrentApplication);
	homeCity = (NSString*)CFPreferencesCopyAppValue((CFStringRef) @"homeCity", kCFPreferencesCurrentApplication);
	hasAnAccount = (NSString*)CFPreferencesCopyAppValue((CFStringRef) @"autoLogin", kCFPreferencesCurrentApplication);
    priorLoads = [(NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"priorLoads", kCFPreferencesCurrentApplication) intValue];
    mapToLoad = [(NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"initMap", kCFPreferencesCurrentApplication) intValue];
    morningExists = [(NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"morningExists", kCFPreferencesCurrentApplication) boolValue];
    altMorningExists = [(NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"altMorningExists", kCFPreferencesCurrentApplication) boolValue];
    eveningExists = [(NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"eveningExists", kCFPreferencesCurrentApplication) boolValue];
    altEveningExists = [(NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"altEveningExists", kCFPreferencesCurrentApplication) boolValue];
    screenToLoad = [(NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"screenToLoad", kCFPreferencesCurrentApplication) intValue];
    NSLog(@"Screen to Load = %d",screenToLoad);
    
    locManager = [[CLLocationManager alloc] init];
    locManager.delegate = self;
    probeReadings = [[NSMutableArray alloc] init];
    [self performSelectorInBackground:@selector(manageUUID) withObject:nil];

    [self startReadingLocation];
    
    googleAnalytics = [(NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"googleAnalytics",kCFPreferencesCurrentApplication) intValue];
    /*
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 30;
    // Optional: set debug to YES for extra debugging information.
    [GAI sharedInstance].debug = NO;
    if(googleAnalytics == 1){
        [[GAI sharedInstance]setOptOut:YES];
    }
    */
    // Create tracker instance.
    //id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-33122525-1"];
    
    [GMSServices provideAPIKey:@"AIzaSyAyC541IDExfrRMQj7_Po3ZhjEweGYdCr0"];
    
    [Crashlytics startWithAPIKey:@"421cf140106ed02d6f02431cfd63a4681b06b9a9"];
    //[TestFlight setDeviceIdentifier:[[UIDevice currentDevice] uniqueIdentifier]];
    //[TestFlight takeOff:@"d69cee19-d355-4785-9aa4-b9a0b9028476"];
    NSLog(@"Prior loads = %d",priorLoads);
    if(priorLoads == 10) {
        //alert View with link
        notifier = [[UIAlertView alloc] initWithTitle:@"We depend on your reviews. Please support us by rating our App." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes",@"Remind me later", @"Never ask again",nil];
        askToRate = TRUE;
        [notifier show];

        priorLoads = 1;
        CFPreferencesSetAppValue((CFStringRef)@"priorLoads", [NSString stringWithFormat:@"%d",priorLoads], kCFPreferencesCurrentApplication);
        CFPreferencesAppSynchronize(kCFPreferencesCurrentApplication);
    } else if (priorLoads == 11) {
        
    } else {
        priorLoads++;
        CFPreferencesSetAppValue((CFStringRef)@"priorLoads", [NSString stringWithFormat:@"%d",priorLoads], kCFPreferencesCurrentApplication);
        CFPreferencesAppSynchronize(kCFPreferencesCurrentApplication);
    }
	
	baMapUrl = (NSString*)CFPreferencesCopyAppValue((CFStringRef) @"baMapUrl", kCFPreferencesCurrentApplication);
    
	cpuUrl = @"traffic.calit2.net";
    
	if(userName.length == 0) {
		userName = nil;
	} 
	
	if(password.length == 0) {
		password = nil;
	} 
	
	if([homeCity isEqualToString: @""]) {
		homeCity = nil;
	} 
	
	if(baMapUrl.length == 0) {
		baMapUrl = @"http://traffic.511.org/homepage.gif";
	}
    
    storyBoard = self.window.rootViewController.storyboard;
    //storyBoard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    
    
    return TRUE;
}

-(void) resetApplication {
	
		firstReset = FALSE;
	//} else {
		// size = freemem;
		 //allocation = malloc(freemem);
	//}
	firstTimeLoadingSpeed = TRUE;
	userCreated = FALSE;
	
	userName = (NSString*)CFPreferencesCopyAppValue ((CFStringRef)@"userName", kCFPreferencesCurrentApplication);
	password = (NSString*)CFPreferencesCopyAppValue ((CFStringRef)@"password", kCFPreferencesCurrentApplication);
	homeCity = NULL;
	initMapLoad = TRUE;
	calTransMapLoaded = FALSE;
	gMapLoaded = FALSE;
	refreshingCaltrans = FALSE;
    
    int viewCount = [self.window.subviews count];
    for (UIView* view in self.window.subviews)
    {
        NSLog(@"Removing view: %d",viewCount);
        if(viewCount > 1){
            [view removeFromSuperview];
        }
        viewCount--;
    }
    NSLog(@"grabbing root view");
    RootViewController* initialScene = (RootViewController *)[storyBoard instantiateViewControllerWithIdentifier:@"RootViewController"];
    NSLog(@"loading root view");
    self.window.rootViewController = initialScene;
}

- (void) manageUUID {
    
    self.UUID = [self createUUID];
    NSLog(@"UUID = %@",self.UUID);
    
    BOOL setInNSUserDefaults = NO;
    BOOL setInKeychain = NO;
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:kUUID]){
        CLSLog(@"No UUID in NSUserDefaults. Will check Keychain first, then store the generated one.");
        //[[NSUserDefaults standardUserDefaults] setObject:UUID forKey:kUUID];
        setInNSUserDefaults = YES;
    }else{
        self.UUID = [[NSUserDefaults standardUserDefaults] objectForKey:kUUID];
        CLSLog(@"UUID found in NSUD. Current is now: %@", self.UUID);
        
    }
    
    
    // store into device keychain so the user keeps same ID even if app is deleted
    NSString *retrieveuuid = [SSKeychain passwordForService:@"net.calit2.TrafficViewer" account:@"user"];
    if (retrieveuuid == nil) {
        CLSLog(@"No UUID in Keychain. Will store current one.");
        setInKeychain = YES;
        
    }else{
        
        if(![self.UUID isEqualToString:retrieveuuid]){
            self.UUID = retrieveuuid;
            setInNSUserDefaults = YES;
            
        }
        
    }
    
        
    if(setInNSUserDefaults){
        [[NSUserDefaults standardUserDefaults] setObject:self.UUID forKey:kUUID];
    }
    if(setInKeychain){
        [SSKeychain setPassword:self.UUID forService:@"net.calit2.TrafficViewer" account:@"user"];
    }
    
    
    [Crashlytics setUserIdentifier:self.UUID];
    
    
}

- (NSString *)createUUID {
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return ((NSString *)CFBridgingRelease(string)) ;
}



- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	// the user clicked one of the OK/Cancel buttons
    NSLog(@"button %d", buttonIndex);
    if(buttonIndex == 0) {
        priorLoads = 11;
        CFPreferencesSetAppValue((CFStringRef)@"priorLoads", [NSString stringWithFormat:@"%d",priorLoads], kCFPreferencesCurrentApplication);
        CFPreferencesAppSynchronize(kCFPreferencesCurrentApplication);
        NSString* url = [NSString stringWithFormat: @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%d", 303987371];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    } else if(buttonIndex == 1){
        
    } else {
        priorLoads = 11;
        CFPreferencesSetAppValue((CFStringRef)@"priorLoads", [NSString stringWithFormat:@"%d",priorLoads], kCFPreferencesCurrentApplication);
        CFPreferencesAppSynchronize(kCFPreferencesCurrentApplication);
    }
    
}


- (void)startReadingLocation {
    NSLog(@"method called");
    [locManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"location updated");
    [locManager stopUpdatingLocation];
    if(!sentProbeData){
        sentProbeData = TRUE;
        userLat = newLocation.coordinate.latitude;
        userLong = newLocation.coordinate.longitude;
        NSLog(@"Sending probe data");
        NSString *cityForUrl;
        if([homeCity isEqualToString:@"San Diego"]){
            cityForUrl = @"sd";
        } else if([homeCity isEqualToString:@"Los Angeles"]){
            cityForUrl = @"la";
        } else if([homeCity isEqualToString:@"Bay Area"]){
            cityForUrl = @"bayarea";
        }
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *currentTime = [dateFormatter stringFromDate:[NSDate date]];
        NSString *formattedTime = [currentTime stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        NSString *URLForTraffic = [NSString stringWithFormat:@"http://%@/%@/servlet/appprobeservlet",cpuUrl,cityForUrl];
        
        NSString * URLForTrafficWithHighway = [[NSString alloc] initWithFormat:@"%@?user=%@&lat=%f&long=%f&time=%@",URLForTraffic, [[NSString alloc] initWithFormat:@"'%@'",self.UUID], newLocation.coordinate.latitude,newLocation.coordinate.longitude, [[NSString alloc] initWithFormat:@"'%@'",formattedTime]];
        NSLog(@"URL = %@",URLForTrafficWithHighway);
        NSString *post = @"";
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:URLForTrafficWithHighway]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }

    /*
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    
    //NSTimeInterval ageInSeconds = [newLocation.timestamp timeIntervalSinceNow];
    if(state == UIApplicationStateBackground){
        NSLog(@"I'm in background!");
        if(newLocation.coordinate.latitude > 33.008663){
            [locManager stopUpdatingLocation];
            UILocalNotification* notifyUserLocation = [[UILocalNotification alloc] init];
            notifyUserLocation.alertBody = @"Reached your exit, please upload data now";
            [[UIApplication sharedApplication] presentLocalNotificationNow:notifyUserLocation];
        }
        
        
    } else if(newLocation.speed > -1.0){
        NSLog(@"speed = %f", newLocation.speed);
        if (state == UIApplicationStateBackground || state == UIApplicationStateInactive) {
            [probeReadings addObject:[[ProbeReading alloc] initWithlat:newLocation.coordinate.latitude long:newLocation.coordinate.longitude speed:newLocation.speed*2.23693629]];
        } else {
            if(probeReadings.count < 10){
                [probeReadings addObject:[[ProbeReading alloc] initWithlat:newLocation.coordinate.latitude long:newLocation.coordinate.longitude speed:newLocation.speed*2.23693629]];
            } else {
                NSLog(@"sending points");
                for(int i = 0; i < probeReadings.count; i++){
                    NSString *URLForTraffic = [NSString stringWithFormat:@"http://%@/sd/servlet/probeservlet",cpuUrl];
                    
                    NSString * URLForTrafficWithHighway = [[NSString alloc] initWithFormat:@"%@?lat=%f&long=%f&speed=%f",URLForTraffic, [[probeReadings objectAtIndex:i] lat],[[probeReadings objectAtIndex:i] lng], [[probeReadings objectAtIndex:i] velo]];
                    NSString *post = @"";
                    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
                    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
            
                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                    [request setURL:[NSURL URLWithString:URLForTrafficWithHighway]];
                    [request setHTTPMethod:@"POST"];
                    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                    [request setHTTPBody:postData];
                    [[NSURLConnection alloc] initWithRequest:request delegate:self];
                }
                [probeReadings removeAllObjects];
            }
        }
        self.timer = [NSTimer scheduledTimerWithTimeInterval:60.0 target:self selector:@selector(timeIntervalEnded:) userInfo:nil repeats:NO];
    }
    
    //this puts the GPS to sleep, saving power
     */
    
    //timer fires after 60 seconds, then stops
    }

- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"bye");
    [locManager stopMonitoringSignificantLocationChanges];
    [locManager stopUpdatingLocation];
}
/*
    NSLog(@"to background");
    UIApplication *app = [UIApplication sharedApplication];

    //app.isInBackground = TRUE;
    
    
    // Request permission to run in the background. Provide an
    // expiration handler in case the task runs long.
    UIBackgroundTaskIdentifier myLongTask;
    
    
    // Start the long-running task and return immediately.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"loc manager on!");
        // Do the work associated with the task.
        
        //locManager.distanceFilter = 100;
        locManager.desiredAccuracy = kCLLocationAccuracyBest;
        //[locManager startMonitoringSignificantLocationChanges];
        [locManager startUpdatingLocation];
        
        NSLog(@"App staus: applicationDidEnterBackground");
        // Synchronize the cleanup call on the main thread in case
        // the expiration handler is fired at the same time.
        dispatch_async(dispatch_get_main_queue(), ^{
            if (myLongTask != UIBackgroundTaskInvalid)
            {
                //[app endBackgroundTask:myLongTask];
                //myLongTask = UIBackgroundTaskInvalid;
            }
        });
    });
    
    NSLog(@"backgroundTimeRemaining: %.0f", [[UIApplication sharedApplication] backgroundTimeRemaining]);
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    UIAlertView *alertDialog;
    alertDialog = [[UIAlertView alloc]
                   initWithTitle: @"Welcome back!"
                   message:@"Sending speed info"
                   delegate: nil
                   cancelButtonTitle: @"OK"
                   otherButtonTitles: nil];
    [alertDialog show];
    [alertDialog release];
    
    NSLog(@"sending points");
    for(int i = 0; i < probeReadings.count; i++){
        NSString *URLForTraffic = [NSString stringWithFormat:@"http://%@/sd/servlet/probeservlet",cpuUrl];
        
        NSString * URLForTrafficWithHighway = [[NSString alloc] initWithFormat:@"%@?lat=%f&long=%f&speed=%f",URLForTraffic, [[probeReadings objectAtIndex:i] lat],[[probeReadings objectAtIndex:i] lng], [[probeReadings objectAtIndex:i] velo]];
        NSString *post = @"";
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:URLForTrafficWithHighway]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    [probeReadings removeAllObjects];
}
*/

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    /*
    UIAlertView *alertUserConFailed = [[UIAlertView alloc] initWithTitle:@"Conn Failed" message:[NSString stringWithFormat:@"%@",error] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertUserConFailed show];
     */
}


//this is a wrapper method to fit the required selector signature
/*
- (void)timeIntervalEnded:(NSTimer*)timer {
    [self startReadingLocation];
}*/

// from UIApplicationDelegate protocol


- (void)applicationWillTerminate:(UIApplication *)application {
	// Save data if appropriate
	NSLog(@"app is terminated");
    [locManager stopUpdatingLocation];
}



- (void)dealloc {
	NSLog(@"in trafficapp dealloc");
	
	//[window release];
	//[navigationController release];
	//[tabBarController release];
	//[user release];
	//[selectedHighwayName release];
	//[selectedHighwayImagePath release];
	//[userName release];
	//[password release];
	//[homeCity release];
	
	[super dealloc];
}

@end
