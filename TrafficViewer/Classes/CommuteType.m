//
//  CommuteType.m
//  TrafficViewer
//
//  Created by sfernald on 7/17/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "CommuteType.h"


@implementation CommuteType

@synthesize icon, alternate;

-(id) initWithImg:(UIImage*)cIcon title:(NSString*)alt {
	icon = cIcon;
	alternate = alt;
	return self;
}
	

@end
