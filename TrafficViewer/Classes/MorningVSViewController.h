//
//  MorningVSViewController.h
//  TrafficViewer
//
//  Created by Sam Fernald on 8/1/13.
//
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "CPDStockPriceStore.h"
#import "AppDelegate.h"

@interface MorningVSViewController : UIViewController<CPTPlotDataSource, UINavigationControllerDelegate>
{
    int day;
    IBOutlet UINavigationController *navControl;
    IBOutlet UINavigationBar *navBar;
    AppDelegate *appDelegate;
    int connectionType;
    NSMutableArray *morningVS;
    NSArray *timesOfDay;
    NSURLConnection *conn;
    NSMutableData *receivedData;
    NSMutableData *receivedIDData;
    
    
    float xGlobalOrigin;
    float xGlobalLength;
    
    float yGlobalOrigin;
    float yGlobalLength;
    
    //plot area value range that is showing constantly
    float xShowAreaOrigin;
    float xShowAreaLength;
    
    float yShowAreaOrigin;
    float yShowAreaLength;
    
    //spacing - stretching factor of each axis
    float xShowAreaScaleFactor;
    float yShowAreaScaleFactor;
    
    float scaleFactor;
}


//-(CPTPlotRange *)plotSpace: (CPTPlotSpace *)space willChangePlotRangeTo:(CPPlotRange *)newRange forCoordinate(CPCoordinate)coordinate;


//@property (nonatomic) int day;
@property (nonatomic, retain) NSMutableArray *graphData;
@property (nonatomic) bool morningOrEvening;
@property (nonatomic, retain) IBOutlet UILabel *infoLabel;
@property (nonatomic, retain) IBOutlet UINavigationBar *navBar;
@property (nonatomic, strong) NSString *chosenLaneType; //this is the text of infolabel


-(IBAction)resetGraph:(id)sender;
-(IBAction)doneButtonPressed:(id)sender;
-(IBAction)backToMenu:(id)sender;


@end