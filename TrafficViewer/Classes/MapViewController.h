//
//  MapViewController.h
//  TrafficViewer
//
//  Created by sfernald on 11/14/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "CSMapRouteLayerView.h"
#import "pointInfo.h"
#import "DejalActivityView.h"
//#import "GAITrackedViewController.h"


@interface MapViewController : UIViewController /*<UIScrollViewDelegate, MKMapViewDelegate>*/{
    
    AppDelegate *appDelegate;
    
	IBOutlet UIBarButtonItem *refreshButton;
    UIBarButtonItem *switchMapButton;
    UISegmentedControl *switchMapSegmentedControl;
    
    UIButton *switchDataButton;
	
	BOOL viaRefresh;
	BOOL obtainIncidentData;

	NSDate *lastRefresh;
	UIImageView *mapView;
	UIImageView *mapViewLA;
	UIImageView *mapViewIE;
	UIImageView *mapViewRS;
	UIScrollView * scrollViewLA;
	UIScrollView * scrollViewIE;
	UIScrollView * scrollViewRS;
	UILabel * pemsLabelLA;
	UILabel * pemsLabelIE;
	UILabel * pemsLabelRS;
	NSMutableData * receivedData;
	NSMutableData * receivedDataIncident;
	NSString * mapUrl;
	NSString * incidentUrl;
	NSURLConnection * conn;
	NSURLConnection * conn2;
	UISegmentedControl * segmentedControl;
    DejalActivityView *temp;
	
	NSMutableDictionary *speedMarkers;
	NSMutableDictionary *incidentMarkers;
	NSMutableArray *speedInfo;
	NSMutableArray *incidentInfo;
	
	
	MKMapView* _myMapView;
	CSMapRouteLayerView* _routeView;
	
	
	UIAlertView *notifier;
     
	
}

@property BOOL viaRefresh;
@property BOOL obtainIncidentData;

@property (nonatomic, retain) UIImageView *mapView;
@property (nonatomic, retain) UIImageView *mapViewLA;
@property (nonatomic, retain) UIImageView *mapViewIE;
@property (nonatomic, retain) UIImageView *mapViewRS;
@property (nonatomic, retain) UIScrollView *scrollViewLA;
@property (nonatomic, retain) UIScrollView *scrollViewIE;
@property (nonatomic, retain) UIScrollView *scrollViewRS;
@property (nonatomic, retain) UILabel * pemsLabelLA;
@property (nonatomic, retain) UILabel * pemsLabelIE;
@property (nonatomic, retain) UILabel * pemsLabelRS;
@property (nonatomic, retain) NSMutableDictionary * speedMarkers;
@property (nonatomic, retain) NSMutableDictionary * incidentMarkers;
@property (nonatomic, retain) NSMutableArray * speedInfo;
@property (nonatomic, retain) NSMutableArray * incidentInfo;
@property (nonatomic, retain) NSString * mapUrl;
@property (nonatomic, retain) NSString * incidentUrl;

@property(nonatomic,retain) NSDate *lastRefresh;
@property(nonatomic,retain) UIBarButtonItem * refreshButton;
@property(nonatomic,retain) UIBarButtonItem *switchMapButton;


@property (nonatomic, retain) MKMapView* myMapView;
@property (nonatomic, retain) CSMapRouteLayerView* routeView;

@property (nonatomic, retain) UIAlertView * notifier;

@property (nonatomic, retain) UISegmentedControl *segmentedControl;



-(IBAction) selectArea:(id) sender;

-(void) segmentAction:(id)sender;

-(void) swapMaps:(id) sender;


@end
