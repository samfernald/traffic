//
//  MorningCommuteViewController.m
//  TrafficViewer
//
//  Created by sfernald on 10/29/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "CommuteViewController.h"
#import "EveningCommuteViewController.h"

#import "CommuteInfo.h"
#import "DejalActivityView.h"

static UIImage *borderBanner = nil;


@implementation CommuteViewController

@synthesize altButton, refreshButton, eveningButton, commuteCellForImage;
@synthesize lastAcceleration,segmentedControl,hornSound, filePath;

+ (void)initialize{
    // The magnitude images are cached as part of the class, so they need to be
    // explicitly retained.
    borderBanner = [[UIImage imageNamed:@"borderAppBanner.png"] retain];
}


/*
 - (id)initWithStyle:(UITableViewStyle)style {
 // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 if (self = [super initWithStyle:style]) {
 }
 return self;
 }
 */




// Implement viewDidLoad to do additional setup after loading the view.
- (void)viewDidLoad {
	appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	//NSBundle *mainBundle = [NSBundle mainBundle];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterNoStyle];
	[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
	NSString* currentTime = [dateFormatter stringFromDate:[NSDate date]];
	[dateFormatter release];
	loadMorning = [currentTime hasSuffix:@"AM"];
    refreshing = FALSE;
	if(loadMorning) {
		//[self setTitle:@"Morning"];
		//[[[appDelegate.tabBarController.viewControllers objectAtIndex:1] tabBarItem] setTitle:@"My Traffic"];
	} else {
		//[self setTitle:@"Evening"];
		//[[[appDelegate.tabBarController.viewControllers objectAtIndex:1] tabBarItem] setTitle:@"My Traffic"];
	}
	morningCommuteSegments = altMorningCommuteSegments = eveningCommuteSegments = altEveningCommuteSegments = segmentsRemaining = 0;
	//[[UIAccelerometer sharedAccelerometer] setDelegate: self];
	//[[UIAccelerometer sharedAccelerometer] setUpdateInterval: 1/20];
	
	priAlt = NO;
	NSString *commuteToLoad;
	if(loadMorning) {
		commuteToLoad = @"Evening";
	} else {
		commuteToLoad = @"Morning";
	}
	NSArray *segmentTextContent = [NSArray arrayWithObjects:commuteToLoad,@"    Refresh     ",@"Alternate", nil];
	
	segmentedControl = [[UISegmentedControl alloc] initWithItems:segmentTextContent];
	segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	//segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
	segmentedControl.momentary = YES;
	[segmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
	//UIBarButtonItem *segmentBarItem = [[UIBarButtonItem alloc] initWithCustomView:segmentedControl];
	self.navigationItem.titleView = segmentedControl;
	NSNumber * screenToLoad = (NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"screenToLoad", kCFPreferencesCurrentApplication);	

	if([screenToLoad intValue] == 2 || [screenToLoad intValue] == 3) {
		NSLog(@"start downloading commute info");
		
        if(appDelegate.priorLoads != 1) {
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Speeds are updated once every 3 minutes."];
/*
            NSString *downloadMessage = [NSString stringWithFormat:@"Fetching %@ traffic data. Please wait",appDelegate.homeCity];
            notifier = [[UIAlertView alloc] initWithTitle:@"" message:downloadMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
            UIActivityIndicatorView *activityView1 = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            activityView1.frame = CGRectMake(139.0f-18.0f, 85.0f, 33.0f, 33.0f);
            [notifier addSubview:activityView1];
            [activityView1 startAnimating];
            [notifier show];
 */
        }
		
		NSString *post = @""; 
		NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];  
	
		NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];  
	
		NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
		[request setURL:[NSURL URLWithString:appDelegate.urlForCommute]];
		[request setHTTPMethod:@"POST"];  
		[request setValue:postLength forHTTPHeaderField:@"Content-Length"];  
		[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];  
		[request setHTTPBody:postData];
		NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
		NSArray *cookies = [scs cookies];
		NSEnumerator* e = [cookies objectEnumerator];
		NSHTTPCookie* cookie;
	
		while ((cookie = [e nextObject])) {
			NSString* name = [cookie name];
			if ([name isEqualToString:@"userId"]) {
				[scs deleteCookie:cookie];
			}	
		}
	
		conn=[[NSURLConnection alloc] initWithRequest:request delegate:self]; 
		if (conn) {  
			receivedData = [[NSMutableData data] retain];
		} else {  
			//[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
		}  
		[request release];
		NSLog(@"end dl commute");
		//[self segmentAction:segmentedControl];
		/*
		 altButton = [[UIBarButtonItem alloc]
		 initWithTitle:NSLocalizedString(@"Alternate", @"")
		 style:UIBarButtonItemStyleBordered
		 target:self
		 action:@selector(switchToAlt:)];
	 
		 refreshButton = [[UIBarButtonItem alloc]
		 initWithTitle:NSLocalizedString(@"Refresh", @"")
		 style:UIBarButtonItemStyleBordered
		 target:self
		 action:@selector(refresh:)];
		 */
		//self.navigationItem.rightBarButtonItem = altButton;
		//self.navigationItem.leftBarButtonItem = refreshButton;
		//self.navigationItem.leftBarButtonItem.enabled = NO;
	} else {
	
		if(user.altMorning.numSegments <= 0) {
		}
	
		if(!appDelegate.userCreated){
			[self createCommuteInfo];
			appDelegate.lastRefresh = [[NSDate date] retain];
			appDelegate.userCreated = TRUE;
		}
		if(loadMorning) {
			[self prepareCommuteCells:user.morning];
		} else {
			[self prepareCommuteCells:user.evening];
		}
		loadMorning = !loadMorning;
		self.tableView.bounces = YES;
		[self.tableView reloadData];
	}
	
	
}
//------------------------------------------------------------------------------
- (void)switchToAlt:(id)sender{
	priAlt = !priAlt;
	UISegmentedControl* segCtl = sender;
	if(loadMorning) {
		if([segCtl titleForSegmentAtIndex:2]==@"Alternate"){
			if(user.altEvening.numSegments <= 0) {
				//[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
				[noCommuteNotifier dismissWithClickedButtonIndex:0 animated:FALSE];
				noCommuteNotifier = [[UIAlertView alloc] initWithTitle:@"" message:@"You have not added an alternate commute.  Login to http://traffic.calit2.net to add one." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
				[noCommuteNotifier show];
			} else {
				[segCtl setTitle:@"Primary" forSegmentAtIndex:2];
				//[self setTitle:@"Alternate"];
				//[[[appDelegate.tabBarController.viewControllers objectAtIndex:1] tabBarItem] setTitle:@"My Traffic"];
				//load Alt Data
				[self prepareCommuteCells:user.altEvening];
				[self.tableView reloadData];
			}
			
		} else {
			if(user.evening.numSegments <= 0) {
				//[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
				[noCommuteNotifier dismissWithClickedButtonIndex:0 animated:FALSE];
				noCommuteNotifier = [[UIAlertView alloc] initWithTitle:@"" message:@"You have not added a commute for this time.  Login to http://traffic.calit2.net to add one." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
				[noCommuteNotifier show];
			} else {
				[segCtl setTitle:@"Alternate" forSegmentAtIndex:2];
				//[self setTitle:@"Evening"];
				//[[[appDelegate.tabBarController.viewControllers objectAtIndex:1] tabBarItem] setTitle:@"My Traffic"];
				//load Pri Data
				[self prepareCommuteCells:user.evening];
				[self.tableView reloadData];
			}
		}
	} else {
		if([segCtl titleForSegmentAtIndex:2]==@"Alternate"){
			if(user.altMorning.numSegments <= 0) {
				//[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
				[noCommuteNotifier dismissWithClickedButtonIndex:0 animated:FALSE];
				noCommuteNotifier = [[UIAlertView alloc] initWithTitle:@"" message:@"You have not added an alternate commute.  Login to http://traffic.calit2.net to add one." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
				[noCommuteNotifier show];
			} else {
				[segCtl setTitle:@"Primary" forSegmentAtIndex:2];
				//[self setTitle:@"Alternate"];
				//[[[appDelegate.tabBarController.viewControllers objectAtIndex:1] tabBarItem] setTitle:@"My Traffic"];
				//load Alt Data
				[self prepareCommuteCells:user.altMorning];
				[self.tableView reloadData];
			}
			
		} else {
			if(user.morning.numSegments <= 0) {
				//[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
				[noCommuteNotifier dismissWithClickedButtonIndex:0 animated:FALSE];
				noCommuteNotifier = [[UIAlertView alloc] initWithTitle:@"" message:@"You have not added a commute for this time.  Login to http://traffic.calit2.net to add one." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
				[noCommuteNotifier show];
			} else {
				[segCtl setTitle:@"Alternate" forSegmentAtIndex:2];
				//[self setTitle:@"Morning"];
				//[[[appDelegate.tabBarController.viewControllers objectAtIndex:1] tabBarItem] setTitle:@"My Traffic"];
				//load Pri Data
				[self prepareCommuteCells:user.morning];
				[self.tableView reloadData];
			}
		}
	}
	[segCtl setHighlighted:NO];
}
//------------------------------------------------------------------------------
- (void)refresh:(id)sender{
	@try{ 
		NSTimeInterval timeSinceRefresh = [appDelegate.lastRefresh timeIntervalSinceNow];
		
		NSString *downloadMessage;
		if(appDelegate.selectedCity == 1) {
			downloadMessage = [NSString stringWithFormat:@"Speeds updated once every minute."];
		} else {
			downloadMessage = [NSString stringWithFormat:@"Speeds updated every 5 minutes."];
		}
		notifier = [[UIAlertView alloc] initWithTitle:@"Refreshing" message:downloadMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
		UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
		activityView.frame = CGRectMake(139.0f-18.0f, 70.0f, 33.0f, 33.0f);
		[notifier addSubview:activityView];
		[activityView startAnimating];
		[notifier show];
		
		if(timeSinceRefresh <= -60) {
			[appDelegate.lastRefresh release];
			appDelegate.lastRefresh = [[NSDate date] retain];
            
            refreshing = TRUE;
			congestionInfo = nil;
			NSString *post = @""; 
			NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];  
			
			NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];  
			
			NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
			[request setURL:[NSURL URLWithString:appDelegate.urlForCommute]];
			[request setHTTPMethod:@"POST"];  
			[request setValue:postLength forHTTPHeaderField:@"Content-Length"];  
			[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];  
			[request setHTTPBody:postData];
			NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
			NSArray *cookies = [scs cookies];
			NSEnumerator* e = [cookies objectEnumerator];
			NSHTTPCookie* cookie;
			
			while ((cookie = [e nextObject])) {
				NSString* name = [cookie name];
				if ([name isEqualToString:@"userId"]) {
					[scs deleteCookie:cookie];
				}	
			}
			
			conn=[[NSURLConnection alloc] initWithRequest:request delegate:self]; 
			if (conn) {  
				receivedData = [[NSMutableData data] retain];
			} else {  
				//[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
			}  
			[request release];
		} else {
			[notifier dismissWithClickedButtonIndex:0 animated:YES];
			[notifier release];
		}
	} @catch (NSException *e) {
		NSLog(@"Failed to Login");
	}
}
//------------------------------------------------------------------------------
- (void)downloadingMessageNotifier {
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc ] init ];
    // Do processor intensive tasks here knowing that it will not block the main thread.
	NSString *downloadMessage;
	if(appDelegate.selectedCity == 1) {
		downloadMessage = [NSString stringWithFormat:@"Speeds updated once every minute."];
	} else {
		downloadMessage = [NSString stringWithFormat:@"Speeds updated every 5 minutes."];
	}
	notifier = [[UIAlertView alloc] initWithTitle:@"Refreshing" message:downloadMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
	UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityView.frame = CGRectMake(139.0f-18.0f, 70.0f, 33.0f, 33.0f);
    [notifier addSubview:activityView];
    [activityView startAnimating];
	[notifier show];	
	[ pool drain ];
}
//------------------------------------------------------------------------------
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{	
	NSLog(@"start authent");
	
    if ([challenge previousFailureCount] == 0) {
        NSLog(@"user name = %@",appDelegate.userName);
        NSLog(@"pw = %@",appDelegate.password);
        NSURLCredential *newCredential;
        newCredential=[NSURLCredential credentialWithUser:appDelegate.userName
                                                 password:appDelegate.password
                                              persistence:NSURLCredentialPersistenceNone];
        [[challenge sender] useCredential:newCredential
               forAuthenticationChallenge:challenge];
		
    } else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        // inform the user that the user name and password
        // in the preferences are incorrect
    }
	
}
//------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
	NSLog(@"received data");
	// append the new data to the receivedData
	// receivedData is declared as a method instance elsewhere
	
	[receivedData appendData:data];
	
}
//------------------------------------------------------------------------------
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	NSLog(@"conn failed with error: %@",error);
	[appDelegate.notifier dismissWithClickedButtonIndex:0 animated:FALSE];
	[appDelegate.notifier release];
    [notifier dismissWithClickedButtonIndex:0 animated:FALSE];
	notifier = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Incorrect Login Info" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[notifier show];
    //[[[appDelegate.tabBarController.viewControllers objectAtIndex:1] tabBarItem] setEnabled:NO];
    //[appDelegate.tabBarController setSelectedIndex:0];
    
    
}
//------------------------------------------------------------------------------
- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
	NSLog(@"done loading");
	appDelegate.hasAnAccount = @"YES";
	// do something with the data
    // receivedData is declared as a method instance elsewhere
	//[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
	//[notifier release];
	NSString * outputString = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
	NSString * middleString = [outputString stringByReplacingOccurrencesOfString:@"<incidents/>" withString:@""];
	NSString * inputString = [middleString stringByReplacingOccurrencesOfString:@"<segments/>" withString:@""];
	NSLog(@"output = %@",outputString);
	
    NSLog(@"Commute info downloaded in commute view");
	appDelegate.tempCommute = inputString;
    CFPreferencesSetAppValue((CFStringRef)@"userName", appDelegate.userName, kCFPreferencesCurrentApplication);
	CFPreferencesSetAppValue((CFStringRef)@"password", appDelegate.password, kCFPreferencesCurrentApplication);
	//NSString *fileAddress = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"commuteInfo.xml"];
	
	//NSString *fileAddress = @"Users/Sam/Documents/TrafficViewer/commuteInfo.xml";//for simulation
	
	/*
	 if([inputString writeToFile:fileAddress atomically:FALSE encoding:NSUTF8StringEncoding error:NULL]){
	 } else {
	 NSLog(@"write failed");
	 }
	 */
	
	[self createCommuteInfo];
    NSLog(@"load morning = %d",loadMorning);
	if(loadMorning) {
		[self prepareCommuteCells:user.morning];
	} else {
		[self prepareCommuteCells:user.evening];
	}
    if(refreshing){
        refreshing = FALSE;
    } else {
        loadMorning = !loadMorning;
    }
	[self.tableView reloadData];
	NSLog(@"dismissing");
    if(appDelegate.priorLoads != 10) {
        [notifier dismissWithClickedButtonIndex:0 animated:FALSE];
        [notifier release];
    }
    [DejalBezelActivityView removeViewAnimated:TRUE];

	[connection release];
    [receivedData release];
}
//------------------------------------------------------------------------------
- (void)switchToEvening:(id)sender{
	loadMorning = !loadMorning;
	[self prepareCommuteCells:user.evening];
	//[self setTitle:@"Evening"];
	//[[[appDelegate.tabBarController.viewControllers objectAtIndex:1] tabBarItem] setTitle:@"My Traffic"];
	//[self.navigationItem.rightBarButtonItem setTitle:@"Alternate"];
	[self.tableView reloadData];

}
//------------------------------------------------------------------------------
-(void)switchToMorning:(id)sender{
	loadMorning = !loadMorning;
	[self prepareCommuteCells:user.morning];
	//[self setTitle:@"Morning"];
	//[[[appDelegate.tabBarController.viewControllers objectAtIndex:1] tabBarItem] setTitle:@"My Traffic"];
	//[self.navigationItem.rightBarButtonItem setTitle:@"Alternate"];
	
	[self.tableView reloadData];
}
//------------------------------------------------------------------------------
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
//------------------------------------------------------------------------------
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [commuteCells count];
}
//------------------------------------------------------------------------------
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

    static NSString *CellIdentifier = @"Cell";
	
	int cellEntryIndex = [indexPath indexAtPosition: [indexPath length] - 1];
    NSLog(@"indexpath.row = %d and count = %d",indexPath.row, [commuteCells count]);
    /*
    if(indexPath.row == [commuteCells count]){
        UITableViewCell *cellAdd = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, 320, 80)];
        UIImageView *userIcon = [[UIImageView alloc] initWithFrame:CGRectMake(cellAdd.contentView.frame.origin.x, cellAdd.contentView.frame.origin.y, 320, 77)];
        userIcon.image = borderBanner;
        [cellAdd.contentView addSubview:userIcon];
        return cellAdd;
        
    } else {
	*/if([[commuteCells objectAtIndex:cellEntryIndex] isKindOfClass:[UIButton class]]) {
		CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		}
		
		[cell sendButtonForCell:[commuteCells objectAtIndex:cellEntryIndex]];
		
		[cell setData:@"Switch to Evening" 
		 currentSpeed:-100 
				 even:0
		   isIncident:NO];
		
		
		return cell;
	}else if([[commuteCells objectAtIndex:cellEntryIndex] isMemberOfClass:[CommuteInfo class]]){
		CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == NULL) {
			cell = [[[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		}
		//[cell addTimeStamp:user.morning.timeStamp];
		[cell setDataForCommute:[commuteCells objectAtIndex:cellEntryIndex]];
		commuteCellForImage = cell;
		return cell;
		
	}else if([[commuteCells objectAtIndex:cellEntryIndex] isKindOfClass:[NSString class]]){
		CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		}
		
		[cell setDataForHighway:[commuteCells objectAtIndex:cellEntryIndex]];
		
		return cell;
	}else if([[commuteCells objectAtIndex:cellEntryIndex] isMemberOfClass:[CommuteType class]]) {
		CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		}
		
		[cell setDataForCommuteType: [commuteCells objectAtIndex:cellEntryIndex]];
		
		return cell;
	}else if([[commuteCells objectAtIndex:cellEntryIndex] isKindOfClass:[UILabel class]]){
		
		CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		
		if (cell == nil) {
			cell = [[[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		}
		if (user.morning.timeStamp != nil) {
			[cell addTimeStamp:user.morning.timeStamp];
		} else if (user.evening.timeStamp != nil) {
			[cell addTimeStamp:user.evening.timeStamp];
		} else if (user.altMorning.timeStamp != nil) {
			[cell addTimeStamp:user.altMorning.timeStamp];
		} else if (user.altEvening.timeStamp != nil) {
			[cell addTimeStamp:user.altEvening.timeStamp];
		}
		
		
		
		[cell setData:@"" currentSpeed:-200 even:0 isIncident:NO];
		
		return cell;
		
	} else if([[commuteCells objectAtIndex:cellEntryIndex] isKindOfClass:[UIImage class]]) {
		CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		}
		if(self.title == @"Morning"){
			[cell setData:@"     Shake phone to see Alternate Commute" currentSpeed:-201 even:0 isIncident:FALSE];
		} else {
			[cell setData:@"     Shake phone to see Primary Commute" currentSpeed:-201 even:0 isIncident:FALSE];
		}
		
		return cell;
		
	} else {
        
        CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
        }
		
        [cell setData:[[commuteCells objectAtIndex:cellEntryIndex] exitName]
         currentSpeed:[[commuteCells objectAtIndex:cellEntryIndex] mph]
                 even:cellEntryIndex%2
           isIncident:[[commuteCells objectAtIndex:cellEntryIndex]isIncident]];
		
        return cell;
	}
    //}
	[pool drain];
}
//------------------------------------------------------------------------------
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath { 
	int cellEntryIndex = [indexPath indexAtPosition: [indexPath length] -1];
    /*if(indexPath.row == [commuteCells count]){
        return 80.0;
    }*/
	if([indexPath indexAtPosition: [indexPath length] -1] == 0 ){
		return 50.0;
	} else if([[commuteCells objectAtIndex:cellEntryIndex] isKindOfClass:[NSString class]]){
		return 40.0;
	} else if([[commuteCells objectAtIndex:cellEntryIndex] isKindOfClass:[UIButton class]]){
		return 90.0;
	} else if([[commuteCells objectAtIndex:cellEntryIndex] isKindOfClass:[UILabel class]]){
		return 25.0;
	} else if([[commuteCells objectAtIndex:cellEntryIndex] isMemberOfClass:[CommuteType class]]){
		return 40.0;
	} else {
		return 25.0;
	}
}
//------------------------------------------------------------------------------
-(void) grabRSSFeed:(NSString *)blogAddress {
	int segmentId = 0;
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

	
    // Initialize the congestionInfo MutableArray that we declared in the header
    congestionInfo = [[NSMutableArray alloc] init];
	userInfo = [[NSMutableArray alloc] init];
	morningCommuteInfo = [[NSMutableArray alloc] init];
	eveningCommuteInfo = [[NSMutableArray alloc] init];
	altEveningCommuteInfo = [[NSMutableArray alloc] init];
	altMorningCommuteInfo = [[NSMutableArray alloc] init];
	segmentInfo = [[NSMutableArray alloc] init];
	segmentsCounter = [[NSMutableArray alloc] init];
	incidentInfo = [[NSMutableArray alloc] init];
	NSLog(@"no mem issues");
    // Convert the supplied URL string into a usable URL object
    //NSURL *url = [NSURL URLWithString: blogAddress];
	
    // Create a new rssParser object based on the TouchXML "CXMLDocument" class, this is the
    // object that actually grabs and processes the RSS data
	//CXMLDocument *rssParser = [[[CXMLDocument alloc] initWithContentsOfURL:url options:0 error:nil] autorelease];
	
    CXMLDocument *rssParser = [[[CXMLDocument alloc] initWithXMLString:blogAddress options:0 error:nil] autorelease];
    // Create a new Array object to be used with the looping of the results from the rssParser
    NSArray *congestionNodes = NULL;
	NSArray *userInfoNodes = NULL;
	NSArray *morningCommuteNodes = NULL;
	NSArray *eveningCommuteNodes = NULL;
	NSArray *altMorningCommuteNodes = NULL;
	NSArray *altEveningCommuteNodes = NULL;
	NSArray *segmentNodes = NULL;
	NSArray *segmentsCounterNodes = NULL;
	NSArray *incidentNodes = NULL;
	
	
	
    // Set the congestionNodes Array to contain an object for every instance of an  node in our RSS feed
    congestionNodes = [rssParser nodesForXPath:@"//congestion" error:nil];
	userInfoNodes = [rssParser nodesForXPath:@"//user" error:nil];
	morningCommuteNodes = [rssParser nodesForXPath:@"//morning" error:nil];
	eveningCommuteNodes = [rssParser nodesForXPath:@"//evening" error:nil];
	altEveningCommuteNodes = [rssParser nodesForXPath:@"//altEvening" error:nil];
	altMorningCommuteNodes = [rssParser nodesForXPath:@"//altMorning" error:nil];
	segmentNodes = [rssParser nodesForXPath:@"//segment" error:nil];
	segmentsCounterNodes = [rssParser nodesForXPath:@"//segments" error:nil];
	incidentNodes = [rssParser nodesForXPath:@"//commute.Incident" error:nil];
	
	segmentsTracker=0;
    // Loop through the congestionNodes to access each items actual data
	for (CXMLElement *resultElement in userInfoNodes) {
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[userInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
    NSNumber * falseNumber = [NSNumber numberWithBool:FALSE];
    CFPreferencesSetAppValue((CFStringRef)@"morningExists", falseNumber , kCFPreferencesCurrentApplication);
    CFPreferencesSetAppValue((CFStringRef)@"altMorningExists", falseNumber , kCFPreferencesCurrentApplication);
    CFPreferencesSetAppValue((CFStringRef)@"eveningExists", falseNumber , kCFPreferencesCurrentApplication);
    CFPreferencesSetAppValue((CFStringRef)@"altEveningExists", falseNumber , kCFPreferencesCurrentApplication);

	//set up the segmentId to identify each type of commute (monrning, evening, and alts)
    NSLog(@"COMMUTE TIMES EXIST!");
	if([[[userInfo objectAtIndex: 0] objectForKey: @"morningExists"] boolValue]){
        appDelegate.morningExists = TRUE;
        NSNumber * morningE = [NSNumber numberWithBool:TRUE];
        CFPreferencesSetAppValue((CFStringRef)@"morningExists", morningE , kCFPreferencesCurrentApplication);
		segmentId+=1000;
	}
	if([[[userInfo objectAtIndex: 0] objectForKey: @"altMorningExists"] boolValue]){
        appDelegate.altMorningExists = TRUE;
        NSNumber * altMorningE = [NSNumber numberWithBool:TRUE];
        CFPreferencesSetAppValue((CFStringRef)@"altMorningExists", altMorningE , kCFPreferencesCurrentApplication);
		segmentId+=100;
	}
	if([[[userInfo objectAtIndex: 0] objectForKey: @"eveningExists"] boolValue]){
        appDelegate.eveningExists = TRUE;
        NSNumber * eveningE = [NSNumber numberWithBool:TRUE];
        CFPreferencesSetAppValue((CFStringRef)@"eveningExists", eveningE , kCFPreferencesCurrentApplication);
		segmentId+=10;
	}
	if([[[userInfo objectAtIndex: 0] objectForKey: @"altEveningExists"] boolValue]){
        appDelegate.altEveningExists = TRUE;
        NSNumber * altEveningE = [NSNumber numberWithBool:TRUE];
        CFPreferencesSetAppValue((CFStringRef)@"altEveningExists", altEveningE , kCFPreferencesCurrentApplication);
		segmentId+=1;
	}
    CFPreferencesAppSynchronize(kCFPreferencesCurrentApplication);
	for (CXMLElement *resultElement in segmentsCounterNodes) {
		@try{
			segmentsTracker++;
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			
			if(segmentsTracker == 1){
				if(segmentId >=1000){
					morningCommuteSegments = counter/2;
					segmentId-=1000;
				} else if(segmentId >=100){
					altMorningCommuteSegments = counter/2;
					segmentId-=100;
				} else if(segmentId >=10){
					eveningCommuteSegments = counter/2;
					segmentId-=10;
				} else {
					altEveningCommuteSegments = counter/2;
				}
			}else if(segmentsTracker == 2){
				if(segmentId >=100){
					altMorningCommuteSegments = counter/2;
					segmentId-=100;
				} else if(segmentId >=10){
					eveningCommuteSegments = counter/2;
					segmentId-=10;
				} else {
					altEveningCommuteSegments = counter/2;
				}
			} else if(segmentsTracker == 3){
				if(segmentId >=10){
					eveningCommuteSegments = counter/2;
					segmentId-=10;
				} else {
					altEveningCommuteSegments = counter/2;
				}
			} else {
				altEveningCommuteSegments = counter/2;
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[segmentsCounter addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	segmentsRemaining = morningCommuteSegments + altMorningCommuteSegments + eveningCommuteSegments + altEveningCommuteSegments;
	
	for (CXMLElement *resultElement in segmentNodes) {
		
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[segmentInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	
	for (CXMLElement *resultElement in morningCommuteNodes) {
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[morningCommuteInfo addObject:[infoItem copy]];
			[infoItem release];
			
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	
	for (CXMLElement *resultElement in altMorningCommuteNodes) {
		@try{
			
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[altMorningCommuteInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	
	for (CXMLElement *resultElement in eveningCommuteNodes) {
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[eveningCommuteInfo addObject:[infoItem copy]];
			[infoItem release];
			
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	
	for (CXMLElement *resultElement in altEveningCommuteNodes) {
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[altEveningCommuteInfo addObject:[infoItem copy]];
			[infoItem release];
			
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	
    for (CXMLElement *resultElement in congestionNodes) {
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[congestionInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	
	[pool drain];
	
	//NSFileManager *fileManager = [NSFileManager defaultManager];
	//[fileManager removeItemAtPath:blogAddress error:NULL];
	/*
	 for (CXMLElement *resultElement in incidentNodes) {
	 @try{
	 // Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
	 NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
	 
	 // Create a counter variable as type "int"
	 int counter;
	 
	 // Loop through the children of the current  node
	 for(counter = 0; counter < [resultElement childCount]; counter++) {
	 
	 // Add each field to the blogItem Dictionary with the node name as key and node value as the value
	 [infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
	 }
	 
	 // Add the blogItem to the global congestionInfo Array so that the view can access it.
	 [incidentInfo addObject:[infoItem copy]];
	 [infoItem release];
	 }@catch (NSException * e) {
	 NSLog(@"Exception %@: %@", [e name], [e  reason]);
	 }
	 }
	 */
}
//------------------------------------------------------------------------------
-(void) createCommuteInfo{
	BOOL getOut = FALSE;
	BOOL alreadyAdded = FALSE;
	segmentsDone = 0;
	@try{
		if([congestionInfo count] == 0) {
			// Create the feed string
			//NSString *blogAddress = [@"file://" stringByAppendingString:[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"commuteInfo.xml"]];
			//For simulator
			//NSString *blogAddress = @"file://localhost/Users/Sam/Documents/TrafficViewer/commuteInfo.xml";
			
			// Call the grabRSSFeed function with the above
			// string as a parameter
			
			[self grabRSSFeed:appDelegate.tempCommute];
			
			//[self grabRSSFeed:blogAddress];
			congestionIndex = 0;
			int prevSeq=0;
			user = [[User alloc] initWithInfo:[[[userInfo objectAtIndex: 0] objectForKey: @"userId"] intValue]
									  carrier:[[[userInfo objectAtIndex: 0] objectForKey: @"carrier"] intValue]
								accountExists:[[[userInfo objectAtIndex: 0] objectForKey: @"accountExists"] boolValue]
								morningExists:[[[userInfo objectAtIndex: 0] objectForKey: @"morningExists"] boolValue]
							 altMorningExists:[[[userInfo objectAtIndex: 0] objectForKey: @"altMorningExists"] boolValue]
								eveningExists:[[[userInfo objectAtIndex: 0] objectForKey: @"eveningExists"] boolValue]
							 altEveningExists:[[[userInfo objectAtIndex: 0] objectForKey: @"altEveningExists"] boolValue]
								   infoExists:[[[userInfo objectAtIndex: 0] objectForKey: @"infoExists"] boolValue]
										  ani:[[userInfo objectAtIndex: 0] objectForKey: @"ani"]
									firstName:[[userInfo objectAtIndex: 0] objectForKey: @"firstName"]
									 lastName:[[userInfo objectAtIndex: 0] objectForKey: @"lastName"] 
								 carrierEmail:[[userInfo objectAtIndex: 0] objectForKey: @"carrierEmail"]
										email:[[userInfo objectAtIndex: 0] objectForKey: @"email"]];
			
			if(user.morningExists){
				tempMorning = [[Commute alloc] initWithType:[[[morningCommuteInfo objectAtIndex: 0] objectForKey: @"morning"] boolValue] 
												  alternate:[[[morningCommuteInfo objectAtIndex: 0] objectForKey: @"alternate"] boolValue]  
												numSegments:morningCommuteSegments
												currentTime:[[[morningCommuteInfo objectAtIndex: 0] objectForKey: @"currentTime"] intValue]  
												averageTime:[[[morningCommuteInfo objectAtIndex: 0] objectForKey: @"averageTime"] intValue]  
												 difference:[[[morningCommuteInfo objectAtIndex: 0] objectForKey: @"difference"] floatValue]];
				tempMorning.timeStamp = [[segmentInfo objectAtIndex: 0] objectForKey: @"timestamp"];
				
				[user addMorningCommute:tempMorning];
				/*----uncomment to display commute time as a badge on the tabBar
				 NSString *commuteTimeForBadge = [NSString stringWithFormat:@"%d",tempMorning.currentTime];
				 self.navigationController.tabBarItem.badgeValue = commuteTimeForBadge;
				 */
				[tempMorning release];
				
				
				for(int i = 0; i<morningCommuteSegments;i++){
					segmentsDone++;
					tempSeg = [[Segment alloc] initWithID:[[[segmentInfo objectAtIndex: i] objectForKey: @"segmentId"] intValue]
												   onramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"onramp"] intValue]
												  offramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"offramp"] intValue]
												threshold:[[[segmentInfo objectAtIndex: i] objectForKey: @"threshold"] intValue]
													hwyId:[[[segmentInfo objectAtIndex: i] objectForKey: @"hwyId"] intValue]
											startDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"startDistance"] floatValue] 
											  endDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"endDistance"] floatValue] 
												  hwyName:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyName"] 
											   hwyWavFile:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyWaveFile"]  
												congested:[[[segmentInfo objectAtIndex: i] objectForKey: @"congested"] boolValue]  
										   incidentExists:[[[segmentInfo objectAtIndex: i] objectForKey: @"incidentExists"] boolValue]];
					
					prevSeq = [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue];
					for(int j = 0;
						j <= tempSeg.offramp - tempSeg.onramp 
						&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
						&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq
						&& !getOut;
						j++)
					{
						@try{
							tempCong = [[Congestion alloc] initWithName:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitName"] 
																wavFile:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitWavFile"]  
															   sequence:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue]
																	mph:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"mph"] intValue]];
							[tempSeg addCongestion:tempCong];
							prevSeq = tempCong.sequence;
							
						}@catch(NSException * e) {
							NSLog(@"Exception %@: %@", [e name], [e  reason]);
						}
						if(!([[[congestionInfo lastObject] objectForKey:@"exitName"] isEqual:tempCong.exitName]
							 && [[[congestionInfo lastObject] objectForKey:@"exitWavFile"] isEqual:tempCong.exitWaveFile]
							 && [[[congestionInfo lastObject] objectForKey:@"sequence"]intValue] == tempCong.sequence
							 && [[[congestionInfo lastObject] objectForKey:@"mph"] intValue] == tempCong.mph
						     && segmentsDone == segmentsRemaining)){
							congestionIndex++;
							@try {
								if(j <= tempSeg.offramp - tempSeg.onramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq) {
								}
							}@catch (NSException * e) {
								[user.morning addSegment:tempSeg];
								alreadyAdded = TRUE;
							}
						} else{
							getOut = true;
						}
					}
					/*
					 if(tempSeg.incidentExists){
					 
					 tempCong = [[Congestion alloc] initWithName:[[incidentInfo objectAtIndex:0] objectForKey:@"exitName"] 
					 wavFile:[[incidentInfo objectAtIndex:0] objectForKey:@"exitWavFile"]  
					 sequence:[[[incidentInfo objectAtIndex:0] objectForKey:@"sequence"] intValue]
					 mph:0];
					 tempCong.isIncident = TRUE;
					 [tempSeg addCongestion:tempCong];
					 }
					 */
					if(!alreadyAdded){
						[user.morning addSegment:tempSeg];
						[tempSeg release];
						[tempCong release];
					}
					
				}
			}
			alreadyAdded = FALSE;
			getOut = FALSE;
			if(user.altMorningExists){
				tempAltMorning = [[Commute alloc] initWithType:[[[altMorningCommuteInfo objectAtIndex: 0] objectForKey: @"morning"] boolValue] 
													 alternate:[[[altMorningCommuteInfo objectAtIndex: 0] objectForKey: @"alternate"] boolValue]  
												   numSegments:altMorningCommuteSegments
												   currentTime:[[[altMorningCommuteInfo objectAtIndex: 0] objectForKey: @"currentTime"] intValue]  
												   averageTime:[[[altMorningCommuteInfo objectAtIndex: 0] objectForKey: @"averageTime"] intValue]  
													difference:[[[altMorningCommuteInfo objectAtIndex: 0] objectForKey: @"difference"] floatValue]];
				tempAltMorning.timeStamp = [[segmentInfo objectAtIndex: 0] objectForKey:@"timestamp"];
				[user addAltMorningCommute:tempAltMorning];
				[tempAltMorning release];
				
				for(int i = morningCommuteSegments;i<morningCommuteSegments+altMorningCommuteSegments;i++){
					segmentsDone++;
					tempSeg = [[Segment alloc] initWithID:[[[segmentInfo objectAtIndex: i] objectForKey: @"segmentId"] intValue]
												   onramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"onramp"] intValue]
												  offramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"offramp"] intValue]
												threshold:[[[segmentInfo objectAtIndex: i] objectForKey: @"threshold"] intValue]
													hwyId:[[[segmentInfo objectAtIndex: i] objectForKey: @"hwyId"] intValue]
											startDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"startDistance"] floatValue] 
											  endDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"endDistance"] floatValue] 
												  hwyName:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyName"] 
											   hwyWavFile:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyWaveFile"]  
												congested:[[[segmentInfo objectAtIndex: i] objectForKey: @"congested"] boolValue]  
										   incidentExists:[[[segmentInfo objectAtIndex: i] objectForKey: @"incidentExists"] boolValue]];
					prevSeq = [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue];
					for(int j = 0;
						j <= tempSeg.offramp - tempSeg.onramp 
						&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
						&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq
						&& !getOut;
						j++)
					{
						@try{
							tempCong = [[Congestion alloc] initWithName:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitName"] 
																wavFile:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitWavFile"]  
															   sequence:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue]
																	mph:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"mph"] intValue]];
							[tempSeg addCongestion:tempCong];
							prevSeq = tempCong.sequence;
						}@catch(NSException * e) {
							NSLog(@"Exception %@: %@", [e name], [e  reason]);
						}
						if(!([[[congestionInfo lastObject] objectForKey:@"exitName"] isEqual:tempCong.exitName]
							 && [[[congestionInfo lastObject] objectForKey:@"exitWavFile"] isEqual:tempCong.exitWaveFile]
							 && [[[congestionInfo lastObject] objectForKey:@"sequence"] intValue] == tempCong.sequence
							 && [[[congestionInfo lastObject] objectForKey:@"mph"] intValue] == tempCong.mph
						     && segmentsDone == segmentsRemaining)){
							congestionIndex++;
							@try {
								if(j <= tempSeg.offramp - tempSeg.onramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq) {
								}
							}@catch (NSException * e) {
								NSLog(@"caught it");
								[user.altMorning addSegment:tempSeg];
								alreadyAdded = TRUE;
								NSLog(@"Adding alt evening segment");
							}
						} else{
							getOut = true;
						}
					}
					/*
					 if(tempSeg.incidentExists){
					 
					 tempCong = [[Congestion alloc] initWithName:[[incidentInfo objectAtIndex:0] objectForKey:@"exitName"] 
					 wavFile:[[incidentInfo objectAtIndex:0] objectForKey:@"exitWavFile"]  
					 sequence:[[[incidentInfo objectAtIndex:0] objectForKey:@"sequence"] intValue]
					 mph:0];
					 tempCong.isIncident = TRUE;
					 [tempSeg addCongestion:tempCong];
					 }
					 */
					if(!alreadyAdded) {
						[user.altMorning addSegment:tempSeg];
						[tempSeg release];
						[tempCong release];
					}
					
				}
			}
			alreadyAdded = FALSE;
			getOut = false;
			if(user.eveningExists){
				tempEvening = [[Commute alloc] initWithType:[[[eveningCommuteInfo objectAtIndex: 0] objectForKey: @"morning"] boolValue] 
												  alternate:[[[eveningCommuteInfo objectAtIndex: 0] objectForKey: @"alternate"] boolValue]  
												numSegments:eveningCommuteSegments
												currentTime:[[[eveningCommuteInfo objectAtIndex: 0] objectForKey: @"currentTime"] intValue]  
												averageTime:[[[eveningCommuteInfo objectAtIndex: 0] objectForKey: @"averageTime"] intValue]  
												 difference:[[[eveningCommuteInfo objectAtIndex: 0] objectForKey: @"difference"] floatValue]];
				tempEvening.timeStamp = [[segmentInfo objectAtIndex: 0] objectForKey:@"timestamp"];
				[user addEveningCommute:tempEvening];
				[tempEvening release];
				
				
				for(int i = morningCommuteSegments+altMorningCommuteSegments;i<morningCommuteSegments+altMorningCommuteSegments+eveningCommuteSegments;i++){
					segmentsDone++;
					tempSeg = [[Segment alloc] initWithID:[[[segmentInfo objectAtIndex: i] objectForKey: @"segmentId"] intValue]
												   onramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"onramp"] intValue]
												  offramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"offramp"] intValue]
												threshold:[[[segmentInfo objectAtIndex: i] objectForKey: @"threshold"] intValue]
													hwyId:[[[segmentInfo objectAtIndex: i] objectForKey: @"hwyId"] intValue]
											startDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"startDistance"] floatValue] 
											  endDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"endDistance"] floatValue] 
												  hwyName:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyName"] 
											   hwyWavFile:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyWaveFile"]  
												congested:[[[segmentInfo objectAtIndex: i] objectForKey: @"congested"] boolValue]  
										   incidentExists:[[[segmentInfo objectAtIndex: i] objectForKey: @"incidentExists"] boolValue]];
					prevSeq = [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue];
					for(int j = 0;
						j <= tempSeg.offramp - tempSeg.onramp 
						&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
						&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq
						&& !getOut;
						j++)
					{
						@try{
							tempCong = [[Congestion alloc] initWithName:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitName"] 
																wavFile:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitWavFile"]  
															   sequence:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue]
																	mph:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"mph"] intValue]];
							[tempSeg addCongestion:tempCong];
							prevSeq = tempCong.sequence;
						}@catch(NSException * e) {
							NSLog(@"Exception %@: %@", [e name], [e  reason]);
						}
						if(!([[[congestionInfo lastObject] objectForKey:@"exitName"] isEqual:tempCong.exitName]
							 && [[[congestionInfo lastObject] objectForKey:@"exitWavFile"] isEqual:tempCong.exitWaveFile]
							 && [[[congestionInfo lastObject] objectForKey:@"sequence"]intValue] == tempCong.sequence
							 && [[[congestionInfo lastObject] objectForKey:@"mph"] intValue]== tempCong.mph
						     && segmentsDone == segmentsRemaining)){
							congestionIndex++;
							@try {
								if(j <= tempSeg.offramp - tempSeg.onramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq) {
								}
							}@catch (NSException * e) {
								NSLog(@"caught it");
								[user.evening addSegment:tempSeg];
								alreadyAdded = TRUE;
								NSLog(@"Adding alt evening segment");
							}
						} else{
							getOut = true;
						}
					}
					/*
					 if(tempSeg.incidentExists){
					 
					 tempCong = [[Congestion alloc] initWithName:[[incidentInfo objectAtIndex:0] objectForKey:@"exitName"] 
					 wavFile:[[incidentInfo objectAtIndex:0] objectForKey:@"exitWavFile"]  
					 sequence:[[[incidentInfo objectAtIndex:0] objectForKey:@"sequence"] intValue]
					 mph:0];
					 tempCong.isIncident = TRUE;
					 [tempSeg addCongestion:tempCong];
					 }
					 */
					if(!alreadyAdded) {
						[user.evening addSegment:tempSeg];
						[tempSeg release];
						[tempCong release];
					}
					
				}
				
			}
			alreadyAdded = FALSE;
			getOut = false;
			if(user.altEveningExists){
				tempAltEvening =  [[Commute alloc] initWithType:[[[altEveningCommuteInfo objectAtIndex: 0] objectForKey: @"morning"] boolValue] 
													  alternate:[[[altEveningCommuteInfo objectAtIndex: 0] objectForKey: @"alternate"] boolValue]  
													numSegments:altEveningCommuteSegments
													currentTime:[[[altEveningCommuteInfo objectAtIndex: 0] objectForKey: @"currentTime"] intValue]  
													averageTime:[[[altEveningCommuteInfo objectAtIndex: 0] objectForKey: @"averageTime"] intValue]  
													 difference:[[[altEveningCommuteInfo objectAtIndex: 0] objectForKey: @"difference"] floatValue]];
				tempAltEvening.timeStamp = [[segmentInfo objectAtIndex: 0] objectForKey:@"timestamp"];
				
				[user addAltEveningCommute:tempAltEvening];
				[tempAltEvening release];
				
				
				for(int i = morningCommuteSegments+altMorningCommuteSegments+eveningCommuteSegments;
					i<morningCommuteSegments+altMorningCommuteSegments+eveningCommuteSegments+altEveningCommuteSegments;
					i++){
					segmentsDone++;
					tempSeg = [[Segment alloc] initWithID:[[[segmentInfo objectAtIndex: i] objectForKey: @"segmentId"] intValue]
												   onramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"onramp"] intValue]
												  offramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"offramp"] intValue]
												threshold:[[[segmentInfo objectAtIndex: i] objectForKey: @"threshold"] intValue]
													hwyId:[[[segmentInfo objectAtIndex: i] objectForKey: @"hwyId"] intValue]
											startDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"startDistance"] floatValue] 
											  endDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"endDistance"] floatValue] 
												  hwyName:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyName"] 
											   hwyWavFile:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyWaveFile"]  
												congested:[[[segmentInfo objectAtIndex: i] objectForKey: @"congested"] boolValue]  
										   incidentExists:[[[segmentInfo objectAtIndex: i] objectForKey: @"incidentExists"] boolValue]];
					prevSeq = [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue];					
					for(int j = 0; j <= tempSeg.offramp - tempSeg.onramp 
						&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
						&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq
						&& !getOut; j++)
					{
						@try{
							tempCong = [[Congestion alloc] initWithName:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitName"] 
																wavFile:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitWavFile"]  
															   sequence:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue]
																	mph:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"mph"] intValue]];
							[tempSeg addCongestion:tempCong];
							prevSeq = tempCong.sequence;							
						}@catch(NSException * e) {
							NSLog(@"Exception %@: %@", [e name], [e  reason]);
						}
						if(!([[[congestionInfo lastObject] objectForKey:@"exitName"] isEqual:tempCong.exitName]
							 && [[[congestionInfo lastObject] objectForKey:@"exitWavFile"] isEqual:tempCong.exitWaveFile]
							 && [[[congestionInfo lastObject] objectForKey:@"sequence"]intValue] == tempCong.sequence
							 && [[[congestionInfo lastObject] objectForKey:@"mph"] intValue]== tempCong.mph
						     && segmentsDone == segmentsRemaining)){
							congestionIndex++;
							@try {
								if(j <= tempSeg.offramp - tempSeg.onramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq) {
								}
							}@catch (NSException * e) {
								NSLog(@"caught it");
								[user.altEvening addSegment:tempSeg];
								alreadyAdded = TRUE;
								NSLog(@"Adding alt evening segment");
							}
						} else{
							getOut = true;
						}
					}
					/*
					 if(tempSeg.incidentExists){
					 
					 tempCong = [[Congestion alloc] initWithName:[[incidentInfo objectAtIndex:0] objectForKey:@"exitName"] 
					 wavFile:[[incidentInfo objectAtIndex:0] objectForKey:@"exitWavFile"]  
					 sequence:[[[incidentInfo objectAtIndex:0] objectForKey:@"sequence"] intValue]
					 mph:0];
					 tempCong.isIncident = TRUE;
					 [tempSeg addCongestion:tempCong];
					 }
					 */
					if(!alreadyAdded) {
						[user.altEvening addSegment:tempSeg];
						[tempSeg release];
						[tempCong release];
					}
				}
			}
			
			
			// Call the reloadData function on the blogTable, this
			// will cause it to refresh itself with our new data
		}
	}@catch (NSException * e) {
		NSLog(@"Error");
	}
	
	appDelegate.user = user;
	//[user release];
	[congestionInfo release];
	[userInfo release];
	[morningCommuteInfo release];
	[eveningCommuteInfo release];
	[altEveningCommuteInfo release];
	[altMorningCommuteInfo release];
	[segmentInfo release];
	[segmentsCounter release];
	NSLog(@"END OF CREATE COMMUTE");
}
//------------------------------------------------------------------------------
-(void) prepareCommuteCells:(Commute *)c{
	NSLog(@"START OF PREPARE CELLS");
	[commuteCells dealloc];
	//[appDelegate addQuickCommute];
    NSLog(@"current time = %d",c.currentTime);
	if(c.currentTime == 0){
		//[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
		noCommuteNotifier = [[UIAlertView alloc] initWithTitle:@"" message:@"You have not added a commute for this time.  Login to http://traffic.calit2.net to add one." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[noCommuteNotifier show];
	}
	commuteCells = [[NSMutableArray alloc] init];
	float distance = 0;
	for(int i = 0; i<[c.segments count]; i++){
		distance+=[[c.segments objectAtIndex:i] endDistance] - [[c.segments objectAtIndex:i] startDistance];
	}
	CommuteInfo * commuteDetail;
	if(!c.alternate) {
		commuteDetail = [[CommuteInfo alloc] initWithCurrent:[c currentTime] 
												 averageTime:[c averageTime] 
												   otherTime: user.altMorning.currentTime
												  difference:[c difference] 
													distance:distance 
												 commuteType:[c alternate]];
	} else {
		commuteDetail = [[CommuteInfo alloc] initWithCurrent:[c currentTime] 
												 averageTime:[c averageTime] 
												   otherTime: user.morning.currentTime
												  difference:[c difference]
													distance:distance
												 commuteType:[c alternate]];
	}
	
	
	
	[commuteCells addObject: commuteDetail];
	[commuteDetail release];
	
	NSString * commuteTypeForCell;
	NSString * altTypeForCell;
	if(c.morning) {
		if(!c.alternate) { 
			commuteTypeForCell = @"Pri";
			altTypeForCell = @"Morning";
			//commuteTypeForCell = [[CommuteType alloc] initWithImg: retain] title:@"Primary"];
		}else {
			commuteTypeForCell = @"Alt";
			altTypeForCell = @"Morning";
			//commuteTypeForCell = [[CommuteType alloc] initWithImg:[[UIImage imageNamed:@"Morning_Commute_Icon.png"] retain] title:@"Alternate"];
		}
	} else {
		if(!c.alternate) {
			commuteTypeForCell = @"Pri";
			altTypeForCell = @"Evening";
			//commuteTypeForCell = [[CommuteType alloc] initWithImg:[[UIImage imageNamed:@"Evening_Commute_Icon.png"] retain] title:@"Primary"];
		}else {
			commuteTypeForCell = @"Alt";
			altTypeForCell = @"Evening";
			//commuteTypeForCell = [[CommuteType alloc] initWithImg:[[UIImage imageNamed:@"Evening_Commute_Icon.png"] retain] title:@"Alternate"];
		}
	}
	//[commuteCells addObject: commuteTypeForCell];
	//[commuteTypeForCell release];
	
	for(int i = 0; i<[c.segments count]; i++){
		NSNumber *distance = [[NSNumber alloc] initWithFloat:[[c.segments objectAtIndex:i] endDistance] - [[c.segments objectAtIndex:i] startDistance]];
		NSString *segmentLength = [[NSString alloc] initWithFormat:@"%.2f", [distance floatValue]];
		if(i==0) {
			[commuteCells addObject:[[NSString alloc] initWithFormat:@"%@***%@***%@***%@***yay***bonus", [[c.segments objectAtIndex:i] hwyName],segmentLength,commuteTypeForCell,altTypeForCell]];
			UILabel *labelForCommuteCellSetup = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
			[commuteCells addObject:labelForCommuteCellSetup];
		} else {
			[commuteCells addObject:[[NSString alloc] initWithFormat:@"%@***%@***%@***%@***yay", [[c.segments objectAtIndex:i] hwyName],segmentLength,commuteTypeForCell,altTypeForCell]];
		}
		for(int j = 0; j<[[[c.segments objectAtIndex:i] congestions] count];j++){
			[commuteCells addObject:[[[c.segments objectAtIndex:i] congestions] objectAtIndex:j]];
		}
	}
	/*
	 //used to add a button at end of cells
	 if(loadMorning) {
	 UIButton * switchToOtherCommute = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	 [switchToOtherCommute setTitle:@"Switch to Evening" forState:UIControlStateNormal];
	 switchToOtherCommute.showsTouchWhenHighlighted = YES;
	 switchToOtherCommute.frame = CGRectMake(120,2,150,35);
	 [switchToOtherCommute addTarget:self action:@selector(switchToEvening:) forControlEvents:UIControlEventTouchUpInside];
	 [commuteCells addObject:switchToOtherCommute];
	 self.navigationItem.leftBarButtonItem.enabled = YES;
	 } else {
	 UIButton * switchToOtherCommute = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	 [switchToOtherCommute setTitle:@"Switch to Morning" forState:UIControlStateNormal];
	 switchToOtherCommute.showsTouchWhenHighlighted = YES;
	 switchToOtherCommute.frame = CGRectMake(120,2,150,35);
	 [switchToOtherCommute addTarget:self action:@selector(switchToMorning:) forControlEvents:UIControlEventTouchUpInside];
	 [commuteCells addObject:switchToOtherCommute];
	 self.navigationItem.leftBarButtonItem.enabled = YES;
	 }
	 */
    [DejalBezelActivityView removeViewAnimated:TRUE];

	self.navigationItem.leftBarButtonItem.enabled = YES;
}
//------------------------------------------------------------------------------
-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	if (self.lastAcceleration) {
		if (!histeresisExcited && [self L0AccelerationIsShaking:self.lastAcceleration current:acceleration threshold:2.7]) {
			histeresisExcited = YES;
			if(appDelegate.tabBarController.selectedIndex == 1) {
				[self switchToAlt:segmentedControl];
				
			}
			
		} else if (histeresisExcited && ![self L0AccelerationIsShaking:self.lastAcceleration current:acceleration threshold:1.0]) {
			histeresisExcited = NO;
		}
	}
	
	self.lastAcceleration = acceleration;
	
}
//------------------------------------------------------------------------------
-(void) alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	// the user clicked one of the OK/Cancel buttons
	if(buttonIndex == 1){
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://traffic.calit2.net"]];
	}
}
//------------------------------------------------------------------------------
-(void) viewDidAppear:(BOOL) animated {
	
	//[appDelegate.notifier dismissWithClickedButtonIndex:0 animated:FALSE];
	//[appDelegate.notifier release];
	[super viewDidAppear:animated];
}
//------------------------------------------------------------------------------
-(BOOL) L0AccelerationIsShaking:(UIAcceleration*)last current:(UIAcceleration*)current threshold:(double) threshold {
	double deltaX = fabs(last.x - current.x),
	deltaY = fabs(last.y - current.y),
	deltaZ = fabs(last.z - current.z);
	
	
	//return ((deltaX > threshold && deltaY > threshold) || (deltaX > threshold && deltaZ > threshold) || (deltaY > threshold && deltaZ > threshold));
	return (deltaY > threshold);
}
//------------------------------------------------------------------------------
- (void) segmentAction:(id)sender {
	UISegmentedControl* segCtl = sender;
	if( [segCtl selectedSegmentIndex] == 0 ){
		if(loadMorning){
			[self switchToMorning:segCtl];
			[segCtl setTitle:@"Evening" forSegmentAtIndex:0];
			[segCtl setTitle:@"Alternate" forSegmentAtIndex:2];
		} else {
			[self switchToEvening:segCtl];
			[segCtl setTitle:@"Morning" forSegmentAtIndex:0];
			[segCtl setTitle:@"Alternate" forSegmentAtIndex:2];
			
		}
		
	} else if ( [segCtl selectedSegmentIndex] == 2 ){
		[self switchToAlt:segCtl]; 
	} else if ( [segCtl selectedSegmentIndex] == 1) {
		[self refresh:segCtl];
	}
	
	
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == [commuteCells count]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.apple.com/us/app/best_time-to-cross-the-border/id570288644?mt=8"]];
    }
}

/*
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 }
 */

/*
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 }
 if (editingStyle == UITableViewCellEditingStyleInsert) {
 }
 }
 */

/*
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */

/*
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */



- (void)viewWillAppear:(BOOL)animated {
	[self.tableView reloadData];
	[super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)dealloc {
    [super dealloc];
	/*
	[commuteCells release];
	[congestionInfo release];
	[userInfo release];
	[morningCommuteInfo release];
	[eveningCommuteInfo release];
	[altEveningCommuteInfo release];
	[altMorningCommuteInfo release];
	[segmentInfo release];
	[segmentsCounter release];
	[refreshButton release];
	[eveningButton release];
	
	
	[receivedData release];
	[conn release];
	[notifier release];
	[noCommuteNotifier release];
	
	
	[commuteCells release];
	[incidentInfo release];
	[commuteCellForImage release];
	
	[lastAcceleration release];
	[segmentedControl release];
	
	
	//User contains user info
	[user release];
	[tempMorning release];
	[tempAltMorning release];
	[tempEvening release];
	[tempAltEvening release];
	[tempCong release];
	[tempSeg release];
	
	//SoundEffect * hornSound;
	*/
	
}


@end

