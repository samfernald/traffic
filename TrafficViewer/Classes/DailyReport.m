//
//  DailyReport.m
//  TrafficViewer
//
//  Created by Sam Fernald on 9/10/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import "DailyReport.h"

@implementation DailyReport
@synthesize reportID, commuteType, hour, minute, ampm, notify, day;


-(id) initWithID:(int)rid
     commuteType:(int)t 
            hour:(int)h 
          minute:(int)m 
            ampm:(int)a 
          notify:(int)n 
             day:(int)d {
    self.reportID = rid;
    self.commuteType = t;
    self.hour = h;
    self.minute = m;
    self.ampm = a;
    self.notify = n;
    self.day = d;
    return self;
}


@end
