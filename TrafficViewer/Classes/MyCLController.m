//
//  MyCLController.m
//  TrafficViewer
//
//  Created by sfernald on 6/29/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "MyCLController.h"

@implementation MyCLController

@synthesize locationManager;
@synthesize delegate;

- (id) init {
	self = [super init];
	if (self != nil) {
		NSLog(@"mem check 1");

		self.locationManager = [[[CLLocationManager alloc] init] autorelease];
		self.locationManager.delegate = self; // send loc updates to myself
		NSLog(@"mem check 2");

	}
	return self;
}

- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation
{
	[self.delegate locationUpdate:newLocation];
}

- (void)locationManager:(CLLocationManager *)manager
	   didFailWithError:(NSError *)error
{
	[self.delegate locationError:error];
}
/*
-(void) webView:(UIWebView *) webViewDidStartLoad:(UIWebView*) webView {
	NSLog(@"webViewDidStartLoad");
}

-(void) webViewDidFinishLoad:(UIWebView*) webView {
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	NSLog(@"webViewDidFinishLoad");
	[appDelegate.notifier dismissWithClickedButtonIndex:0 animated:YES];\
}
*/
- (void)dealloc {
	[self.locationManager release];
    [super dealloc];
}

@end
