//
//  SignupViewController.h
//  TrafficViewer
//
//  Created by Sam Fernald on 8/29/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface SignupViewController : UIViewController {
    IBOutlet UIButton *signup;
    IBOutlet UIButton *cancel;
    IBOutlet UIButton *whyWeAsk;
    IBOutlet UITextField *email;
    IBOutlet UITextField *password;
    IBOutlet UITextField *passwordAgain;
    IBOutlet UITextField *phoneNumber;
    
    AppDelegate *appDelegate;
    NSMutableData * receivedData;
}

@end
