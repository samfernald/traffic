//
//  CommuteCreationViewController.m
//  TrafficViewer
//
//  Created by Sam Fernald on 8/31/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import "CommuteCreationViewController.h"
#import "CJSONSerializer.h"
#import "CJSONDeserializer.h"
#import "ProfileCell.h"


@interface CommuteCreationViewController ()

@end

@implementation CommuteCreationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    commuteParts = [[NSMutableArray alloc] init];
    addParts = [[NSMutableArray alloc] init];
    isEditingSegments = NO;
    isAdding = NO;
    
    [actionSheetType dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetType=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarType = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarType sizeToFit];
    pickerToolbarType.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsType = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnType = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedType:)];
    [barItemsType addObject:doneBtnType];
    [doneBtnType release];
    [pickerToolbarType setItems:barItemsType animated:YES];
    [actionSheetType addSubview:pickerToolbarType];
    [barItemsType release];
    [pickerToolbarType release];
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    typePicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    typePicker.showsSelectionIndicator = YES;
    typePicker.dataSource = self;
    typePicker.delegate = self;
    typePicker.tag = 0;
    typePicker.showsSelectionIndicator = YES;
    [actionSheetType addSubview:typePicker];
    
    //--------------Highway ACTION SHEET
    [actionSheetHighway dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetHighway=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarHighway = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarHighway sizeToFit];
    pickerToolbarHighway.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsHighway = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnHighway = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedHighway:)];
    [barItemsHighway addObject:doneBtnHighway];
    [doneBtnHighway release];
    [pickerToolbarHighway setItems:barItemsHighway animated:YES];
    [actionSheetHighway addSubview:pickerToolbarHighway];
    [barItemsHighway release];
    [pickerToolbarHighway release];
    highwayPicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    highwayPicker.showsSelectionIndicator = YES;
    highwayPicker.dataSource = self;
    highwayPicker.delegate = self;
    highwayPicker.tag = 1;
    highwayPicker.showsSelectionIndicator = YES;
    [actionSheetHighway addSubview:highwayPicker];
    
    //--------------onRamp ACTION SHEET
    [actionSheetOnRamp dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetOnRamp = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarOnRamp = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarOnRamp sizeToFit];
    pickerToolbarOnRamp.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsOnRamp = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnOnRamp = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedOnRamp:)];
    [barItemsOnRamp addObject:doneBtnOnRamp];
    [doneBtnOnRamp release];
    [pickerToolbarOnRamp setItems:barItemsOnRamp animated:YES];
    [actionSheetOnRamp addSubview:pickerToolbarOnRamp];
    [barItemsOnRamp release];
    [pickerToolbarOnRamp release];
    onRampPicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    onRampPicker.showsSelectionIndicator = YES;
    onRampPicker.dataSource = self;
    onRampPicker.delegate = self;
    onRampPicker.tag = 2;
    onRampPicker.showsSelectionIndicator = YES;
    [actionSheetOnRamp addSubview:onRampPicker];
    
    //--------------offRamp ACTION SHEET
    [actionSheetOffRamp dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetOffRamp=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarOffRamp = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarOffRamp sizeToFit];
    pickerToolbarOffRamp.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsOffRamp = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnOffRamp = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedOffRamp:)];
    [barItemsOffRamp addObject:doneBtnOffRamp];
    [doneBtnOffRamp release];
    [pickerToolbarOffRamp setItems:barItemsOffRamp animated:YES];
    [actionSheetOffRamp addSubview:pickerToolbarOffRamp];
    [barItemsOffRamp release];
    [pickerToolbarOffRamp release];
    offRampPicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    offRampPicker.showsSelectionIndicator = YES;
    offRampPicker.dataSource = self;
    offRampPicker.delegate = self;
    offRampPicker.tag = 3;
    offRampPicker.showsSelectionIndicator = YES;
    [actionSheetOffRamp addSubview:offRampPicker];
    
    typeList = [[NSMutableArray alloc] init];
    highwayList = [[NSMutableArray alloc] init];
    exitOnList = [[NSMutableArray alloc] init];
    exitOffList = [[NSMutableArray alloc] init];
    
    [typeList addObject:[[ProfileCell alloc] initWithDisplay:@"Morning" type:0 connection:NULL]];
    [typeList addObject:[[ProfileCell alloc] initWithDisplay:@"Evening" type:1 connection:NULL]];
    [typeList addObject:[[ProfileCell alloc] initWithDisplay:@"Alternate Morning" type:2 connection:NULL]];
    [typeList addObject:[[ProfileCell alloc] initWithDisplay:@"Alternate Evening" type:3 connection:NULL]];
    
    if(appDelegate.selectedCity == 1){
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"5 North" type:1 connection:@"5n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"5 South" type:2 connection:@"5s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"8 East" type:3 connection:@"8e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"8 West" type:4 connection:@"8w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"15 North" type:5 connection:@"15n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"15 South" type:6 connection:@"15s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"52 East" type:7 connection:@"52e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"52 West" type:8 connection:@"52w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"54 East" type:9 connection:@"54e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"54 West" type:10 connection:@"54w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"56 East" type:11 connection:@"56e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"56 West" type:12 connection:@"56w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"78 East" type:13 connection:@"78e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"78 West" type:14 connection:@"78w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"94 East" type:15 connection:@"94e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"94 West" type:16 connection:@"94w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"125 North" type:17 connection:@"125n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"125 South" type:18 connection:@"125s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"163 North" type:19 connection:@"163n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"163 South" type:20 connection:@"163s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"805 North" type:21 connection:@"805n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"805 South" type:22 connection:@"805s"]];
        
    } else if(appDelegate.selectedCity == 2){
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"2 North" type:1 connection:@"2n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"2 South" type:2 connection:@"2s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"5 North" type:3 connection:@"5n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"5 South" type:4 connection:@"5s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"10 East" type:5 connection:@"10e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"10 West" type:6 connection:@"10w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"14 North" type:7 connection:@"14n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"14 South" type:8 connection:@"14s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"15 North" type:49 connection:@"15n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"15 South" type:50 connection:@"15s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"22 East" type:9 connection:@"22e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"22 West" type:10 connection:@"22w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"55 North" type:13 connection:@"55n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"55 South" type:14 connection:@"55s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"57 North" type:15 connection:@"57n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"57 South" type:16 connection:@"57s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"60 East" type:17 connection:@"60e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"60 West" type:18 connection:@"60w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"71 North" type:51 connection:@"71n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"71 South" type:52 connection:@"71s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"73 North" type:19 connection:@"73n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"73 South" type:20 connection:@"73s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"91 East" type:21 connection:@"91e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"91 West" type:22 connection:@"91w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"101 North" type:23 connection:@"101n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"101 South" type:24 connection:@"101s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"105 East" type:25 connection:@"105e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"105 West" type:26 connection:@"105w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"110 North" type:27 connection:@"110n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"110 South" type:28 connection:@"110s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"118 East" type:29 connection:@"118e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"118 West" type:30 connection:@"118w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"133 North" type:31 connection:@"133n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"133 South" type:32 connection:@"133s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"134 East" type:33 connection:@"134e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"134 West" type:34 connection:@"134w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"170 North" type:35 connection:@"170n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"170 South" type:36 connection:@"170s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"210 East" type:37 connection:@"210e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"210 West" type:38 connection:@"210w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"215 North" type:53 connection:@"215n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"215 South" type:54 connection:@"215s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"241 North" type:39 connection:@"241n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"241 South" type:40 connection:@"241s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"261 North" type:41 connection:@"261n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"261 South" type:42 connection:@"261s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"405 North" type:43 connection:@"405n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"405 South" type:44 connection:@"405s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"605 North" type:45 connection:@"605n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"605 South" type:46 connection:@"605s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"710 North" type:47 connection:@"710n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"710 South" type:48 connection:@"710s"]];
        
    } else if(appDelegate.selectedCity == 3){
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"1 North" type:41 connection:@"1n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"1 South" type:42 connection:@"1s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"4 East"  type:1 connection:@"4e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"4 West"  type:2 connection:@"4w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"17 North" type:5 connection:@"17n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"17 South" type:6 connection:@"17s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"24 East" type:7 connection:@"24e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"24 West" type:8 connection:@"24w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"37 East" type:9 connection:@"37e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"37 West" type:10 connection:@"37w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"80 East" type:11 connection:@"80e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"80 West" type:12 connection:@"80w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"84 East" type:13 connection:@"84e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"84 West" type:14 connection:@"84w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"85 North" type:15 connection:@"85n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"85 South" type:16 connection:@"85s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"87 North" type:45 connection:@"87n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"87 South" type:46 connection:@"87s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"92 East" type:17 connection:@"92e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"92 West" type:18 connection:@"92w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"101 North" type:19 connection:@"101n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"101 South" type:20 connection:@"101s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"205 East" type:43 connection:@"205e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"205 West" type:44 connection:@"205w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"237 East" type:21 connection:@"237e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"237 West" type:22 connection:@"237w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"238 North" type:23 connection:@"238n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"238 South" type:24 connection:@"238s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"242 North" type:25 connection:@"242n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"242 South" type:26 connection:@"242s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"280 North" type:27 connection:@"280n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"280 South" type:28 connection:@"280s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"505 North" type:47 connection:@"505n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"505 South" type:48 connection:@"505s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"580 East" type:31 connection:@"580e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"580 West" type:32 connection:@"580w"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"680 North" type:33 connection:@"680n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"680 South" type:34 connection:@"680s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"880 North" type:37 connection:@"880n"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"880 South" type:38 connection:@"880s"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"980 East" type:39 connection:@"980e"]];
        [highwayList addObject:[[ProfileCell alloc] initWithDisplay:@"980 West" type:40 connection:@"980w"]];
    }
    NSString* filePath;
    if(appDelegate.selectedCity == 1) {
        filePath = [[NSBundle mainBundle] pathForResource:@"SanDiegoMap" ofType:@"csv"];
    } else if(appDelegate.selectedCity == 2){
        filePath = [[NSBundle mainBundle] pathForResource:@"LAMap" ofType:@"csv"];
    } else {
        filePath = [[NSBundle mainBundle] pathForResource:@"BayAreaMap" ofType:@"csv"];
    }
    NSString* fileContents = [NSString stringWithContentsOfFile:filePath];
    NSArray* exitStrings = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    NSLog(@"exits = %d",exitStrings.count);
    exits = [[NSMutableArray alloc] initWithCapacity:exitStrings.count];
    
    
    for(int idx = 0; idx < exitStrings.count; idx++) {
        NSString* currentExit = [exitStrings objectAtIndex:idx];
        NSArray* exitData = [currentExit componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
        [exits addObject:[[ExitListing alloc] initWithName:[exitData objectAtIndex:0] exitId:[[exitData objectAtIndex:1] intValue] highwayId:[[exitData objectAtIndex:3] intValue]]];
    }
    
    
    @try{
        
        NSLog(@"user id = %d",appDelegate.user.userId);
        if(appDelegate.user.userId <= 0) {
            connectionType = 1;
            NSString *URL;
            if(appDelegate.selectedCity == 1){
                URL = [NSString stringWithFormat: @"http://%@/sd/servlet/useridservlet?email=%@",appDelegate.cpuUrl,appDelegate.userName];;
            } else if(appDelegate.selectedCity == 2){
                URL = [NSString stringWithFormat: @"http://%@/la/servlet/useridservlet?email=%@",appDelegate.cpuUrl,appDelegate.userName];
            } else {
                URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/useridservlet?email=%@",appDelegate.cpuUrl,appDelegate.userName];
            }
            NSString *post = @"";
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
            //NSString *post = [NSString ];
            //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:URL]];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
            
            NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSArray *cookies = [scs cookies];
            NSEnumerator* e = [cookies objectEnumerator];
            NSHTTPCookie* cookie;
            while ((cookie = [e nextObject])) {
                NSString* name = [cookie name];
                if ([name isEqualToString:@"userId"]) {
                    [scs deleteCookie:cookie];
                }
            }
            
            conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (conn)
            {
                NSLog(@"received Data");
                receivedIDData = [[NSMutableData data] retain];
            } else {
                NSLog(@"no conn");
            }

        } else {
            connectionType = 2;
            appDelegate.userID = appDelegate.user.userId;
            NSString *URL;
            if(appDelegate.selectedCity == 1){
                URL = [NSString stringWithFormat: @"http://%@/sd/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.user.userId];
            } else if(appDelegate.selectedCity == 2){
                URL = [NSString stringWithFormat: @"http://%@/la/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.user.userId];
            } else {
                URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.user.userId];
            }
            NSString *post = @""; 
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];  
        
            NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];	
            //NSString *post = [NSString ]; 
            //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];  
            NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
            [request setURL:[NSURL URLWithString:URL]];
            [request setHTTPMethod:@"POST"];  
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];  
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];  
            [request setHTTPBody:postData];
            
            //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
        
            NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSArray *cookies = [scs cookies];
            NSEnumerator* e = [cookies objectEnumerator];
            NSHTTPCookie* cookie;
            while ((cookie = [e nextObject])) {
                NSString* name = [cookie name];
                if ([name isEqualToString:@"userId"]) {
                    [scs deleteCookie:cookie];
                }
            }
        
            conn=[[NSURLConnection alloc] initWithRequest:request delegate:self]; 
            if (conn)   
            {  
                NSLog(@"received Data");
                receivedData = [[NSMutableData data] retain];
            } else {  
                NSLog(@"no conn");
            }
        }
    } @catch (NSException *e) {
        NSLog(@"Failed to Login");
    }

    //Do any additional setup after loading the view from its nib.
    [super viewDidLoad];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSLog(@"append text data");
    if(connectionType == 1) {
        [receivedIDData appendData:data];
    } else if(connectionType == 2){
        [receivedData appendData:data];
    } else if(connectionType == 5){
        [receivedNewCommuteData appendData:data];
    }
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"conn failed with error: %@", error);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    if(connectionType == 1){
        
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedIDData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedIDData encoding:NSASCIIStringEncoding];
        NSLog(@"returned data = %@",outputString);
        appDelegate.userID = [outputString intValue];
        connectionType = 2;
        NSString *URL;
        if(appDelegate.selectedCity == 1){
            URL = [NSString stringWithFormat: @"http://%@/sd/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.userID];
        } else if(appDelegate.selectedCity == 2){
            URL = [NSString stringWithFormat: @"http://%@/la/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.userID];
        } else {
            URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.userID];
        }

        NSString *post = @"";
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        //NSString *post = [NSString ];
        //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:URL]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
        
        NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray *cookies = [scs cookies];
        NSEnumerator* e = [cookies objectEnumerator];
        NSHTTPCookie* cookie;
        while ((cookie = [e nextObject])) {
            NSString* name = [cookie name];
            if ([name isEqualToString:@"userId"]) {
                [scs deleteCookie:cookie];
            }
        }
        
        conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (conn)
        {
            NSLog(@"received Data");
            receivedData = [[NSMutableData data] retain];
        } else {
            NSLog(@"no conn");
        }

    } else if(connectionType == 2){
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
        //NSLog(@"returned data = %@",outputString);
    
        NSDictionary *returnedActivityInfo = [[CJSONDeserializer alloc] deserializeAsDictionary:receivedData error:&error];
        // Set up the edit and add buttons.
        NSLog(@"segs = %@", [returnedActivityInfo valueForKey:@"segs"]);
        NSMutableArray *segments = [returnedActivityInfo valueForKey:@"segs"];
        numMorning = numAltMorning = numEvening = numAltEvening = 0;
        @try{
            [commuteParts addObject:[[ProfileCell alloc] initWithDisplay:@"My Segments" type:0 connection:NULL]];
            segmentList = [[NSMutableArray alloc] init];
            for(int i = 0; i < [segments count]; i++) {
             
                [segmentList addObject:[[SegmentProfile alloc] initWithID:[[[segments objectAtIndex:i] valueForKey:@"segmentID"] intValue] onRamp:[[[segments objectAtIndex:i] valueForKey:@"onRamp"]intValue] offRamp:[[[segments objectAtIndex:i] valueForKey:@"offRamp"]intValue] commuteType:[[[segments objectAtIndex:i] valueForKey:@"commuteType"]intValue] sequence:[[[segments objectAtIndex:i] valueForKey:@"sequence"]intValue] highway:[[segments objectAtIndex:i] valueForKey:@"highway"] onRampName:[[segments objectAtIndex:i] valueForKey:@"onRampName"] offRampName:[[segments objectAtIndex:i] valueForKey:@"offRampName"]]];
                if(i == 0){
                    if([[segmentList objectAtIndex:i] commuteType] == 0){
                        [commuteParts addObject:[[ProfileCell alloc] initWithDisplay:@"Morning" type:4 connection:NULL]];
                    } else if([[segmentList objectAtIndex:i] commuteType] == 1){
                        [commuteParts addObject:[[ProfileCell alloc] initWithDisplay:@"Evening" type:4 connection:NULL]];
                    } else if([[segmentList objectAtIndex:i] commuteType] == 2){
                        [commuteParts addObject:[[ProfileCell alloc] initWithDisplay:@"Alternate Morning" type:4 connection:NULL]];
                    } else if([[segmentList objectAtIndex:i] commuteType] == 3){
                        [commuteParts addObject:[[ProfileCell alloc] initWithDisplay:@"Alternate Evening" type:4 connection:NULL]];
                    }
                } else if (i>=1){
                    if([[segmentList objectAtIndex:i-1] commuteType] < [[segmentList objectAtIndex:i] commuteType]){
                        if([[segmentList objectAtIndex:i] commuteType] == 1){
                            [commuteParts addObject:[[ProfileCell alloc] initWithDisplay:@"Evening" type:4 connection:NULL]];
                        } else if([[segmentList objectAtIndex:i] commuteType] == 2){
                            [commuteParts addObject:[[ProfileCell alloc] initWithDisplay:@"Alternate Morning" type:4 connection:NULL]];
                        } else if([[segmentList objectAtIndex:i] commuteType] == 3){
                            [commuteParts addObject:[[ProfileCell alloc] initWithDisplay:@"Alternate Evening" type:4 connection:NULL]];
                        }
                    }
                }
                if([[segmentList objectAtIndex:i] commuteType] == 0){
                    numMorning++;
                } else if([[segmentList objectAtIndex:i] commuteType] == 1){
                    numEvening++;
                } else if([[segmentList objectAtIndex:i] commuteType] == 2){
                    numAltMorning++;
                } else if([[segmentList objectAtIndex:i] commuteType] == 3){
                    numAltEvening++;
                }
                
                [commuteParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"%@: From %@ to \n %@",[[segmentList objectAtIndex:i] highway], [[segmentList objectAtIndex:i] onRampName], [[segmentList objectAtIndex:i] offRampName]] type:1 connection:[segmentList objectAtIndex:i]]];
            }

        } @catch(NSException *e) {
        
        }
        
        [commuteProfile reloadData];
    } else if(connectionType == 3){
        //delete
        for(int i = 0; i < commuteParts.count; i++){
            if([[[commuteParts objectAtIndex:i] container] segmentID] == selectedSegmentID){
                NSLog(@"deleting segment");
                if([[[commuteParts objectAtIndex:i] container] commuteType] == 0){
                    numMorning--;
                    if(numMorning == 0){
                        [commuteParts removeObjectAtIndex:i];
                        [commuteParts removeObjectAtIndex:i-1];
                    } else {
                        [commuteParts removeObjectAtIndex:i];
                    }
                } else if([[[commuteParts objectAtIndex:i] container] commuteType] == 1){
                    numEvening--;
                    if(numEvening == 0){
                        [commuteParts removeObjectAtIndex:i];
                        [commuteParts removeObjectAtIndex:i-1];
                    }else {
                        [commuteParts removeObjectAtIndex:i];
                    }
                } else if([[[commuteParts objectAtIndex:i] container] commuteType] == 2){
                    numAltMorning--;
                    if(numAltMorning == 0){
                        [commuteParts removeObjectAtIndex:i];
                        [commuteParts removeObjectAtIndex:i-1];
                    } else {
                        [commuteParts removeObjectAtIndex:i];
                    }
                } else if([[[commuteParts objectAtIndex:i] container] commuteType] == 3){
                    numAltEvening--;
                    if(numAltEvening == 0){
                        [commuteParts removeObjectAtIndex:i];
                        [commuteParts removeObjectAtIndex:i-1];
                    } else {
                        [commuteParts removeObjectAtIndex:i];
                    }
                }
                
            }
        }
        isEditingSegments = NO;
        [self removePartIn:commuteParts forType:10];
        [commuteProfile reloadData];
        [commuteProfile reloadInputViews];
    } else if(connectionType == 4){
        
    } else if(connectionType == 5){
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedNewCommuteData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedNewCommuteData encoding:NSASCIIStringEncoding];
        NSLog(@"returned data = %@",outputString);
        NSDictionary *returnedActivityInfo = [[CJSONDeserializer alloc] deserializeAsDictionary:receivedNewCommuteData error:&error];
        // Set up the edit and add buttons.
        int insertIndex = 1;
        if([commuteParts count] == 1){
            if(commuteType == 0){
                [commuteParts insertObject:[[ProfileCell alloc] initWithDisplay:@"Morning" type:4 connection:NULL] atIndex:insertIndex];
            } else if(commuteType == 1){
                [commuteParts insertObject:[[ProfileCell alloc] initWithDisplay:@"Evening" type:4 connection:NULL] atIndex:insertIndex];
            } else if(commuteType == 2){
                [commuteParts insertObject:[[ProfileCell alloc] initWithDisplay:@"Alternate Morning" type:4 connection:NULL] atIndex:insertIndex];
            } else if(commuteType == 3){
                [commuteParts insertObject:[[ProfileCell alloc] initWithDisplay:@"Alternate Evening" type:4 connection:NULL]atIndex:insertIndex];
            }
            insertIndex++;
        } else {
            for(int i=1; i< [commuteParts count]; i++){
                NSLog(@"INSERT INDEX = %d Commute type = %d compare = %d sequence = %d compare = %d",i, [[[commuteParts objectAtIndex:i] container] commuteType], commuteType, [[[commuteParts objectAtIndex:i] container] sequence] , seqToSend);
                if([[commuteParts objectAtIndex:i] displayType] == 4){
                    
                } else {
                    if([[[commuteParts objectAtIndex:i] container] commuteType] < commuteType && i != [commuteParts count]-1){
                        
                    } else if([[[commuteParts objectAtIndex:i] container] commuteType] == commuteType || seqToSend == 1){
                        if(seqToSend == 1) {
                            NSLog(@"FOUND INSERT INDEX A");
                            insertIndex = i+1;
                            if(commuteType == 0){
                                [commuteParts insertObject:[[ProfileCell alloc] initWithDisplay:@"Morning" type:4 connection:NULL] atIndex:insertIndex];
                                numMorning++;
                            } else if(commuteType == 1){
                                [commuteParts insertObject:[[ProfileCell alloc] initWithDisplay:@"Evening" type:4 connection:NULL] atIndex:insertIndex];
                                numEvening++;
                            } else if(commuteType == 2){
                                [commuteParts insertObject:[[ProfileCell alloc] initWithDisplay:@"Alternate Morning" type:4 connection:NULL] atIndex:insertIndex];
                                numAltMorning++;
                            } else if(commuteType == 3){
                                [commuteParts insertObject:[[ProfileCell alloc] initWithDisplay:@"Alternate Evening" type:4 connection:NULL]atIndex:insertIndex];
                                numAltEvening++;
                            }
                            insertIndex++;
                            i = [commuteParts count];
                        } else if([[[commuteParts objectAtIndex:i] container] sequence] < seqToSend-1){
                            
                        } else {
                            if(commuteType == 0){
                                numMorning++;
                            } else if(commuteType == 1){
                                numEvening++;
                            } else if(commuteType == 2){
                                numAltMorning++;
                            } else if(commuteType == 3){
                                numAltEvening++;
                            }
                            insertIndex = i+1;
                            NSLog(@"FOUND INSERT INDEX B");
                        }
                    }
                }
            }
        }
        
        [segmentList addObject:[[SegmentProfile alloc] initWithID:[[returnedActivityInfo valueForKey:@"id"]intValue] onRamp:onRampID offRamp:offRampID commuteType:commuteType sequence:seqToSend highway:[[addParts objectAtIndex:1] displayText] onRampName:[[addParts objectAtIndex:2] displayText] offRampName:[[addParts objectAtIndex:3] displayText]]];
        int segmentIndex = [segmentList count]-1;
        
        [commuteParts insertObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"%@: From %@ to \n %@",[[segmentList objectAtIndex:segmentIndex] highway], [[segmentList objectAtIndex:segmentIndex] onRampName], [[segmentList objectAtIndex:segmentIndex] offRampName]] type:1 connection:[segmentList objectAtIndex:segmentIndex]] atIndex:insertIndex];
        isAdding = FALSE;
        [addParts removeAllObjects];
        [onRampPicker selectRow:0 inComponent:0 animated:NO];
        [offRampPicker selectRow:0 inComponent:0 animated:NO];
        [commuteProfile reloadData];
        [commuteProfile reloadInputViews];
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        return [addParts count];
    } else if(section == 1){
        return [commuteParts count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    // Configure the cell.
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cell.contentView subviews]){
        for (UIView *subview in [cell.contentView subviews]) {
            [subview removeFromSuperview];
        }
    }
     
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
    if(indexPath.section == 0){
        cell.textLabel.numberOfLines = 1;
        cell.textLabel.font = [UIFont systemFontOfSize:17.0];
        cell.textLabel.text = [[addParts objectAtIndex:indexPath.row] displayText];
        if([[addParts objectAtIndex:indexPath.row] displayType] == 2){
            if(typeSelected) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 3){
            if(hwySelected) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 4){
            if(onRampSelected) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 5){
            if(offRampSelected) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        }
        
    } else if(indexPath.section == 1){
        if([[commuteParts objectAtIndex:indexPath.row] displayType] == 0){
            cell.textLabel.numberOfLines = 1;
            cell.textLabel.font = [UIFont systemFontOfSize:20.0];
            [cell.contentView setBackgroundColor: [UIColor colorWithRed:.96 green:.76 blue:.09 alpha:1]];
        } else if([[commuteParts objectAtIndex:indexPath.row] displayType] == 1){
            cell.textLabel.numberOfLines = 2;
            cell.textLabel.font = [UIFont systemFontOfSize:13.0];
            //[cell.contentView setBackgroundColor: [UIColor colorWithRed:.65 green:.9 blue:1.0 alpha:1]];
        } else if([[commuteParts objectAtIndex:indexPath.row] displayType] == 4){
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.numberOfLines = 1;
            cell.textLabel.font = [UIFont systemFontOfSize:18.0];
            [cell.contentView setBackgroundColor: [UIColor colorWithRed:1 green:1 blue:204.0/255.0 alpha:1]];
        } else if([[commuteParts objectAtIndex:indexPath.row] displayType] == 10){
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIButton *editButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
            editButton.backgroundColor = [UIColor clearColor];
            editButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
            [editButton setTitle:@"Edit" forState:UIControlStateNormal];
            [editButton addTarget:self action:@selector(editSegment) forControlEvents:UIControlEventTouchUpInside];
            // Work out required size
            CGRect buttonFrame = CGRectMake(40, 5, 80, 35);
            [editButton setFrame:buttonFrame];
            
            [cell.contentView addSubview:editButton];
            
            UIButton *deleteButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
            deleteButton.backgroundColor = [UIColor clearColor];
            deleteButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
            [deleteButton setTitle:@"Delete" forState:UIControlStateNormal];
            [deleteButton addTarget:self action:@selector(deleteSegment) forControlEvents:UIControlEventTouchUpInside];
            // Work out required size
            CGRect deleteButtonFrame = CGRectMake(160, 5, 80, 35);
            [deleteButton setFrame:deleteButtonFrame];
            
            [cell.contentView addSubview:deleteButton];
        }
        cell.textLabel.text = [[commuteParts objectAtIndex:indexPath.row] displayText];
  
    }        
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        if([[addParts objectAtIndex:indexPath.row] displayType] == 1){
            [self selectType];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 2){
            [self selectHighway];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 3){
            [self selectOnRamp];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 4){
            [self selectOffRamp];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 5){
            //isAdding = FALSE;
            //[addParts removeAllObjects];
            [self addSegment];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 6){
        
        }
        
    } else if(indexPath.section == 1){
        if([[commuteParts objectAtIndex:indexPath.row] displayType] == 0){
            if(!isAdding){
                isAdding = TRUE;
                typeSelected = FALSE;
                hwySelected = FALSE;
                onRampSelected = FALSE;
                offRampSelected = FALSE;
            
                [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Select Type" type:1 connection:NULL]];
                [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Select Highway" type:2 connection:NULL]];
                [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Select OnRamp" type:3 connection:NULL]];
                [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Select OffRamp" type:4 connection:NULL]];
                [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Add Segment" type:5 connection:NULL]];
                [commuteProfile reloadData];
                [commuteProfile reloadInputViews];
            }
        } else if([[commuteParts objectAtIndex:indexPath.row] displayType] == 1){
            if(!isEditingSegments){
                isEditingSegments = YES;
                editsDisplayed = NO;
                isAdding = YES;
                selectedSegmentID = [[[commuteParts objectAtIndex:indexPath.row] container] segmentID];
                selectedSegment = [[commuteParts objectAtIndex:indexPath.row] container];
                if([commuteParts count] == indexPath.row-1){
                    [commuteParts addObject:[[ProfileCell alloc] initWithDisplay:@"" type:10 connection:NULL]];
                } else {
                    [commuteParts insertObject:[[ProfileCell alloc] initWithDisplay:@"" type:10 connection:NULL] atIndex:indexPath.row+1];
                }
            } else {
                isAdding = FALSE;
                isEditingSegments = NO;
                [self removePartIn:commuteParts forType:10];
            }
            [commuteProfile reloadData];
            [commuteProfile reloadInputViews]; 
        }
        
    }
}

- (void) removePartIn:(NSMutableArray*)parts forType:(int) t{
    for(int i = 0; i < [parts count]; i++){
        if([[parts objectAtIndex:i] displayType] == t){
            [parts removeObjectAtIndex:i];
        }
    }
}

/*
- (void) segmentAction:(id)sender {
    [commuteProfile reloadData];
}
 */

-(void)addSegment {
    connectionType = 5;
    NSString *URL;
    seqToSend = 1;
    NSLog(@"SEGS: %d %d %d %d",numMorning, numEvening, numAltMorning, numAltEvening);
    if(commuteType == 0){
        seqToSend = numMorning;
    } else if(commuteType == 1){
        seqToSend = numEvening;
    } else if(commuteType == 2){
        seqToSend = numAltMorning;
    } else if(commuteType == 3){
        seqToSend = numAltEvening;
    }
    seqToSend++;
    if(appDelegate.selectedCity == 1){
        URL = [NSString stringWithFormat: @"http://%@/sd/servlet/segmentservlet?type=%d&user=%d&onramp=%d&offramp=%d&ctype=%d&seq=%d",appDelegate.cpuUrl, 1, appDelegate.userID, onRampID, offRampID, commuteType, seqToSend];
        NSLog(@"URL = %@",URL);
    } else if(appDelegate.selectedCity == 2){
        URL = [NSString stringWithFormat: @"http://%@/la/servlet/segmentservlet?type=%d&user=%d&onramp=%d&offramp=%d&ctype=%d&seq=%d",appDelegate.cpuUrl, 1, appDelegate.userID, onRampID, offRampID, commuteType, seqToSend];
    } else {
        URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/segmentservlet?type=%d&user=%d&onramp=%d&offramp=%d&ctype=%d&seq=%d",appDelegate.cpuUrl, 1, appDelegate.userID, onRampID, offRampID, commuteType, seqToSend];
    }
        
        
        
    NSError *error = nil;
    @try{
            
            
        NSString *post = @"";
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:URL]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray *cookies = [scs cookies];
        NSEnumerator* e = [cookies objectEnumerator];
        NSHTTPCookie* cookie;
        while ((cookie = [e nextObject])) {
            NSString* name = [cookie name];
            if ([name isEqualToString:@"userId"]) {
                [scs deleteCookie:cookie];
            }
        }
        
        conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (conn)
        {
            NSLog(@"received Data");
            receivedNewCommuteData = [[NSMutableData data] retain];
        }
        else
        {
            NSLog(@"no conn");
        }
    } @catch (NSException *e) {
        NSLog(@"Failed to Login");
    }
}

-(void) editSegment {
    if(!editsDisplayed){
        commuteTypeSet = TRUE;
        editsDisplayed = TRUE;
        connectionType = 4;
        int rowSelection = -1;
        if(commuteType == 0) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@"Morning" type:0 connection:NULL]];
        } else if(commuteType == 1) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@"Evening" type:1 connection:NULL]];
        } else if(commuteType == 2) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@"Alternate Morning" type:2 connection:NULL]];
        } else if(commuteType == 3) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@"Alternate Evening" type:3 connection:NULL]];
        }
        
        for(int i = 0; i< [typeList count]; i++){
            if(commuteType == [[typeList objectAtIndex:i] displayType]){
                rowSelection = i;
            }
        }
        [typePicker selectRow:rowSelection inComponent:0 animated:NO];
        typeSelected = TRUE;
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:[[typeList objectAtIndex:rowSelection] displayText] type:1 connection:NULL]];
        
        for(int i = 0; i < [highwayList count]; i++){
            if([[[highwayList objectAtIndex:i] container] isEqualToString: [selectedSegment highway]]){
                rowSelection = i;
            }
        }
        [highwayPicker selectRow:rowSelection inComponent:0 animated:NO];
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:[[highwayList objectAtIndex:rowSelection] displayText] type:2 connection:NULL]];
        hwySelected = TRUE;
        int highwayforRamps = [[highwayList objectAtIndex:rowSelection] displayType];
        [self setOnRamps:highwayforRamps];
        
        for(int i = 0; i < [exitOnList count]; i++){
            if([[exitOnList objectAtIndex:i] displayType] == [selectedSegment onRamp]){
                rowSelection = i;
            }
        }
        [onRampPicker selectRow:rowSelection inComponent:0 animated:NO];
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:[[exitOnList objectAtIndex:rowSelection] displayText] type:3 connection:NULL]];
        onRampID = [[exitOnList objectAtIndex:[onRampPicker selectedRowInComponent:0]] displayType];
        onRampSelected = TRUE;
        [self setOffRamps:highwayforRamps onRampSEQ:rowSelection];
        
        for(int i = 0; i < [exitOffList count]; i++){
            if([[exitOffList objectAtIndex:i] displayType] == [selectedSegment offRamp]){
                rowSelection = i;
            }
        }
        [offRampPicker selectRow:rowSelection inComponent:0 animated:NO];
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:[[exitOffList objectAtIndex:rowSelection] displayText] type:4 connection:NULL]];
        offRampID = [[exitOffList objectAtIndex:[offRampPicker selectedRowInComponent:0]] displayType];
        offRampSelected = TRUE;
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Submit Edit" type:6 connection:NULL]];

        
        [commuteProfile reloadData];
        [commuteProfile reloadInputViews];
    }
}

-(void) deleteSegment {
    connectionType = 3;
 
    NSString *URL;
    if(appDelegate.selectedCity == 1){
        URL = [NSString stringWithFormat: @"http://%@/sd/servlet/segmentservlet?user=%d&type=%d&segid=%d",appDelegate.cpuUrl,appDelegate.userID,3,selectedSegmentID];
    } else if(appDelegate.selectedCity == 2){
        URL = [NSString stringWithFormat: @"http://%@/la/servlet/segmentservlet?user=%d&type=%d&segid=%d",appDelegate.cpuUrl,appDelegate.userID,3,selectedSegmentID];
    } else {
        URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/segmentservlet?user=%d&type=%d&segid=%d",appDelegate.cpuUrl,appDelegate.userID,3,selectedSegmentID];
    }
    
    NSString *post = @"";
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    //NSString *post = [NSString ];
    //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:URL]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
    
    NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *cookies = [scs cookies];
    NSEnumerator* e = [cookies objectEnumerator];
    NSHTTPCookie* cookie;
    while ((cookie = [e nextObject])) {
        NSString* name = [cookie name];
        if ([name isEqualToString:@"userId"]) {
            [scs deleteCookie:cookie];
        }
    }
    
    conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (conn)
    {
        NSLog(@"received Data");
        receivedData = [[NSMutableData data] retain];
    } else {
        NSLog(@"no conn");
    }

}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    int tag = pickerView.tag;
    if(tag == 0) {
        return [typeList count];
    } else if(tag == 1) {
        return [highwayList count];
    } else if(tag == 2) {
        return [exitOnList count]-1;
    } else if(tag == 3) {
        return [exitOffList count];
    }
    return 0;
}

-(NSString *)pickerView:(UIPickerView *)pickerView
            titleForRow:(NSInteger)row
           forComponent:(NSInteger)component
{
    int tag = pickerView.tag;
    if(tag == 0) {
        return [[typeList objectAtIndex:row] displayText];
    } else if(tag == 1) {
        return [[highwayList objectAtIndex:row] displayText];
    } else if(tag == 2){
        return [[exitOnList objectAtIndex:row] displayText];
    } else if(tag == 3){
        return [[exitOffList objectAtIndex:row] displayText];
    }
    return 0;
}

-(void) selectType {
    [actionSheetType showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetType setBounds:CGRectMake(0, 0, 320, 485)];
}

-(void) selectHighway {
    [actionSheetHighway showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetHighway setBounds:CGRectMake(0, 0, 320, 485)];
}

-(void) selectOnRamp {
    [actionSheetOnRamp showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetOnRamp setBounds:CGRectMake(0, 0, 320, 485)];
}

-(void) selectOffRamp {
    [actionSheetOffRamp showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetOffRamp setBounds:CGRectMake(0, 0, 320, 485)];
}

-(void) selectedType:(id)sender {
    [actionSheetType dismissWithClickedButtonIndex:0 animated:YES];
    typeSelected = TRUE;
    //set cell display
    [[addParts objectAtIndex:0] setDisplayText:[[typeList objectAtIndex:[typePicker selectedRowInComponent:0]] displayText]];
    commuteType = [[typeList objectAtIndex:[typePicker selectedRowInComponent:0]] displayType];
    [commuteProfile reloadData];
    [commuteProfile reloadInputViews];
}

-(void) selectedHighway:(id)sender{
    [actionSheetHighway dismissWithClickedButtonIndex:0 animated:YES];
    hwySelected = TRUE;
    onRampSelected = FALSE;
    offRampSelected = FALSE;
    [[addParts objectAtIndex:2] setDisplayText:@"Select OnRamp"];
    [[addParts objectAtIndex:3] setDisplayText:@"Select OffRamp"];
    [[addParts objectAtIndex:1] setDisplayText:[[highwayList objectAtIndex:[highwayPicker selectedRowInComponent:0]] displayText]];
    highway = [[highwayList objectAtIndex:[highwayPicker selectedRowInComponent:0]] displayType];
    [commuteProfile reloadData];
    [commuteProfile reloadInputViews];
    [self setOnRamps:highway];
}

-(void) selectedOnRamp:(id)sender{
    [actionSheetOnRamp dismissWithClickedButtonIndex:0 animated:YES];
    onRampSelected = TRUE;
    [[addParts objectAtIndex:2] setDisplayText:[[exitOnList objectAtIndex:[onRampPicker selectedRowInComponent:0]] displayText]];
    onRampID = [[exitOnList objectAtIndex:[onRampPicker selectedRowInComponent:0]] displayType];
    onRampIndex = [onRampPicker selectedRowInComponent:0];
    [exitOffList removeAllObjects];
    [commuteProfile reloadData];
    [commuteProfile reloadInputViews];
    [self setOffRamps:highway onRampSEQ:[onRampPicker selectedRowInComponent:0]];
}

-(void) selectedOffRamp:(id)sender{
    [actionSheetOffRamp dismissWithClickedButtonIndex:0 animated:YES];
    offRampSelected = TRUE;
    [[addParts objectAtIndex:3] setDisplayText:[[exitOffList objectAtIndex:[offRampPicker selectedRowInComponent:0]] displayText]];
    offRampID = [[exitOffList objectAtIndex:[offRampPicker selectedRowInComponent:0]] displayType];
    offRampIndex = onRampIndex + [offRampPicker selectedRowInComponent:0];
    [commuteProfile reloadData];
    [commuteProfile reloadInputViews];
}

-(void) setOnRamps:(int) highwaySelected {
    [exitOnList removeAllObjects];
    for(int i = 0; i < [exits count]; i++){
        if([[exits objectAtIndex:i] highwayID] == highwaySelected){
            [exitOnList addObject:[[ProfileCell alloc] initWithDisplay:[[exits objectAtIndex:i] exitName] type:[[exits objectAtIndex:i] exitID] connection:NULL]];
        }
    }
    [onRampPicker reloadAllComponents];
}

-(void) setOffRamps:(int) highwaySelected onRampSEQ:(int) selectedOnRampID {
    [exitOffList removeAllObjects];
    for(int i = selectedOnRampID+1; i < [exitOnList count]; i++){
        [exitOffList addObject:[exitOnList objectAtIndex:i]];
    }
    [offRampPicker reloadAllComponents];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [self setTitle:@"Commute Setup"];
    [[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:NO];
}

- (void)viewDidDisappear:(BOOL) animated {
	[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:YES];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
