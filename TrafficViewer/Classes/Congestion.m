//
//  Congestion.m
//  SimpleCommuteParser
//
//  Created by sfernald on 9/18/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "Congestion.h"


@implementation Congestion
@synthesize sequence;
@synthesize mph;
@synthesize exitName;
@synthesize exitWaveFile;
@synthesize isIncident;

-(id)initWithName:(NSString *)en wavFile:(NSString *)ewf sequence:(int)seq mph:(int)speed{
	self.exitName = en;
	self.exitWaveFile = ewf;
	self.sequence = seq;
	self.mph = speed;
	self.isIncident = FALSE;
	return self;
}

-(id)initWithCongestion:(Congestion *)c{
	self.exitName = c.exitName;
	self.exitWaveFile = c.exitWaveFile;
	self.sequence = c.sequence;
	self.mph = c.mph;
	self.isIncident = c.isIncident;
	return self;
}
@end
