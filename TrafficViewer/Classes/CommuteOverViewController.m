//
//  CommuteOverViewController.m
//  TrafficViewer
//
//  Created by Sam Fernald on 11/13/12.
//
//

#import "CommuteOverViewController.h"
#import "CJSONSerializer.h"
#import "CJSONDeserializer.h"

@interface CommuteOverViewController ()

@end

@implementation CommuteOverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	//NSBundle *mainBundle = [NSBundle mainBundle];
    
    NSString *post = @"";
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
	
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
	
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:appDelegate.urlForCommute]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *cookies = [scs cookies];
    NSEnumerator* e = [cookies objectEnumerator];
    NSHTTPCookie* cookie;
	
    while ((cookie = [e nextObject])) {
        NSString* name = [cookie name];
        if ([name isEqualToString:@"userId"]) {
            [scs deleteCookie:cookie];
        }
    }
	
    conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (conn) {
        receivedData = [[NSMutableData data] retain];
    } else {
        //[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
    }  
    [request release];
    NSLog(@"end dl commute");

    // Do any additional setup after loading the view from its nib.
}


-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
	NSLog(@"start authent");
	
    if ([challenge previousFailureCount] == 0) {
        NSLog(@"user name = %@",appDelegate.userName);
        NSLog(@"pw = %@",appDelegate.password);
        NSURLCredential *newCredential;
        newCredential=[NSURLCredential credentialWithUser:appDelegate.userName
                                                 password:appDelegate.password
                                              persistence:NSURLCredentialPersistenceNone];
        [[challenge sender] useCredential:newCredential
               forAuthenticationChallenge:challenge];
		
    } else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        // inform the user that the user name and password
        // in the preferences are incorrect
    }
	
}
//------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
	NSLog(@"received data");
	// append the new data to the receivedData
	// receivedData is declared as a method instance elsewhere
	
	[receivedData appendData:data];
	
}
//------------------------------------------------------------------------------
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	NSLog(@"conn failed with error: %@",error);
    
}
//------------------------------------------------------------------------------
- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
	NSLog(@"done loading");
	appDelegate.hasAnAccount = @"YES";
	// do something with the data
    // receivedData is declared as a method instance elsewhere
	//[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
	//[notifier release];
	NSString * outputString = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
	NSString * middleString = [outputString stringByReplacingOccurrencesOfString:@"<incidents/>" withString:@""];
	NSString * inputString = [middleString stringByReplacingOccurrencesOfString:@"<segments/>" withString:@""];
	NSLog(@"output = %@",outputString);
	
    NSLog(@"Commute info downloaded in commute overview");
	appDelegate.tempCommute = inputString;
    CFPreferencesSetAppValue((CFStringRef)@"userName", appDelegate.userName, kCFPreferencesCurrentApplication);
	CFPreferencesSetAppValue((CFStringRef)@"password", appDelegate.password, kCFPreferencesCurrentApplication);
	//NSString *fileAddress = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"commuteInfo.xml"];
	
	//NSString *fileAddress = @"Users/Sam/Documents/TrafficViewer/commuteInfo.xml";//for simulation
	
	/*
	 if([inputString writeToFile:fileAddress atomically:FALSE encoding:NSUTF8StringEncoding error:NULL]){
	 } else {
	 NSLog(@"write failed");
	 }
	 */
	
	[self parseCommute:appDelegate.tempCommute];
    
	[connection release];
    [receivedData release];
}

-(void) parseCommute:(NSString *)commuteToParse {
    //NSDictionary *returnedLocationInfo = [[CJSONDeserializer alloc] deserializeAsDictionary:commuteToParse error:&error];
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
