//
//  IncidentPin.h
//  TrafficViewer
//
//  Created by sfernald on 8/3/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface IncidentPin : NSObject<MKAnnotation> {
	CLLocationCoordinate2D coordinate;
	NSString * title;
	NSString * description;
	NSString * postDate;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D)c title:(NSString *)title description:(NSString *)des postdate:(NSString *) date;

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * description;
@property (nonatomic, retain) NSString * postDate;

@end
