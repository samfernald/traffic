//
//  WebViewController.m
//  TrafficViewer
//
//  Created by Sam Fernald on 8/21/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import "WebViewController.h"



@implementation WebViewController

@synthesize accountSignup;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
	appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString * urlToSignUp;
    //accountSignup = [[UIWebView alloc] init];
	if(appDelegate.selectedCity == 1){
		urlToSignUp = [NSString stringWithFormat:@"http://%@/sd/signup.jsp",appDelegate.cpuUrl];
		//urlToSignUp = @"http://traffic.calit2.net/sd/signup.jsp";
	} else if(appDelegate.selectedCity == 2){
		urlToSignUp = [NSString stringWithFormat:@"http://%@/la/signup.jsp",appDelegate.cpuUrl];
		//urlToSignUp = @"http://traffic.calit2.net/la/signup.jsp";
	} else {
		urlToSignUp = [NSString stringWithFormat:@"http://%@/bayarea/signup.jsp",appDelegate.cpuUrl];
		//urlToSignUp = @"http://traffic.calit2.net/bayarea/signup.jsp";
	}
    
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:urlToSignUp];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    //Load the request in the UIWebView.
    [accountSignup loadRequest:requestObj];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
