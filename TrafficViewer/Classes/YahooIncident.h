//
//  YahooIncident.h
//  TrafficViewer
//
//  Created by sfernald on 8/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface YahooIncident : NSObject {
	float latitude, longitude;
	NSString * title;
	NSString * description;
	NSString * postDate;	
}

@property float latitude;
@property float longitude;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * description;
@property (nonatomic, retain) NSString * postDate;

-(id)initWithTitle:(NSString *)title description:(NSString *)des postDate:(NSString *)pd latitude:(float)latitude longitude:(float)longitude;

-(id)initWithCongestion:(YahooIncident *)yi;

@end