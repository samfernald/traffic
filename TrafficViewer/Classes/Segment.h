//
//  Segment.h
//  SimpleCommuteParser
//
//  Created by sfernald on 9/18/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Congestion.h"


@interface Segment : NSObject {
	int segmentId,onramp,offramp,threshold,hwyId,numCongestions;
	float startDistance, endDistance;
	NSString * hwyName;
	NSString * hwyWavFile;
	bool congested, incidentExists;
	NSMutableArray * congestions;
}

@property int segmentId;
@property int onramp;
@property int offramp;
@property int threshold;
@property int hwyId;
@property int numCongestions;
@property float startDistance;
@property float endDistance;
@property (nonatomic,retain) NSString * hwyName;
@property (nonatomic,retain) NSString * hwyWavFile;
@property bool congested;
@property bool incidentExists;
@property (nonatomic,retain) NSMutableArray * congestions;


-(id)initWithID:(int)segId 
		 onramp:(int)onr 
		offramp:(int)offr
	  threshold:(int)thres 
		  hwyId:(int)hid 
  startDistance:(float)start 
	endDistance:(float)end 
		hwyName:(NSString *)hwynm
	 hwyWavFile:(NSString *)hwf 
	  congested:(bool)constd 
 incidentExists:(bool)icdex;

-(id)initWithSegment:(Segment *)s;
-(void) addCongestion:(Congestion *)cong;


@end
