//
//  TrafficViewController.h
//  TrafficViewer
//
//  Created by sfernald on 11/12/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SDHighwaysViewController.h"
#import "LAHighwaysViewController.h"
#import "BAHighwaysViewController.h"
//#import "UIViewController.h"

@interface TrafficViewController : UIViewController {
	
	IBOutlet UIView * sdhighways;
	IBOutlet UIView * lahighways;
	IBOutlet UIView * bahighways;
    
    //IBOutlet UIScrollView * scrollView;
	
	HighwayFlowViewController *highwayFlowViewController; 
}

@property (nonatomic, retain) HighwayFlowViewController *highwayFlowViewController;

@property(nonatomic,retain) UIView * sdhighways;
@property(nonatomic,retain) UIView * lahighways;
@property(nonatomic,retain) UIView * bahighways;

-(IBAction) highwaySelected:(id) sender;

@end
