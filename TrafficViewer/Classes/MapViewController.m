//
//  MapViewController.m
//  TrafficViewer
//
//  Created by sfernald on 11/14/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MapViewController.h"
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import "CSMapRouteLayerView.h"
#import "TouchXML.h"
#import "SpeedMarker.h"
#import "GAI.h"
#import <GoogleMaps/GoogleMaps.h>


@implementation MapViewController {
    GMSMapView *mapGoogleView_;
}

@synthesize viaRefresh;
@synthesize obtainIncidentData;
@synthesize mapView;
@synthesize mapViewLA;
@synthesize mapViewIE;
@synthesize mapViewRS;
@synthesize scrollViewLA;
@synthesize scrollViewIE;
@synthesize scrollViewRS;
@synthesize pemsLabelLA;
@synthesize pemsLabelIE;
@synthesize pemsLabelRS;
@synthesize segmentedControl;
@synthesize speedMarkers;
@synthesize incidentMarkers;
@synthesize mapUrl;
@synthesize incidentUrl;
@synthesize speedInfo;
@synthesize incidentInfo;
@synthesize refreshButton;
@synthesize switchMapButton;
@synthesize lastRefresh;
@synthesize notifier;
@synthesize routeView = _routeView;
@synthesize myMapView = _myMapView;



- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	//AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	if(appDelegate.selectedCity == 2){
		if([segmentedControl selectedSegmentIndex] == 0){
			return mapViewLA;
		} else if([segmentedControl selectedSegmentIndex] == 1){
			return mapViewIE;
		} else {
			return mapViewRS;
		}
	} else {
		return mapView;
	}
}

-(void)setTrafficUrls {
	//AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	id path;
	NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc ] init ];
	
	path =@"http://traffic.calit2.net/bayarea/servlet/mapurlservlet?mapId=1";
	//path = @"http://traffic.calit2.net/la/servlet/mapurlservlet?mapId=2";
	NSURL *url = [NSURL URLWithString:path];
	NSData *data = [NSData dataWithContentsOfURL:url];
	NSString *pure = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
	NSString *edit1 = [pure stringByReplacingOccurrencesOfString:@"<map__url>" withString:@""];
	NSString *final = [edit1 stringByReplacingOccurrencesOfString:@"</map__url>" withString:@""];
	if(final.length > 0) {
		appDelegate.baMapUrl = final;
		CFPreferencesSetAppValue((CFStringRef)@"baMapUrl",final, kCFPreferencesCurrentApplication);
		NSLog(@"BA Url = %@",final);
	}
	[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
	[notifier release];
	
	
	[pool release];
}

-(void) getTrafficForMap {
	NSLog(@"starting connection");
    
	if(!viaRefresh) {
        NSLog(@"making map view");
		_myMapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
		
		self.view = _myMapView;
		
		// create our route layer view, and initialize it with the map on which it will be rendered. 
		_routeView = [[CSMapRouteLayerView alloc] initWithMapView:_myMapView];
		//[self.view bringSubviewToFront:_myMapView];
	}
    [self notifyDownloading];

	NSLog(@"url = %@",mapUrl);
	NSString *post = @""; 
	NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
	NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];  
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
	[request setURL:[NSURL URLWithString:mapUrl]];
	[request setHTTPMethod:@"POST"];  
	[request setValue:postLength forHTTPHeaderField:@"Content-Length"];  
	[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];  
	[request setHTTPBody:postData];

	//if(conn == nil) {
		conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
	//}
	if (conn) {  
		receivedData = [[NSMutableData data] retain];
	} else {  
		// inform the user that the download could not be made  
	} 
	
	NSLog(@"end making con");
}


-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge previousFailureCount] == 0) {
        NSURLCredential *newCredential;
        newCredential=[NSURLCredential credentialWithUser:@""
                                                 password:@""
                                              persistence:NSURLCredentialPersistenceNone];
        [[challenge sender] useCredential:newCredential
               forAuthenticationChallenge:challenge];
    } else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        // inform the user that the user name and password
        // in the preferences are incorrect
        [self showPreferencesCredentialsAreIncorrectPanel:self];
    }	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{  
	NSLog(@"did receive data");
	//append the new data to the receivedData
	//receivedData is declared as a method instance elsewhere
	
	//if(!obtainIncidentData){
	//	NSLog(@"not obtain incident");
		[receivedData appendData:data];
	//} else {
	//	NSLog(@"obtain incident");
	//	[receivedDataIncident appendData:data];
	//}
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSString * outputString;
	//if(!obtainIncidentData){
		outputString = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
	//} else {
	//	outputString = [[NSString alloc] initWithData:receivedDataIncident encoding:NSASCIIStringEncoding];
	//	NSLog(@" output string = %@",outputString);
	//}
	//NSString *fileAddress = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"trafficFlowInfo.xml"];
	//for simulator
	//NSString *fileAddress = @"Users/Sam/Documents/TrafficViewer/trafficFlowInfo.xml";
	//self.speedData = outputString;
	
    //do something with the data
    //receivedData is declared as a method instance elsewhere
	//[self grabRSSFeed:[@"file://" stringByAppendingString:[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"trafficFlowInfo.xml"]]];
	NSLog(@"about to grab RSS");
	[self grabRSSFeed:outputString];
	//For simulator
	//[self grabRSSFeed:@"file://localhost/Users/Sam/Documents/TrafficViewer/trafficFlowInfo.xml"];
	
	
	//NSLog(@"OUTPUT: %@",outputString);
	//[receivedData dealloc];
	
	// release the connection, and the data object
    //[connection release];
    //[receivedData release];
}

-(void) grabRSSFeed:(NSString *)blogAddress {
	if(!obtainIncidentData) {
		NSLog(@"parsing speeds");
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
		// Initialize the congestionInfo MutableArray that we declared in the header
		speedInfo = [[NSMutableArray alloc] init];
	
		// Convert the supplied URL string into a usable URL object
		//NSURL *url = [NSURL URLWithString: blogAddress];
		//NSLog(@"OUTPUT: %@",blogAddress);
	
		// Create a new rssParser object based on the TouchXML "CXMLDocument" class, this is the
		// object that actually grabs and processes the RSS data
		CXMLDocument *rssParser = [[[CXMLDocument alloc] initWithXMLString:blogAddress options:0 error:nil] autorelease];
		//CXMLDocument *rssParser = [[[CXMLDocument alloc] initWithContentsOfURL:url options:0 error:nil] autorelease];
		// Create a new Array object to be used with the looping of the results from the rssParser
		NSArray *speedNodes = NULL;
		@try{
			// Set the congestionNodes Array to contain an object for every instance of an  node in our RSS feed
			speedNodes = [rssParser nodesForXPath:@"//commute.SpeedMarker" error:nil];
			
		} @catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
		// Loop through the congestionNodes to access each items actual data
		
		for (CXMLElement *resultElement in speedNodes) {
			@try{
				// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
				NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
				// Create a counter variable as type "int"
				int counter;
			
				// Loop through the children of the current  node
				for(counter = 0; counter < [resultElement childCount]; counter++) {
					
					// Add each field to the blogItem Dictionary with the node name as key and node value as the value
					[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
				}	
			
				// Add the blogItem to the global congestionInfo Array so that the view can access it.
				[speedInfo addObject:[infoItem copy]];
				[infoItem release];
			}@catch (NSException * e) {
				NSLog(@"Exception %@: %@", [e name], [e  reason]);
			}
		}

		@try{
			NSLog(@"speedinfo count = %d",[speedInfo count]);
			if([speedInfo count] > 5) {
				[self prepareSpeedArray:speedInfo];
			} else {
				[NSException raise:@"An error occured while attempting to download the traffic data." format:@"bad download"];
			
			}
			//NSFileManager *fileManager = [NSFileManager defaultManager];
			//[fileManager removeItemAtPath:blogAddress error:NULL];
			[pool drain];
		
		}@catch (NSException * e) {
			[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
			[notifier release];
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
			UIAlertView * errorMessage = [[UIAlertView alloc] initWithTitle:@"Error" message:@"An error occured while attempting to download the traffic data." delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
			[errorMessage show];
		
		}
	} else {
		NSLog(@"parsing incidents");
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		
		// Initialize the congestionInfo MutableArray that we declared in the header
		incidentInfo = [[NSMutableArray alloc] init];
		
		// Convert the supplied URL string into a usable URL object
		//NSURL *url = [NSURL URLWithString: blogAddress];
		//NSLog(@"OUTPUT: %@",blogAddress);
		
		// Create a new rssParser object based on the TouchXML "CXMLDocument" class, this is the
		// object that actually grabs and processes the RSS data
		CXMLDocument *rssParser = [[[CXMLDocument alloc] initWithXMLString:blogAddress options:0 error:nil] autorelease];
		//CXMLDocument *rssParser = [[[CXMLDocument alloc] initWithContentsOfURL:url options:0 error:nil] autorelease];
		// Create a new Array object to be used with the looping of the results from the rssParser
		NSArray *speedNodes = NULL;
		@try{
			// Set the congestionNodes Array to contain an object for every instance of an  node in our RSS feed
			speedNodes = [rssParser nodesForXPath:@"//commute.YahooIncident" error:nil];
			
		} @catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
		// Loop through the congestionNodes to access each items actual data
		
		for (CXMLElement *resultElement in speedNodes) {
			@try{
				// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
				NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
				
				// Create a counter variable as type "int"
				int counter;
				
				// Loop through the children of the current  node
				for(counter = 0; counter < [resultElement childCount]; counter++) {
					
					// Add each field to the blogItem Dictionary with the node name as key and node value as the value
					[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
				}	
				
				// Add the blogItem to the global congestionInfo Array so that the view can access it.
				[incidentInfo addObject:[infoItem copy]];
				[infoItem release];
			}@catch (NSException * e) {
				NSLog(@"Exception %@: %@", [e name], [e  reason]);
			}
		}
		
		@try{
			NSLog(@"incidentinfo count = %d",[incidentInfo count]);
			if([incidentInfo count] >= 1) {
				[self prepareIncidentArray:incidentInfo];
			} else {
				NSLog(@"An error occured while attempting to download the traffic data.");
				
			}
			//NSFileManager *fileManager = [NSFileManager defaultManager];
			//[fileManager removeItemAtPath:blogAddress error:NULL];
			//[pool drain];
			
		}@catch (NSException * e) {
			[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
			[notifier release];
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
			UIAlertView * errorMessage = [[UIAlertView alloc] initWithTitle:@"Error" message:@"An error occured while attempting to download the traffic data." delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
			[errorMessage show];
		}
	}

}

-(void)prepareSpeedArray:(NSMutableArray *) trafficFlow{
	//AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	[speedMarkers dealloc];
	speedMarkers = [[NSMutableDictionary alloc] initWithCapacity:[trafficFlow count]];
	NSLog(@"building speedMarkers");
	for(int i = 0; i<[trafficFlow count];i++){
		SpeedMarker * tempMarker = [[SpeedMarker alloc] initWithHwyExitId:[[[trafficFlow objectAtIndex:i] objectForKey:@"hwyExitId"] intValue]  hwyId:[[[trafficFlow objectAtIndex:i] objectForKey:@"hwyId"] intValue] speed:[[[trafficFlow objectAtIndex:i] objectForKey:@"speed"] intValue]];
		[speedMarkers setObject:tempMarker forKey:[[trafficFlow objectAtIndex:i] objectForKey:@"hwyExitId"]];
		//[tempMarker release];
	}
	NSLog(@"speed markers built");
	[self drawMap];
}
	
-(void)prepareIncidentArray:(NSMutableArray *) incidentList{
	NSLog(@"sending incidents to map");
	//AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[_routeView addIncidents:incidentList];
}	
	

-(void) drawMap {

	NSLog(@"about to build points");
	//appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	//NSNumber * mapToLoad = (NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"initMap", kCFPreferencesCurrentApplication);
	NSMutableArray* points;
	NSMutableArray* points2;
	NSMutableArray* points3;
	NSMutableArray* points4;
	NSMutableArray* points5;
	NSMutableArray* pointDetails;
	if(appDelegate.selectedCity == 1){
		[super viewDidLoad];
		
		//
		// load the points from our local resource
		//
		NSString* filePath;
		//if([mapToLoad intValue] == 0) {
			filePath = [[NSBundle mainBundle] pathForResource:@"routeSD" ofType:@"csv"];
		/*} else {
			filePath = [[NSBundle mainBundle] pathForResource:@"routeSDBoth" ofType:@"csv"];
		}*/
		NSString* fileContents = [NSString stringWithContentsOfFile:filePath];
		NSArray* pointStrings = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		
		points = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
		points2 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
		points3 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
		points4 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
		points5 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
		pointDetails = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
		
		CLLocationDegrees latitude;
		CLLocationDegrees longitude;
		CLLocationDegrees latitude2;
		CLLocationDegrees longitude2;
		CLLocationDegrees latitude3;
		CLLocationDegrees longitude3;
		CLLocationDegrees latitude4;
		CLLocationDegrees longitude4;
		CLLocationDegrees latitude5;
		CLLocationDegrees longitude5;
		
		double lato,longo;
		
		
		
		NSLog(@"count = %d",pointStrings.count);
		
		for(int idx = 0; idx < pointStrings.count; idx++)
		{
			if(idx%3 == 0) {
				// break the string down even further to latitude and longitude fields. 
				NSString* currentPointString = [pointStrings objectAtIndex:idx];
				//NSString* currentPointString2 = [pointStrings2 objectAtIndex:idx];

				NSArray* latLonArr = [currentPointString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
				//NSArray* latLonArr2 = [currentPointString2 componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];

				lato = [[latLonArr objectAtIndex:0] doubleValue];
				longo = [[latLonArr objectAtIndex:1] doubleValue];
				latitude = lato;
				longitude = longo;
				latitude2 = lato + [[latLonArr objectAtIndex:2] doubleValue];

				longitude2 = longo + [[latLonArr objectAtIndex:3] doubleValue];
				
				latitude3 = lato + [[latLonArr objectAtIndex:4] doubleValue];

				longitude3 = longo + [[latLonArr objectAtIndex:5] doubleValue];

				latitude4 = lato + [[latLonArr objectAtIndex:6] doubleValue];

				longitude4 = longo + [[latLonArr objectAtIndex:7] doubleValue];

				latitude5 = lato + [[latLonArr objectAtIndex:8] doubleValue];

				longitude5 = longo + [[latLonArr objectAtIndex:9] doubleValue];
		
				pointInfo * pointInfoTemp = [[[pointInfo alloc] initWithHwy:[[speedMarkers objectForKey:[latLonArr objectAtIndex:10]] highway] speed:[[speedMarkers objectForKey:[latLonArr objectAtIndex:10]] mph]] autorelease];
				[pointDetails addObject:pointInfoTemp];
				CLLocation* currentLocation  = [[[CLLocation alloc] initWithLatitude:latitude longitude:longitude] autorelease];
				CLLocation* currentLocation2 = [[[CLLocation alloc] initWithLatitude:latitude2 longitude:longitude2] autorelease];
				CLLocation* currentLocation3 = [[[CLLocation alloc] initWithLatitude:latitude3 longitude:longitude3] autorelease];
				CLLocation* currentLocation4 = [[[CLLocation alloc] initWithLatitude:latitude4 longitude:longitude4] autorelease];
				CLLocation* currentLocation5 = [[[CLLocation alloc] initWithLatitude:latitude5 longitude:longitude5] autorelease];

				[points addObject:currentLocation];
				[points2 addObject:currentLocation2];
				[points3 addObject:currentLocation3];
				[points4 addObject:currentLocation4];
				[points5 addObject:currentLocation5];
				
				 //pull speed based on closest ID and add to array: 
				 //NSNumber speed = [[latLonArr objectAtIndex:2] intValue];
				 //[points addObject:speed];
				 
			}
		}


		//[_routeView addPoints:points pointsZoom1:points2 pointsZoom2:points3 pointsZoom3:points4 pointsZoom4:points5 pointDetails:pointDetails];
		
		//[points release];

	} else if(appDelegate.selectedCity == 2){
		[super viewDidLoad];
		//
		// load the points from our local resource
		//
		NSString* filePath;
		//if([mapToLoad intValue] == 0) {
			filePath = [[NSBundle mainBundle] pathForResource:@"routeLA" ofType:@"csv"];
		/*} else {
			filePath = [[NSBundle mainBundle] pathForResource:@"routeLABoth" ofType:@"csv"];
		}*/
		//NSString* filePath2 = [[NSBundle mainBundle] pathForResource:@"route" ofType:@"csv"];
		NSString* fileContents = [NSString stringWithContentsOfFile:filePath];
		//NSString* fileContents2 = [NSString stringWithContentsOfFile:filePath2];
		NSArray* pointStrings = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		//NSArray* pointStrings2 = [fileContents2 componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		
		points  = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
		points2 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
		points3 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
		points4 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
		points5 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
		pointDetails = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
		//NSLog(@"point length %d",pointStrings2.count);
		CLLocationDegrees latitude;
		CLLocationDegrees longitude;
		CLLocationDegrees latitude2;
		CLLocationDegrees longitude2;
		CLLocationDegrees latitude3;
		CLLocationDegrees longitude3;
		CLLocationDegrees latitude4;
		CLLocationDegrees longitude4;
		CLLocationDegrees latitude5;
		CLLocationDegrees longitude5;
		
		double lato,longo;

		
		NSLog(@"count = %d",pointStrings.count);
		for(int idx = 0; idx < pointStrings.count; idx++)
		{
			if(idx%3 == 0) {
				// break the string down even further to latitude and longitude fields. 
				NSString* currentPointString = [pointStrings objectAtIndex:idx];
				//NSString* currentPointString2 = [pointStrings2 objectAtIndex:idx];
				NSArray* latLonArr = [currentPointString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
				//NSArray* latLonArr2 = [currentPointString2 componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
			
				lato = [[latLonArr objectAtIndex:0] doubleValue];
				longo = [[latLonArr objectAtIndex:1] doubleValue];
				latitude = lato;
				longitude = longo;
				latitude2 = lato + [[latLonArr objectAtIndex:2] doubleValue];
				
				longitude2 = longo + [[latLonArr objectAtIndex:3] doubleValue];
				
				latitude3 = lato + [[latLonArr objectAtIndex:4] doubleValue];
				
				longitude3 = longo + [[latLonArr objectAtIndex:5] doubleValue];
				
				latitude4 = lato + [[latLonArr objectAtIndex:6] doubleValue];
				
				longitude4 = longo + [[latLonArr objectAtIndex:7] doubleValue];
				
				latitude5 = lato + [[latLonArr objectAtIndex:8] doubleValue];
				
				longitude5 = longo + [[latLonArr objectAtIndex:9] doubleValue];
				
			
				
				pointInfo * pointInfoTemp = [[[pointInfo alloc] initWithHwy:[[speedMarkers objectForKey:[latLonArr objectAtIndex:10]] highway] speed:[[speedMarkers objectForKey:[latLonArr objectAtIndex:10]] mph]] autorelease];
				[pointDetails addObject:pointInfoTemp];
			
				CLLocation* currentLocation  = [[[CLLocation alloc] initWithLatitude:latitude  longitude:longitude]  autorelease];
				CLLocation* currentLocation2 = [[[CLLocation alloc] initWithLatitude:latitude2 longitude:longitude2] autorelease];
				CLLocation* currentLocation3 = [[[CLLocation alloc] initWithLatitude:latitude3 longitude:longitude3] autorelease];
				CLLocation* currentLocation4 = [[[CLLocation alloc] initWithLatitude:latitude4 longitude:longitude4] autorelease];
				CLLocation* currentLocation5 = [[[CLLocation alloc] initWithLatitude:latitude5 longitude:longitude5] autorelease];
			
				[points addObject:currentLocation];
				[points2 addObject:currentLocation2];
				[points3 addObject:currentLocation3];
				[points4 addObject:currentLocation4];
				[points5 addObject:currentLocation5];
			}
		}
	}else if(appDelegate.selectedCity == 3){
			[super viewDidLoad];
			//
			// load the points from our local resource
			//
			NSString* filePath;
			filePath = [[NSBundle mainBundle] pathForResource:@"routeBA" ofType:@"csv"];
					
			//NSString* filePath2 = [[NSBundle mainBundle] pathForResource:@"route" ofType:@"csv"];
			NSString* fileContents = [NSString stringWithContentsOfFile:filePath];
			//NSString* fileContents2 = [NSString stringWithContentsOfFile:filePath2];
			NSArray* pointStrings = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			//NSArray* pointStrings2 = [fileContents2 componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			
			points  = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
			points2 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
			points3 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
			points4 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
			points5 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
			pointDetails = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
			//NSLog(@"point length %d",pointStrings2.count);
			CLLocationDegrees latitude;
			CLLocationDegrees longitude;
			CLLocationDegrees latitude2;
			CLLocationDegrees longitude2;
			CLLocationDegrees latitude3;
			CLLocationDegrees longitude3;
			CLLocationDegrees latitude4;
			CLLocationDegrees longitude4;
			CLLocationDegrees latitude5;
			CLLocationDegrees longitude5;
			
			double lato,longo;
			
			
			for(int idx = 0; idx < pointStrings.count; idx++)
			{
				//if(idx%3 == 0) {
				if(TRUE){
					// break the string down even further to latitude and longitude fields. 
					NSString* currentPointString = [pointStrings objectAtIndex:idx];
					//NSString* currentPointString2 = [pointStrings2 objectAtIndex:idx];
					NSArray* latLonArr = [currentPointString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
					//NSArray* latLonArr2 = [currentPointString2 componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
					
					lato = [[latLonArr objectAtIndex:0] doubleValue];
					longo = [[latLonArr objectAtIndex:1] doubleValue];
					latitude = lato;
					longitude = longo;
					latitude2 = lato + [[latLonArr objectAtIndex:2] doubleValue];
					longitude2 = longo + [[latLonArr objectAtIndex:3] doubleValue];
					latitude3 = lato + [[latLonArr objectAtIndex:4] doubleValue];
					longitude3 = longo + [[latLonArr objectAtIndex:5] doubleValue];
					latitude4 = lato + [[latLonArr objectAtIndex:6] doubleValue];
					longitude4 = longo + [[latLonArr objectAtIndex:7] doubleValue];
					latitude5 = lato + [[latLonArr objectAtIndex:8] doubleValue];
					longitude5 = longo + [[latLonArr objectAtIndex:9] doubleValue];
					
					
					
					pointInfo * pointInfoTemp = [[[pointInfo alloc] initWithHwy:[[speedMarkers objectForKey:[latLonArr objectAtIndex:10]] highway] speed:[[speedMarkers objectForKey:[latLonArr objectAtIndex:10]] mph]] autorelease];
					[pointDetails addObject:pointInfoTemp];
					
					CLLocation* currentLocation  = [[[CLLocation alloc] initWithLatitude:latitude  longitude:longitude]  autorelease];
					CLLocation* currentLocation2 = [[[CLLocation alloc] initWithLatitude:latitude2 longitude:longitude2] autorelease];
					CLLocation* currentLocation3 = [[[CLLocation alloc] initWithLatitude:latitude3 longitude:longitude3] autorelease];
					CLLocation* currentLocation4 = [[[CLLocation alloc] initWithLatitude:latitude4 longitude:longitude4] autorelease];
					CLLocation* currentLocation5 = [[[CLLocation alloc] initWithLatitude:latitude5 longitude:longitude5] autorelease];
					
					[points addObject:currentLocation];
					[points2 addObject:currentLocation2];
					[points3 addObject:currentLocation3];
					[points4 addObject:currentLocation4];
					[points5 addObject:currentLocation5];
				}
			}

		//NSLog(@"done building and sending points to mapview");
		//[_routeView addPoints:points pointsZoom1:points2 pointsZoom2:points3 pointsZoom3:points4 pointsZoom4:points5 pointDetails:pointDetails];
		
		//[points release];

	}
	//obtainIncidentData = YES;
	//NSLog(@"get incident Data");
	//[self getIncidentData];
	NSLog(@"adding points to map");
	[_routeView addPoints:points pointsZoom1:points2 pointsZoom2:points3 pointsZoom3:points4 pointsZoom4:points5 pointDetails:pointDetails];
    [DejalBezelActivityView removeViewAnimated:TRUE];
	[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
	//[notifier release];
    if(appDelegate.priorLoads == 1 && appDelegate.askToRate) {
        appDelegate.askToRate = FALSE;
        [appDelegate.notifier show];
    }

}


// Implement viewDidLoad to do additional setup after loading the view.
- (void)viewDidLoad {
    NSLog(@"viewDidLoad");
    //self.trackedViewName = @"Map view";
    //id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-33122525-1"];
    //[tracker sendView:@"Map view"];
    //id myDefault = [GAI sharedInstance].defaultTracker;
    
    if(appDelegate == NULL){
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    refreshButton = [[UIBarButtonItem alloc] initWithTitle:@"Refresh" style:UIBarButtonItemStyleBordered target:self action:@selector(refresh:)];
    self.navigationItem.rightBarButtonItem = refreshButton;

    //self.title = @"Traffic Map";
    if(appDelegate.mapToLoad == 0 && !appDelegate.mapViewLoaded){
        appDelegate.mapViewLoaded = TRUE;
        NSString *downloadMessage = [NSString stringWithFormat:@"Fetching %@ traffic map data.",appDelegate.homeCity];
        lastRefresh = [[NSDate date] retain];
        viaRefresh = NO;
        obtainIncidentData = NO;
        //AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSNumber * screenToLoad = (NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"screenToLoad", kCFPreferencesCurrentApplication);
        if(appDelegate.priorLoads != 1 || [screenToLoad intValue] != 2) {
        
            // [self performSelectorOnMainThread:@selector(notifyDownloading) withObject:nil waitUntilDone:TRUE];
        
        }
        //self.title = @"Traffic Map";
	
        //[NSThread detachNewThreadSelector: @selector(downloadingMessageNotifier) toTarget: self withObject: nil ];
        id path;
        //NSNumber * mapToLoad = (NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"initMap", kCFPreferencesCurrentApplication);
        NSLog(@"refresh button");
        if(appDelegate.selectedCity == 1){
            
                        
            switchMapSegmentedControl = [[UISegmentedControl alloc]initWithItems:[[NSArray alloc] initWithObjects:@"Switch to Google Data",nil]];
            switchMapSegmentedControl.momentary = YES;
            switchMapSegmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
            [switchMapSegmentedControl addTarget:self action:@selector(swapMaps:)
                                forControlEvents:UIControlEventValueChanged];
                    
            self.navigationItem.titleView = switchMapSegmentedControl;

		//incidentUrl = [NSString stringWithFormat: @"http://%@/sd/servlet/incidentreport",appDelegate.cpuUrl];
		//[incidentUrl retain];
		//NSLog(@"url = %@",incidentUrl);

            //if([mapToLoad intValue] == 0) {
                //mapUrl = @"http://traffic.calit2.net/sd/servlet/speedsformap";
                mapUrl = [NSString stringWithFormat: @"http://%@/sd/servlet/speedsformap",appDelegate.cpuUrl];
                [self getTrafficForMap];
                //[super viewDidLoad];
            /*} else {
                //mapUrl = @"http://traffic.calit2.net/sd/servlet/speedsforbothmaps";
                mapUrl = [NSString stringWithFormat: @"http://%@/sd/servlet/speedsforbothmaps",appDelegate.cpuUrl];
                [self getTrafficForMap];
                //[super viewDidLoad];
            }*/
        } else if(appDelegate.selectedCity == 2){
            switchMapSegmentedControl = [[UISegmentedControl alloc]initWithItems:[[NSArray alloc] initWithObjects:@"Switch to Google Data",nil]];
            switchMapSegmentedControl.momentary = YES;
            //switchMapSegmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
            [switchMapSegmentedControl addTarget:self action:@selector(swapMaps:)
                                forControlEvents:UIControlEventValueChanged];
            
            self.navigationItem.titleView = switchMapSegmentedControl;
            
            incidentUrl = [NSString stringWithFormat: @"http://%@/la/servlet/incidentreport",appDelegate.cpuUrl];
            NSLog(@"url = %@",incidentUrl);

            //if([mapToLoad intValue] == 0) {
                //mapUrl = @"http://traffic.calit2.net/la/servlet/speedsformap";
                mapUrl = [NSString stringWithFormat: @"http://%@/la/servlet/speedsformap",appDelegate.cpuUrl];
                [self getTrafficForMap];
                //[super viewDidLoad];
            /*} else {
                //mapUrl = @"http://traffic.calit2.net/la/servlet/speedsforbothmaps";
                mapUrl = [NSString stringWithFormat: @"http://%@/la/servlet/speedsforbothmaps",appDelegate.cpuUrl];
                [self getTrafficForMap];
                //[super viewDidLoad];
            }*/
        } else {
            switchMapSegmentedControl = [[UISegmentedControl alloc]initWithItems:[[NSArray alloc] initWithObjects:@"Switch to Google Data",nil]];
            switchMapSegmentedControl.momentary = YES;
            switchMapSegmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
            [switchMapSegmentedControl addTarget:self action:@selector(swapMaps:)
                                forControlEvents:UIControlEventValueChanged];
            
            self.navigationItem.titleView = switchMapSegmentedControl;
            mapUrl = [NSString stringWithFormat: @"http://%@/bayarea/servlet/speedsformap",appDelegate.cpuUrl];
                [self getTrafficForMap];
                    //[super viewDidLoad];
        }
	
        //[NSThread detachNewThreadSelector: @selector(setTrafficUrls) toTarget:self withObject:nil];
        //[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:YES];
    } else {
        switchMapSegmentedControl = [[UISegmentedControl alloc]initWithItems:[[NSArray alloc] initWithObjects:@"Switch to Caltrans Data",nil]];
        switchMapSegmentedControl.momentary = YES;
        switchMapSegmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
        [switchMapSegmentedControl addTarget:self action:@selector(swapMaps:)
                            forControlEvents:UIControlEventValueChanged];
        
        self.navigationItem.titleView = switchMapSegmentedControl;
        GMSCameraPosition *camera;
        NSLog(@"user lat = %f and city = %d",appDelegate.userLat, appDelegate.selectedCity);
        if(appDelegate.userLat != 0){
            camera = [GMSCameraPosition cameraWithLatitude:appDelegate.userLat
                                                 longitude:appDelegate.userLong
                                                      zoom:12];
        } else {
            if(appDelegate.selectedCity == 1){
                camera = [GMSCameraPosition cameraWithLatitude:32.78
                                                     longitude:-117.15
                                                          zoom:12];
            } else if(appDelegate.selectedCity == 2){
                camera = [GMSCameraPosition cameraWithLatitude:33.8
                                                     longitude:-118.05
                                                          zoom:12];
            } else {
                camera = [GMSCameraPosition cameraWithLatitude:37.57
                                                     longitude:-122.00
                                                          zoom:12];
            }
        }
        mapGoogleView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
        mapGoogleView_.myLocationEnabled = YES;
        mapGoogleView_.trafficEnabled = YES;
        //NSLog(@"lat = %f and long = %f",mapGoogleView_.myLocation.coordinate.latitude, mapGoogleView_.myLocation.coordinate.longitude);
        //[mapView_ animateToLocation:CLLocationCoordinate2DMake(mapView_.myLocation.coordinate.latitude, mapView_.myLocation.coordinate.longitude)];
        self.view = mapGoogleView_;
    }

	
}

-(void) notifyDownloading {
    NSLog(@"view for load = %@",self.view);
//[NSString stringWithFormat:@"Conecting to %@ server.",appDelegate.homeCity]
    [DejalBezelActivityView activityViewForView:self.view withLabel:[NSString stringWithFormat:@"Fetching %@ traffic map data.",appDelegate.homeCity]];

}

- (void)refresh:(id)sender{
	NSLog(@"in refresh");
	viaRefresh = YES;
    //AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

	NSTimeInterval timeSinceRefresh = [lastRefresh timeIntervalSinceNow];
	if(timeSinceRefresh <= -180) {
		NSLog(@"refreshing");
		[lastRefresh release];
		lastRefresh = [[NSDate date] retain];
		lastRefresh = [[NSDate date] retain];
        [DejalBezelActivityView activityViewForView:self.view withLabel:[NSString stringWithFormat:@"Fetching %@ traffic map data.",appDelegate.homeCity]];


		//[NSThread detachNewThreadSelector: @selector(downloadingMessageNotifier) toTarget: self withObject: nil ];
		//NSNumber * mapToLoad = (NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"initMap", kCFPreferencesCurrentApplication);
		if(appDelegate.selectedCity == 1){
			
			//if([mapToLoad intValue] == 0) {
				mapUrl = [NSString stringWithFormat: @"http://%@/sd/servlet/speedsformap",appDelegate.cpuUrl];
				[self getTrafficForMap];
			/*} else {
				mapUrl = [NSString stringWithFormat: @"http://%@/sd/servlet/speedsforbothmaps",appDelegate.cpuUrl];
				[self getTrafficForMap];
			}*/
		} else if(appDelegate.selectedCity == 2){
			
			//if([mapToLoad intValue] == 0) {
				mapUrl = [NSString stringWithFormat: @"http://%@/la/servlet/speedsformap",appDelegate.cpuUrl];
				[self getTrafficForMap];
			/*} else {
				mapUrl = [NSString stringWithFormat: @"http://%@/la/servlet/speedsforbothmaps",appDelegate.cpuUrl];
				[self getTrafficForMap];
			}*/
		}else if(appDelegate.selectedCity == 3){
			
			//if([mapToLoad intValue] == 0) {
				mapUrl = [NSString stringWithFormat: @"http://%@/bayarea/servlet/speedsformap",appDelegate.cpuUrl];
				[self getTrafficForMap];
			/*} else {
				mapUrl = [NSString stringWithFormat: @"http://%@/bayarea/servlet/speedsforbothmaps",appDelegate.cpuUrl];
				[self getTrafficForMap];
			}*/
		}
	} else {
        NSLog(@"view for refresh = %@",self.view);
        if(appDelegate.selectedCity == 1){
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Speeds are updated once every minute."];
        } else {
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Speeds are updated once every 5 minutes."];
        }
        [self performSelector:@selector(dropAlert) withObject:nil afterDelay:3.0];
        

	}
}

-(void) dropAlert {
    [DejalBezelActivityView removeViewAnimated:TRUE];
}

- (void) segmentAction:(id)sender {
	UISegmentedControl* segCtl = sender;
	if( [segCtl selectedSegmentIndex] == 0 ){
		
		self.view = scrollViewLA;
		
	} else if ( [segCtl selectedSegmentIndex] == 1 ){
		
		self.view = scrollViewIE;
		
	} else {
		
		self.view = scrollViewRS;
		
	}
	
}

- (void)downloadingMessageNotifier {
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc ] init ];
    // Do processor intensive tasks here knowing that it will not block the main thread.
	NSString *downloadMessage = [NSString stringWithFormat:@"Fetching %@ traffic map data.",appDelegate.homeCity];
	notifier = [[UIAlertView alloc] initWithTitle:@"" message:downloadMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
	UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityView.frame = CGRectMake(139.0f-18.0f, 85.0f, 33.0f, 33.0f);
    [notifier addSubview:activityView];
    [activityView startAnimating];
	[notifier show];	
	[pool release];
}

-(void) swapMaps:(id)sender {
    UISegmentedControl *tempSeg = (UISegmentedControl*)sender;
    if([[tempSeg titleForSegmentAtIndex:0] isEqualToString:@"Switch to Google Data"]){
        [tempSeg setTitle:@"Switch to Caltrans Data" forSegmentAtIndex:0];
        appDelegate.mapToLoad = 1;
        appDelegate.mapViewLoaded = FALSE;
        [self viewDidLoad];
    } else {
        [tempSeg setTitle:@"Switch to Google Data" forSegmentAtIndex:0];
        appDelegate.mapToLoad = 0;
        appDelegate.mapViewLoaded = FALSE;
        [self viewDidLoad];
    }
}

/*
- (void)loadView {
    // Create a GMSCameraPosition that tells the map to display the
    // coordinate -33.86,151.20 at zoom level 6.
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = @"Traffic Map";
    NSLog(@"loadView");
    if(appDelegate.mapToLoad == 0){
        NSLog(@"loading view");
    } else {
        GMSCameraPosition *camera;
        NSLog(@"user lat = %f and city = %d",appDelegate.userLat, appDelegate.selectedCity);
        if(appDelegate.userLat != 0){
            camera = [GMSCameraPosition cameraWithLatitude:appDelegate.userLat
                                                            longitude:appDelegate.userLong
                                                                 zoom:10];
        } else {
            if(appDelegate.selectedCity == 1){
                camera = [GMSCameraPosition cameraWithLatitude:32.78
                                                 longitude:-117.15
                                                      zoom:10];
            } else if(appDelegate.selectedCity == 2){
                camera = [GMSCameraPosition cameraWithLatitude:33.8
                                                     longitude:-118.05
                                                          zoom:8];
            } else {
                camera = [GMSCameraPosition cameraWithLatitude:37.57
                                                longitude:-122.00
                                                          zoom:10];
            }
        }
        mapGoogleView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
        mapGoogleView_.myLocationEnabled = YES;
        mapGoogleView_.trafficEnabled = YES;
        NSLog(@"lat = %f and long = %f",mapGoogleView_.myLocation.coordinate.latitude, mapGoogleView_.myLocation.coordinate.longitude);
    //[mapView_ animateToLocation:CLLocationCoordinate2DMake(mapView_.myLocation.coordinate.latitude, mapView_.myLocation.coordinate.longitude)];
        self.view = mapGoogleView_;
    }
    
}
 */


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}


- (void)didReceiveMemoryWarning {
	NSLog(@"mem warning in mapview");
    [super didReceiveMemoryWarning]; 
	// Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


- (void)dealloc {
    [super dealloc];
	
}


@end
