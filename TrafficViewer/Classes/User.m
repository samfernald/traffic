//
//  User.m
//  SimpleCommuteParser
//
//  Created by sfernald on 9/18/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "User.h"


@implementation User

@synthesize userId,carrierId,accountExists,morningExists,altMorningExists,eveningExists;
@synthesize altEveningExists,infoExists,ani,firstName,lastName,carrierEmail,email;
@synthesize morning,evening,altMorning,altEvening;
@synthesize userInfo;
-(id)initWithInfo:(int)ui 
		  carrier:(int)ci 
	accountExists:(bool)ae 
	morningExists:(bool)me
 altMorningExists:(bool)ame 
	eveningExists:(bool)ee 
 altEveningExists:(bool)aee 
	   infoExists:(bool)ie 
			  ani:(NSString*)an 
		firstName:(NSString*)fn 
		 lastName:(NSString*)ln
	 carrierEmail:(NSString*)ce
			email:(NSString*)em{
	self.userId = ui;
	self.carrierId = ci;
	self.accountExists = ae;
	self.morningExists = me;
	self.altMorningExists = ame;
	self.eveningExists = ee;
	self.altEveningExists = aee;
	self.infoExists = ie;
	self.ani = an;
	self.firstName = fn;
	self.lastName = ln;
	self.carrierEmail = ce;
	self.email = em;
	morning = [[Commute alloc] init];
	evening = [[Commute alloc] init];
	altMorning = [[Commute alloc] init];
	altEvening = [[Commute alloc] init];
	[self createUserInfo];
	return self;
}

-(void)addMorningCommute:(Commute *) com{
	self.morning = [[Commute alloc] initWithCommute:com];
}

-(void)addEveningCommute:(Commute *) com{
	self.evening = [[Commute alloc] initWithCommute:com];
}

-(void)addAltMorningCommute:(Commute *) com{
	self.altMorning = [[Commute alloc] initWithCommute:com];
}

-(void)addAltEveningCommute:(Commute *) com{
	self.altEvening = [[Commute alloc] initWithCommute:com];
}
-(id)initWithUser:(User *)u{
	self.userId = u.userId;
	self.carrierId = u.carrierId;
	self.accountExists = u.accountExists;
	self.morningExists = u.morningExists;
	self.altMorningExists = u.altMorningExists;
	self.eveningExists = u.eveningExists;
	self.altEveningExists = u.altEveningExists;
	self.infoExists = u.infoExists;
	self.ani = u.ani;
	self.firstName = u.firstName;
	self.lastName = u.lastName;
	self.carrierEmail = u.carrierEmail;
	self.email = u.email;
	[self addMorningCommute:u.morning];
	[self addAltMorningCommute:u.altMorning];
	[self addEveningCommute:u.evening];
	[self addAltEveningCommute:u.altEvening];
	[self createUserInfo];
	return self;
}
-(void)createUserInfo{
	userInfo = [[NSMutableArray alloc] init];
	[userInfo addObject:[NSString stringWithFormat:@"User ID = %d", userId]];
	[userInfo addObject:[NSString stringWithFormat:@"CarrierID = %d",carrierId]];
	[userInfo addObject:[NSString stringWithFormat:@"AccountExists = %d",accountExists]];
	[userInfo addObject:[NSString stringWithFormat:@"morningExists = %d",morningExists]];
	[userInfo addObject:[NSString stringWithFormat:@"altMorningExists = %d",altMorningExists]];	
	[userInfo addObject:[NSString stringWithFormat:@"eveningExists = %d",eveningExists]];
	[userInfo addObject:[NSString stringWithFormat:@"altEveningExists = %d",altEveningExists]];
	[userInfo addObject:[NSString stringWithFormat:@"infoExists = %d",infoExists]];
	[userInfo addObject:[NSString stringWithFormat:@"firstName = %@",firstName]];
	[userInfo addObject:[NSString stringWithFormat:@"lastName = %@",lastName]];
	[userInfo addObject:[NSString stringWithFormat:@"ANI = %@",ani]];
	[userInfo addObject:[NSString stringWithFormat:@"carrierEmail = %@",carrierEmail]];
	[userInfo addObject:[NSString stringWithFormat:@"email = %@",email]];

}

@end
