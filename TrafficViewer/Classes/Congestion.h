//
//  Congestion.h
//  SimpleCommuteParser
//
//  Created by sfernald on 9/18/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface Congestion : NSObject {
	int sequence, mph;
	NSString * exitName;
	NSString * exitWaveFile;
	BOOL isIncident;

}

@property int sequence;
@property int mph;
@property BOOL isIncident;
@property (nonatomic,retain) NSString * exitName;
@property (nonatomic,retain) NSString * exitWaveFile;

-(id)initWithName:(NSString *)en wavFile:(NSString *)ewf sequence:(int)seq mph:(int)speed;

-(id)initWithCongestion:(Congestion *)c;

@end
