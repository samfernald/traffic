//
//  CommuteCreationViewController.h
//  TrafficViewer
//
//  Created by Sam Fernald on 8/31/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "DailyReportViewController.h"
#import "AlertViewController.h"
#import "SegmentCreationViewController.h"
#import "Alert.h"
#import "DailyReport.h"
#import "SegmentProfile.h"

@interface CommuteCreationViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate, UIAlertViewDelegate>{
    AppDelegate *appDelegate;
    IBOutlet UITableView *commuteProfile;
    NSMutableArray *commuteParts;
    NSMutableArray *addParts;
    
    UIPickerView * typePicker;
    UIPickerView * highwayPicker;
    UIPickerView * onRampPicker;
    UIPickerView * offRampPicker;
    
    UIActionSheet *actionSheetType;
    UIActionSheet *actionSheetHighway;
    UIActionSheet *actionSheetOnRamp;
    UIActionSheet *actionSheetOffRamp;
    
    NSMutableArray *typeList;
    NSMutableArray *highwayList;
    NSMutableArray *highwayCompareList;
    NSMutableArray *exitOnList;
    NSMutableArray *exitOffList;
    NSMutableArray *exits;

    
    NSURLConnection *conn;
    NSMutableData *receivedData;
    NSMutableData *receivedIDData;
    NSMutableData *receivedNewCommuteData;
    NSMutableArray *segmentList;
    UISegmentedControl *segmentedControl;
    
    SegmentProfile *selectedSegment;
    
    BOOL isEditingSegments;
    BOOL isAdding;
    BOOL typeSelected;
    BOOL hwySelected;
    BOOL onRampSelected;
    BOOL offRampSelected;
    BOOL editsDisplayed;
    BOOL commuteTypeSet;
    
    int commuteType;
    int highway;
    int onRampID;
    int offRampID;
    int onRampIndex;
    int offRampIndex;
    int selectedSegmentID;
    int seqToSend;
    int numMorning;
    int numAltMorning;
    int numEvening;
    int numAltEvening;
    
    int connectionType;

}

-(void) editSegment;
-(void) deleteSegment;

-(IBAction) selectType:(id)sender;
-(IBAction) selectHighway:(id)sender;
-(IBAction) selectOnRamp:(id)sender;
-(IBAction) selectOffRamp:(id)sender;
-(IBAction) addSegment:(id)sender;

-(void) selectedType:(id)sender;
-(void) selectedHighway:(id)sender;
-(void) selectedOnRamp:(id)sender;
-(void) selectedOffRamp:(id)sender;

-(void) removePartIn:(NSMutableArray*)parts forType:(int) t;

@end
