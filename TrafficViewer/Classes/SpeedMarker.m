//
//  SpeedMarker.m
//  TrafficViewer
//
//  Created by sfernald on 3/15/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SpeedMarker.h"


@implementation SpeedMarker
@synthesize highway;
@synthesize mph;
@synthesize hwyExitId;


-(id)initWithHwyExitId:(int)hei hwyId:(int)hwy speed:(int)speed{
	self.highway = hwy;
	self.mph = speed;
	self.hwyExitId = hei;
	return self;
}

@end
