//
//  User.h
//  SimpleCommuteParser
//
//  Created by sfernald on 9/18/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Commute.h"



@interface User : NSObject {
	int userId,carrierId;
	bool accountExists,morningExists,altMorningExists,eveningExists,altEveningExists,infoExists;
	NSString * ani;
	NSString * firstName;
	NSString * lastName;
	NSString * carrierEmail;
	NSString * email;
	Commute * morning;
	Commute * altMorning;
	Commute * evening;
	Commute * altEvening;
	NSMutableArray * userInfo;
}

@property int userId;
@property int carrierId;
@property bool accountExists;
@property bool morningExists;
@property bool altMorningExists;
@property bool eveningExists;
@property bool altEveningExists;

@property bool infoExists;
@property (nonatomic, retain) NSString * ani;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * carrierEmail;
@property (nonatomic, retain) NSString * email;

@property (nonatomic, retain) Commute * morning;
@property (nonatomic, retain) Commute * evening;
@property (nonatomic, retain) Commute * altMorning;
@property (nonatomic, retain) Commute * altEvening;

@property (nonatomic,retain) NSMutableArray * userInfo;

-(id)initWithInfo:(int)ui 
		  carrier:(int)ci 
	accountExists:(bool)ae 
	morningExists:(bool)me
altMorningExists:(bool)ame 
	eveningExists:(bool)ee 
 altEveningExists:(bool)aee 
	   infoExists:(bool)ie 
			  ani:(NSString*)ani 
		firstName:(NSString*)fn
		 lastName:(NSString*)ln 
	 carrierEmail:(NSString*)ce 
			email:(NSString*)email;

-(id)initWithUser:(User*)u;

-(void)addMorningCommute:(Commute *) com;
-(void)addEveningCommute:(Commute *) com;
-(void)addAltMorningCommute:(Commute *) com;
-(void)addAltEveningCommute:(Commute *) com;
-(void)createUserInfo;



@end
