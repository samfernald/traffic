//
//  Alert.m
//  TrafficViewer
//
//  Created by Sam Fernald on 9/10/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import "Alert.h"

@implementation Alert

@synthesize alertID, congestion, commuteType, start, end, notify;

-(id) initWithID:(int)ai 
            type:(int)t 
           start:(NSString *)s 
             end:(NSString *)e 
      congestion:(int)c 
          notify:(int)n{
    self.alertID = ai;
    self.commuteType = t;
    self.start = s;
    self.end = e;
    self.congestion = c;
    self.notify = n;
    return self;
}

@end
