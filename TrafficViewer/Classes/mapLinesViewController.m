//
//  mapLinesViewController.m
//  mapLines
//
//  Created by Craig on 4/12/09.
//  Copyright Craig Spitzkoff 2009. All rights reserved.
//

#import "mapLinesViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "CSMapRouteLayerView.h"

@implementation mapLinesViewController
@synthesize routeView = _routeView;
@synthesize mapView   = _mapView;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	//
	// load the points from our local resource
	//
	NSString* filePath = [[NSBundle mainBundle] pathForResource:@"routeLA" ofType:@"csv"];
	//NSString* filePath2 = [[NSBundle mainBundle] pathForResource:@"route" ofType:@"csv"];
	NSString* fileContents = [NSString stringWithContentsOfFile:filePath];
	//NSString* fileContents2 = [NSString stringWithContentsOfFile:filePath2];
	NSArray* pointStrings = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	//NSArray* pointStrings2 = [fileContents2 componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	NSMutableArray* points = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
	NSMutableArray* points2 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
	NSMutableArray* points3 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
	NSMutableArray* points4 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
	NSMutableArray* points5 = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
	NSMutableArray* pointDetails = [[NSMutableArray alloc] initWithCapacity:pointStrings.count];
	//NSLog(@"point length %d",pointStrings2.count);
	
	
	for(int idx = 0; idx < pointStrings.count; idx++)
	{
		// break the string down even further to latitude and longitude fields. 
		NSString* currentPointString = [pointStrings objectAtIndex:idx];
		//NSString* currentPointString2 = [pointStrings2 objectAtIndex:idx];
		NSArray* latLonArr = [currentPointString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
		//NSArray* latLonArr2 = [currentPointString2 componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
		
		CLLocationDegrees latitude  = [[latLonArr objectAtIndex:0] doubleValue];
		CLLocationDegrees longitude = [[latLonArr objectAtIndex:1] doubleValue];
		CLLocationDegrees latitude2 = [[latLonArr objectAtIndex:2] doubleValue];
		CLLocationDegrees longitude2 = [[latLonArr objectAtIndex:3] doubleValue];
		CLLocationDegrees latitude3 = [[latLonArr objectAtIndex:4] doubleValue];
		CLLocationDegrees longitude3 = [[latLonArr objectAtIndex:5] doubleValue];
		CLLocationDegrees latitude4 = [[latLonArr objectAtIndex:6] doubleValue];
		CLLocationDegrees longitude4 = [[latLonArr objectAtIndex:7] doubleValue];
		CLLocationDegrees latitude5 = [[latLonArr objectAtIndex:8] doubleValue];
		CLLocationDegrees longitude5 = [[latLonArr objectAtIndex:9] doubleValue];
		pointInfo * pointInfoTemp = [[[pointInfo alloc] initWithHwy:[[latLonArr objectAtIndex:10] intValue] speed:[[latLonArr objectAtIndex:11] intValue]]autorelease];
		[pointDetails addObject:pointInfoTemp];
		CLLocation* currentLocation = [[[CLLocation alloc] initWithLatitude:latitude longitude:longitude] autorelease];
		CLLocation* currentLocation2 = [[[CLLocation alloc] initWithLatitude:latitude2 longitude:longitude2] autorelease];
		CLLocation* currentLocation3 = [[[CLLocation alloc] initWithLatitude:latitude3 longitude:longitude3] autorelease];
		CLLocation* currentLocation4 = [[[CLLocation alloc] initWithLatitude:latitude4 longitude:longitude4] autorelease];
		CLLocation* currentLocation5 = [[[CLLocation alloc] initWithLatitude:latitude5 longitude:longitude5] autorelease];
		[points addObject:currentLocation];
		[points2 addObject:currentLocation2];
		[points3 addObject:currentLocation3];
		[points4 addObject:currentLocation4];
		[points5 addObject:currentLocation5];
		/*
		 pull speed based on closest ID and add to array: 
		 NSNumber speed = [[latLonArr objectAtIndex:2] intValue];
		 [points addObject:speed];
		 */
	}
	

	//
	// Create our map view and add it as as subview. 
	//
	_mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
	[self.view addSubview:_mapView];
	
	// create our route layer view, and initialize it with the map on which it will be rendered. 
	_routeView = [[CSMapRouteLayerView alloc] initWithPoints:points pointsZoom1:points2 pointsZoom2:points3 pointsZoom3:points4 pointsZoom4:points5 pointDetails:pointDetails mapView:_mapView];
	
	[points release];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	self.mapView   = nil;
	self.routeView = nil;
}


- (void)dealloc {	
    [super dealloc];
}

@end
