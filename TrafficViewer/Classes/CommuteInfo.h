//
//  CommuteInfo.h
//  TrafficViewer
//
//  Created by sfernald on 11/5/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CommuteInfo : NSObject {
	int currentTime;
	int averageTime;
	int otherTime;
	float difference;
	float distance;
	BOOL commuteType;
}

@property int currentTime;
@property int averageTime;
@property int otherTime;
@property float difference;
@property float distance;
@property BOOL commuteType;

-(id) initWithCurrent:(int)currTime averageTime:(int)ave otherTime:(int)other difference:(float)diff distance:(float)dist commuteType:(BOOL)type;

@end
