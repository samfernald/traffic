//
//  Segment.m
//  SimpleCommuteParser
//
//  Created by sfernald on 9/18/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "Segment.h"


@implementation Segment
@synthesize segmentId,onramp,offramp,threshold,hwyId,numCongestions;
@synthesize startDistance,endDistance;
@synthesize hwyName,hwyWavFile;
@synthesize congested,incidentExists;
@synthesize congestions;

-(id)initWithID:(int)segId 
		 onramp:(int)onr 
		offramp:(int)offr
	  threshold:(int)thres 
		  hwyId:(int)hid 
  startDistance:(float)start 
	endDistance:(float)end 
		hwyName:(NSString *)hwynm
	 hwyWavFile:(NSString *)hwf 
	  congested:(bool)constd 
 incidentExists:(bool)incex{
	self.segmentId = segId;
	self.onramp = onr;
	self.offramp = offr;
	self.threshold = thres;
	self.hwyId = hid;
	self.startDistance = start;
	self.endDistance = end;
	self.hwyName = hwynm;
	self.hwyWavFile = hwf;
	self.congested =constd;
	self.incidentExists = incex;
	congestions = [[NSMutableArray alloc]init];
	return self;
}
-(id)initWithSegment:(Segment *)s{
	self.segmentId = s.segmentId;
	self.onramp = s.onramp;
	self.offramp = s.offramp;
	self.threshold = s.threshold;
	self.hwyId = s.hwyId;
	self.startDistance = s.startDistance;
	self.endDistance = s.endDistance;
	self.hwyName = s.hwyName;
	self.hwyWavFile = s.hwyWavFile;
	self.congested = s.congested;
	self.incidentExists = s.incidentExists;
	self.numCongestions = s.numCongestions;
	self.congestions = s.congestions;
	return self;
}
-(void) addCongestion:(Congestion *)cong{
	[congestions addObject:[[Congestion alloc]initWithCongestion:cong]];
	numCongestions = [congestions count];
}

@end
