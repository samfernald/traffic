//
//  ProbeReading.h
//  TrafficViewer
//
//  Created by Sam Fernald on 4/10/13.
//
//

#import <Foundation/Foundation.h>

@interface ProbeReading : NSObject{
    double lat;
    double lng;
    double velo;
}

@property double lat;
@property double lng;
@property double velo;

-(id) initWithlat:(double)latt long:(double)lngg speed:(double)speedd;


@end
