//
//  IncidentPin.m
//  TrafficViewer
//
//  Created by sfernald on 8/3/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "IncidentPin.h"


@implementation IncidentPin

@synthesize title;
@synthesize coordinate;
@synthesize description;
@synthesize postDate;


-(id)initWithCoordinate:(CLLocationCoordinate2D)c title:(NSString *)title description:(NSString *)des postdate:(NSString *) date{
	coordinate=c;
	self.title = title;
	self.description = des;
	self.postDate = date;
	return self;
}

@end
