//
//  HighwayFlowViewController.h
//  TrafficViewer
//
//  Created by sfernald on 10/29/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "GAITrackedViewController.h"


@interface HighwayFlowViewController : UITableViewController {
	
	IBOutlet UIBarButtonItem *refreshButton;
	
	NSMutableArray *displayCells;
	NSMutableArray *congestionInfo;
	NSMutableArray *highwayInfo;
	NSMutableArray *incidentInfo;
	NSString * URLForTraffic;
	NSMutableData * receivedData;
	NSURLConnection * conn;
	UIAlertView *downloadingMessage;
	NSDate *lastRefresh;
	NSString * speedData;
}

@property(nonatomic,retain) NSDate *lastRefresh;
@property(nonatomic,retain) UIBarButtonItem * refreshButton;
@property(nonatomic,retain) NSString * speedData;


-(void)prepareDisplayCells:(NSMutableArray *) trafficFlow;





@end
