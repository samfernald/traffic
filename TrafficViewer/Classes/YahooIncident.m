//
//  YahooIncident.m
//  TrafficViewer
//
//  Created by sfernald on 8/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "YahooIncident.h"


@implementation YahooIncident
@synthesize title;
@synthesize description;
@synthesize postDate;
@synthesize latitude;
@synthesize longitude;

-(id)initWithTitle:(NSString *)title description:(NSString *)des postDate:(NSString *)pd latitude:(float)latitude longitude:(float)longitude{
	self.title = title;
	self.description = des;
	self.postDate = postDate;
	self.latitude = latitude;
	self.longitude = longitude;
	return self;
}

-(id)initWithCongestion:(YahooIncident *)yi {
	self.title = yi.title;
	self.description = yi.description;
	self.postDate = yi.postDate;
	self.latitude = yi.latitude;
	self.longitude = yi.longitude;
	return self;
}


@end
