


//CURRENTLY USING THIS AS THE DATA SOURCE DELEGATE


#import "CPDStockPriceStore.h"


@interface CPDStockPriceStore ()


@end

@implementation CPDStockPriceStore

#pragma mark - Class methods

+ (CPDStockPriceStore *)sharedInstance
{
    static CPDStockPriceStore *sharedInstance;
    
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

#pragma mark - API methods




//should be calles daysOfWeek
- (NSArray *)datesInWeek
{
    static NSArray *dates = nil;
    if (!dates)
    {
        dates = [NSArray arrayWithObjects:
                 @"Monday",
                 @"Tuesday",
                 @"Wednesday",
                 @"Thursday",
                 @"Friday",
//                 @"Saturday",
//                 @"Sunday",
                 nil];
    }
    return dates;
}


//was called  datesInMonth
- (NSArray *)xAxisLabels
{
    static NSArray *dates = nil;
    if (!dates)
    {
        dates = [NSArray arrayWithObjects:
                 @"4",
                 @"5",
                 @"6",
                 @"7",
                 @"8",// number 5
                 @"10",
//                 @"11",
//                 @"12",
//                 @"1",
//                 @"2",//10
//                 @"3",
//                 @"4",
//                 @"5",
//                 @"6",
//                 @"7",// number 15
//                 @"8",
                 nil];
    }
    return dates;
}



#pragma mark - Values for wait times



#pragma mark - ScatterPlot Datasources


+(NSMutableArray *)generateData
{
        NSMutableArray *week = [[NSMutableArray alloc] init];
        for (int i = 0; i < 5; i++) {
            NSMutableArray *day = [[NSMutableArray alloc] init];
            
            for (int j = 0; j < 16; j++) {
                NSNumber *value = [NSNumber numberWithInt:arc4random() % 35 + 50];
                [day addObject:value];
            }
            [week addObject:day];
        }
        
        NSLog(@"value of 0,4 is  %@: ", [[week objectAtIndex:0] objectAtIndex:4] );
        NSLog(@"passed");
        
        return week;
}

    
    


#pragma -

@end
