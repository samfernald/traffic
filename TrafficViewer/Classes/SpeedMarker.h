//
//  SpeedMarker.h
//  TrafficViewer
//
//  Created by sfernald on 3/15/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SpeedMarker : NSObject {
	int highway;
	int mph;
	int hwyExitId;
	
}

@property int highway;
@property int mph;
@property int hwyExitId;

-(id)initWithHwyExitId:(int)hei hwyId:(int)hwy speed:(int)speed;

@end
