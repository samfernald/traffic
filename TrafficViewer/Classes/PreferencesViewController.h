//
//  PreferencesViewController.h
//  TrafficViewer
//
//  Created by sfernald on 7/31/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


@interface PreferencesViewController : UIViewController {
    AppDelegate *appDelegate;
	IBOutlet UISegmentedControl *startScreen;
	IBOutlet UISegmentedControl *startMap;
	IBOutlet UILabel *cityLabel;
	IBOutlet UILabel *trafficMapOps;
	IBOutlet UILabel *loadMapFaster;
    IBOutlet UISwitch *googleAnalyticsSwitch;

}

@property (nonatomic, retain) UISegmentedControl * startMap;
@property (nonatomic, retain) UISegmentedControl * startScreen;
@property (nonatomic, retain) UILabel * cityLabel;
@property (nonatomic, retain) UILabel * trafficMapOps;
@property (nonatomic, retain) UILabel * loadMapFaster;


-(void) setLoadScreen:(id) sender;

//-(void) setLoadMap:(id) sender;
	
@end
