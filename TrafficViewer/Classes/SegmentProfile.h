//
//  SegmentProfile.h
//  TrafficViewer
//
//  Created by Sam Fernald on 9/10/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SegmentProfile : NSObject{
    int segmentID;
    int onRamp;
    int offRamp;
    int commuteType;
    int sequence;
    NSString *highway;
    NSString *onRampName;
    NSString *offRampName;
}

@property int segmentID;
@property int onRamp;
@property int offRamp;
@property int commuteType;
@property int sequence;
@property (nonatomic, retain) NSString * highway;
@property (nonatomic, retain) NSString * onRampName;
@property (nonatomic, retain) NSString * offRampName;

-(id) initWithID:(int)sid 
          onRamp:(int)on 
         offRamp:(int)off
     commuteType:(int)t 
        sequence:(int)s 
         highway:(NSString *)h 
      onRampName:(NSString*)onrn 
     offRampName:(NSString*)offrn;

@end
