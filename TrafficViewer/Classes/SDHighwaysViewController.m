//
//  SDHighwaysViewController.m
//  TrafficViewer
//
//  Created by sfernald on 10/29/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "SDHighwaysViewController.h"
#import "AppDelegate.h"


@implementation SDHighwaysViewController

@synthesize highwayFlowViewController;

/*
// Override initWithNibName:bundle: to load the view using a nib file then perform additional customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view.
- (void)viewDidLoad {
    [super viewDidLoad];
	[self setTitle:@"San Diego Highways"];
	self.navigationItem.hidesBackButton= YES;
	UIScrollView * scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 160)];//(0,0,320,160)
	//scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
		
	[scrollView setContentSize:CGSizeMake(320,500)];	
	
	scrollView.bounces = NO;
	scrollView.scrollEnabled = YES;
	[self.view setBackgroundColor:[[UIColor colorWithRed: 249.0/255.0 green:207.0/255.0 blue:28.0/255.0 alpha:1]retain]];
	//self.view.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"TrafficAppBackground.png"]];
	[scrollView addSubview:self.view];
	self.view = scrollView;
	[scrollView retain];

	
}



-(IBAction) highwaySelected:(id)sender{
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	appDelegate.selectedHighway = [sender tag];
	switch([sender tag]){
		case 1: {appDelegate.selectedHighwayImagePath = @"5.png"; break;}
		case 2: {appDelegate.selectedHighwayImagePath = @"5.png"; break;}
		case 3: {appDelegate.selectedHighwayImagePath = @"8.png"; break;}
		case 4: {appDelegate.selectedHighwayImagePath = @"8.png"; break;}
		case 5: {appDelegate.selectedHighwayImagePath = @"15.png"; break;}
		case 6: {appDelegate.selectedHighwayImagePath = @"15.png"; break;}
		case 7: {appDelegate.selectedHighwayImagePath = @"52.png"; break;}
		case 8: {appDelegate.selectedHighwayImagePath = @"52.png"; break;}
		case 9: {appDelegate.selectedHighwayImagePath = @"54.png"; break;}
		case 10: {appDelegate.selectedHighwayImagePath = @"54.png"; break;}
		case 11: {appDelegate.selectedHighwayImagePath = @"56.png"; break;}
		case 12: {appDelegate.selectedHighwayImagePath = @"56.png"; break;}
		case 13: {appDelegate.selectedHighwayImagePath = @"78.png"; break;}
		case 14: {appDelegate.selectedHighwayImagePath = @"78.png"; break;}
		case 15: {appDelegate.selectedHighwayImagePath = @"94.png"; break;}
		case 16: {appDelegate.selectedHighwayImagePath = @"94.png"; break;}
		case 17: {appDelegate.selectedHighwayImagePath = @"125.png"; break;}
		case 18: {appDelegate.selectedHighwayImagePath = @"125.png"; break;}
		case 19: {appDelegate.selectedHighwayImagePath = @"163.png"; break;}
		case 20: {appDelegate.selectedHighwayImagePath = @"163.png"; break;}
		case 21: {appDelegate.selectedHighwayImagePath = @"805.png"; break;}
		case 22: {appDelegate.selectedHighwayImagePath = @"805.png"; break;}
	}
	HighwayFlowViewController *speedsView = [[HighwayFlowViewController alloc] initWithNibName:@"HighwayFlowViewController" bundle:[NSBundle mainBundle]];
	[[self navigationController] pushViewController:speedsView animated:YES];
}
/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


- (void)dealloc {
    [super dealloc];
}


@end
