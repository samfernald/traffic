/*
 * Copyright (c) 2008, eSpace Technologies.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution.
 * 
 * Neither the name of eSpace nor the names of its contributors may be used 
 * to endorse or promote products derived from this software without specific 
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "MapWebView.h"
#define DEFAULT_ZOOM_LEVEL	10

@interface MapWebView (Private)
- (void) loadMap;
@end

@implementation MapWebView

//-- Public Methods ------------------------------------------------------------
@synthesize mDelegate;
//------------------------------------------------------------------------------
- (void) didMoveToSuperview {
    // this hook method is used to initialize the view; we don't want 
    // any user input to be delivered to the UIWebView, instead, the 
    // MapView overlay will receive all input and convert it to commands 
    // that can be performed by the Google Maps Javascript API directly
    
    self.userInteractionEnabled = NO;
    self.scalesPageToFit = NO;
    self.autoresizingMask = 
	UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [self loadMap];
}
//------------------------------------------------------------------------------
- (NSString*) evalJS:(NSString*)script {
    return [self stringByEvaluatingJavaScriptFromString:script];
}
//------------------------------------------------------------------------------
- (void) moveByDx:(int)dX dY:(int)dY {
    int centerX = ((int)[self bounds].size.width) >> 1;
    int centerY = ((int)[self bounds].size.height) >> 1;
    [self setCenterWithPixel:GPointMake(centerX - dX, centerY - dY)];
}

//-- Methods corresponding to Google Maps Javascript API methods ---------------
- (int) getZoom {
	NSLog(@"1");
    return [[self evalJS:@"map.getZoom();"] intValue];
}
//------------------------------------------------------------------------------
- (void) setZoom:(int)level {
	NSLog(@"2");

    [self evalJS:[NSString stringWithFormat:@"map.setZoom(%d);", level]];
    
    if (self.delegate)
        [self.delegate mapZoomUpdatedTo:level];
}
//------------------------------------------------------------------------------
- (void) zoomIn {
	NSLog(@"3");

    [self evalJS:@"map.zoomIn();"];
    
    if (self.delegate)
        [self.delegate mapZoomUpdatedTo:[self getZoom]];
}
//------------------------------------------------------------------------------
- (void) zoomOut {
	NSLog(@"4");

    [self evalJS:@"map.zoomOut();"];
    
    if (self.delegate)
        [self.delegate mapZoomUpdatedTo:[self getZoom]];
}
//------------------------------------------------------------------------------
- (void) setCenterWithPixel:(GPoint)pixel {
	NSLog(@"5");

    NSString *script = 
	[NSString stringWithFormat:
	 @"var newCenterPixel = new GPoint(%ld, %ld);"
	 "var newCenterLatLng = map.fromContainerPixelToLatLng(newCenterPixel);"
	 "map.setCenter(newCenterLatLng);", 
	 pixel.x, pixel.y];
    
    [self evalJS:script];
    
    if (self.delegate)
        [self.delegate mapCenterUpdatedToPixel:pixel];
}
//------------------------------------------------------------------------------
- (void) setCenterWithLatLng:(GLatLng)latlng {
	NSLog(@"6");

    NSString *script = 
	[NSString stringWithFormat:
	 @"var newCenterLatLng = map.fromContainerPixelToLatLng(newCenterPixel);"
	 "map.setCenter(new GLatLng(%lf, %lf));", 
	 latlng.lat, latlng.lng];
    
    [self evalJS:script];
    
    if (self.delegate)
        [self.delegate mapCenterUpdatedToLatLng:latlng];
}
//------------------------------------------------------------------------------
- (GLatLng) getCenterLatLng {
	NSLog(@"7");

    // the result should be in the form "(<latitude>, <longitude>)"
    NSString *centerStr = [self evalJS:@"map.getCenter().toString();"];
    
    GLatLng latlng;
    sscanf([centerStr UTF8String], "(%lf, %lf)", &latlng.lat, &latlng.lng);
    
    return latlng;
}
//------------------------------------------------------------------------------
- (GPoint) getCenterPixel {
	NSLog(@"8");

    // the result should be in the form "(<x>, <y>)"
    NSString *centerStr = 
	[self evalJS:@"map.fromLatLngToContainerPixel(map.getCenter()).toString();"];
    
    GPoint pixel;
    sscanf([centerStr UTF8String], "(%ld, %ld)", &pixel.x, &pixel.y);
    
    return pixel;
}
//------------------------------------------------------------------------------
- (void) panToCenterWithPixel:(GPoint)pixel {
	NSLog(@"9");

    NSString *script = 
	[NSString stringWithFormat:
	 @"var newCenterPixel = new GPoint(%ld, %ld);"
	 "var newCenterLatLng = map.fromContainerPixelToLatLng(newCenterPixel);"
	 "map.panTo(newCenterLatLng);"
	 "map.zoomIn();", 
	 pixel.x, pixel.y];
    
    [self evalJS:script];
    
    if (self.delegate) {
        [self.delegate mapZoomUpdatedTo:[self getZoom]];
        [self.delegate mapCenterUpdatedToPixel:[self getCenterPixel]];
    }
}
//------------------------------------------------------------------------------
- (GLatLng) fromContainerPixelToLatLng:(GPoint)pixel {
	NSLog(@"10");

    NSString *script = 
	[NSString stringWithFormat:
	 @"map.fromContainerPixelToLatLng(new GPoint(%ld, %ld)).toString();", 
	 pixel.x, pixel.y];
    
    NSString *latlngStr = [self evalJS:script];
    
    GLatLng latlng;
    sscanf([latlngStr UTF8String], "(%lf, %lf)", &latlng.lat, &latlng.lng);
    
    return latlng;
}
//------------------------------------------------------------------------------
- (GPoint) fromLatLngToContainerPixel:(GLatLng)latlng {
	NSLog(@"11");

    NSString *script = 
	[NSString stringWithFormat:
	 @"map.fromLatLngToContainerPixel(new GLatLng(%lf, %lf)).toString();", 
	 latlng.lat, latlng.lng];
    
    NSString *pixelStr = [self evalJS:script];
    
    GPoint pixel;
    sscanf([pixelStr UTF8String], "(%ld, %ld)", &pixel.x, &pixel.y);
    
    return pixel;
}
//------------------------------------------------------------------------------
- (int) getBoundsZoomLevel:(GLatLngBounds)bounds {
	NSLog(@"12");

    NSString *script;
    
    script = 
	[NSString stringWithFormat:
	 @"map.getBoundsZoomLevel(new GLatLngBounds(new GLatLng(%lf, %lf), new GLatLng(%lf, %lf))).toString();", 
	 bounds.minLat, bounds.minLng, bounds.maxLat, bounds.maxLng];
    
    NSString *zoomLevelStr = [self evalJS:script];
    
    int zoomLevel;
    sscanf([zoomLevelStr UTF8String], "%d", &zoomLevel);
    
    return zoomLevel;
}
//------------------------------------------------------------------------------
- (void) setMapType:(NSString*)mapType {
	NSLog(@"13");

    [self evalJS:[NSString stringWithFormat:@"map.setMapType(%@);", mapType]];
}
//------------------------------------------------------------------------------
-(void) addTrafficOverlay {
	NSLog(@"14");

	NSString *script;
	script = @"var trafficInfo = new GTrafficOverlay();map.addOverlay(trafficInfo);";
	[self evalJS:script];
}
//------------------------------------------------------------------------------
-(void) removeTrafficOverlay {
	NSLog(@"15");

	NSString *script;
	script = @"map.removeOverlay(trafficInfo)";
	[self evalJS:script];
}
//------------------------------------------------------------------------------
-(void) addUserMarker {
	NSLog(@"16");

	NSLog(@"adding user location");
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

	NSString *script;
	if(locationController.locationManager.location.coordinate.latitude == 0) {
		float lat, lng;
		if(appDelegate.selectedCity == 1) {
			lat = 32.9;
			lng = -117.24115;
		} else if(appDelegate.selectedCity == 2) {
			lat = 34.0;
			lng = -118.24296;
		} else {
			lat = 37.568528;
			lng = -122.007294;
		}
		script = 
		
		[NSString stringWithFormat:
		 @"map.addOverlay(new GMarker(new GLatLng(%.5f, %.5f)));",lat, lng];
		
	} else {
		script = 
			[NSString stringWithFormat:
			 @"map.addOverlay(new GMarker(new GLatLng(%.5f, %.5f)));",
			 locationController.locationManager.location.coordinate.latitude, locationController.locationManager.location.coordinate.longitude];
	}

	[self evalJS:script];
}

//-- Private Methods -----------------------------------------------------------
- (void) loadMap {
	NSLog(@"17");

	locationController = [[MyCLController alloc] init];
	locationController.delegate = self;
	locationHasUpdated = FALSE;
	[locationController.locationManager startUpdatingLocation];
	
}
//------------------------------------------------------------------------------
- (void)locationUpdate:(CLLocation *)location {
	NSLog(@"18");

	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	int width = (int) self.frame.size.width;
    int height = (int) self.frame.size.height;
	width = 320;
	height = 480;
	if(!locationHasUpdated) {
		locationHasUpdated = TRUE;
		appDelegate.usingLocation = YES;
		//[locationController.locationManager stopUpdatingLocation];

		//appDelegate.lastLocationUpdate = [[NSDate date] retain];
		NSString *urlStr = 
			[NSString stringWithFormat:
			@"http://traffic.calit2.net/sd/TrafficAppMap.jsp?width=%d&height=%d&zoom=%d&latitude=%f&longitude=%f", 
			width, height, DEFAULT_ZOOM_LEVEL,location.coordinate.latitude,location.coordinate.longitude];
			NSLog(@"Lat = %f and long = %f",location.coordinate.latitude,location.coordinate.longitude);

		[self loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
		

		//[appDelegate.notifier dismissWithClickedButtonIndex:0 animated:YES];
	} else {
		NSLog(@"location updated");
		NSTimeInterval timeSinceRefresh = [appDelegate.lastRefresh timeIntervalSinceNow];
		if(timeSinceRefresh <= -60) {
			[appDelegate.lastRefresh release];
			appDelegate.lastRefresh = [[NSDate date] retain];
			
			NSString *urlStr = 
			[NSString stringWithFormat:
			 @"http://traffic.calit2.net/sd/TrafficAppMap.jsp?width=%d&height=%d&zoom=%d&latitude=%f&longitude=%f", 
			 width, height, DEFAULT_ZOOM_LEVEL,location.coordinate.latitude,location.coordinate.longitude];
			NSLog(@"Lat = %f and long = %f",location.coordinate.latitude,location.coordinate.longitude);
			
			[self loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
			
		}
	}
}
-(void) webViewDidStartLoad:(UIWebView*) webView {
	NSLog(@"19");

	NSLog(@"webViewDidStartLoad");
}

-(void) webViewDidFinishLoad:(UIWebView*) webView {
	NSLog(@"20");

	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	NSLog(@"webViewDidFinishLoad");
	[appDelegate.notifier dismissWithClickedButtonIndex:0 animated:YES];
}

//------------------------------------------------------------------------------
- (void)locationError:(NSError*) error {
	NSLog(@"21");

	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[locationController.locationManager stopUpdatingLocation];
	appDelegate.usingLocation = NO;
	int width = (int) self.frame.size.width;
    int height = (int) self.frame.size.height;
	float lat,lng;
	if(appDelegate.selectedCity == 1) {
		lat = 32.9;
		lng = -117.24115;
	} else if(appDelegate.selectedCity == 2) {
		lat = 34.0;
		lng = -118.24296;
	} else {
		lat = 37.568528;
		lng = -122.007294;
	}
	NSString *urlStr = 
		[NSString stringWithFormat:
		@"http://traffic.calit2.net/sd/TrafficAppMap.jsp?width=%d&height=%d&zoom=%d&latitude=%f&longitude=%f", 
		width, height, DEFAULT_ZOOM_LEVEL,lat,lng];
		NSLog(@"Lat = %f and long = %f",lat,lng);
	//[self setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"TrafficAppBackground.png"]]];

	[self loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
	
}
//------------------------------------------------------------------------------
-(void) refreshMap {
	NSLog(@"22");

	NSLog(@"Refreshing Map");
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	int width = (int) self.frame.size.width;
    int height = (int) self.frame.size.height;
	width = 50;
	height = 50;
	NSString *urlStr;
	if(!appDelegate.usingLocation) {
		int width = (int) self.frame.size.width;
		int height = (int) self.frame.size.height;
		width = 50;
		height = 50;
		float lat,lng;
		if(appDelegate.selectedCity == 1) {
			lat = 32.9;
			lng = -117.24115;
		} else if(appDelegate.selectedCity == 2) {
			lat = 34.0;
			lng = -118.24296;
		} else {
			lat = 37.568528;
			lng = -122.007294;
		}
		urlStr = 
		[NSString stringWithFormat:
		 @"http://traffic.calit2.net/sd/TrafficAppMap.jsp?width=%d&height=%d&zoom=%d&latitude=%f&longitude=%f", 
		 width, height, DEFAULT_ZOOM_LEVEL,lat,lng];
		NSLog(@"Lat = %f and long = %f",lat,lng);
	} else {
		urlStr = 
			[NSString stringWithFormat:
			 @"http://traffic.calit2.net/sd/TrafficAppMap.jsp?width=%d&height=%d&zoom=%d&latitude=%f&longitude=%f", 
			 width, height, DEFAULT_ZOOM_LEVEL,locationController.locationManager.location.coordinate.latitude,locationController.locationManager.location.coordinate.longitude];
			NSLog(@"Lat = %f and long = %f",locationController.locationManager.location.coordinate.latitude,locationController.locationManager.location.coordinate.longitude);
	
	}
	//[self setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"TrafficAppBackground.png"]]];

	[self loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
	
	
}
//------------------------------------------------------------------------------


@end
