//
//  ProbeReading.m
//  TrafficViewer
//
//  Created by Sam Fernald on 4/10/13.
//
//

#import "ProbeReading.h"

@implementation ProbeReading

@synthesize lat, lng, velo;

-(id) initWithlat:(double)latt long:(double)lngg speed:(double)speedd {
    self.lat = latt;
    self.lng = lngg;
    self.velo = speedd;
    return self;
}

@end
