//
//  ProfileCell.m
//  TrafficViewer
//
//  Created by Sam Fernald on 9/12/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import "ProfileCell.h"

@implementation ProfileCell

@synthesize displayType, displayText, container;

-(id) initWithDisplay:(NSString*)d type:(int)t connection:(id)o{
    self.displayText = d;
    self.displayType = t;
    self.container = o;
    return self;
}

@end
