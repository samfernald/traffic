//
//  CommuteCell.m
//  AdvancedBlogTutorial
//
//  Created by sfernald on 10/10/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "CommuteCell.h"

static UIImage *hwy10Image = nil;
static UIImage *hwy101Image = nil;
static UIImage *hwy105Image = nil;
static UIImage *hwy110Image = nil;
static UIImage *hwy118Image = nil;
static UIImage *hwy125Image = nil;
static UIImage *hwy133Image = nil;
static UIImage *hwy134Image = nil;
static UIImage *hwy14Image = nil;
static UIImage *hwy15Image = nil;
static UIImage *hwy163Image = nil;
static UIImage *hwy17Image = nil;
static UIImage *hwy170Image = nil;
static UIImage *hwy2Image = nil;
static UIImage *hwy210Image = nil;
static UIImage *hwy215Image = nil;
static UIImage *hwy22Image = nil;
static UIImage *hwy237Image = nil;
static UIImage *hwy238Image = nil;
static UIImage *hwy24Image = nil;
static UIImage *hwy241Image = nil;
static UIImage *hwy242Image = nil;
static UIImage *hwy261Image = nil;
static UIImage *hwy280Image = nil;
static UIImage *hwy37Image = nil;
static UIImage *hwy4Image = nil;
static UIImage *hwy405Image = nil;
static UIImage *hwy5Image = nil;
static UIImage *hwy52Image = nil;
static UIImage *hwy54Image = nil;
static UIImage *hwy55Image = nil;
static UIImage *hwy56Image = nil;
static UIImage *hwy57Image = nil;
static UIImage *hwy580Image = nil;
static UIImage *hwy60Image = nil;
static UIImage *hwy605Image = nil;
static UIImage *hwy680Image = nil;
static UIImage *hwy71Image = nil;
static UIImage *hwy710Image = nil;
static UIImage *hwy73Image = nil;
static UIImage *hwy78Image = nil;
static UIImage *hwy8Image = nil;
static UIImage *hwy80Image = nil;
static UIImage *hwy805Image = nil;
static UIImage *hwy84Image = nil;
static UIImage *hwy85Image = nil;
static UIImage *hwy880Image = nil;
static UIImage *hwy91Image = nil;
static UIImage *hwy92Image = nil;
static UIImage *hwy94Image = nil;
static UIImage *hwy980Image = nil;
static UIImage *hwy1Image = nil;
static UIImage *hwy87Image = nil;
static UIImage *hwy205Image = nil;
static UIImage *hwy505Image = nil;
static UIImage *blueSquare = nil;




@implementation CommuteCell

@synthesize exitNameLabel, speedLabel,commuteLabel, distanceLabel,diffForCommuteLabel,distanceForCommuteLabel,commuteTimeLabel, commuteMinLabel, timeStampLabel, timeStampTime;
@synthesize isCommuteCell, isHighwayCell, showDistance, showCommuteType, hasAverageCommuteTime, isCommuteCellType;
@synthesize switcherForCommute;
@synthesize commuteCellBG,commuteType;
@synthesize speedBar,mphForSpeedBar, speeds;
@synthesize indexForSpeedBars;

+ (void)initialize{
    // The magnitude images are cached as part of the class, so they need to be
    // explicitly retained.
    hwy10Image = [[UIImage imageNamed:@"10.png"]retain];
	hwy101Image = [[UIImage imageNamed:@"101.png"]retain];
	hwy105Image = [[UIImage imageNamed:@"105.png"]retain];
	hwy110Image = [[UIImage imageNamed:@"110.png"]retain];
	hwy118Image = [[UIImage imageNamed:@"118.png"]retain];
	hwy125Image = [[UIImage imageNamed:@"125.png"]retain];
	hwy133Image = [[UIImage imageNamed:@"133.png"]retain];
	hwy134Image = [[UIImage imageNamed:@"134.png"]retain];
	hwy14Image = [[UIImage imageNamed:@"14.png"]retain];
	hwy15Image = [[UIImage imageNamed:@"15.png"]retain];
	hwy163Image = [[UIImage imageNamed:@"163.png"]retain];
	hwy17Image = [[UIImage imageNamed:@"17.png"]retain];
	hwy170Image = [[UIImage imageNamed:@"170.png"]retain];
	hwy2Image = [[UIImage imageNamed:@"2.png"]retain];
	hwy210Image = [[UIImage imageNamed:@"210.png"]retain];
	hwy215Image = [[UIImage imageNamed:@"215.png"]retain];
	hwy22Image = [[UIImage imageNamed:@"22.png"]retain];
	hwy237Image = [[UIImage imageNamed:@"237.png"]retain];
	hwy238Image = [[UIImage imageNamed:@"238.png"]retain];
	hwy24Image = [[UIImage imageNamed:@"24.png"]retain];
	hwy241Image = [[UIImage imageNamed:@"241.png"]retain];
	hwy242Image = [[UIImage imageNamed:@"242.png"]retain];
	hwy261Image = [[UIImage imageNamed:@"261.png"]retain];
	hwy280Image = [[UIImage imageNamed:@"280.png"]retain];
	hwy37Image = [[UIImage imageNamed:@"37.png"]retain];
	hwy4Image = [[UIImage imageNamed:@"4.png"]retain];
	hwy405Image = [[UIImage imageNamed:@"405.png"]retain];
	hwy5Image = [[UIImage imageNamed:@"5.png"]retain];
	hwy52Image = [[UIImage imageNamed:@"52.png"]retain];
	hwy54Image = [[UIImage imageNamed:@"54.png"]retain];
	hwy55Image = [[UIImage imageNamed:@"55.png"]retain];
	hwy56Image = [[UIImage imageNamed:@"56.png"]retain];
	hwy57Image = [[UIImage imageNamed:@"57.png"]retain];
	hwy580Image =[[UIImage imageNamed:@"580.png"]retain];
	hwy60Image = [[UIImage imageNamed:@"60.png"]retain];
	hwy605Image = [[UIImage imageNamed:@"605.png"]retain];
	hwy680Image = [[UIImage imageNamed:@"680.png"]retain];
	hwy71Image = [[UIImage imageNamed:@"71.png"]retain];
	hwy710Image = [[UIImage imageNamed:@"710.png"]retain];
	hwy73Image = [[UIImage imageNamed:@"73.png"]retain];
	hwy78Image = [[UIImage imageNamed:@"78.png"]retain];
	hwy8Image = [[UIImage imageNamed:@"8.png"]retain];
	hwy80Image = [[UIImage imageNamed:@"80.png"]retain];
	hwy805Image = [[UIImage imageNamed:@"805.png"]retain];
	hwy84Image = [[UIImage imageNamed:@"84.png"]retain];
	hwy85Image = [[UIImage imageNamed:@"85.png"]retain];
	hwy880Image = [[UIImage imageNamed:@"880.png"]retain];
	hwy91Image = [[UIImage imageNamed:@"91.png"]retain];
	hwy92Image = [[UIImage imageNamed:@"92.png"]retain];
	hwy94Image = [[UIImage imageNamed:@"94.png"]retain];
	hwy980Image = [[UIImage imageNamed:@"980.png"]retain];
	hwy1Image = [[UIImage imageNamed:@"1.png"] retain];
	hwy87Image = [[UIImage imageNamed:@"87.png"] retain];
	hwy205Image = [[UIImage imageNamed:@"205.png"] retain];
	hwy505Image = [[UIImage imageNamed:@"505.png"] retain];
	blueSquare = [[UIImage imageNamed:@"blueSquare.gif"]retain];
	
}


- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier {
	indexForSpeedBars = 0;
	if (self = [super initWithFrame:frame reuseIdentifier:reuseIdentifier]) {
		UIView *myContentView = self.contentView;
		UIImageView * imgView = [[UIImageView alloc] initWithImage:hwy2Image];
		UIImageView * imgType = [[UIImageView alloc] initWithImage:hwy2Image];
		self.commuteCellBG = imgView;
		self.commuteType = imgType;
		[imgType release];
		[imgView release];
		[myContentView addSubview:self.commuteCellBG];
		[myContentView addSubview:self.commuteType];
		
		self.exitNameLabel = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor clearColor] fontSize:14.0 bold:NO];
		self.exitNameLabel.textAlignment = UITextAlignmentLeft; // default
		[myContentView addSubview:self.exitNameLabel];
		
        self.speedLabel = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor clearColor] fontSize:14.0 bold:YES];
		self.speedLabel.textAlignment = UITextAlignmentRight; 
		[myContentView addSubview:self.speedLabel];
		
		self.commuteLabel = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor clearColor] fontSize:14.0 bold:YES];
		self.commuteLabel.textAlignment = UITextAlignmentLeft; //default
		[myContentView addSubview:self.commuteLabel];
		
		self.commuteTimeLabel = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor clearColor] fontSize:45.0 bold:YES];
		self.commuteTimeLabel.textAlignment = UITextAlignmentCenter;
		[myContentView addSubview:self.commuteTimeLabel];
		
		self.commuteMinLabel = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor clearColor] fontSize:14.0 bold:YES];
		self.commuteMinLabel.textAlignment = UITextAlignmentRight;
		[myContentView addSubview:self.commuteMinLabel];
		
		self.distanceLabel = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor clearColor] fontSize:14.0 bold:YES];
		self.distanceLabel.textAlignment = UITextAlignmentRight;
		[myContentView addSubview:self.distanceLabel];
		
		self.distanceForCommuteLabel = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor clearColor] fontSize:14.0 bold:YES];
		self.distanceForCommuteLabel.textAlignment = UITextAlignmentLeft;
		[myContentView addSubview:self.distanceForCommuteLabel];
		
		self.diffForCommuteLabel = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor clearColor] fontSize:14.0 bold:YES];
		self.diffForCommuteLabel.textAlignment = UITextAlignmentLeft;
		[myContentView addSubview:self.diffForCommuteLabel];
		
		self.speedBar = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor clearColor] fontSize:14.0 bold:YES];
		[myContentView addSubview:self.speedBar];
		
		self.timeStampLabel = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor clearColor] fontSize:14.0 bold:YES];
		self.timeStampLabel.textAlignment = UITextAlignmentLeft;
		[myContentView addSubview:self.timeStampLabel];
		
		self.timeStampTime = [self newLabelWithPrimaryColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:125.0/255.0 alpha:.85] selectedColor:[UIColor clearColor] fontSize:14.0 bold:YES];
		self.timeStampTime.textAlignment = UITextAlignmentLeft;
		[myContentView addSubview:self.timeStampTime];
		
		[myContentView bringSubviewToFront:self.commuteCellBG];
		self.userInteractionEnabled = NO;
	}
	self.hasAverageCommuteTime = FALSE;
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	return self;
}
/*
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
	[super setSelected:selected animated:animated];
}
*/

-(void)setData:(NSString *)exit currentSpeed:(int)mph even:(int)lastCommuteCell isIncident:(BOOL)isIncident{
	isCommuteCell = NO;
	isHighwayCell = NO;
	self.mphForSpeedBar = mph;
	isCommuteCellType = NO;
	self.exitNameLabel.font = [UIFont systemFontOfSize:14.0];
	self.commuteCellBG.image = NULL;
	self.commuteType.image = NULL;
	self.distanceLabel.text = self.commuteMinLabel.text = self.commuteLabel.text = @"";
	self.commuteTimeLabel.text = self.diffForCommuteLabel.text = self.distanceForCommuteLabel.text = @"";
	if(mph!=-200) {
		self.timeStampLabel.text = self.timeStampTime.text = @"";
	}
	self.accessoryView = nil;
	
	[self.commuteLabel setBackgroundColor:[UIColor clearColor]];
	[self.distanceLabel setBackgroundColor:[UIColor clearColor]];
	[self.distanceForCommuteLabel setBackgroundColor:[UIColor clearColor]];
	[self.diffForCommuteLabel setBackgroundColor:[UIColor clearColor]];
	[self.commuteTimeLabel setBackgroundColor:[UIColor clearColor]];
	[self.timeStampLabel setBackgroundColor:[UIColor clearColor]];
	[self.timeStampTime setBackgroundColor:[UIColor clearColor]];
	[self.speedBar setBackgroundColor:[UIColor clearColor]];

	self.exitNameLabel.text = exit;
	self.exitNameLabel.textColor = [UIColor blackColor];
	if(mph <= 0) {
		if(mph == -100) {
			self.exitNameLabel.text = nil;
			self.speedLabel.text = nil;
			self.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 350, 40)];
			//self.accessoryView.backgroundColor = [UIColor colorWithRed:.96 green:.76 blue:.09 alpha:1];
			//UILabel * switchToOtherCommute = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor clearColor] fontSize:14.0 bold:YES];
			self.userInteractionEnabled = YES;
			[self.accessoryView addSubview:switcherForCommute];
		} else if(mph == -50) {
			self.speedLabel.text = @"";
			self.speedLabel.textColor = [UIColor blackColor];
			self.exitNameLabel.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:125.0/255.0 alpha:.85];
			//self.contentView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"TrafficAppBackground.png"]];
			self.contentView.backgroundColor = [UIColor colorWithRed:.96 green:.76 blue:.09 alpha:1];
			self.speedLabel.backgroundColor = [UIColor clearColor];
			self.exitNameLabel.backgroundColor = [UIColor clearColor];

		} else if(mph == -51) {
			self.speedLabel.text = @"";
			self.exitNameLabel.text = @"";
		} else if(mph == -200) {
			self.exitNameLabel.text = nil;
			self.speedLabel.text = nil;
			[self.contentView setBackgroundColor: [UIColor colorWithRed:.96 green:.76 blue:.09 alpha:1]];
		} else if (mph == -201) {
			self.exitNameLabel.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:125.0/255.0 alpha:.85];
			self.exitNameLabel.backgroundColor = [UIColor clearColor];
			//self.contentView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"TrafficAppBackground.png"]];
			self.contentView.backgroundColor = [UIColor colorWithRed:.96 green:.76 blue:.09 alpha:1];
			self.speedLabel.text = nil;
			self.speedLabel.backgroundColor = [UIColor clearColor];
		} else {
			self.speedLabel.text = @"-";
			self.speedLabel.textColor = [UIColor blackColor];
		}
	} else {
		self.speedLabel.text = [NSString stringWithFormat:@"%d", mph];
	}
	if(mph <= 35 && mph > 0) {
		self.speedLabel.textColor = [UIColor redColor];
		self.speedLabel.font = [UIFont boldSystemFontOfSize:16.0];
		self.exitNameLabel.font = [UIFont boldSystemFontOfSize:16.0];
		self.exitNameLabel.textColor = [UIColor redColor];
		[self.speedBar setBackgroundColor:[UIColor clearColor]];
		//[self.speedBar setBackgroundColor:[UIColor colorWithRed:1.0 green:0 blue:0 alpha:.5]];
		[speeds addObject:[NSNumber numberWithInt:mph]];
		
	}else if(mph <= 50 && mph > 0) {
		self.speedLabel.textColor = [UIColor orangeColor];
		self.speedLabel.font = [UIFont boldSystemFontOfSize: 14.0];
		[self.speedBar setBackgroundColor:[UIColor clearColor]];
		//[self.speedBar setBackgroundColor:[UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:.5]];
		[speeds addObject:[NSNumber numberWithInt:mph]];

	} else if(mph > 0) {
		self.speedLabel.textColor = [UIColor colorWithRed:0 green:0.392 blue:0 alpha:1];
		[self.speedBar setBackgroundColor:[UIColor clearColor]];
		//[self.speedBar setBackgroundColor:[UIColor colorWithRed:0 green:0.392 blue:0 alpha:.5]];
		self.speedLabel.font = [UIFont boldSystemFontOfSize: 14.0];
		[speeds addObject:[NSNumber numberWithInt:mph]];
	}
	if(mph !=-50) {
		if(mph != -201) {
			[self.contentView setBackgroundColor:[UIColor whiteColor]];
			self.exitNameLabel.backgroundColor = [UIColor clearColor];
			self.speedLabel.backgroundColor = [UIColor clearColor];
		}
	}
    [self setNeedsDisplay];
}

-(void)sendButtonForCell:(UIButton *)switcher {
	switcherForCommute = switcher;
}

-(void)addTimeStamp:(NSString *) timeStamp {
	self.timeStampLabel.text = [[NSString alloc] initWithFormat:@"Updated: "];
	self.timeStampTime.text = timeStamp;
	self.timeStampTime.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:125.0/255.0 alpha:.85];
	[self.timeStampLabel setBackgroundColor:[UIColor clearColor]];
	[self.timeStampTime setBackgroundColor:[UIColor clearColor]];
}

-(void)setDataForHighway:(NSString *)info{
	if (info != nil) {
		isCommuteCell = NO;
		isHighwayCell = YES;
		isCommuteCellType = NO;
		showDistance = YES;
		showCommuteType = NO;
		NSString *hwy;
		self.timeStampLabel.text = @"";
		self.timeStampTime.text = @"";
		self.accessoryView = nil;
		if([info rangeOfString:@"***" options:NSCaseInsensitiveSearch].location != NSNotFound){
			NSArray *hwyAndDist = [info componentsSeparatedByString:@"***"];
			hwy = [hwyAndDist objectAtIndex:0];
			NSString *dist = [hwyAndDist objectAtIndex:1];
		
			//NSLog([hwyAndDist objectAtIndex:3]);
			NSString *ctype = [hwyAndDist objectAtIndex:3];
			if([hwyAndDist count] == 6) {
				self.timeStampLabel.text = [hwyAndDist objectAtIndex:2];
				if([ctype  compare:@"Morning"]) {
					self.commuteType.image = [[UIImage imageNamed:@"Evening_Commute_Icon.png"] retain];
				} else {
					self.commuteType.image = [[UIImage imageNamed:@"Morning_Commute_Icon.png"] retain];
				}
			} else {
				self.commuteType.image = NULL;
			}
			self.distanceLabel.textColor = [UIColor blackColor];
			NSString *distanceLabelText = [[NSString alloc] initWithFormat:@"Dist: %@mi", dist];
			self.distanceLabel.text = distanceLabelText;
		} else {
			self.commuteType.image = NULL;

			hwy = info;
			showDistance = NO;
			[self.contentView setBackgroundColor: [UIColor colorWithRed:.96 green:.76 blue:.09 alpha:1]];
			//self.distanceLabel.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:125.0/255.0 alpha:.85];
			//self.distanceLabel.text = @"http://traffic.calit2.net";
		}
		//--------
		NSString * highwayIconFilePath;
		NSString * highwayDirection;
		highwayIconFilePath = [hwy substringWithRange:NSMakeRange(0,3)];
		int highwayNumber = [highwayIconFilePath intValue];
		int stringIndex = [hwy length]-5;
		highwayDirection = [hwy substringWithRange:NSMakeRange(stringIndex, 5)];
		//--------
		self.exitNameLabel.font = [UIFont systemFontOfSize:20.0];
		self.exitNameLabel.textColor = [UIColor blackColor];
		@try {
			self.commuteCellBG.image = [self getHighwayIcon:highwayNumber];
		}
		@catch (NSException * e) {
			NSLog(@"Exception %@: %@",[e name], [e reason]);
		}
		@finally {
		}
		self.commuteLabel.text = @"";
		self.commuteTimeLabel.text = @"";
		self.diffForCommuteLabel.text = @"";
		self.distanceForCommuteLabel.text = @"";
		self.accessoryView = nil;
		[self.distanceLabel setBackgroundColor:[UIColor clearColor]];
		[self.commuteLabel setBackgroundColor:[UIColor clearColor]];
		[self.commuteTimeLabel setBackgroundColor:[UIColor clearColor]];
		[self.diffForCommuteLabel setBackgroundColor:[UIColor clearColor]];
		[self.distanceForCommuteLabel setBackgroundColor:[UIColor clearColor]];
		[self.timeStampLabel setBackgroundColor:[UIColor clearColor]];
		self.exitNameLabel.text = highwayDirection;
		self.speedLabel.text = self.commuteMinLabel.text = @"";
		[self.contentView setBackgroundColor: [UIColor colorWithRed: 255.0/255.0 green:255.0/255.0 blue:204.0/255.0 alpha:1]];
		self.exitNameLabel.backgroundColor = [UIColor clearColor];
		self.speedLabel.backgroundColor = [UIColor clearColor];
		self.speedBar.backgroundColor = [UIColor clearColor];
		[self setNeedsDisplay];
	}
}

-(UIImage *) getHighwayIcon:(int) hwyNum{
	switch(hwyNum){
		case 1:{return hwy1Image;}
		case 10:{return hwy10Image;}
		case 101:{return hwy101Image;}
		case 105:{return hwy105Image;}
		case 110:{return hwy110Image;}
		case 118:{return hwy118Image;}
		case 125:{return hwy125Image;}
		case 133:{return hwy133Image;}
		case 134:{return hwy134Image;}
		case 14:{return hwy14Image;}
		case 15:{return hwy15Image;}
		case 163:{return hwy163Image;}
		case 17:{return hwy17Image;}
		case 170:{return hwy170Image;}
		case 2:{return hwy2Image;}
		case 205:{return hwy205Image;}
		case 210:{return hwy210Image;}
		case 215:{return hwy215Image;}
		case 22:{return hwy22Image;}
		case 237:{return hwy237Image;}
		case 238:{return hwy238Image;}
		case 24:{return hwy24Image;}
		case 241:{return hwy241Image;}
		case 242:{return hwy242Image;}
		case 261:{return hwy261Image;}
		case 280:{return hwy280Image;}
		case 37:{return hwy37Image;}
		case 4:{return hwy4Image;}
		case 405:{return hwy405Image;}
		case 5:{return hwy5Image;}
		case 505:{return hwy505Image;}
		case 52:{return hwy52Image;}
		case 54:{return hwy54Image;}
		case 55:{return hwy55Image;}
		case 56:{return hwy56Image;}
		case 57:{return hwy57Image;}
		case 580:{return hwy580Image;}
		case 60:{return hwy60Image;}
		case 605:{return hwy605Image;}
		case 680:{return hwy680Image;}
		case 71:{return hwy71Image;}
		case 710:{return hwy710Image;}
		case 73:{return hwy73Image;}
		case 78:{return hwy78Image;}
		case 8:{return hwy8Image;}
		case 80:{return hwy80Image;}
		case 805:{return hwy805Image;}
		case 84:{return hwy84Image;}
		case 85:{return hwy85Image;}
		case 87:{return hwy87Image;}
		case 880:{return hwy880Image;}
		case 91:{return hwy91Image;}
		case 92:{return hwy92Image;}
		case 94:{return hwy94Image;}
		case 980:{return hwy980Image;}
	}
	return NULL;
}

-(void)setDataForCommuteType:(CommuteType *) c {
	isCommuteCell = NO;
	isHighwayCell = NO;
	isCommuteCellType = YES;
	self.timeStampLabel.text = @"";
	self.timeStampTime.text = @"";
	self.accessoryView = nil;
	showDistance = NO;
	[self.contentView setBackgroundColor: [UIColor colorWithRed:.96 green:.76 blue:.09 alpha:1]];
		//self.distanceLabel.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:125.0/255.0 alpha:.85];
		//self.distanceLabel.text = @"http://traffic.calit2.net";
	
	//--------
	self.exitNameLabel.font = [UIFont systemFontOfSize:20.0];
	self.exitNameLabel.textColor = [UIColor blackColor];
	self.commuteCellBG.image = [c icon];
	self.commuteLabel.text = @"";
	self.commuteTimeLabel.text = @"";
	self.diffForCommuteLabel.text = @"";
	self.distanceForCommuteLabel.text = @"";
	self.accessoryView = nil;
	
	[self.distanceLabel setBackgroundColor:[UIColor clearColor]];
	[self.commuteLabel setBackgroundColor:[UIColor clearColor]];
	[self.commuteTimeLabel setBackgroundColor:[UIColor clearColor]];
	[self.diffForCommuteLabel setBackgroundColor:[UIColor clearColor]];
	[self.distanceForCommuteLabel setBackgroundColor:[UIColor clearColor]];
	[self.timeStampLabel setBackgroundColor:[UIColor clearColor]];
	self.exitNameLabel.text = [c alternate];
	self.speedLabel.text = self.commuteMinLabel.text = @"";
	[self.contentView setBackgroundColor: [UIColor colorWithRed: 255.0/255.0 green:255.0/255.0 blue:204.0/255.0 alpha:1]];
	self.exitNameLabel.backgroundColor = [UIColor clearColor];
	self.speedLabel.backgroundColor = [UIColor clearColor];
	
	[self setNeedsDisplay];
}

-(void)setDataForCommute:(CommuteInfo *)c {
	if (c != nil) {
		isCommuteCell = YES;
		isHighwayCell = NO;
		isCommuteCellType = NO;
		self.exitNameLabel.font = [UIFont boldSystemFontOfSize:14.0];
		self.commuteLabel.font = self.distanceLabel.font = [UIFont boldSystemFontOfSize:14.0];
		self.accessoryView = nil;
		self.commuteCellBG.image = NULL;
		self.commuteType.image = NULL;
		//self.contentView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"TrafficAppBackground.png"]];
		self.contentView.backgroundColor = [UIColor colorWithRed:.96 green:.76 blue:.09 alpha:1];
		self.commuteLabel.backgroundColor = self.commuteMinLabel.backgroundColor = [UIColor clearColor];
		self.commuteTimeLabel.backgroundColor = self.distanceForCommuteLabel.backgroundColor = [UIColor clearColor];
		self.diffForCommuteLabel.backgroundColor = self.distanceLabel.backgroundColor = [UIColor clearColor];
		self.speedLabel.backgroundColor = self.exitNameLabel.backgroundColor = [UIColor clearColor];
		self.timeStampLabel.text = self.timeStampTime.text = @"";
		self.timeStampTime.backgroundColor = self.timeStampLabel.backgroundColor = [UIColor clearColor];
	
		self.speedLabel.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:125.0/255.0 alpha:.85];
		self.commuteLabel.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:125.0/255.0 alpha:.85];
		self.exitNameLabel.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:125.0/255.0 alpha:.85];
		self.distanceLabel.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:125.0/255.0 alpha:.85];
		self.commuteTimeLabel.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:125.0/255.0 alpha:1];
		self.diffForCommuteLabel.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:125.0/255.0 alpha:.85];
		self.distanceForCommuteLabel.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:125.0/255.0 alpha:.85];
		self.commuteMinLabel.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:125.0/255.0 alpha:.85];
		/*
		self.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(3, 0, 70, 80)];
		self.accessoryView.backgroundColor = [UIColor colorWithRed:.96 green:.76 blue:.09 alpha:1];
		UILabel * refreshLabel = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor clearColor] fontSize:14.0 bold:YES];
		refreshLabel.textAlignment = UITextAlignmentCenter;
		refreshLabel.frame = CGRectMake(0, 5, 70, 20);
		refreshLabel.backgroundColor = [UIColor clearColor];
		refreshLabel.text = @"Refresh";
		UIImageView * refreshImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"refresh.png"]];
		refreshImage.frame = CGRectMake(15, 30, 45, 45);
		[self.accessoryView addSubview:refreshLabel];
		[self.accessoryView addSubview:refreshImage];
		*/
	
	
		self.distanceLabel.text = [NSString stringWithFormat:@"Dist: "];
		self.distanceForCommuteLabel.text = [NSString stringWithFormat:@"%.1f mi",c.distance];
		if(c.commuteType) {
			self.commuteLabel.text = [NSString stringWithFormat:@"Commute Time: "];
		} else {
			self.commuteLabel.text = [NSString stringWithFormat:@"Commute Time: "];
		}
		self.commuteTimeLabel.text = [NSString stringWithFormat: @"%d",c.currentTime];
		self.commuteMinLabel.text = @"min";
	

		if(c.averageTime != 0){
			self.hasAverageCommuteTime = TRUE;
			self.exitNameLabel.text = [NSString stringWithFormat:@"Typical: %d min",c.averageTime];
			self.speedLabel.text = [NSString stringWithFormat:@"Diff: "];
			self.diffForCommuteLabel.text = [NSString stringWithFormat:@"%.1f %%",c.difference];
			if(c.difference <= 0) {
				self.diffForCommuteLabel.textColor = [UIColor colorWithRed:0 green:0.392 blue:0 alpha:1];
			}else if(c.difference > 0) {
				self.diffForCommuteLabel.textColor = [UIColor redColor];
			}
		
		} else {
			self.exitNameLabel.text = [NSString stringWithFormat:@""];
			self.speedLabel.text = [NSString stringWithFormat:@""];
			self.diffForCommuteLabel.text = [NSString stringWithFormat:@""];
		}
	}
}


/*
 this function will layout the subviews for the cell
 if the cell is not in editing mode we want to position them
*/
- (void)layoutSubviews {
	
    [super layoutSubviews];
	
	// getting the cell size
    CGRect contentRect = self.contentView.bounds;
	
	// In this example we will never be editing, but this illustrates the appropriate pattern
    if (!self.editing) {
		
		// get the X pixel spot
        CGFloat boundsX = contentRect.origin.x;
		CGRect frame;
		
         /*
		 Place the title label.
		 place the label whatever the current X is plus 10 pixels from the left
		 place the label 4 pixels from the top
		 make the label 200 pixels wide
		 make the label 20 pixels high
		 */
		if(isCommuteCell){
			
			
			/*
			frame = CGRectMake(boundsX , 4, 30, 42);
			UIImageView *imageView = self.commuteCellBackground;
			frame = [imageView frame];
			frame.origin.x = boundsX + 10;
			frame.origin.y = 0;
			imageView.frame = frame;
			*/
			
			frame = CGRectMake(boundsX + 10 , 4, 300, 20);
			self.commuteLabel.frame = frame;
			
			frame = CGRectMake(boundsX + 90, 2, 120, 45);
			self.commuteTimeLabel.frame = frame;
			
			frame = CGRectMake(boundsX + 145, 25, 70, 20);
			self.commuteMinLabel.frame = frame;
			
			frame = CGRectMake(boundsX + 10, 25, 140, 20);
			self.exitNameLabel.frame = frame;
			
			frame = CGRectMake(boundsX + 105, 25, 150, 20);
			self.speedLabel.frame = frame;
			
			frame = CGRectMake(boundsX + 260, 25, 150, 20);
			self.diffForCommuteLabel.frame = frame;
			
			frame = CGRectMake(boundsX + 105, 4, 150, 20);
			self.distanceLabel.frame = frame;
			
			frame = CGRectMake(boundsX + 260, 4, 100, 20);
			self.distanceForCommuteLabel.frame = frame;
			/*
			 frame = CGRectMake(boundsX + 90, 53, 160, 20);
			 self.timeStampLabel.frame = frame;
			 
			 frame = CGRectMake(boundsX + 160, 53, 160, 20);
			 self.timeStampTime.frame = frame;
			 */			
		} else if(isHighwayCell) {
			frame = CGRectMake(boundsX + 60, 5, 200, 35);
			self.exitNameLabel.frame = frame;
			if(showDistance) {
				frame = CGRectMake(boundsX + 150, 12, 160, 20);
				self.distanceLabel.frame = frame;
				UIImageView *imgTypeView = self.commuteType;
				frame = CGRectMake(boundsX, 2, 32, 32);
				frame.origin.x = boundsX + 140;
				frame.origin.y = 2;
				imgTypeView.frame = frame;
				
				frame = CGRectMake(boundsX + 179, 10, 160, 20);
				self.timeStampLabel.frame = frame;
				
			} else {
				frame = CGRectMake(boundsX + 170, 12, 160, 20);
				self.timeStampLabel.frame = frame;
				
				frame = CGRectMake(boundsX + 240, 12, 160, 20);
				self.timeStampTime.frame = frame;
			}
			
			UIImageView *imageView = self.commuteCellBG;
			frame = [imageView frame];
			frame.origin.x = boundsX + 10;
			frame.origin.y = 0;
			imageView.frame = frame;
			
			
			
		} else if (isCommuteCellType) {
			frame = CGRectMake(boundsX + 150, 5, 200, 35);
			self.exitNameLabel.frame = frame;
				
			
			UIImageView *imageView = self.commuteCellBG;
			frame = CGRectMake(boundsX , 5, 30, 30);
			frame.origin.x = boundsX + 100;
			imageView.frame = frame;
			
		} else {
			if(self.accessoryView == nil) {
				if([self.timeStampTime.text isEqualToString:@""]) {
					frame = CGRectMake(boundsX + 10, 5, 275, 20);
					self.exitNameLabel.frame = frame;
					
					// place the url label
					frame = CGRectMake(boundsX + 275, 4, 25, 20);
					self.speedLabel.frame = frame;
					
					int boundExtension = 170 + 2*[self.speedLabel.text integerValue];
					
					frame = CGRectMake(boundsX + boundExtension, 1, 300, 22);

					self.speedBar.frame = frame;
					
				} else {
					frame = CGRectMake(boundsX + 90, 4, 160, 20);
					self.timeStampLabel.frame = frame;
					
					frame = CGRectMake(boundsX + 160, 4, 160, 20);
					self.timeStampTime.frame = frame;
				}
			} 
		}
	}
}

- (UILabel *)newLabelWithPrimaryColor:(UIColor *)primaryColor selectedColor:(UIColor *)selectedColor fontSize:(CGFloat)fontSize bold:(BOOL)bold
{
	/*
	 Create and configure a label.
	 */
	
    UIFont *font;
    if (bold) {
        font = [UIFont boldSystemFontOfSize:fontSize];
    } else {
        font = [UIFont systemFontOfSize:fontSize];
    }
	
    /*
	 Views are drawn most efficiently when they are opaque and do not have a clear background, so set these defaults.  To show selection properly, however, the views need to be transparent (so that the selection color shows through).  This is handled in setSelected:animated:.
	 */
	UILabel *newLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	newLabel.backgroundColor = [UIColor whiteColor];
	newLabel.opaque = YES;
	newLabel.textColor = primaryColor;
	newLabel.highlightedTextColor = selectedColor;
	newLabel.font = font;
	
	return newLabel;
}

- (void)dealloc {
	
	//[exitNameLabel release];
	//[speedLabel release];
	//[commuteLabel release];
	//[distanceLabel release];
	//[distanceForCommuteLabel release];
	//[diffForCommuteLabel release];
	//[commuteTimeLabel release];
	/*
	[hwy10Image release];
	[hwy101Image release];
	[hwy105Image release];
	[hwy110Image release];
	[hwy118Image release]; 
	[hwy125Image release]; 
	[hwy133Image release];
	[hwy134Image release];
	[hwy14Image release];
	[hwy15Image release];
	[hwy163Image release];
	[hwy17Image release];
	[hwy170Image release];
	[hwy2Image release];
	[hwy210Image release];
	[hwy215Image release];
	[hwy22Image release];
	[hwy237Image release];
	[hwy238Image release];
	[hwy24Image release];
	[hwy241Image release];
	[hwy242Image release];
	[hwy261Image release];
	[hwy280Image release];
	[hwy37Image release];
	[hwy4Image release];
	[hwy405Image release];
	[hwy5Image release];
	[hwy52Image release];
	[hwy54Image release];
	[hwy55Image release];
	[hwy56Image release];
	[hwy57Image release];
	[hwy580Image release];
	[hwy60Image release];
	[hwy605Image release];
	[hwy680Image release];
	[hwy71Image release];
	[hwy710Image  release];
	[hwy73Image release];
	[hwy78Image release];
	[hwy8Image release];
	[hwy80Image release];
	[hwy805Image release];
	[hwy84Image release];
	[hwy85Image release];
	[hwy880Image release];
	[hwy91Image release];
	[hwy92Image release];
	[hwy94Image release];
	[hwy980Image release];
	[hwy1Image release];
	[hwy87Image release];
	[hwy205Image release];
	[hwy505Image release];
	[blueSquare release];
	*/
	[super dealloc];
}

@end
