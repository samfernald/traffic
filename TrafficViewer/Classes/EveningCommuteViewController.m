//
//  EveningCommuteViewController.m
//  TrafficViewer
//
//  Created by sfernald on 10/29/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "EveningCommuteViewController.h"
#import "MorningCommuteViewController.h"
#import "CommuteCell.h"
#import "CommuteInfo.h"

@implementation EveningCommuteViewController

@synthesize altButton, morningButton, refreshButton;
@synthesize lastAcceleration;


/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if (self = [super initWithStyle:style]) {
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view.
- (void)viewDidLoad {
	[super viewDidLoad];
	
	morningCommuteSegments = altMorningCommuteSegments = eveningCommuteSegments = altEveningCommuteSegments = segmentsRemaining = 0;
	[[UIAccelerometer sharedAccelerometer] setDelegate:self];
	[[UIAccelerometer sharedAccelerometer] setUpdateInterval: 1/20];
	appDelegate = (TrafficViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	[self setTitle:@"Evening"];
	priAlt = NO;
	altButton = [[UIBarButtonItem alloc]
								   initWithTitle:NSLocalizedString(@"Alternate", @"")
								   style:UIBarButtonItemStyleBordered
								   target:self
								   action:@selector(switchToAlt:)];
	
	refreshButton = [[UIBarButtonItem alloc]
					 initWithTitle:NSLocalizedString(@"Refresh", @"")
					 style:UIBarButtonItemStyleBordered
					 target:self
					 action:@selector(refresh:)];
	
	self.navigationItem.rightBarButtonItem = altButton;
	self.navigationItem.leftBarButtonItem = refreshButton;
	
	if(!appDelegate.userCreated){
		[self createCommuteInfo];
		appDelegate.lastRefresh = [[NSDate date] retain];
		appDelegate.userCreated = TRUE;
	}
	
	[self prepareCommuteCells:appDelegate.user.evening];
	self.tableView.bounces = YES;

	[self.tableView reloadData];
}

- (void)switchToAlt:(id)sender{
	if([altButton title]==@"Alternate"){
		if(appDelegate.user.altEvening.numSegments <= 0) {
			[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
			noCommuteNotifier = [[UIAlertView alloc] initWithTitle:@"" message:@"You have not added an alternate commute.  Login to http://traffic.calit2.net to add one." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[noCommuteNotifier show];
		} else {
			[altButton setTitle:@"Primary"];
			[self setTitle:@"Alternate"];
		
			//load Alt Data
			[self prepareCommuteCells:appDelegate.user.altEvening];
			[self.tableView reloadData];
		}

	} else {
		if(appDelegate.user.evening.numSegments <= 0) {
			[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
			noCommuteNotifier = [[UIAlertView alloc] initWithTitle:@"" message:@"You have not added a commute.  Login to http://traffic.calit2.net to add one." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[noCommuteNotifier show];
		} else {
			[altButton setTitle:@"Alternate"];
			[self setTitle:@"Evening"];
		
			//load Pri Data
			[self prepareCommuteCells:appDelegate.user.evening];
			[self.tableView reloadData];
		}
	}
}

- (void)refresh:(id)sender{
	@try{ 
		NSTimeInterval timeSinceRefresh = [appDelegate.lastRefresh timeIntervalSinceNow];
		
		NSString *downloadMessage;
		if(appDelegate.selectedCity == 1) {
			downloadMessage = [NSString stringWithFormat:@"Speeds updated once every minute."];
		} else {
			downloadMessage = [NSString stringWithFormat:@"Speeds updated every 5 minutes."];
		}
		appDelegate.notifier = [[UIAlertView alloc] initWithTitle:@"Refreshing" message:downloadMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
		UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
		activityView.frame = CGRectMake(139.0f-18.0f, 70.0f, 33.0f, 33.0f);
		[appDelegate.notifier addSubview:activityView];
		[activityView startAnimating];
		[appDelegate.notifier show];
		
		if(timeSinceRefresh <= -60) {
			[appDelegate.lastRefresh release];
			appDelegate.lastRefresh = [[NSDate date] retain];
			congestionInfo = nil;
			NSString *post = @""; 
			NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];  
			
			NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];  
			
			NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
			[request setURL:[NSURL URLWithString:appDelegate.urlForCommute]];
			[request setHTTPMethod:@"POST"];  
			[request setValue:postLength forHTTPHeaderField:@"Content-Length"];  
			[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];  
			[request setHTTPBody:postData];
			NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
			NSArray *cookies = [scs cookies];
			NSEnumerator* e = [cookies objectEnumerator];
			NSHTTPCookie* cookie;
			
			while ((cookie = [e nextObject])) {
				NSString* name = [cookie name];
				if ([name isEqualToString:@"userId"]) {
					[scs deleteCookie:cookie];
				}	
			}
			
			conn=[[NSURLConnection alloc] initWithRequest:request delegate:self]; 
			if (conn) {  
				receivedData = [[NSMutableData data] retain];
			} else {  
				NSLog(@"no conn");
				//[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
			}  
			[request release];
		} else {
			[appDelegate.notifier dismissWithClickedButtonIndex:0 animated:YES];
			[appDelegate.notifier release];
		}
	} @catch (NSException *e) {
		NSLog(@"Failed to Login");
	}
}

- (void)downloadingMessageNotifier {
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc ] init ];
    // Do processor intensive tasks here knowing that it will not block the main thread.
	NSString *downloadMessage;
	if(appDelegate.selectedCity == 1) {
		downloadMessage = [NSString stringWithFormat:@"Speeds updated once every minute."];
	} else {
		downloadMessage = [NSString stringWithFormat:@"Speeds updated every 5 minutes."];
	}
	appDelegate.notifier = [[UIAlertView alloc] initWithTitle:@"Refreshing" message:downloadMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
	UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityView.frame = CGRectMake(139.0f-18.0f, 70.0f, 33.0f, 33.0f);
    [appDelegate.notifier addSubview:activityView];
    [activityView startAnimating];
	[appDelegate.notifier show];	
	[ pool release ];
}

-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{	
	
    if ([challenge previousFailureCount] == 0) {
        NSURLCredential *newCredential;
        newCredential=[NSURLCredential credentialWithUser:appDelegate.userName
                                                 password:appDelegate.password
                                              persistence:NSURLCredentialPersistenceNone];
        [[challenge sender] useCredential:newCredential
               forAuthenticationChallenge:challenge];
		
    } else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        // inform the user that the user name and password
        // in the preferences are incorrect
    }
	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
	// append the new data to the receivedData
	// receivedData is declared as a method instance elsewhere
	
	[receivedData appendData:data];
	NSString * outputString = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
	NSString * middleString = [outputString stringByReplacingOccurrencesOfString:@"<incidents/>" withString:@""];
	NSString * inputString = [middleString stringByReplacingOccurrencesOfString:@"<segments/>" withString:@""];
	
	
	
	NSString *fileAddress = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"commuteInfo.xml"];
	//NSString *fileAddress = @"Users/Sam/Documents/TrafficViewer/commuteInfo.xml";//for simulation
	
	if([inputString writeToFile:fileAddress atomically:FALSE encoding:NSUTF8StringEncoding error:NULL]){
	} else {
		NSLog(@"write failed");
	}
	
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	NSLog(@"conn failed");
	[appDelegate.notifier dismissWithClickedButtonIndex:0 animated:FALSE];
	notifier = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Unable to connect to traffic.calit2.net" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[notifier show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
	
	appDelegate.hasAnAccount = @"YES";
	// do something with the data
    // receivedData is declared as a method instance elsewhere
	//[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
	//[notifier release];
	[self createCommuteInfo];
	if(self.title ==@"Evening") {
		[self prepareCommuteCells:user.evening];
	} else {
		[self prepareCommuteCells:user.altEvening];
	}
	[self.tableView reloadData];
	[appDelegate.notifier dismissWithClickedButtonIndex:0 animated:FALSE];
	[connection release];
    [receivedData release];
}

- (void)switchToMorning:(id)sender{
	MorningCommuteViewController *view = [[MorningCommuteViewController alloc] initWithNibName:@"MorningCommuteViewController" bundle:[NSBundle mainBundle]];
	[[self navigationController] pushViewController:view animated:YES];
	[view release];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [commuteCells count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {  
	int cellEntryIndex = [indexPath indexAtPosition: [indexPath length] -1];
	if([indexPath indexAtPosition: [indexPath length] -1] ==0 ){
		return 50.0;
	} else if([[commuteCells objectAtIndex:cellEntryIndex] isKindOfClass:[NSString class]]){
		return 40.0;
	} else if([[commuteCells objectAtIndex:cellEntryIndex] isKindOfClass:[UIButton class]]){
		return 90.0;
	} else if([[commuteCells objectAtIndex:cellEntryIndex] isKindOfClass:[UILabel class]]){
		return 25.0;
	} else {
		return 25.0;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"Cell";
	
	int cellEntryIndex = [indexPath indexAtPosition: [indexPath length] -1];
	if([[commuteCells objectAtIndex:cellEntryIndex] isKindOfClass:[UIButton class]]) {
		CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		}
		
		[cell sendButtonForCell:[commuteCells objectAtIndex:cellEntryIndex]];
		
		[cell setData:@"Switch to Morning" 
		 currentSpeed:-100 
				 even:0
		   isIncident:NO];
		
		
		return cell;
	} else if([[commuteCells objectAtIndex:cellEntryIndex] isMemberOfClass:[CommuteInfo class]]){
		CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == NULL) {
			cell = [[[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		}
		//[cell addTimeStamp:appDelegate.user.evening.timeStamp];
		[cell setDataForCommute:[commuteCells objectAtIndex:cellEntryIndex]];
		return cell;
		
	} else if([[commuteCells objectAtIndex:cellEntryIndex] isKindOfClass:[NSString class]]){
		CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		}
		
		[cell setDataForHighway:[commuteCells objectAtIndex:cellEntryIndex]];
		
		return cell;
	} else if([[commuteCells objectAtIndex:cellEntryIndex] isKindOfClass:[UILabel class]]){
		
		CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		
		if (cell == nil) {
			cell = [[[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		}
		
		[cell addTimeStamp:appDelegate.user.evening.timeStamp];
		[cell setData:@"" currentSpeed:-200 even:0 isIncident:NO];
		
		return cell;
		
	} else if([[commuteCells objectAtIndex:cellEntryIndex] isKindOfClass:[UIImage class]]) {
		CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		}
		if(self.title == @"Evening"){
			[cell setData:@"     Shake phone to see Alternate Commute" currentSpeed:-201 even:0 isIncident:FALSE];
		} else {
			[cell setData:@"     Shake phone to see Primary Commute" currentSpeed:-201 even:0 isIncident:FALSE];
		}
		
		return cell;
		
	} else {
		CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		}
		
		[cell setData:[[commuteCells objectAtIndex:cellEntryIndex] exitName] 
		 currentSpeed:[[commuteCells objectAtIndex:cellEntryIndex] mph] 
				 even:cellEntryIndex%2
		   isIncident:[[commuteCells objectAtIndex:cellEntryIndex] isIncident]];
		
		return cell;
	}
	
}

-(void) grabRSSFeed:(NSString *)blogAddress {
	int segmentId = 0;
    // Initialize the congestionInfo MutableArray that we declared in the header
    congestionInfo = [[NSMutableArray alloc] init];
	userInfo = [[NSMutableArray alloc] init];
	morningCommuteInfo = [[NSMutableArray alloc] init];
	eveningCommuteInfo = [[NSMutableArray alloc] init];
	altEveningCommuteInfo = [[NSMutableArray alloc] init];
	altMorningCommuteInfo = [[NSMutableArray alloc] init];
	segmentInfo = [[NSMutableArray alloc] init];
	segmentsCounter = [[NSMutableArray alloc]init];
	incidentInfo = [[NSMutableArray alloc] init];
	
    // Convert the supplied URL string into a usable URL object
    NSURL *url = [NSURL URLWithString: blogAddress];
	
    // Create a new rssParser object based on the TouchXML "CXMLDocument" class, this is the
    // object that actually grabs and processes the RSS data
    CXMLDocument *rssParser = [[[CXMLDocument alloc] initWithContentsOfURL:url options:0 error:nil] autorelease];
	
    // Create a new Array object to be used with the looping of the results from the rssParser
    NSArray *congestionNodes = NULL;
	NSArray *userInfoNodes = NULL;
	NSArray *morningCommuteNodes = NULL;
	NSArray *eveningCommuteNodes = NULL;
	NSArray *altMorningCommuteNodes = NULL;
	NSArray *altEveningCommuteNodes = NULL;
	NSArray *segmentNodes = NULL;
	NSArray *segmentsCounterNodes = NULL;
	NSArray *incidentNodes = NULL;
	
	
	
    // Set the congestionNodes Array to contain an object for every instance of an  node in our RSS feed
    congestionNodes = [rssParser nodesForXPath:@"//congestion" error:nil];
	userInfoNodes = [rssParser nodesForXPath:@"//user" error:nil];
	morningCommuteNodes = [rssParser nodesForXPath:@"//morning" error:nil];
	eveningCommuteNodes = [rssParser nodesForXPath:@"//evening" error:nil];
	altEveningCommuteNodes = [rssParser nodesForXPath:@"//altEvening" error:nil];
	altMorningCommuteNodes = [rssParser nodesForXPath:@"//altMorning" error:nil];
	segmentNodes = [rssParser nodesForXPath:@"//segment" error:nil];
	segmentsCounterNodes = [rssParser nodesForXPath:@"//segments" error:nil];
	incidentNodes = [rssParser nodesForXPath:@"//incidents" error:nil];
	segmentsTracker=0;
	
    // Loop through the congestionNodes to access each items actual data
	for (CXMLElement *resultElement in userInfoNodes) {
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[userInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	
	if([[[userInfo objectAtIndex: 0] objectForKey: @"morningExists"] boolValue]){
		segmentId+=1000;
	}
	if([[[userInfo objectAtIndex: 0] objectForKey: @"altMorningExists"] boolValue]){
		segmentId+=100;
	}
	if([[[userInfo objectAtIndex: 0] objectForKey: @"eveningExists"] boolValue]){
		segmentId+=10;
	}
	if([[[userInfo objectAtIndex: 0] objectForKey: @"altEveningExists"] boolValue]){
		segmentId+=1;
	}
	for (CXMLElement *resultElement in segmentsCounterNodes) {
		@try{
			segmentsTracker++;
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			
			if(segmentsTracker == 1){
				if(segmentId >=1000){
					morningCommuteSegments = counter/2;
					segmentId-=1000;
				} else if(segmentId >=100){
					altMorningCommuteSegments = counter/2;
					segmentId-=100;
				} else if(segmentId >=10){
					eveningCommuteSegments = counter/2;
					segmentId-=10;
				} else {
					altEveningCommuteSegments = counter/2;
				}
			}else if(segmentsTracker == 2){
				if(segmentId >=100){
					altMorningCommuteSegments = counter/2;
					segmentId-=100;
				} else if(segmentId >=10){
					eveningCommuteSegments = counter/2;
					segmentId-=10;
				} else {
					altEveningCommuteSegments = counter/2;
				}
			} else if(segmentsTracker == 3){
				if(segmentId >=10){
					eveningCommuteSegments = counter/2;
					segmentId-=10;
				} else {
					altEveningCommuteSegments = counter/2;
				}
			} else {
				altEveningCommuteSegments = counter/2;
			}

			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[segmentsCounter addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	segmentsRemaining = morningCommuteSegments + altMorningCommuteSegments + eveningCommuteSegments + altEveningCommuteSegments;
	for (CXMLElement *resultElement in segmentNodes) {
		
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[segmentInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	for (CXMLElement *resultElement in morningCommuteNodes) {
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[morningCommuteInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	for (CXMLElement *resultElement in altMorningCommuteNodes) {
		@try{
			
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[altMorningCommuteInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	
	for (CXMLElement *resultElement in eveningCommuteNodes) {
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[eveningCommuteInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	
	for (CXMLElement *resultElement in altEveningCommuteNodes) {
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[altEveningCommuteInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }

	
    for (CXMLElement *resultElement in congestionNodes) {
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[congestionInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	/*
	for (CXMLElement *resultElement in incidentNodes) {
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[incidentInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	*/
	
}

-(void) createCommuteInfo{
	BOOL getOut = FALSE;
	BOOL alreadyAdded = FALSE;

	@try{
		if([congestionInfo count] == 0) {
			// Create the feed string			
			NSString *blogAddress = [@"file://" stringByAppendingString:[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"commuteInfo.xml"]];
			//For simulator
			//NSString *blogAddress = @"file://localhost/Users/Sam/Documents/TrafficViewer/commuteInfo.xml";

			// Call the grabRSSFeed function with the above
			// string as a parameter
			[self grabRSSFeed:blogAddress];
			congestionIndex = 0;
			int prevSeq = 0;
			user = [[User alloc] initWithInfo:[[[userInfo objectAtIndex: 0] objectForKey: @"userId"] intValue]
									  carrier:[[[userInfo objectAtIndex: 0] objectForKey: @"carrier"] intValue]
								accountExists:[[[userInfo objectAtIndex: 0] objectForKey: @"accountExists"] boolValue]
								morningExists:[[[userInfo objectAtIndex: 0] objectForKey: @"morningExists"] boolValue]
							 altMorningExists:[[[userInfo objectAtIndex: 0] objectForKey: @"altMorningExists"] boolValue]
								eveningExists:[[[userInfo objectAtIndex: 0] objectForKey: @"eveningExists"] boolValue]
							 altEveningExists:[[[userInfo objectAtIndex: 0] objectForKey: @"altEveningExists"] boolValue]
								   infoExists:[[[userInfo objectAtIndex: 0] objectForKey: @"infoExists"] boolValue]
										  ani:[[userInfo objectAtIndex: 0] objectForKey: @"ani"]
									firstName:[[userInfo objectAtIndex: 0] objectForKey: @"firstName"]
									 lastName:[[userInfo objectAtIndex: 0] objectForKey: @"lastName"] 
								 carrierEmail:[[userInfo objectAtIndex: 0] objectForKey: @"carrierEmail"]
										email:[[userInfo objectAtIndex: 0] objectForKey: @"email"]];
			
			if(user.morningExists){
				tempMorning = [[Commute alloc] initWithType:[[[morningCommuteInfo objectAtIndex: 0] objectForKey: @"morning"] boolValue] 
												  alternate:[[[morningCommuteInfo objectAtIndex: 0] objectForKey: @"alternate"] boolValue]  
												numSegments:morningCommuteSegments
												currentTime:[[[morningCommuteInfo objectAtIndex: 0] objectForKey: @"currentTime"] intValue]  
												averageTime:[[[morningCommuteInfo objectAtIndex: 0] objectForKey: @"averageTime"] intValue]  
												 difference:[[[morningCommuteInfo objectAtIndex: 0] objectForKey: @"difference"] floatValue]];
				tempMorning.timeStamp = [[segmentInfo objectAtIndex: 0] objectForKey: @"timestamp"];
				[user addMorningCommute:tempMorning];
				[tempMorning release];
				
				
				for(int i = 0; i<morningCommuteSegments;i++){
					segmentsDone++;
					tempSeg = [[Segment alloc] initWithID:[[[segmentInfo objectAtIndex: i] objectForKey: @"segmentId"] intValue]
												   onramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"onramp"] intValue]
												  offramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"offramp"] intValue]
												threshold:[[[segmentInfo objectAtIndex: i] objectForKey: @"threshold"] intValue]
													hwyId:[[[segmentInfo objectAtIndex: i] objectForKey: @"hwyId"] intValue]
											startDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"startDistance"] intValue] 
											  endDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"endDistance"] intValue] 
												  hwyName:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyName"] 
											   hwyWavFile:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyWaveFile"]  
												congested:[[[segmentInfo objectAtIndex: i] objectForKey: @"congested"] boolValue]  
										   incidentExists:[[[segmentInfo objectAtIndex: i] objectForKey: @"incidentExists"] boolValue]];
					
					prevSeq = [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue];
					for(int j = 0;
						j <= tempSeg.offramp - tempSeg.onramp 
						&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
						&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq
						&& !getOut;
						j++)
					{
						@try{
							tempCong = [[Congestion alloc] initWithName:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitName"] 
																wavFile:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitWavFile"]  
															   sequence:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue]
																	mph:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"mph"] intValue]];
							[tempSeg addCongestion:tempCong];
							prevSeq = tempCong.sequence;
							
						}@catch(NSException * e) {
							NSLog(@"Exception %@: %@", [e name], [e  reason]);
						}
						if(!([[[congestionInfo lastObject] objectForKey:@"exitName"] isEqual:tempCong.exitName]
							 && [[[congestionInfo lastObject] objectForKey:@"exitWavFile"] isEqual:tempCong.exitWaveFile]
							 && [[[congestionInfo lastObject] objectForKey:@"sequence"] intValue] == tempCong.sequence
							 && [[[congestionInfo lastObject] objectForKey:@"mph"] intValue] == tempCong.mph
						     && segmentsDone == segmentsRemaining)){
							congestionIndex++;
							@try {
								if(j <= tempSeg.offramp - tempSeg.onramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq) {
								}
							}@catch (NSException * e) {
								NSLog(@"caught it");
								[user.morning addSegment:tempSeg];
								alreadyAdded = TRUE;
								NSLog(@"Adding alt evening segment");
							}
						} else{
							NSLog(@"Last exit!");
							getOut = true;
						}
					}
					/*
					if(tempSeg.incidentExists){
						
						tempCong = [[Congestion alloc] initWithName:[[incidentInfo objectAtIndex:0] objectForKey:@"exitName"] 
															wavFile:[[incidentInfo objectAtIndex:0] objectForKey:@"exitWavFile"]  
														   sequence:[[[incidentInfo objectAtIndex:0] objectForKey:@"sequence"] intValue]
																mph:0];
						tempCong.isIncident = TRUE;
						[tempSeg addCongestion:tempCong];
					}
					 */
					if(!alreadyAdded) {
						[user.morning addSegment:tempSeg];
						[tempSeg release];
						[tempCong release];
					}
				}
			}
			alreadyAdded = FALSE;
			getOut = false;
			if(user.altMorningExists){
				tempAltMorning = [[Commute alloc] initWithType:[[[altMorningCommuteInfo objectAtIndex: 0] objectForKey: @"morning"] boolValue] 
													 alternate:[[[altMorningCommuteInfo objectAtIndex: 0] objectForKey: @"alternate"] boolValue]  
												   numSegments:altMorningCommuteSegments
												   currentTime:[[[altMorningCommuteInfo objectAtIndex: 0] objectForKey: @"currentTime"] intValue]  
												   averageTime:[[[altMorningCommuteInfo objectAtIndex: 0] objectForKey: @"averageTime"] intValue]  
													difference:[[[altMorningCommuteInfo objectAtIndex: 0] objectForKey: @"difference"] floatValue]];
				tempAltMorning.timeStamp = [[segmentInfo objectAtIndex: 0] objectForKey:@"timestamp"];
				[user addAltMorningCommute:tempAltMorning];
				[tempAltMorning release];
				
				for(int i = morningCommuteSegments;i<morningCommuteSegments+altMorningCommuteSegments;i++){
					segmentsDone++;
					tempSeg = [[Segment alloc] initWithID:[[[segmentInfo objectAtIndex: i] objectForKey: @"segmentId"] intValue]
												   onramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"onramp"] intValue]
												  offramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"offramp"] intValue]
												threshold:[[[segmentInfo objectAtIndex: i] objectForKey: @"threshold"] intValue]
													hwyId:[[[segmentInfo objectAtIndex: i] objectForKey: @"hwyId"] intValue]
											startDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"startDistance"] floatValue] 
											  endDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"endDistance"] floatValue] 
												  hwyName:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyName"] 
											   hwyWavFile:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyWaveFile"]  
												congested:[[[segmentInfo objectAtIndex: i] objectForKey: @"congested"] boolValue]  
										   incidentExists:[[[segmentInfo objectAtIndex: i] objectForKey: @"incidentExists"] boolValue]];
					prevSeq = [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue];
					for(int j = 0;
						j <= tempSeg.offramp - tempSeg.onramp 
						&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
						&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq
						&& !getOut;
						j++)
					{

						@try{
							tempCong = [[Congestion alloc] initWithName:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitName"] 
																wavFile:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitWavFile"]  
															   sequence:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue]
																	mph:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"mph"] intValue]];
							[tempSeg addCongestion:tempCong];
							prevSeq = tempCong.sequence;
							
						}@catch(NSException * e) {
							NSLog(@"Exception %@: %@", [e name], [e  reason]);
						}
						if(!([[[congestionInfo lastObject] objectForKey:@"exitName"] isEqual:tempCong.exitName]
							 && [[[congestionInfo lastObject] objectForKey:@"exitWavFile"] isEqual:tempCong.exitWaveFile]
							 && [[[congestionInfo lastObject] objectForKey:@"sequence"]intValue] == tempCong.sequence
							 && [[[congestionInfo lastObject] objectForKey:@"mph"] intValue]== tempCong.mph
						     && segmentsDone == segmentsRemaining)){
							congestionIndex++;
							@try {
								if(j <= tempSeg.offramp - tempSeg.onramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq) {
								}
							}@catch (NSException * e) {
								NSLog(@"caught it");
								[user.altMorning addSegment:tempSeg];
								alreadyAdded = TRUE;
								NSLog(@"Adding alt evening segment");
							}
						} else{
							getOut = true;
						}
					}
					/*
					if(tempSeg.incidentExists){
						
						tempCong = [[Congestion alloc] initWithName:[[incidentInfo objectAtIndex:0] objectForKey:@"exitName"] 
															wavFile:[[incidentInfo objectAtIndex:0] objectForKey:@"exitWavFile"]  
														   sequence:[[[incidentInfo objectAtIndex:0] objectForKey:@"sequence"] intValue]
																mph:0];
						tempCong.isIncident = TRUE;
						[tempSeg addCongestion:tempCong];
					}
					 */
					if(!alreadyAdded) {
						[user.altMorning addSegment:tempSeg];
						[tempSeg release];
						[tempCong release];
					}
				}
			}
			alreadyAdded = FALSE;
			getOut = FALSE;
			if(user.eveningExists){
				tempEvening = [[Commute alloc] initWithType:[[[eveningCommuteInfo objectAtIndex: 0] objectForKey: @"morning"] boolValue] 
												  alternate:[[[eveningCommuteInfo objectAtIndex: 0] objectForKey: @"alternate"] boolValue]  
												numSegments:eveningCommuteSegments
												currentTime:[[[eveningCommuteInfo objectAtIndex: 0] objectForKey: @"currentTime"] intValue]  
												averageTime:[[[eveningCommuteInfo objectAtIndex: 0] objectForKey: @"averageTime"] intValue]  
												 difference:[[[eveningCommuteInfo objectAtIndex: 0] objectForKey: @"difference"] floatValue]];
				tempEvening.timeStamp = [[segmentInfo objectAtIndex: 0] objectForKey:@"timestamp"];
				[user addEveningCommute:tempEvening];
				/*----uncomment to display commute time as a badge on the tabBar
				NSString *commuteTimeForBadge = [NSString stringWithFormat:@"%d",tempEvening.currentTime];
				self.navigationController.tabBarItem.badgeValue = commuteTimeForBadge;
				*/
				[tempEvening release];
				
				for(int i = morningCommuteSegments+altMorningCommuteSegments;i<morningCommuteSegments+altMorningCommuteSegments+eveningCommuteSegments;i++){
					segmentsDone++;
					tempSeg = [[Segment alloc] initWithID:[[[segmentInfo objectAtIndex: i] objectForKey: @"segmentId"] intValue]
												   onramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"onramp"] intValue]
												  offramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"offramp"] intValue]
												threshold:[[[segmentInfo objectAtIndex: i] objectForKey: @"threshold"] intValue]
													hwyId:[[[segmentInfo objectAtIndex: i] objectForKey: @"hwyId"] intValue]
											startDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"startDistance"] floatValue] 
											  endDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"endDistance"] floatValue] 
												  hwyName:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyName"] 
											   hwyWavFile:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyWaveFile"]  
												congested:[[[segmentInfo objectAtIndex: i] objectForKey: @"congested"] boolValue]  
										   incidentExists:[[[segmentInfo objectAtIndex: i] objectForKey: @"incidentExists"] boolValue]];
					prevSeq = [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue];
					for(int j = 0;
						j <= tempSeg.offramp - tempSeg.onramp 
						&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
						&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq
						&& !getOut;
						j++)
					{

						@try{
							tempCong = [[Congestion alloc] initWithName:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitName"] 
																wavFile:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitWavFile"]  
															   sequence:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue]
																	mph:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"mph"] intValue]];
							[tempSeg addCongestion:tempCong];
							prevSeq = tempCong.sequence;							
						}@catch(NSException * e) {
							NSLog(@"Exception %@: %@", [e name], [e  reason]);
						}
						if(!([[[congestionInfo lastObject] objectForKey:@"exitName"] isEqual:tempCong.exitName]
							 && [[[congestionInfo lastObject] objectForKey:@"exitWavFile"] isEqual:tempCong.exitWaveFile]
							 && [[[congestionInfo lastObject] objectForKey:@"sequence"]intValue] == tempCong.sequence
							 && [[[congestionInfo lastObject] objectForKey:@"mph"] intValue]== tempCong.mph
						     && segmentsDone == segmentsRemaining)){
							congestionIndex++;
							@try {
								if(j <= tempSeg.offramp - tempSeg.onramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq) {
								}
							}@catch (NSException * e) {
								NSLog(@"caught it");
								[user.evening addSegment:tempSeg];
								alreadyAdded = TRUE;
								NSLog(@"Adding alt evening segment");
							}
						} else{
							getOut = true;
						}
					}
					/*
					if(tempSeg.incidentExists){
						
						tempCong = [[Congestion alloc] initWithName:[[incidentInfo objectAtIndex:0] objectForKey:@"exitName"] 
															wavFile:[[incidentInfo objectAtIndex:0] objectForKey:@"exitWavFile"]  
														   sequence:[[[incidentInfo objectAtIndex:0] objectForKey:@"sequence"] intValue]
																mph:[[[incidentInfo objectAtIndex:0] objectForKey:@"exitId"] intValue]];
						tempCong.isIncident = TRUE;
						[tempSeg addCongestion:tempCong];
					}
					 */
					if(!alreadyAdded) {
						[user.evening addSegment:tempSeg];
						[tempSeg release];
						[tempCong release];
					}
				}
				
			}

			alreadyAdded = FALSE;
			getOut = false;
			if(user.altEveningExists){
				tempAltEvening =  [[Commute alloc] initWithType:[[[altEveningCommuteInfo objectAtIndex: 0] objectForKey: @"morning"] boolValue] 
													  alternate:[[[altEveningCommuteInfo objectAtIndex: 0] objectForKey: @"alternate"] boolValue]  
													numSegments:altEveningCommuteSegments
													currentTime:[[[altEveningCommuteInfo objectAtIndex: 0] objectForKey: @"currentTime"] intValue]  
													averageTime:[[[altEveningCommuteInfo objectAtIndex: 0] objectForKey: @"averageTime"] intValue]  
													 difference:[[[altEveningCommuteInfo objectAtIndex: 0] objectForKey: @"difference"] floatValue]];
				tempAltEvening.timeStamp = [[segmentInfo objectAtIndex: 0] objectForKey:@"timestamp"];
				[user addAltEveningCommute:tempAltEvening];
				[tempAltEvening release];
				
				for(int i = morningCommuteSegments+altMorningCommuteSegments+eveningCommuteSegments;
					i<morningCommuteSegments+altMorningCommuteSegments+eveningCommuteSegments+altEveningCommuteSegments;
					i++){
					segmentsDone++;
					tempSeg = [[Segment alloc] initWithID:[[[segmentInfo objectAtIndex: i] objectForKey: @"segmentId"] intValue]
												   onramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"onramp"] intValue]
												  offramp:[[[segmentInfo objectAtIndex: i] objectForKey: @"offramp"] intValue]
												threshold:[[[segmentInfo objectAtIndex: i] objectForKey: @"threshold"] intValue]
													hwyId:[[[segmentInfo objectAtIndex: i] objectForKey: @"hwyId"] intValue]
											startDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"startDistance"] floatValue] 
											  endDistance:[[[segmentInfo objectAtIndex: i] objectForKey: @"endDistance"] floatValue] 
												  hwyName:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyName"] 
											   hwyWavFile:[[segmentInfo objectAtIndex: i] objectForKey: @"hwyWaveFile"]  
												congested:[[[segmentInfo objectAtIndex: i] objectForKey: @"congested"] boolValue]  
										   incidentExists:[[[segmentInfo objectAtIndex: i] objectForKey: @"incidentExists"] boolValue]];
					prevSeq = [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue];
					for(int j = 0; j <= tempSeg.offramp - tempSeg.onramp 
					  && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
					  && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq
					  && !getOut; j++)
					{
						

						@try{
							tempCong = [[Congestion alloc] initWithName:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitName"] 
																wavFile:[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"exitWavFile"]  
															   sequence:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue]
																	mph:[[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"mph"] intValue]];
							[tempSeg addCongestion:tempCong];
							prevSeq = tempCong.sequence;							
						}@catch(NSException * e) {
							NSLog(@"Exception %@: %@", [e name], [e  reason]);
						}
						if(!([[[congestionInfo lastObject] objectForKey:@"exitName"] isEqual:tempCong.exitName]
							 && [[[congestionInfo lastObject] objectForKey:@"exitWavFile"] isEqual:tempCong.exitWaveFile]
							 && [[[congestionInfo lastObject] objectForKey:@"sequence"]intValue] == tempCong.sequence
							 && [[[congestionInfo lastObject] objectForKey:@"mph"] intValue]== tempCong.mph
						     && segmentsDone == segmentsRemaining)){
							congestionIndex++;
							@try {
								if(j <= tempSeg.offramp - tempSeg.onramp 
									&& [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] <= tempSeg.offramp 
								   && [[[congestionInfo objectAtIndex:congestionIndex] objectForKey:@"sequence"] intValue] >= prevSeq) {
								}
							}@catch (NSException * e) {
								NSLog(@"caught it");
								[user.altEvening addSegment:tempSeg];
								alreadyAdded = TRUE;
								NSLog(@"Adding alt evening segment");
							}
						} else{
							getOut = true;
						}
					}
					
					if(!alreadyAdded) {
						[user.altEvening addSegment:tempSeg];
						[tempSeg release];
						[tempCong release];
					}
				}
			}

			// Call the reloadData function on the blogTable, this
			// will cause it to refresh itself with our new data
		}
	}@catch (NSException * e) {
		NSLog(@"Error: %@ cause of %@", [e name], [e reason]);
	}
	appDelegate.user = user;
	[user release];
	[congestionInfo release];
	[userInfo release];
	[morningCommuteInfo release];
	[eveningCommuteInfo release];
	[altEveningCommuteInfo release];
	[altMorningCommuteInfo release];
	[segmentInfo release];
	[segmentsCounter release];
}

-(void)prepareCommuteCells:(Commute *)c{
	[commuteCells dealloc];
	if(c.currentTime == 0){
		[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
		noCommuteNotifier = [[UIAlertView alloc] initWithTitle:@"" message:@"You have not added a commute for this time.  Login to http://traffic.calit2.net to add one." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[noCommuteNotifier show];
	} else if([altButton title]==@"Alternate") {
	}
	commuteCells = [[NSMutableArray alloc] init];
	float distance = 0;
	for(int i = 0; i<[c.segments count]; i++){
		distance+=[[c.segments objectAtIndex:i] endDistance] - [[c.segments objectAtIndex:i] startDistance];
	}
	CommuteInfo * commuteDetail;
	if(!c.alternate) {
		commuteDetail = [[CommuteInfo alloc] initWithCurrent:[c currentTime] 
															   averageTime:[c averageTime] 
																 otherTime: appDelegate.user.altEvening.currentTime
																difference:[c difference] 
																  distance:distance 
															   commuteType:[c alternate]];
	} else {
		 commuteDetail = [[CommuteInfo alloc] initWithCurrent:[c currentTime] 
															   averageTime:[c averageTime] 
																 otherTime: appDelegate.user.evening.currentTime
																difference:[c difference]
																  distance:distance
															   commuteType:[c alternate]];
	}	
	
	[commuteCells addObject: commuteDetail];
	[commuteDetail release];
	//UILabel *labelForCommuteCellSetup = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
	//[commuteCells addObject:labelForCommuteCellSetup];
	
	for(int i = 0; i<[c.segments count]; i++){
		NSNumber *distance = [[NSNumber alloc] initWithFloat:[[c.segments objectAtIndex:i] endDistance] - [[c.segments objectAtIndex:i] startDistance]];
		NSString *segmentLength = [[NSString alloc] initWithFormat:@"%.2f",[distance floatValue]];
		[commuteCells addObject:[[NSString alloc] initWithFormat:@"%@***%@ ",[[c.segments objectAtIndex:i] hwyName],segmentLength]];
		if(i==0) {
			UILabel *labelForCommuteCellSetup = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
			[commuteCells addObject:labelForCommuteCellSetup];
		}
		for(int j = 0; j<[[[c.segments objectAtIndex:i] congestions] count];j++){
			[commuteCells addObject:[[[c.segments objectAtIndex:i] congestions] objectAtIndex:j]];
		}
	}
	UIButton * switchToOtherCommute = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[switchToOtherCommute setTitle:@"Switch to Morning" forState:UIControlStateNormal];
	switchToOtherCommute.showsTouchWhenHighlighted = YES;
	switchToOtherCommute.frame = CGRectMake(120,2,150,35);
	[switchToOtherCommute addTarget:self action:@selector(switchToMorning:) forControlEvents:UIControlEventTouchUpInside];
	[commuteCells addObject:switchToOtherCommute];
	[commuteCells addObject:[UIImage imageNamed:@"SDButton.png"]];

	
}

-(void) viewDidAppear:(BOOL) animated {
	//[appDelegate.notifier dismissWithClickedButtonIndex:0 animated:FALSE];
	[super viewDidAppear:animated];
}

- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	if (self.lastAcceleration) {
		if (!histeresisExcited && [self L0AccelerationIsShaking:self.lastAcceleration current:acceleration threshold:0.7]) {
			histeresisExcited = YES;
			if(appDelegate.tabBarController.selectedIndex == 1) {
				
				if(priAlt) {
					if(appDelegate.user.evening.numSegments <= 0) {
						
					} else {
						[self setTitle:@"Evening"];
						[altButton setTitle:@"Alternate"];
						priAlt = NO;
						[self prepareCommuteCells:appDelegate.user.evening];
						[self.tableView reloadData];
					}
				} else {
					if(appDelegate.user.altEvening.numSegments <= 0) {
						
					} else {
						[self setTitle:@"Alternate"];
						[altButton setTitle:@"Primary"];
						priAlt = YES;
						[self prepareCommuteCells:appDelegate.user.altEvening];
						[self.tableView reloadData];
					}
				}
			}
			
		} else if (histeresisExcited && ![self L0AccelerationIsShaking:self.lastAcceleration current:acceleration threshold:0.2]) {
			histeresisExcited = NO;
		}
	}
	
	self.lastAcceleration = acceleration;
    
}

-(BOOL) L0AccelerationIsShaking:(UIAcceleration*)last current:(UIAcceleration*)current threshold:(double) threshold {
	double deltaX = fabs(last.x - current.x),
	deltaY = fabs(last.y - current.y),
	deltaZ = fabs(last.z - current.z);
	
	return ((deltaX > threshold && deltaY > threshold) || (deltaX > threshold && deltaZ > threshold) || (deltaY > threshold && deltaZ > threshold));
}


/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
*/

/*
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
    }
    if (editingStyle == UITableViewCellEditingStyleInsert) {
    }
}
*/

/*
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)dealloc {
    [super dealloc];
	[commuteCells release];
	[altButton release];
	[morningButton release];
}


@end

