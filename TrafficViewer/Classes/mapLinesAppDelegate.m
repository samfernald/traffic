//
//  pointInfo.h
//  mapLines
//
//  Created by sfernald on 2/17/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "mapLinesAppDelegate.h"
#import "mapLinesViewController.h"

@implementation mapLinesAppDelegate

@synthesize window;
@synthesize viewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
	
	
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
