//
//  RootViewController.h
//  TrafficViewer
//
//  Created by sfernald on 10/29/08.
//  Copyright __MyCompanyName__ 2008. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MenuViewController.h"
#import "TrafficViewController.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "WebViewController.h"
#import "SignupViewController.h"
//#import "GAITrackedViewController.h"


@interface RootViewController : UIViewController <UIAlertViewDelegate> {
	IBOutlet UIButton *LAButton, *SDButton, *BAButton;
	IBOutlet UIButton *loginButton, *guestButton, *signUpButton;
	IBOutlet UITextField *txtUser;
	IBOutlet UITextField *txtPassword;
	NSMutableData * receivedData;
	NSString *userName,*password, *URLForCommute, *URLForTraffic;
	NSURLConnection * conn;
	AppDelegate *appDelegate;
	BOOL connected;
	BOOL autoLogin;
	BOOL autoLoginGuest;
    BOOL jumpToMap;
	int debugger,debuggerMini,debuggerNano;
	NSString * filePath;
	
	
	MenuViewController *menuViewController;
	TrafficViewController *TrafficViewController;
	
	UIAlertView *notifier;
	UIAlertView *commuteLoginNotifier;
	UIActivityIndicatorView *activityView1;
	UIActivityIndicatorView *activityView2;
	
	NetworkStatus remoteHostStatus;
	NetworkStatus internetConnectionStatus;
	NetworkStatus localWiFiConnectionStatus;
	
}

@property(nonatomic,retain) UITextField *txtUser;
@property(nonatomic,retain) UITextField *txtPassword;
@property(nonatomic,retain) UIButton *loginButton;
@property(nonatomic,retain) UIButton *guestButton;
@property(nonatomic,retain) UIButton *signUpButton;
@property(nonatomic,retain) UIButton *LAButton;
@property(nonatomic,retain) UIButton *SDButton;
@property(nonatomic,retain) UIButton *BAButton;
@property(nonatomic,retain) NSString * userName;
@property(nonatomic,retain) NSString * password;
@property(nonatomic,retain) NSString * URLForCommute;
@property(nonatomic,retain) NSString * URLForTraffic;
@property(nonatomic,retain) NSString * filePath;
@property(nonatomic, retain) AppDelegate *appDelegate;


@property NetworkStatus remoteHostStatus;
@property NetworkStatus internetConnectionStatus;
@property NetworkStatus localWiFiConnectionStatus;

@property BOOL connected;
@property BOOL autoLogin;
@property BOOL autoLoginGuest;



@property(nonatomic,retain) MenuViewController *menuViewController;
@property(nonatomic,retain) TrafficViewController *TrafficViewController;



-(IBAction) login:(id) sender;

-(IBAction) loginAsGuest:(id) sender;

-(IBAction) selectCity:(id) sender;

-(IBAction) signUp:(id) sender;

-(BOOL) checkConnection;

-(void) checkAutoLogin;

-(void) downloadingMessageNotifier;


@end
