//
//  HighwayFlowViewController.m
//  TrafficViewer
//
//  Created by sfernald on 10/29/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "HighwayFlowViewController.h"
#import "AppDelegate.h"
#import "TouchXML.h"
#import "CommuteCell.h"
#import "DejalActivityView.h"

static UIImage *borderBanner = nil;


@implementation HighwayFlowViewController

@synthesize refreshButton;
@synthesize lastRefresh;
@synthesize speedData;

+ (void)initialize{
    // The magnitude images are cached as part of the class, so they need to be
    // explicitly retained.
    borderBanner = [[UIImage imageNamed:@"borderAppBanner.png"] retain];
}
/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if (self = [super initWithStyle:style]) {
    }
    return self;
}
*/

-(void)viewDidAppear:(BOOL) animated {
	[super viewDidAppear:animated];
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	//[[[appDelegate.tabBarController.viewControllers objectAtIndex:2] tabBarItem] setEnabled:NO];
}


// Implement viewDidLoad to do additional setup after loading the view.
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"HighwayFlowView loaded");
    //self.trackedViewName = @"HighwayFlow View";
	lastRefresh = [[NSDate date] retain];
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	///[[[appDelegate.tabBarController.viewControllers objectAtIndex:2] tabBarItem] setEnabled:NO];
	
	refreshButton = [[UIBarButtonItem alloc]
				 initWithTitle:NSLocalizedString(@"Refresh", @"")
				 style:UIBarButtonItemStyleBordered
				 target:self
				 action:@selector(refresh:)];
	
	
	//refreshButton = [[UIBarButtonItem alloc] initWithImage:<#(UIImage *)image#> style:<#(UIBarButtonItemStyle)style#> target:<#(id)target#> action:<#(SEL)action#>
	self.navigationItem.rightBarButtonItem = refreshButton;
	
	appDelegate.speedsViewLoaded = YES;
	appDelegate.testingCounter++;
	int hwyId = appDelegate.selectedHighway;
	self.tableView.bounces = YES;
	if(appDelegate.firstTimeLoadingSpeed){
		downloadingMessage = [[UIAlertView alloc] initWithTitle:@"" message:@"       Fetching traffic data.              Please wait" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
		UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
		activityView.frame = CGRectMake(139.0f-18.0f, 85.0f, 37.0f, 37.0f);
		[downloadingMessage addSubview:activityView];
		[activityView startAnimating];
		[downloadingMessage show];
		appDelegate.firstTimeLoadingSpeed = FALSE;
	}
	
	
	@try{
		
		if(appDelegate.selectedCity == 1){
			URLForTraffic = [NSString stringWithFormat:@"http://%@/sd/servlet/highwayflow",appDelegate.cpuUrl];
		} else if (appDelegate.selectedCity == 2){
			URLForTraffic = [NSString stringWithFormat:@"http://%@/la/servlet/highwayflow",appDelegate.cpuUrl];
		} else {
			URLForTraffic = [NSString stringWithFormat:@"http://%@/bayarea/servlet/highwayflow",appDelegate.cpuUrl];
		}
		
		NSString * URLForTrafficWithHighway = [[NSString alloc] initWithFormat:@"%@?h=%d",URLForTraffic,hwyId];
		NSString *post = @""; 
		NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];  
		
		NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];  
		
		NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
		[request setURL:[NSURL URLWithString:URLForTrafficWithHighway]];  
		[request setHTTPMethod:@"POST"];  
		[request setValue:postLength forHTTPHeaderField:@"Content-Length"];  
		[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];  
		[request setHTTPBody:postData];  
		conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];  
		if (conn) {  
			receivedData = [[NSMutableData data] retain];
		} else {  
			// inform the user that the download could not be made  
		} 
		[post retain];
		[postData retain];
		[postLength retain];
		[request release];

	} @catch (NSException *e) {
		NSLog(@"Failed to Login");
	}
		
}

- (void)refresh:(id)sender{
	
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	NSString *downloadMessage;
	if(appDelegate.selectedCity == 1) {
		downloadMessage = [NSString stringWithFormat:@"Speeds updated once every minute."];
	} else {
		downloadMessage = [NSString stringWithFormat:@"Speeds updated every 5 minutes."];
	}
	UIAlertView * refreshingMessage = [[UIAlertView alloc] initWithTitle:@"Refreshing" message:downloadMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
	[refreshingMessage show];
	
	//[[[appDelegate.tabBarController.viewControllers objectAtIndex:2] tabBarItem] setEnabled:NO];
	NSTimeInterval timeSinceRefresh = [lastRefresh timeIntervalSinceNow];
	if(timeSinceRefresh <= -60) {
		[lastRefresh release];
		lastRefresh = [[NSDate date] retain];
		@try{
			int hwyId = appDelegate.selectedHighway;

			if(appDelegate.selectedCity == 1){
				URLForTraffic = [NSString stringWithFormat:@"http://%@/sd/servlet/highwayflow",appDelegate.cpuUrl];
			} else if (appDelegate.selectedCity == 2){
				URLForTraffic = [NSString stringWithFormat:@"http://%@/la/servlet/highwayflow",appDelegate.cpuUrl];
			} else {
				URLForTraffic = [NSString stringWithFormat:@"http://%@/bayarea/servlet/highwayflow",appDelegate.cpuUrl];
			}
			
			NSString * URLForTrafficWithHighway = [[NSString alloc] initWithFormat:@"%@?h=%d",URLForTraffic,hwyId];
			NSString *post = @""; 
			NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];  
			
			NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];  
			
			NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
			[request setURL:[NSURL URLWithString:URLForTrafficWithHighway]];  
			[request setHTTPMethod:@"POST"];  
			[request setValue:postLength forHTTPHeaderField:@"Content-Length"];  
			[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];  
			[request setHTTPBody:postData];  
			conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];  
			if (conn)   
			{  
				receivedData = [[NSMutableData data] retain];
			}   
			else   
			{  
				// inform the user that the download could not be made  
			} 
			[post retain];
			[postData retain];
			[postLength retain];
			[request release];
			
			
			
		} @catch (NSException *e) {
			NSLog(@"Failed to Login");
		}
	}
	[refreshingMessage dismissWithClickedButtonIndex:0 animated:YES];
	[refreshingMessage release];
	[self.tableView reloadData];
	NSLog(@"fin");

}


-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge previousFailureCount] == 0) {
        NSURLCredential *newCredential;
        newCredential=[NSURLCredential credentialWithUser:@""
                                                 password:@""
                                              persistence:NSURLCredentialPersistenceNone];
        [[challenge sender] useCredential:newCredential
               forAuthenticationChallenge:challenge];
    } else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        // inform the user that the user name and password
        // in the preferences are incorrect
        [self showPreferencesCredentialsAreIncorrectPanel:self];
    }
	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{

	//append the new data to the receivedData
	//receivedData is declared as a method instance elsewhere
	[receivedData appendData:data];
	
	
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSString * middleString = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
	NSString * outputString = [middleString stringByReplacingOccurrencesOfString:@"<incidents/>" withString:@""];
	//NSString *fileAddress = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"trafficFlowInfo.xml"];
	//for simulator
	//NSString *fileAddress = @"Users/Sam/Documents/TrafficViewer/trafficFlowInfo.xml";
	//self.speedData = outputString;

    //do something with the data
    //receivedData is declared as a method instance elsewhere
	//[self grabRSSFeed:[@"file://" stringByAppendingString:[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"trafficFlowInfo.xml"]]];
	
	[self grabRSSFeed:outputString];
	//For simulator
	//[self grabRSSFeed:@"file://localhost/Users/Sam/Documents/TrafficViewer/trafficFlowInfo.xml"];

	if(downloadingMessage.visible){
		[downloadingMessage dismissWithClickedButtonIndex:0 animated:FALSE];
	}
	/*
	 if([outputString writeToFile:fileAddress atomically:FALSE encoding:NSUTF8StringEncoding error:NULL]){
	 //NSLog(@"write worked");
	 } else {
	 NSLog(@"FAIL");
	 }
	 */
	
	//NSLog(@"OUTPUT: %@",outputString);
	//[receivedData dealloc];
	
	// release the connection, and the data object
    [connection release];
    [receivedData release];
}


-(void) grabRSSFeed:(NSString *)blogAddress {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

    // Initialize the congestionInfo MutableArray that we declared in the header
    congestionInfo = [[NSMutableArray alloc] init];
	highwayInfo = [[NSMutableArray alloc] init];
	incidentInfo = [[NSMutableArray alloc] init];
	
    // Convert the supplied URL string into a usable URL object
    //NSURL *url = [NSURL URLWithString: blogAddress];
	//NSLog(@"OUTPUT: %@",blogAddress);

    // Create a new rssParser object based on the TouchXML "CXMLDocument" class, this is the
    // object that actually grabs and processes the RSS data
	CXMLDocument *rssParser = [[[CXMLDocument alloc] initWithXMLString:blogAddress options:0 error:nil] autorelease];
    //CXMLDocument *rssParser = [[[CXMLDocument alloc] initWithContentsOfURL:url options:0 error:nil] autorelease];
    // Create a new Array object to be used with the looping of the results from the rssParser
    NSArray *congestionNodes = NULL;
	NSArray *hwyInfo = NULL;
	NSArray *incidentNodes = NULL;
	@try{
		// Set the congestionNodes Array to contain an object for every instance of an  node in our RSS feed
		congestionNodes = [rssParser nodesForXPath:@"//commute.Congestion" error:nil];
		hwyInfo = [rssParser nodesForXPath:@"//commute.Segment" error:nil];
		incidentNodes = [rssParser nodesForXPath:@"//commute.Incident" error:nil];
		
	} @catch (NSException * e) {
		NSLog(@"Exception %@: %@", [e name], [e  reason]);
	}
	// Loop through the congestionNodes to access each items actual data
	
	for (CXMLElement *resultElement in congestionNodes) {
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[congestionInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	for (CXMLElement *resultElement in hwyInfo) {
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[highwayInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	/*
	for (CXMLElement *resultElement in incidentNodes) {
		@try{
			// Create a temporary MutableDictionary to store the items fields in, which will eventually end up in congestionInfo
			NSMutableDictionary *infoItem = [[NSMutableDictionary alloc] init];
			
			// Create a counter variable as type "int"
			int counter;
			
			// Loop through the children of the current  node
			for(counter = 0; counter < [resultElement childCount]; counter++) {
				
				// Add each field to the blogItem Dictionary with the node name as key and node value as the value
				[infoItem setObject:[[resultElement childAtIndex:counter] stringValue] forKey:[[resultElement childAtIndex:counter] name]];
			}
			
			// Add the blogItem to the global congestionInfo Array so that the view can access it.
			[incidentInfo addObject:[infoItem copy]];
			[infoItem release];
		}@catch (NSException * e) {
			NSLog(@"Exception %@: %@", [e name], [e  reason]);
		}
    }
	*/
	@try{
		//natural_t freemem =[AppDelegate get_free_memory];
		//NSLog(@" freemem = %d",freemem);
		[self prepareDisplayCells:congestionInfo];
		[self.tableView reloadData];
		//NSFileManager *fileManager = [NSFileManager defaultManager];
		//[fileManager removeItemAtPath:blogAddress error:NULL];
		[pool drain];
		 
	}@catch (NSException * e) {
		NSLog(@"Exception %@: %@", [e name], [e  reason]);
		UIAlertView * errorMessage = [[UIAlertView alloc] initWithTitle:@"Error" message:@"An error occured while attempting to download the traffic data." delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
		[errorMessage show];
	}

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if([displayCells count] != 0){
		return ([displayCells count]+2);//+1
	} else {
		return 0;
	}
}

//prepares traffic data to be displayed
-(void)prepareDisplayCells:(NSMutableArray *) trafficFlow{
	[displayCells dealloc];
	displayCells = [[NSMutableArray alloc] init];
	if(congestionInfo == nil){
		displayCells = trafficFlow;
	} else {
		[displayCells addObject:[[highwayInfo objectAtIndex:0] objectForKey:@"hwyName"]];
		
		for(int i = 0; i<[trafficFlow count];i++){
			Congestion * tempCong = [[Congestion alloc] initWithName:[[trafficFlow objectAtIndex:i] objectForKey:@"exitName"] wavFile:NULL sequence:[[[trafficFlow objectAtIndex:i] objectForKey:@"sequence"] intValue] mph:[[[trafficFlow objectAtIndex:i] objectForKey:@"mph"]intValue]];
			[displayCells addObject:tempCong];
			[tempCong release];
		}
		/*
		[displayCells addObject:[[highwayInfo objectAtIndex:0] objectForKey:@"hwyName"]];
		//[displayCells addObject:[[highwayInfo objectAtIndex:0] objectForKey:@"timestamp"]];
		for(int i = 0; i<[trafficFlow count];i++){
			Congestion * tempCong = [[Congestion alloc] initWithName:[[trafficFlow objectAtIndex:i] objectForKey:@"exitName"] wavFile:NULL sequence:[[[trafficFlow objectAtIndex:i] objectForKey:@"sequence"] intValue] mph:[[[trafficFlow objectAtIndex:i] objectForKey:@"mph"]intValue]];
			[displayCells addObject:tempCong];
			[tempCong release];
		}
		if([incidentInfo count] > 0){
			for(int i = 0; i<[incidentInfo count];i++){
				Congestion * tempCong = [[Congestion alloc] initWithName:[[incidentInfo objectAtIndex:i] objectForKey:@"exitName"] wavFile:NULL sequence:[[[incidentInfo objectAtIndex:i] objectForKey:@"sequence"] intValue] mph:0];
				tempCong.isIncident = TRUE;
				[displayCells addObject:tempCong];
				[tempCong release];
			}
		}
		 */
	}
	
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  
{  
	if([indexPath indexAtPosition: [indexPath length] -1] == 0){
		return 40.0;
	} else if(indexPath.row != [displayCells count] +1){
		return 25.0;
	} else {
        return 80.0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
	//if(displayCells != nil) {
		CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
		if (cell == nil) {
            if(indexPath.row != [displayCells count] +1){
                cell = [[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier];
            }
		}
	
		// Set up the cell
		
		int cellEntryIndex = [indexPath indexAtPosition: [indexPath length] -1];
		
		if(!(cellEntryIndex == [displayCells count]) && cellEntryIndex < [displayCells count]){
			if([[displayCells objectAtIndex:cellEntryIndex] isKindOfClass:[NSString class]]){
				[cell setDataForHighway:[displayCells objectAtIndex:cellEntryIndex]];
				[cell addTimeStamp:[[highwayInfo objectAtIndex:0] objectForKey:@"timestamp"]];
			} else {		
				[cell setData:[[displayCells objectAtIndex:cellEntryIndex] exitName] 
				 currentSpeed:[[displayCells objectAtIndex:cellEntryIndex] mph] 
						 even:cellEntryIndex%2
				   isIncident:[[displayCells objectAtIndex:cellEntryIndex] isIncident]];
				[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
			}
		} else if(cellEntryIndex == [displayCells count]) {
			[cell setData:@"                     http://traffic.calit2.net" currentSpeed:-50 even:0 isIncident:FALSE];
		} else if(cellEntryIndex == [displayCells count]+1) {
            NSLog(@"last cell");
            UITableViewCell *cellAdd = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, 320, 80)];
            UIImageView *userIcon = [[UIImageView alloc] initWithFrame:CGRectMake(cellAdd.contentView.frame.origin.x, cellAdd.contentView.frame.origin.y, 320, 77)];
            userIcon.image = borderBanner;
            [cellAdd.contentView addSubview:userIcon];
            return cellAdd;
			//[cell setData:@"" currentSpeed:-202 even:0 isIncident:FALSE];
		}
		return cell;
		/*
		
			if([[displayCells objectAtIndex:cellEntryIndex] isKindOfClass:[NSString class]]){
				
				[cell setDataForHighway:[displayCells objectAtIndex:cellEntryIndex]];
				[cell addTimeStamp:[[highwayInfo objectAtIndex:0] objectForKey:@"timestamp"]];
			
			}
		} 
		
		if(![[displayCells objectAtIndex:cellEntryIndex] isKindOfClass:[NSString class]]){		
				[cell setData:[[displayCells objectAtIndex:cellEntryIndex] exitName] 
				 currentSpeed:[[displayCells objectAtIndex:cellEntryIndex] mph] 
						 even:cellEntryIndex%2
				   isIncident:[[displayCells objectAtIndex:cellEntryIndex] isIncident]];
				[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
				
		}
	
		//int cellEntryIndex = [indexPath indexAtPosition: [indexPath length] -1];
		//[cell setData:[[congestionInfo objectAtIndex:cellEntryIndex] objectForKey:@"exitName"] currentSpeed:[[[congestionInfo objectAtIndex:cellEntryIndex] objectForKey:@"mph"] intValue]];
		 */
		
	/*} else {
		CommuteCell *cell = (CommuteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		
		if (cell == nil) {
			cell = [[CommuteCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier];
		}
		
		// Set up the cell
		
		int cellEntryIndex = [indexPath indexAtPosition: [indexPath length] -1];
		[cell setData:@"traffic.calit2.net" 
		 currentSpeed:0 
				 even:0
		   isIncident:FALSE];
		return cell;
	}*/
	
}	



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == [displayCells count]+1){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.apple.com/us/app/best_time-to-cross-the-border/id570288644?mt=8"]];
    }
}

/*
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
    }
    if (editingStyle == UITableViewCellEditingStyleInsert) {
    }
}
*/

/*
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
}
*/

- (void)viewDidDisappear:(BOOL)animated {

	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	appDelegate.speedsViewLoaded = NO;
	//[[[appDelegate.tabBarController.viewControllers objectAtIndex:2] tabBarItem] setEnabled:YES];
}

/*
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
*/

- (void)dealloc {
    [super dealloc];
	[conn release];
	[downloadingMessage release];


}


@end

