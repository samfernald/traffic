//
//  CommuteCell.h
//  AdvancedBlogTutorial
//
//  Created by sfernald on 10/10/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommuteInfo.h"
#import "CommuteType.h"


@interface CommuteCell : UITableViewCell {
	UILabel *exitNameLabel;
	UILabel *speedLabel;
	UILabel *commuteLabel;
	UILabel *distanceLabel;
	UILabel *distanceForCommuteLabel;
	UILabel *diffForCommuteLabel;
	UILabel *commuteTimeLabel;
	UILabel *commuteMinLabel;
	UILabel *timeStampLabel;
	UILabel *timeStampTime;
	UILabel *speedBar;
	UIImageView *commuteCellBG;
	UIImageView *commuteType;
	UIButton *switcherForCommute;
	NSMutableArray * speeds;
	int mphForSpeedBar;
	int indexForSpeedBars;
	BOOL isCommuteCell,isHighwayCell,showDistance,isCommuteCellType;
	BOOL hasAverageCommuteTime;
	BOOL showCommuteType;

}

-(void)setData:(NSString *)exit currentSpeed:(int)mph even:(int)lastCommuteCell isIncident:(BOOL)isIncident;
-(void)sendButtonForCell:(UIButton *)switcher;
-(void)setDataForHighway:(NSString *)hwy;
-(void)setDataForCommute:(CommuteInfo *)c;
-(void)addTimeStamp:(NSString *) timeStamp;
-(UIImage *) getHighwayIcon:(int) hwyNum;

// internal function to ease setting up label text
-(UILabel *)newLabelWithPrimaryColor:(UIColor *)primaryColor selectedColor:(UIColor *)selectedColor fontSize:(CGFloat)fontSize bold:(BOOL)bold;

// you should know what this is for by know
@property (nonatomic, retain) UILabel *exitNameLabel;
@property (nonatomic, retain) UILabel *speedLabel;
@property (nonatomic, retain) UILabel *commuteLabel;
@property (nonatomic, retain) UILabel *distanceLabel;
@property (nonatomic, retain) UILabel *distanceForCommuteLabel;
@property (nonatomic, retain) UILabel *diffForCommuteLabel;
@property (nonatomic, retain) UILabel *commuteTimeLabel;
@property (nonatomic, retain) UILabel *commuteMinLabel;
@property (nonatomic, retain) UILabel *timeStampLabel;
@property (nonatomic, retain) UILabel *timeStampTime;
@property (nonatomic, retain) UILabel *speedBar;
@property (nonatomic, retain) UIImageView *commuteCellBG;
@property (nonatomic, retain) UIImageView *commuteType;
@property (nonatomic, retain) UIButton *switcherForCommute;
@property (nonatomic, retain) NSMutableArray *speeds;
@property BOOL isCommuteCell;
@property BOOL isHighwayCell;
@property BOOL showDistance;
@property BOOL hasAverageCommuteTime;
@property BOOL isCommuteCellType;
@property BOOL showCommuteType;
@property int mphForSpeedBar;
@property int indexForSpeedBars;



@end
