//
//  pointInfo.m
//  mapLines
//
//  Created by sfernald on 2/17/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "pointInfo.h"


@implementation pointInfo

@synthesize highway;
@synthesize mph;

-(id) initWithHwy:(int)hwy
			speed:(int)milesperhour {
	self.highway = hwy;
	self.mph = milesperhour;
	return self;
}
@end
