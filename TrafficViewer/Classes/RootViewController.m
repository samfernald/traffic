//
//  RootViewController.m
//  TrafficViewer
//
//  Created by sfernald on 10/29/08.
//  Copyright __MyCompanyName__ 2008. All rights reserved.
//

#import "RootViewController.h"
#import "MapViewController.h"

#define kRequiredAccuracy 500.0 //meters
#define kMaxAge 1.0 //seconds

@implementation RootViewController

@synthesize loginButton, guestButton, signUpButton;
@synthesize LAButton, SDButton, BAButton;
@synthesize txtUser, txtPassword;
@synthesize userName, password;
@synthesize URLForCommute, URLForTraffic, filePath;
@synthesize menuViewController,TrafficViewController;
@synthesize remoteHostStatus, internetConnectionStatus, localWiFiConnectionStatus;
@synthesize connected;
@synthesize autoLogin;
@synthesize autoLoginGuest;
@synthesize appDelegate;


-(void) viewWillAppear:(BOOL) animated{
	[super viewWillAppear:animated];

	[SDButton setSelected:NO];
	[BAButton setSelected:NO];
	[LAButton setSelected:NO];	
	[SDButton setEnabled:YES];
	[BAButton setEnabled:YES];
	[LAButton setEnabled:YES];
	[loginButton setHidden:YES];
	[guestButton setHidden:YES];
	[signUpButton setHidden:YES];
	[txtUser setHidden:YES];
	[txtPassword setHidden:YES];
	
	[self checkAutoLogin];
	NSLog(@"init menu load");
	/*
	MenuViewController *menuView = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:[NSBundle mainBundle]];
	self.menuViewController = menuView;
	[menuView release];
	if(appDelegate.homeCity != NULL){
		if(appDelegate.userName == NULL){
			[[self navigationController] pushViewController:menuViewController animated:YES];
		}
	}*/
}

- (void)viewDidLoad {
    NSLog(@"root view did load");
	[super viewDidLoad];
    //self.trackedViewName = @"Home View";
	autoLogin = TRUE;
	connected = FALSE;
    
	appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	[self setTitle:@"Select City"];
	self.navigationController.tabBarItem.title = @"";
	//[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:NO];
	
	[self.view setBackgroundColor:[[UIColor colorWithRed: 249.0/255.0 green:207.0/255.0 blue:28.0/255.0 alpha:1] retain]];
	//UIImageView *menuViewBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default.png"]];
	//[self.view sendSubviewToBack:menuViewBackground];
	
	//NSString *backgroundPath = [[NSBundle mainBundle] pathForResource:@"TrafficAppBackground" ofType:@"png"];
	//UIImage *backgroundImage = [UIImage imageWithContentsOfFile:backgroundPath];
	//UIColor *backgroundColor = [[UIColor alloc] initWithPatternImage:backgroundImage];
	//self.view.backgroundColor = backgroundColor; 
	//[backgroundColor release];
	
	/*
	UIColor *bgColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"TrafficAppBackground.png"]];
	self.view.backgroundColor = bgColor;
	[bgColor release];
	*/
	
	
	self.navigationItem.hidesBackButton = TRUE;
	
	[SDButton setHidden:NO];
	[BAButton setHidden:NO];
	[LAButton setHidden:NO];
	[SDButton setEnabled:YES];
	[BAButton setEnabled:YES];
	[LAButton setEnabled:YES];

}

-(void) checkAutoLogin {
	if(appDelegate.homeCity.length != 0){
		if(appDelegate.userName.length != 0 && appDelegate.password.length != 0){
			txtUser.text = appDelegate.userName;
			txtPassword.text = appDelegate.password;
		} else {
			autoLoginGuest = TRUE;
            NSLog(@"AUTO LOAD GUEST");
		}
	}
	if([self checkConnection]){
		if(appDelegate.userName != NULL && appDelegate.password != NULL && appDelegate.homeCity != NULL){
            NSLog(@"auto-logging in");
			if([appDelegate.homeCity isEqualToString:@"San Diego"]){
                NSLog(@"SD!");
				//URLForCommute = @"http://traffic.calit2.net/sd/servlet/personal";
				URLForCommute = [NSString stringWithFormat:@"http://%@/sd/servlet/personal",appDelegate.cpuUrl];
                appDelegate.urlForCommuteJson = [NSString stringWithFormat:@"http://%@/sd/servlet/personal.json",appDelegate.cpuUrl];
				appDelegate.selectedCity = 1;
				[self login:loginButton];
			} else if([appDelegate.homeCity isEqualToString:@"Los Angeles"]){
                NSLog(@"LA!");
				//URLForCommute = @"http://traffic.calit2.net/la/servlet/personal";
				URLForCommute = [NSString stringWithFormat:@"http://%@/la/servlet/personal",appDelegate.cpuUrl];
                appDelegate.urlForCommuteJson = [NSString stringWithFormat:@"http://%@/la/servlet/personal.json",appDelegate.cpuUrl];
				appDelegate.selectedCity = 2;
				[self login:loginButton];
			} else if([appDelegate.homeCity isEqualToString:@"Bay Area"]){
                NSLog(@"BA!");
				//URLForCommute = @"http://traffic.calit2.net/bayarea/servlet/personal";
				URLForCommute = [NSString stringWithFormat:@"http://%@/bayarea/servlet/personal",appDelegate.cpuUrl];
                appDelegate.urlForCommuteJson = [NSString stringWithFormat:@"http://%@/bayarea/servlet/personal.json",appDelegate.cpuUrl];
				appDelegate.selectedCity = 3;
				[self login:loginButton];
			}
		}else if(autoLoginGuest) {
			autoLogin = FALSE;
			appDelegate.autoLoadedGuest = TRUE;
			if([appDelegate.homeCity isEqualToString:@"San Diego"]){
				appDelegate.selectedCity = 1;
				[self selectCity:SDButton];
			} else if([appDelegate.homeCity isEqualToString:@"Los Angeles"]){
				appDelegate.selectedCity = 2;
				[self selectCity:LAButton];
			} else if([appDelegate.homeCity isEqualToString:@"Bay Area"]){
				appDelegate.selectedCity = 3;
				[self selectCity:BAButton];
			}
			
		} else {
			autoLogin = FALSE;
		}
	}
	appDelegate.urlForCommute = URLForCommute;
}

-(IBAction) selectCity:(id) sender{
	//[self.navigationController popToRootViewControllerAnimated:NO];
	//trafficViewController = nil;
	appDelegate.firstTimeLoadingSpeed = TRUE;
	appDelegate.userCreated = FALSE;
	if(appDelegate.userName != NULL){
		txtUser.text = appDelegate.userName;
		txtPassword.text = appDelegate.password;
	}

	[loginButton setHidden:NO];
	[guestButton setHidden:NO];
	[signUpButton setHidden:NO];
	[txtUser setHidden:NO];
	[txtPassword setHidden:NO];
	[SDButton setSelected:NO];
	[LAButton setSelected:NO];
	[BAButton setSelected:NO];
	if([sender tag] == 1){
		[SDButton setSelected:YES];
		URLForCommute = [NSString stringWithFormat:@"http://%@/sd/servlet/personal",appDelegate.cpuUrl];
        appDelegate.urlForCommuteJson = [NSString stringWithFormat:@"http://%@/sd/servlet/personal.json",appDelegate.cpuUrl];
		//URLForCommute = @"http://traffic.calit2.net/sd/servlet/personal";
		appDelegate.selectedCity = 1;
		appDelegate.homeCity = @"San Diego";
	} else if([sender tag] == 2){
		[LAButton setSelected:YES];
		URLForCommute = [NSString stringWithFormat:@"http://%@/la/servlet/personal",appDelegate.cpuUrl];
        appDelegate.urlForCommuteJson = [NSString stringWithFormat:@"http://%@/la/servlet/personal.json",appDelegate.cpuUrl];
		//URLForCommute = @"http://traffic.calit2.net/la/servlet/personal";
		appDelegate.selectedCity = 2;
		appDelegate.homeCity = @"Los Angeles";
	} else if([sender tag] == 3){
		[BAButton setSelected:YES];
		URLForCommute = [NSString stringWithFormat:@"http://%@/bayarea/servlet/personal",appDelegate.cpuUrl];
        appDelegate.urlForCommuteJson = [NSString stringWithFormat:@"http://%@/bayarea/servlet/personal.json",appDelegate.cpuUrl];
		//URLForCommute = @"http://traffic.calit2.net/bayarea/servlet/personal";
		appDelegate.selectedCity = 3;
		appDelegate.homeCity = @"Bay Area";
	}
	appDelegate.urlForCommute = URLForCommute;
	if(autoLoginGuest) {
		[self loginAsGuest:guestButton];
	}
}

-(IBAction) login:(id) sender{
	NSNumber * screenToLoad = (NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"screenToLoad", kCFPreferencesCurrentApplication);	
	NSString *downloadMessage;
	NSString *originalUserName = [[NSString alloc] initWithString:[txtUser text]];
	password = [[NSString alloc] initWithString:[txtPassword text]];
	userName = [[NSString alloc] initWithString:[originalUserName lowercaseString]];
	//[originalUserName release];
	//NSLog(userName);
	NSLog(@"login screen to load = %d",[screenToLoad intValue]);
	if([screenToLoad intValue] == 0) {
		NSNumber * setMap = [NSNumber numberWithInt:1];
		CFPreferencesSetAppValue((CFStringRef)@"initMap", setMap, kCFPreferencesCurrentApplication);
	}
	if([screenToLoad intValue] == 0 || [screenToLoad intValue] == 1) {
        jumpToMap = FALSE;
		downloadMessage = [NSString stringWithFormat:@"Fetching %@ traffic data.",appDelegate.homeCity];
		commuteLoginNotifier = [[UIAlertView alloc] initWithTitle:@"" message:downloadMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
		activityView1 = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
		activityView1.frame = CGRectMake(139.0f-18.0f, 85.0f, 33.0f, 33.0f);
		[commuteLoginNotifier addSubview:activityView1];
		[activityView1 startAnimating];
		[commuteLoginNotifier show];
		
		@try{
			
			NSString *post = @""; 
			NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];  
			
			NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];  
			NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
			[request setURL:[NSURL URLWithString:URLForCommute]];
			[request setHTTPMethod:@"POST"];  
			[request setValue:postLength forHTTPHeaderField:@"Content-Length"];  
			[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];  
			[request setHTTPBody:postData];
			NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
			NSArray *cookies = [scs cookies];
			NSEnumerator* e = [cookies objectEnumerator];
			NSHTTPCookie* cookie;
			while ((cookie = [e nextObject])) {
				NSString* name = [cookie name];
				if ([name isEqualToString:@"userId"]) {
					[scs deleteCookie:cookie];
				}
			}
			
			conn=[[NSURLConnection alloc] initWithRequest:request delegate:self]; 
			if (conn)   
			{  
				NSLog(@"received Data");
				receivedData = [[NSMutableData data] retain];
				NSLog(@"1");
			}   
			else   
			{  
				NSLog(@"no conn");
				//[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
			}  
			//[request release];
		} @catch (NSException *e) {
			NSLog(@"Failed to Login");
			[commuteLoginNotifier dismissWithClickedButtonIndex:0 animated:FALSE];
		}
		
	} else {
        [DejalBezelActivityView activityViewForView:self.view withLabel:[NSString stringWithFormat:@"Connecting to %@ server.",appDelegate.homeCity]];
        [self performSelector:@selector(dropAlert) withObject:nil afterDelay:3.0];
        jumpToMap = TRUE;
        /*
        if(appDelegate.priorLoads != 10){
            
            downloadMessage = [NSString stringWithFormat:@"Fetching %@ traffic data. Please wait",appDelegate.homeCity];
            appDelegate.notifier = [[UIAlertView alloc] initWithTitle:@"" message:downloadMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
            activityView1 = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            activityView1.frame = CGRectMake(139.0f-18.0f, 85.0f, 33.0f, 33.0f);
            [appDelegate.notifier addSubview:activityView1];
            [activityView1 startAnimating];
            [appDelegate.notifier show];
             
        }
    */
        @try{
			
			NSString *post = @"";
			NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
			
			NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
			NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
			[request setURL:[NSURL URLWithString:URLForCommute]];
			[request setHTTPMethod:@"POST"];
			[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
			[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
			[request setHTTPBody:postData];
			NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
			NSArray *cookies = [scs cookies];
			NSEnumerator* e = [cookies objectEnumerator];
			NSHTTPCookie* cookie;
			while ((cookie = [e nextObject])) {
				NSString* name = [cookie name];
				if ([name isEqualToString:@"userId"]) {
					[scs deleteCookie:cookie];
				}
			}
			
			conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
			if (conn)
			{
				NSLog(@"received Data");
				receivedData = [[NSMutableData data] retain];
				NSLog(@"1");
			}
			else
			{
				NSLog(@"no conn");
				//[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
			}
			//[request release];
		} @catch (NSException *e) {
			NSLog(@"Failed to Login");
			[commuteLoginNotifier dismissWithClickedButtonIndex:0 animated:FALSE];
		}
        
        
    }
	//[txtUser resignFirstResponder];
	//[txtPassword resignFirstResponder];
}

-(void) dropAlert {
    [DejalBezelActivityView removeViewAnimated:TRUE];
}


-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
    if ([challenge previousFailureCount] == 0) {
        NSURLCredential *newCredential;
        newCredential=[NSURLCredential credentialWithUser:userName
                                                 password:password
                                              persistence:NSURLCredentialPersistenceNone];
        [[challenge sender] useCredential:newCredential
               forAuthenticationChallenge:challenge];
		
    } else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        // inform the user that the user name and password
        // in the preferences are incorrect
		[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
		//[notifier release];
		[commuteLoginNotifier dismissWithClickedButtonIndex:0 animated:FALSE];
		//[commuteLoginNotifier release];
        [appDelegate.notifier dismissWithClickedButtonIndex:0 animated:FALSE];
		[txtUser setText:@"Incorrect User Name or Password"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
	// append the new data to the receivedData
	// receivedData is declared as a method instance elsewhere
    if(!jumpToMap){
        [receivedData appendData:data];
    }

}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	NSLog(@"conn failed with error %@",error);
	[commuteLoginNotifier dismissWithClickedButtonIndex:0 animated:FALSE];
    /*
	notifier = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Unable to connect to traffic.calit2.net" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
	[notifier show];
     */
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSLog(@"Con did finish loading");
    if(!jumpToMap){
        appDelegate.hasAnAccount = @"YES";
        appDelegate.urlForCommute = URLForCommute;
        // do something with the data
        // receivedData is declared as a method instance elsewhere
        NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
        NSLog(@"commute data = %@",[[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding]);
        
        
        //[notifier dismissWithClickedButtonIndex:0 animated:FALSE];
        //[notifier release];
        NSString * outputString = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
        
        NSRange morningR = [outputString rangeOfString:@"<morningExists>"];
        NSRange searchMorningRange = NSMakeRange(morningR.location+morningR.length, 4);
        if([[outputString substringWithRange:searchMorningRange] isEqualToString:@"true"]){
            appDelegate.morningExists = TRUE;
            NSNumber * morningE = [NSNumber numberWithBool:TRUE];
            CFPreferencesSetAppValue((CFStringRef)@"morningExists", morningE , kCFPreferencesCurrentApplication);
        }
        NSRange altMorningR = [outputString rangeOfString:@"<altMorningExists>"];
        NSRange searchAltMorningRange = NSMakeRange(altMorningR.location+altMorningR.length, 4);
        if([[outputString substringWithRange:searchAltMorningRange] isEqualToString:@"true"]){
            appDelegate.altMorningExists = TRUE;
            NSNumber * altMorningE = [NSNumber numberWithBool:TRUE];
            CFPreferencesSetAppValue((CFStringRef)@"altMorningExists", altMorningE , kCFPreferencesCurrentApplication);
        }
        NSRange eveningR = [outputString rangeOfString:@"<eveningExists>"];
        NSRange searchEveningRange = NSMakeRange(eveningR.location+eveningR.length, 4);
        if([[outputString substringWithRange:searchEveningRange] isEqualToString:@"true"]){
            appDelegate.eveningExists = TRUE;
            NSNumber * eveningE = [NSNumber numberWithBool:TRUE];
            CFPreferencesSetAppValue((CFStringRef)@"eveningExists", eveningE , kCFPreferencesCurrentApplication);
        }
        NSRange altEveningR = [outputString rangeOfString:@"<altEveningExists>"];
        NSRange searchAltEveningRange = NSMakeRange(altEveningR.location+altEveningR.length, 4);
        if([[outputString substringWithRange:searchAltEveningRange] isEqualToString:@"true"]){
            appDelegate.altEveningExists = TRUE;
            NSNumber * altEveningE = [NSNumber numberWithBool:TRUE];
            CFPreferencesSetAppValue((CFStringRef)@"altEveningExists", altEveningE , kCFPreferencesCurrentApplication);
        }
        
        NSString * middleString = [outputString stringByReplacingOccurrencesOfString:@"<incidents/>" withString:@""];
        NSString * inputString = [middleString stringByReplacingOccurrencesOfString:@"<segments/>" withString:@""];
        
        //NSString *fileAddress = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"commuteInfo.xml"];
        NSLog(@"commute info downloaded in root view!");
        appDelegate.tempCommute = inputString;
        
        //[[[appDelegate.tabBarController.viewControllers objectAtIndex:1] tabBarItem] setEnabled:YES];
        if(self.menuViewController == nil){
            MenuViewController *menuView = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:[NSBundle mainBundle]];
            self.menuViewController = menuView;
            NSLog(@"pushing menu view 2");
            //[[self navigationController] pushViewController:menuViewController animated:YES];
            //[menuView release];
        } else {
            //[[self navigationController] pushViewController:menuViewController animated:YES];
        }
        NSNumber * screenToLoad = (NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"screenToLoad", kCFPreferencesCurrentApplication);	
        
        appDelegate.userName = userName;
        appDelegate.password = password;
        CFPreferencesSetAppValue((CFStringRef)@"userName", userName, kCFPreferencesCurrentApplication);
        CFPreferencesSetAppValue((CFStringRef)@"password", password, kCFPreferencesCurrentApplication);
        CFPreferencesSetAppValue((CFStringRef)@"homeCity", appDelegate.homeCity, kCFPreferencesCurrentApplication);
        CFPreferencesSetAppValue((CFStringRef)@"autoLogin", appDelegate.hasAnAccount, kCFPreferencesCurrentApplication);
        CFPreferencesAppSynchronize(kCFPreferencesCurrentApplication);
        [commuteLoginNotifier dismissWithClickedButtonIndex:0 animated:YES];
        //[commuteLoginNotifier release];
        if(appDelegate.priorLoads == 1 && appDelegate.askToRate) {
            appDelegate.askToRate = FALSE;
            [appDelegate.notifier show];
        }
        
        //[appDelegate.tabBarController setSelectedIndex:[screenToLoad intValue] +1];
        [self performSegueWithIdentifier:@"ModalPush" sender: loginButton];

        //[receivedData release];
    } else {
        if(appDelegate.priorLoads != 10) {
            [appDelegate.notifier dismissWithClickedButtonIndex:0 animated:YES];
        }
        //[[[appDelegate.tabBarController.viewControllers objectAtIndex:1] tabBarItem] setEnabled:YES];
        if(self.menuViewController == nil){
            NSLog(@"pushing menu view 1");
            
            MenuViewController *menuView = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:[NSBundle mainBundle]];
            self.menuViewController = menuView;
            //[[self navigationController] pushViewController:menuViewController animated:YES];
            //[menuView release];
        } else {
            //[[self navigationController] pushViewController:menuViewController animated:YES];
        }/*
        //[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:YES];
        //[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setImage:[[UIImage imageNamed:@"Menu_Icon.png"]retain]];
        [[[appDelegate.tabBarController.viewControllers objectAtIndex:1] tabBarItem] setTitle:@"My Traffic"];
        [[[appDelegate.tabBarController.viewControllers objectAtIndex:1] tabBarItem] setImage:[[UIImage imageNamed:@"my_traffic_icon.png"]retain]];
        [[[appDelegate.tabBarController.viewControllers objectAtIndex:2] tabBarItem] setEnabled:YES];
        [[[appDelegate.tabBarController.viewControllers objectAtIndex:2] tabBarItem] setTitle:@"Speeds"];
        [[[appDelegate.tabBarController.viewControllers objectAtIndex:2] tabBarItem] setImage:[[UIImage imageNamed:@"tabbar_icon_speeds.png"] retain]];
        [[[appDelegate.tabBarController.viewControllers objectAtIndex:3] tabBarItem] setEnabled:YES];
        [[[appDelegate.tabBarController.viewControllers objectAtIndex:3] tabBarItem] setTitle:@"Traffic Map"];
        [[[appDelegate.tabBarController.viewControllers objectAtIndex:3] tabBarItem] setImage:[[UIImage imageNamed:@"Map_icon.png"] retain]];
          */
        NSNumber * screenToLoad = (NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"screenToLoad", kCFPreferencesCurrentApplication);
        appDelegate.userName = userName;
        appDelegate.password = password;
        CFPreferencesSetAppValue((CFStringRef)@"userName", userName, kCFPreferencesCurrentApplication);
        CFPreferencesSetAppValue((CFStringRef)@"password", password, kCFPreferencesCurrentApplication);
        CFPreferencesSetAppValue((CFStringRef)@"homeCity", appDelegate.homeCity, kCFPreferencesCurrentApplication);
        CFPreferencesSetAppValue((CFStringRef)@"autoLogin", appDelegate.hasAnAccount, kCFPreferencesCurrentApplication);
        CFPreferencesAppSynchronize(kCFPreferencesCurrentApplication);
        NSLog(@"present modal VC");
        //[appDelegate.tabBarController setSelectedIndex:[screenToLoad intValue] +1];
        [self performSegueWithIdentifier:@"ModalPush" sender: loginButton];
        
    }
}


-(IBAction) signUp:(id) sender{
    NSString * urlToSignUp;
	if(appDelegate.selectedCity == 1){
		urlToSignUp = [NSString stringWithFormat:@"http://%@/sd/signup.jsp",appDelegate.cpuUrl];
		//urlToSignUp = @"http://traffic.calit2.net/sd/signup.jsp";
	} else if(appDelegate.selectedCity == 2){
		urlToSignUp = [NSString stringWithFormat:@"http://%@/la/signup.jsp",appDelegate.cpuUrl];
		//urlToSignUp = @"http://traffic.calit2.net/la/signup.jsp";
	} else {
		urlToSignUp = [NSString stringWithFormat:@"http://%@/bayarea/signup.jsp",appDelegate.cpuUrl];
		//urlToSignUp = @"http://traffic.calit2.net/bayarea/signup.jsp";
	}
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlToSignUp]];
    

    /*
    SignupViewController *signupView = [[SignupViewController alloc] initWithNibName:@"SignupViewController" bundle:[NSBundle mainBundle]];
    
    [[self navigationController] pushViewController:signupView animated:YES];		
    [signupView release];
     */
}

-(IBAction) loginAsGuest:(id) sender{
	if(!appDelegate.hasAnAccount){
		CFPreferencesSetAppValue((CFStringRef)@"homeCity", appDelegate.homeCity, kCFPreferencesCurrentApplication);
		CFPreferencesAppSynchronize(kCFPreferencesCurrentApplication);
	}
	if(self.menuViewController == nil){
		MenuViewController *menuView = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:[NSBundle mainBundle]];
		self.menuViewController = menuView;
		NSLog(@"pushing menu view 3");
        [self performSegueWithIdentifier:@"ModalPush" sender: loginButton];
		///[[self navigationController] pushViewController:menuViewController animated:YES];
		//[menuView release];
	} else {
		//[[self navigationController] pushViewController:menuViewController animated:YES];
	}/*
	[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:YES];
	[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setImage:[[UIImage imageNamed:@"Menu_Icon.png"]retain]];
	[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setTitle:@"Menu"];
	[[[appDelegate.tabBarController.viewControllers objectAtIndex:2] tabBarItem] setEnabled:YES];
	[[[appDelegate.tabBarController.viewControllers objectAtIndex:2] tabBarItem] setTitle:@"Speeds"];
	[[[appDelegate.tabBarController.viewControllers objectAtIndex:2] tabBarItem] setImage:[[UIImage imageNamed:@"tabbar_icon_speeds.png"] retain]];
	[[[appDelegate.tabBarController.viewControllers objectAtIndex:3] tabBarItem] setEnabled:YES];	
	[[[appDelegate.tabBarController.viewControllers objectAtIndex:3] tabBarItem] setTitle:@"Traffic Map"];
	[[[appDelegate.tabBarController.viewControllers objectAtIndex:3] tabBarItem] setImage:[[UIImage imageNamed:@"Map_icon.png"] retain]];
      */
    //NSLog(@"tabs = %d",[appDelegate.tabBarController.tabBar.items count]);

    //[[appDelegate.tabBarController.tabBar.items objectAtIndex:1] setEnabled:NO];
	NSNumber * screenToLoad = (NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"screenToLoad", kCFPreferencesCurrentApplication);
	NSLog(@"screen to load = %d",[screenToLoad intValue]);
	if([screenToLoad intValue] != 0) {
		//[appDelegate.tabBarController setSelectedIndex:[screenToLoad intValue] +1];
	} else {
		NSNumber * setScreen = [NSNumber numberWithInt:2];
		CFPreferencesSetAppValue((CFStringRef)@"screenToLoad",setScreen, kCFPreferencesCurrentApplication);
		NSNumber * setMap = [NSNumber numberWithInt:1];
		CFPreferencesSetAppValue((CFStringRef)@"initMap", setMap, kCFPreferencesCurrentApplication);
		//[appDelegate.tabBarController setSelectedIndex:3];
	}
    [self performSegueWithIdentifier:@"ModalPush" sender: guestButton];
    //[appDelegate.tabBarController setSelectedIndex:2];
    //[self performSegueWithIdentifier:@"ModalPush" sender: guestButton];


}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	// the user clicked one of the OK/Cancel buttons
	if(!connected){
		BOOL temp = [self checkConnection];
	}
	NSLog(@"auto logging in!!");
	//USING DEV-----------------------
	if(connected && appDelegate.userName != NULL && appDelegate.password != NULL && appDelegate.homeCity != NULL){
		if([appDelegate.homeCity isEqualToString:@"San Diego"]){
			//URLForCommute = @"http://traffic.calit2.net/sd/servlet/personal";
			URLForCommute = [NSString stringWithFormat:@"http://%@/sd/servlet/personal",appDelegate.cpuUrl];
			appDelegate.selectedCity = 1;
			[self login:loginButton];
		} else if([appDelegate.homeCity isEqualToString:@"Los Angeles"]){
			//URLForCommute = @"http://traffic.calit2.net/la/servlet/personal";
			URLForCommute = [NSString stringWithFormat:@"http://%@/la/servlet/personal",appDelegate.cpuUrl];
			appDelegate.selectedCity = 2;
			[self login:loginButton];
		} else if([appDelegate.homeCity isEqualToString:@"Bay Area"]){
			//URLForCommute = @"http://traffic.calit2.net/bayarea/servlet/personal";
			URLForCommute = [NSString stringWithFormat:@"http://%@/bayarea/servlet/personal",appDelegate.cpuUrl];
			appDelegate.selectedCity = 3;
			[self login:loginButton];
		}
	} else {
		autoLogin = FALSE;
	}
		
}

- (BOOL) checkConnection {
    NSLog(@"Checking conn");
	[[Reachability sharedReachability] setHostName:appDelegate.cpuUrl];
	self.remoteHostStatus           = [[Reachability sharedReachability] remoteHostStatus];
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	self.localWiFiConnectionStatus	= [[Reachability sharedReachability] localWiFiConnectionStatus];
	BOOL hasNetWorkConnection = FALSE;
	
	if (self.internetConnectionStatus == NotReachable) {
		NSLog(@"Internet not reachable");
		notifier = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No internet connection found, please establish a network connection and try again." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Retry",nil];
		[notifier show];
		return FALSE;
	} else if (self.internetConnectionStatus == ReachableViaCarrierDataNetwork) {
		NSLog(@"Internet reachable via carrier");
		hasNetWorkConnection = TRUE;
	} else if (self.internetConnectionStatus == ReachableViaWiFiNetwork) {
		NSLog(@"Internet reachable via wifi");
		hasNetWorkConnection = TRUE;
	}

	/*
	if(hasNetWorkConnection) {
		
		if (self.remoteHostStatus == NotReachable) {
			NSLog(@"host not reachable");
			hasNetWorkConnection = FALSE;
			notifier = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Error Connecting to server, please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Retry",nil];
			[notifier show];
			return FALSE;
		} else if (self.remoteHostStatus == ReachableViaCarrierDataNetwork) {
			NSLog(@"host reachable via carier");
		} else if (self.remoteHostStatus == ReachableViaWiFiNetwork) {
			NSLog(@"host reachable via wifi");
		}
	}
	*/
	
	if (self.localWiFiConnectionStatus == NotReachable) {
		NSLog(@"Local wifi not reachable");
	} else if (self.localWiFiConnectionStatus == ReachableViaWiFiNetwork) {
		NSLog(@"Local wifi reachable");
	}
	connected = hasNetWorkConnection;
	return hasNetWorkConnection;
}

- (void)downloadingMessageNotifier {
    if(appDelegate.priorLoads != 1) {
        NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc ] init ];
        // Do processor intensive tasks here knowing that it will not block the main thread.
        NSString *downloadMessage = [NSString stringWithFormat:@"Fetching %@ traffic data. Please wait",appDelegate.homeCity];
        appDelegate.notifier = [[UIAlertView alloc] initWithTitle:@"" message:downloadMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
        activityView2 = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityView2.frame = CGRectMake(139.0f-18.0f, 85.0f, 33.0f, 33.0f);
        [appDelegate.notifier addSubview:activityView2];
        [activityView2 startAnimating];
        [appDelegate.notifier show];	
        [pool drain ];
    }

}

/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
}
*/

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}

- (void)releaseSubviews {
	 
	
}

- (void)didReceiveMemoryWarning {
	
	NSLog(@"didReceiveMemoryWarning: view = %@, superview = %@", [self valueForKey:@"_view"], [[self valueForKey:@"_view"] superview]);
	
	/*
	NSLog(@"commuteLoginNotifier retain count: %i\n", [commuteLoginNotifier retainCount]);
	NSLog(@"LAButton retain count: %i\n", [LAButton retainCount]);
	NSLog(@"SDButton retain count: %i\n", [SDButton retainCount]);
	NSLog(@"BAButton retain count: %i\n", [BAButton retainCount]);
	NSLog(@"loginButton retain count: %i\n", [loginButton retainCount]);
	NSLog(@"guestButton retain count: %i\n", [guestButton retainCount]);
	NSLog(@"signUpButton retain count: %i\n", [signUpButton retainCount]);
	
	NSLog(@"txtUser retain count: %i\n", [txtUser retainCount]);
	NSLog(@"txtPassword retain count: %i\n", [txtPassword retainCount]);
	NSLog(@"receivedData retain count: %i\n", [receivedData retainCount]);
	
	NSLog(@"userName retain count: %i\n", [userName retainCount]);
	NSLog(@"password retain count: %i\n", [password retainCount]);
	NSLog(@"conn retain count: %i\n", [conn retainCount]);
	*/
	
    [super didReceiveMemoryWarning]; 
	// Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data

}


- (void)dealloc {
	NSLog(@"in dealloc");
    /*
	[txtUser release];
	[txtPassword release];
	[loginButton release];
	[guestButton release];
	[signUpButton release];
	[LAButton release];
	[SDButton release];
	[BAButton release];
	[userName release];
	[password release];
	[URLForCommute release];
	[URLForTraffic release];
	[notifier release];
	[menuViewController release];
	[trafficViewController release];
	[conn release];
	
	[activityView1 release];
	[activityView2 release];
	*/
    [super dealloc];
	
}


@end

