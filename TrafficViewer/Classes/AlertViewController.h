//
//  AlertViewController.h
//  TrafficViewer
//
//  Created by Sam Fernald on 8/31/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "DailyReportViewController.h"
#import "AlertViewController.h"
#import "SegmentCreationViewController.h"
#import "Alert.h"
#import "DailyReport.h"
#import "SegmentProfile.h"


@interface AlertViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate, UIAlertViewDelegate>{
    AppDelegate *appDelegate;
    IBOutlet UITableView *alertProfile;
    NSMutableArray *alertParts;
    NSMutableArray *addParts;
    
    UIPickerView * typePicker;
    UIPickerView * startTimePicker;
    UIPickerView * endTimePicker;
    UIPickerView * congestionPicker;
    UIPickerView * notificationPicker;
    
    UIActionSheet *actionSheetType;
    UIActionSheet *actionSheetStart;
    UIActionSheet *actionSheetEnd;
    UIActionSheet *actionSheetCongestion;
    UIActionSheet *actionSheetNotification;
    
    NSMutableArray *typeList;
    NSMutableArray *amTimeList;
    NSMutableArray *pmTimeList;
    NSMutableArray *startTimeList;
    NSMutableArray *endTimeList;
    NSMutableArray *congestionList;
    NSMutableArray *notificationTypeList;
    
    
    NSURLConnection *conn;
    NSMutableData *receivedData;
    NSMutableData *receivedIDData;
    NSMutableData *receivedNewAlertData;
    NSMutableData *receivedEditedAlertData;
    NSMutableArray *alertList;
    UISegmentedControl *segmentedControl;
    
    Alert *morning;
    Alert *evening;
    Alert *altMorning;
    Alert *altEvening;
    
    Alert *selectedAlert;
    
    BOOL isEditingAlerts;
    BOOL isAdding;
    BOOL typeSelected;
    BOOL startTimeSelected;
    BOOL endTimeSelected;
    BOOL congestionSelected;
    BOOL notificationSelected;
    BOOL endTimeEdited;
    BOOL editsDisplayed;
    BOOL alertTypeSet;
    
    int commuteType;
    NSString * startTime;
    NSString * endTime;
    int congestion;
    int notificationType;
    
    int numMorning;
    int numEvening;
    int numAltMorning;
    int numAltEvening;
    
    int selectedAlertID;
        
    int connectionType;
    
}

-(void) editAlert;
-(void) deleteAlert;

-(void) selectType;
-(void) selectStart;
-(void) selectEnd;
-(void) selectCongestion;
-(void) selectNotification;

-(IBAction) addAlert:(id)sender;

-(void) selectedType:(id)sender;
-(void) selectedStart:(id)sender;
-(void) selectedEnd:(id)sender;
-(void) selectedCongestion:(id)sender;
-(void) selectedNotification:(id)sender;


-(void) removePartIn:(NSMutableArray*)parts forType:(int) t;

@end