//
//  SegmentCreationViewController.h
//  TrafficViewer
//
//  Created by Sam Fernald on 8/31/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ProfileCell.h"
#import "ExitListing.h"

@interface SegmentCreationViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate, UIAlertViewDelegate>{
    IBOutlet UIButton * typeButton;
    IBOutlet UIButton * highwayButton;
    IBOutlet UIButton * onRampButton;
    IBOutlet UIButton * offRampButton;
    IBOutlet UIButton * addSegment;
    UIPickerView * typePicker;
    UIPickerView * highwayPicker;
    UIPickerView * onRampPicker;
    UIPickerView * offRampPicker;
    
    UIActionSheet *actionSheetType;
    UIActionSheet *actionSheetHighway;
    UIActionSheet *actionSheetOnRamp;
    UIActionSheet *actionSheetOffRamp;
    
    NSMutableArray *typeList;
    NSMutableArray *highwayList;
    NSMutableArray *exitList;
    NSMutableArray *exitOffList;
    NSMutableArray *exits;
    
    UIAlertView *notifier;
    
    NSMutableData * receivedData;
    NSURLConnection * conn;
    
    int commuteType;
    int highway;
    int onRampID;
    int offRampID;
    int onRampIndex;
    int offRampIndex;
    
    BOOL onRampSelected;
    BOOL offRampSelected;
    
    AppDelegate *appDelegate;
}

@property (nonatomic, retain) NSMutableArray *typeList;
@property (nonatomic, retain) NSMutableArray *highwayList;
@property (nonatomic, retain) NSMutableArray *exitList;
@property (nonatomic, retain) NSMutableArray *exitOffList;

-(IBAction) selectType:(id)sender;
-(IBAction) selectHighway:(id)sender;
-(IBAction) selectOnRamp:(id)sender;
-(IBAction) selectOffRamp:(id)sender;
-(IBAction) addSegment:(id)sender;

-(void) selectedType:(id)sender;
-(void) selectedHighway:(id)sender;
-(void) selectedOnRamp:(id)sender;
-(void) selectedOffRamp:(id)sender;

@end
