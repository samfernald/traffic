//
//  Alert.h
//  TrafficViewer
//
//  Created by Sam Fernald on 9/10/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Alert : NSObject{
    int alertID;
    int commuteType;
    NSString *start;
    NSString *end;
    int congestion;
    int notify;
    
}

@property int alertID;
@property int commuteType;
@property (nonatomic, retain) NSString *start;
@property (nonatomic, retain) NSString *end;
@property int congestion;
@property int notify;

-(id) initWithID:(int)ai 
            type:(int)t 
           start:(NSString *)s 
             end:(NSString *)e 
      congestion:(int)c 
          notify:(int)n;

@end
