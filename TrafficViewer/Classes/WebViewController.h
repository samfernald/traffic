//
//  WebViewController.h
//  TrafficViewer
//
//  Created by Sam Fernald on 8/21/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface WebViewController : UIViewController <UIWebViewDelegate> {
    IBOutlet UIWebView *accountSignup;
    AppDelegate *appDelegate;
}

@property (nonatomic, retain) UIWebView *accountSignup;


@end
