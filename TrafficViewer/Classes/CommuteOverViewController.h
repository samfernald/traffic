//
//  CommuteOverViewController.h
//  TrafficViewer
//
//  Created by Sam Fernald on 11/13/12.
//
//

#import <UIKit/UIKit.h>
#import "Commute.h"
#import "User.h"
#import "AppDelegate.h"



@interface CommuteOverViewController : UIViewController <UITableViewDelegate, UITableViewDelegate> {
    AppDelegate *appDelegate;
	NSMutableData * receivedData;
	NSURLConnection * conn;
    IBOutlet UITableView * commuteSummaryTableView;
    
    User * user;
	Commute * tempMorning;
	Commute * tempAltMorning;
	Commute * tempEvening;
	Commute * tempAltEvening;
	Congestion * tempCong;
	Segment * tempSeg;
    
}

-(void) parseCommute:(NSString*) commuteToParse;

@end
