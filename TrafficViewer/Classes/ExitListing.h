//
//  ExitListing.h
//  TrafficViewer
//
//  Created by Sam Fernald on 9/21/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExitListing : NSObject {
    int exitID;
    int highwayID;
    NSString *exitName;
}

@property int exitID;
@property int highwayID;
@property (nonatomic, retain) NSString * exitName;

-(id) initWithName:(NSString*)n exitId:(int)e highwayId:(int)h;

@end
