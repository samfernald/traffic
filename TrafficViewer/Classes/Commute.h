//
//  Commute.h
//  SimpleCommuteParser
//
//  Created by sfernald on 9/18/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Segment.h"


@interface Commute : NSObject {
	bool morning,alternate;
	NSString * AM_PM;
	NSString * timeStamp;
	NSMutableArray * segments;
	int numSegments;
	//double ** commuteTime;
	int currentTime;
	int averageTime;
	float difference;
}

@property bool morning;
@property bool alternate;

@property(nonatomic,retain) NSString *AM_PM;
@property(nonatomic,retain) NSString *timeStamp;
@property int numSegments;
//@property(nonatomic,retain) double ** commuteTime;
@property int currentTime;
@property int averageTime;
@property float difference;
@property (nonatomic,retain) NSMutableArray * segments;

-(id) initWithType:(bool)morn 
		 alternate:(bool)alt 
	   numSegments:(int)numSegs 
	   currentTime:(int)currTime 
	   averageTime:(int)ave
		difference:(float)diff;

-(id) initWithCommute:(Commute *)c;
-(void) addSegment:(Segment *)seg;


@end
