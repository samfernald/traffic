//
//  AppDelegate.h
//  TrafficViewer
//
//  Created by sfernald on 10/29/08.
//  Copyright __MyCompanyName__ 2008. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "TrafficViewController.h"
#import "Alert.h"
#import "DailyReport.h"
#import "SegmentProfile.h"
#import <GoogleMaps/GoogleMaps.h>
#import "SSKeychain.h"


@interface AppDelegate : NSObject <UIApplicationDelegate, UITabBarDelegate, UIAlertViewDelegate, CLLocationManagerDelegate, UINavigationControllerDelegate> {
   
    UIWindow *window;
    UINavigationController *navigationController;
    UINavigationController *menuNavController;
    UITabBarController *tabBarController;
    //IBOutlet RootViewController *rootViewController;
    
    UIStoryboard *storyBoard;
    
	UIAlertView *notifier;
	UIAlertView *mapNotifier;
	UIAlertView *gMapNotifier;
	User * user;
	int selectedHighway;
	NSString * selectedHighwayName;
	NSString * selectedHighwayImagePath;
	NSString *userName,*password,*homeCity;
	NSString *urlForCommute;
    NSString *urlForCommuteJson;
	
    Alert *alertToEdit;
    DailyReport *dailyReportToEdit;
    SegmentProfile *segmentToEdit;
    Commute *comuteToLoad;
	NSString *tempCommute;
	UIImage *map1, *map2, *map3;
	CFStringRef *un;
	int selectedCity;
    int userID;
    int screenToLoad;
    BOOL morningExists;
    BOOL eveningExists;
    BOOL altMorningExists;
    BOOL altEveningExists;
	BOOL firstTimeLoadingSpeed;
	BOOL userCreated;
	BOOL speedsViewLoaded;
	BOOL autoLoadedGuest;
	BOOL initMapLoad;
	BOOL calTransMapLoaded;
	BOOL gMapLoaded;
	BOOL usingLocation;
	BOOL refreshingCaltrans;
	BOOL firstReset;
    BOOL askToRate;
    BOOL sentProbeData;
    BOOL mapToLoad;
    BOOL mapViewLoaed;
    int googleAnalytics;
    int priorLoads;
    float userLat;
    float userLong;
	NSString * hasAnAccount;
	NSString * sdMapUrl;
	NSString * baMapUrl;
	NSString * la1MapUrl;
	NSString * la2MapUrl;
	NSString * la3MapUrl;
	NSString * cpuUrl;
	NSDate *lastRefresh;
	NSDate *lastLocationUpdate;
    	
	int testingCounter;
    
    NSMutableArray *probeReadings;
    CLLocationManager *locManager;
    CLLocationSpeed speed;
    NSTimer *timer;
	
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;
@property (nonatomic, retain) UIStoryboard *storyBoard;
@property (nonatomic, retain) UINavigationController *menuNavController;
@property (nonatomic, strong) UITabBarController *tabBarController;
@property (nonatomic, retain) UIAlertView *notifier;
@property (nonatomic, retain) UIAlertView *mapNotifier;
@property (nonatomic, retain) UIAlertView *gMapNotifier;
@property (nonatomic, retain) User *user;
@property (nonatomic, retain) NSString * selectedHighwayName;
@property (nonatomic, retain) NSString * selectedHighwayImagePath;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * homeCity;
@property (nonatomic, retain) NSString * hasAnAccount;
@property (nonatomic, retain) NSString * urlForCommute;
@property (nonatomic, retain) NSString * urlForCommuteJson;
@property (nonatomic, retain) NSString * tempCommute;
@property (nonatomic, strong) NSString * UUID;

@property (nonatomic, retain) Alert * alertToEdit;
@property (nonatomic, retain) DailyReport * reportToEdit;
@property (nonatomic, retain) SegmentProfile * segmentToEdit;
@property (nonatomic, retain) Commute *commuteToLoad;

@property (nonatomic, retain) UIImage * map1;
@property (nonatomic, retain) UIImage * map2;
@property (nonatomic, retain) UIImage * map3;

@property (nonatomic, retain) NSString * sdMapUrl;
@property (nonatomic, retain) NSString * baMapUrl;
@property (nonatomic, retain) NSString * la1MapUrl;
@property (nonatomic, retain) NSString * la2MapUrl;
@property (nonatomic, retain) NSString * la3MapUrl;

@property (nonatomic, retain) NSString * cpuUrl;

@property (nonatomic, retain) NSDate * lastRefresh;
@property (nonatomic, retain) NSDate * lastLocationUpdate;

@property(nonatomic,retain) NSTimer *timer;

@property int selectedCity;
@property int selectedHighway;
@property int screenToLoad;
@property BOOL firstTimeLoadingSpeed;
@property BOOL userCreated;
@property BOOL speedsViewLoaded;
@property int testingCounter;
@property BOOL morningExists;
@property BOOL eveningExists;
@property BOOL altMorningExists;
@property BOOL altEveningExists;
@property BOOL autoLoadedGuest;
@property BOOL initMapLoad;
@property BOOL calTransMapLoaded;
@property BOOL gMapLoaded;
@property BOOL usingLocation;
@property BOOL refreshingCaltrans;
@property BOOL firstReset;
@property BOOL askToRate;
@property BOOL mapToLoad;
@property BOOL mapViewLoaded;
@property int googleAnalytics;
@property int priorLoads;
@property int userID;
@property float userLat;
@property float userLong;

+(natural_t) get_free_memory;

+(void) showAlert:(NSString*) message;

+(void) dismissAlert;


@end

