//
//  pointInfo.h
//  mapLines
//
//  Created by sfernald on 2/17/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CSMapRouteLayerView.h"
#import "IncidentPin.h"


@implementation CSMapRouteLayerView
@synthesize mapView        = _mapView;
@synthesize pointsZoom0    = _pointsZoom0;
@synthesize pointsZoom1	   = _pointsZoom1;
@synthesize pointsZoom2    = _pointsZoom2;
@synthesize pointsZoom3    = _pointsZoom3;
@synthesize pointsZoom4    = _pointsZoom4;
@synthesize lineColor      = _lineColor; 
@synthesize pointDetails   = _pointDetails;
@synthesize pointsInSegment;
@synthesize zoomLevel;
@synthesize initMapLoad;
@synthesize pointsAdded;

-(id) initWithMapView:(MKMapView*)mapView {
	NSLog(@"map width = %f and height = %f",mapView.frame.size.width,mapView.frame.size.height);
	self = [super initWithFrame:CGRectMake(0, 0, mapView.frame.size.width, mapView.frame.size.height)];
	[self setBackgroundColor:[UIColor clearColor]];
	self.lineColor = [UIColor redColor];

	[self setMapView:mapView];
	self.mapView.showsUserLocation = YES;

	MKCoordinateRegion region;

	region.center.latitude     = 34.0;
	region.center.longitude    = -118.24296;

	region.span.latitudeDelta  = 30;
	region.span.longitudeDelta = 10;
	self.mapView.scrollEnabled = TRUE;
	self.userInteractionEnabled = NO;
	[self.mapView setDelegate:self];
	[self.mapView addSubview:self];
	[self.mapView setRegion:region];
		
	initMapLoad = TRUE;
	pointsAdded = FALSE;
	return self;
}

-(void) addPoints:(NSArray*)routePointsZoom0
	  pointsZoom1:(NSArray*)routePointsZoom1
	  pointsZoom2:(NSArray*)routePointsZoom2
	  pointsZoom3:(NSArray*)routePointsZoom3 
	  pointsZoom4:(NSArray*)routePointsZoom4
	 pointDetails:(NSArray*)pointDets {

	pointsAdded = TRUE;
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	locationController = [[MyCLController alloc] init];
	locationController.delegate = self;
	locationHasUpdated = FALSE;
	[locationController.locationManager startUpdatingLocation];
	
	[self setPointsZoom0:routePointsZoom0];
	[self setPointsZoom1:routePointsZoom1];
	[self setPointsZoom2:routePointsZoom2];
	[self setPointsZoom3:routePointsZoom3];
	[self setPointsZoom4:routePointsZoom4];
	[self setPointDetails:pointDets];
	zoomLevel = 2;
	NSLog(@"points addded");

	/*
	 MKCoordinateRegion region;
	 float lat,lng;
	 if(appDelegate.selectedCity == 1) {
	 lat = 32.9;
	 lng = -117.24115;
	 } else if(appDelegate.selectedCity == 2) {
	 lat = 34.0;
	 lng = -118.24296;
	 } else {
	 lat = 37.568528;
	 lng = -122.007294;
	 }
	 region.center.latitude     = lat;
	 region.center.longitude    = lng;
	 
	 region.span.latitudeDelta  = .1;
	 region.span.longitudeDelta = .1;
	 */
	
	//test markers
	/*
	CLLocationCoordinate2D c;
	c.latitude = 33.87420536;
	c.longitude = -118.3432597;
	CLLocationCoordinate2D c2;
	c2.latitude = 33.6862322;
	c2.longitude = -117.8656062;
	
	testPin *addAnnotation = [[testPin alloc] initWithCoordinate:c];
	[self.mapView addAnnotation:addAnnotation];
	[addAnnotation release];
	testPin *addAnnotation2 = [[testPin alloc] initWithCoordinate:c2];
	[self.mapView addAnnotation:addAnnotation2];
	[addAnnotation release];
	 */
}

-(void) addIncidents:(NSMutableArray *)incidents {
	NSLog(@"adding %d incidents",[incidents count]);
	if ([incidents count] > 0) {
		CLLocationCoordinate2D c;
		for (int i = 0; i<[incidents count]; i++) {
			c.latitude = [[[incidents objectAtIndex:i] objectForKey:@"latitude"] floatValue];
			c.longitude = [[[incidents objectAtIndex:i] objectForKey:@"longitude"] floatValue];
			IncidentPin *addAnnotation = [[IncidentPin alloc] initWithCoordinate:c title:[[incidents objectAtIndex:i] objectForKey:@"title"]  description:[[incidents objectAtIndex:i] objectForKey:@"description"] postdate:[[incidents objectAtIndex:i] objectForKey:@"postDate"]];
			[self.mapView addAnnotation:addAnnotation];
			//[addAnnotation release];

		}
	}
}



-(id) initWithPoints:(NSArray*)routePointsZoom0 
		 pointsZoom1:(NSArray*)routePointsZoom1  
		 pointsZoom2:(NSArray*)routePointsZoom2
		 pointsZoom3:(NSArray*)routePointsZoom3 
		 pointsZoom4:(NSArray*)routePointsZoom4
		pointDetails:(NSArray*)pointDets 
			 mapView:(MKMapView*)mapView
{
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	locationController = [[MyCLController alloc] init];
	locationController.delegate = self;
	locationHasUpdated = FALSE;
	[locationController.locationManager startUpdatingLocation];
	
	self = [super initWithFrame:CGRectMake(0, 0, mapView.frame.size.width, mapView.frame.size.height)];
	[self setBackgroundColor:[UIColor clearColor]];
	self.lineColor = [UIColor redColor];
	
	[self setMapView:mapView];
	[self setPointsZoom0:routePointsZoom0];
	[self setPointsZoom1:routePointsZoom1];
	[self setPointsZoom2:routePointsZoom2];
	[self setPointsZoom3:routePointsZoom3];
	[self setPointsZoom4:routePointsZoom4];
	[self setPointDetails:pointDets];
	zoomLevel = 2;
	//mapView.showsUserLocation = YES;
	/*
	MKCoordinateRegion region;
	float lat,lng;
	if(appDelegate.selectedCity == 1) {
		lat = 32.9;
		lng = -117.24115;
	} else if(appDelegate.selectedCity == 2) {
		lat = 34.0;
		lng = -118.24296;
	} else {
		lat = 37.568528;
		lng = -122.007294;
	}
	region.center.latitude     = lat;
	region.center.longitude    = lng;
	
	region.span.latitudeDelta  = .1;
	region.span.longitudeDelta = .1;
	*/
	//[self.mapView setRegion:region];
	[self.mapView setDelegate:self];
	[self.mapView addSubview:self];
	/*
	CLLocationCoordinate2D c;
	c.latitude = 33.82652187;
	c.longitude = -118.206982;
	CLLocationCoordinate2D c2;
	c2.latitude = 33.82616152;
	c2.longitude = -118.2489531;
	testPin *addAnnotation = [[testPin alloc] initWithCoordinate:c];
	[self.mapView addAnnotation:addAnnotation];
	[addAnnotation release];
	testPin *addAnnotation2 = [[testPin alloc] initWithCoordinate:c2];
	[self.mapView addAnnotation:addAnnotation2];
	[addAnnotation release];
	 */
	initMapLoad = TRUE;
	return self;
}

- (void)locationUpdate:(CLLocation *)location {
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	MKCoordinateRegion region;
	
	region.center.latitude     = location.coordinate.latitude;
	region.center.longitude    = location.coordinate.longitude;
	
	region.span.latitudeDelta  = .2;
	region.span.longitudeDelta = .2;
	if(!locationHasUpdated) {
		locationHasUpdated = TRUE;
		appDelegate.usingLocation = YES;
		//[locationController.locationManager stopUpdatingLocation];
		
		//appDelegate.lastLocationUpdate = [[NSDate date] retain];
		[self.mapView setRegion:region animated:YES];
	}
}

- (void)locationError:(NSError*) error {	
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[locationController.locationManager stopUpdatingLocation];
	appDelegate.usingLocation = NO;
	float lat,lng;
	if(appDelegate.selectedCity == 1) {
		lat = 32.9;
		lng = -117.24115;
	} else if(appDelegate.selectedCity == 2) {
		lat = 34.0;
		lng = -118.24296;
	} else {
		lat = 37.568528;
		lng = -122.007294;
	}
	MKCoordinateRegion region;
	
	region.center.latitude     = lat;
	region.center.longitude    = lng;
	
	region.span.latitudeDelta  = .2;
	region.span.longitudeDelta = .2;
	[self.mapView setRegion:region animated: YES];
}


- (void) centerMap {

	[self.mapView setCenterCoordinate:self.mapView.userLocation.location.coordinate animated:YES];
}
	

- (void)drawRect:(CGRect)rect {
	if(pointsAdded) {
	NSMutableArray * pointsForSeg = [[NSMutableArray alloc] init];
	// only draw our lines if we're not in the moddie of a transition and we 
	// acutally have some points to draw. 
	NSArray* points;
	if(zoomLevel == 0) {
		points = self.pointsZoom0;
	} else if(zoomLevel == 1) {
		points = self.pointsZoom1;
	} else if(zoomLevel == 2) {
		points = self.pointsZoom2;
	} else if(zoomLevel == 3) {
		points = self.pointsZoom3;
	} else {
		points = self.pointsZoom4;
	}
	if(!self.hidden && nil != points && points.count > 0) {
		int priorHighway = [[self.pointDetails objectAtIndex:0] highway];
		int priorSpeed;
		int newSpeed;
		int mph = [[self.pointDetails objectAtIndex:0] mph];
		if(mph <= 35 && mph > 0) {
			priorSpeed = 1;
		}else if(mph <= 50 && mph > 35) {
			priorSpeed = 2;
		} else if(mph > 50) {
			priorSpeed = 3;
		}
		for(int idx = 0; idx < points.count; idx++){
			pointInfo * p = [[[pointInfo alloc] initWithHwy: [[self.pointDetails objectAtIndex:idx] highway] speed: [[self.pointDetails objectAtIndex:idx] mph]] autorelease];
			if(zoomLevel > 2) {
				
				if(priorHighway == p.highway) {
					mph =  p.mph;
					if(mph <= 35 && mph > 0) {
						newSpeed = 1;
					} else if(mph <= 50 && mph > 35) {
						newSpeed = 2;
					} else if(mph > 50 || mph == 0) {
						newSpeed = 3;
					}
					if(priorSpeed == newSpeed) {
						[pointsForSeg addObject:[points objectAtIndex:idx]];
					} else {
						self.pointsInSegment = pointsForSeg;
						[self addLineSegment:self.pointsInSegment speedRange:priorSpeed];
						[pointsForSeg removeAllObjects];
						self.pointsInSegment = nil;
						if(!(idx-4 < 0)){
							[pointsForSeg addObject:[points objectAtIndex:idx-4]];
						}
						[pointsForSeg addObject:[points objectAtIndex:idx]];
						priorSpeed = newSpeed;
					} 
					
				}else {
					self.pointsInSegment = pointsForSeg;
					[self addLineSegment:self.pointsInSegment speedRange:priorSpeed];
					[pointsForSeg removeAllObjects];
					self.pointsInSegment = nil;
					[pointsForSeg addObject:[points objectAtIndex:idx]];
					priorHighway = [[self.pointDetails objectAtIndex:idx] highway];
				}
				idx+=3;					
			} else if(zoomLevel == 2){
				if(priorHighway == p.highway) {
					mph = p.mph;
					if(mph <= 35 && mph > 0) {
						newSpeed = 1;
					} else if(mph <= 50 && mph > 35) {
						newSpeed = 2;
					} else if(mph > 50 || mph == 0) {
						newSpeed = 3;
					}
					if(priorSpeed == newSpeed) {
						[pointsForSeg addObject:[points objectAtIndex:idx]];
					} else {
						self.pointsInSegment = pointsForSeg;
						[self addLineSegment:self.pointsInSegment speedRange:priorSpeed];
						[pointsForSeg removeAllObjects];
						self.pointsInSegment = nil;
						if(!(idx-2 < 0)){
							[pointsForSeg addObject:[points objectAtIndex:idx-2]];
						}
						[pointsForSeg addObject:[points objectAtIndex:idx]];
						priorSpeed = newSpeed;
					} 
					
				}else {
					self.pointsInSegment = pointsForSeg;
					[self addLineSegment:self.pointsInSegment speedRange:priorSpeed];
					[pointsForSeg removeAllObjects];
					self.pointsInSegment = nil;
					[pointsForSeg addObject:[points objectAtIndex:idx]];
					priorHighway = p.highway;
				}
				idx++;
			} else {
				CLLocationCoordinate2D c = [[points objectAtIndex:idx] coordinate];
				if( c.latitude >= self.mapView.region.center.latitude - (self.mapView.region.span.latitudeDelta*1.5) &&
					c.latitude <= self.mapView.region.center.latitude + (self.mapView.region.span.latitudeDelta*1.5) &&
					c.longitude >= self.mapView.region.center.longitude - (self.mapView.region.span.longitudeDelta*1.5) &&
					c.longitude <= self.mapView.region.center.longitude + (self.mapView.region.span.longitudeDelta*1.5)) {
				
					if(priorHighway == p.highway) {
						mph = p.mph;
						if(mph <= 35 && mph > 0) {
							newSpeed = 1;
						} else if(mph <= 50 && mph > 35) {
							newSpeed = 2;
						} else if(mph > 50 || mph == 0) {
							newSpeed = 3;
						}
						if(priorSpeed == newSpeed) {
							[pointsForSeg addObject:[points objectAtIndex:idx]];
						} else {
							self.pointsInSegment = pointsForSeg;
							[self addLineSegment:self.pointsInSegment speedRange:priorSpeed];
							[pointsForSeg removeAllObjects];
							self.pointsInSegment = nil;
							[pointsForSeg addObject:[points objectAtIndex:idx-1]];
							[pointsForSeg addObject:[points objectAtIndex:idx]];
							priorSpeed = newSpeed;
						} 
					}else {
						self.pointsInSegment = pointsForSeg;
						[self addLineSegment:self.pointsInSegment speedRange:priorSpeed];
						[pointsForSeg removeAllObjects];
						self.pointsInSegment = nil;
						[pointsForSeg addObject:[points objectAtIndex:idx]];
						priorHighway = [[self.pointDetails objectAtIndex:idx] highway];
					}
				} else {
					idx+=4;
				}
			}
		}
		self.pointsInSegment = pointsForSeg;
		[self addLineSegment:self.pointsInSegment speedRange:priorSpeed];
	
	}
	}
	initMapLoad = FALSE;
}


-(void) addLineSegment:(NSArray*)pointsToAdd speedRange:(int)speed {
	if(!self.hidden && nil != pointsToAdd && pointsToAdd.count > 0){
		CGContextRef context = UIGraphicsGetCurrentContext(); 
	
		CGContextSetRGBFillColor(context, 0.0, 0.0, 1.0, 1.0);
		if(speed == 1) {
			self.lineColor = [UIColor colorWithRed:225.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.7];
		} else if(speed == 2) {
			self.lineColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:0.0/255.0 alpha:1.0];
		} else if(speed == 3) {
			self.lineColor = [UIColor colorWithRed:39.0/255.0 green:189.0/255.0 blue:34.0/255.0 alpha:0.8];
		}
	
		// Draw them with a 2.0 stroke width so they are a bit more visible.
		CGContextSetLineWidth(context, 5.0);
		for(int idx = 0; idx < pointsToAdd.count; idx++){
			CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
			CLLocation* location = [pointsToAdd objectAtIndex:idx];
			CGPoint point = [_mapView convertCoordinate:location.coordinate toPointToView:self];
			if(idx == 0){
				// move to the first point
				CGContextMoveToPoint(context, point.x, point.y);
			}else{
				CGContextAddLineToPoint(context, point.x, point.y);
			}
		}
		CGContextStrokePath(context);
	}
}

#pragma mark mapView delegate functions
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
	// turn off the view of the route as the map is chaning regions. This prevents
	// the line from being displayed at an incorrect positoin on the map during the
	// transition. 
	self.hidden = YES;
}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
	// re-enable and re-poosition the route display. 
	self.hidden = NO;
	if(mapView.region.span.latitudeDelta <= 0.02) {
		zoomLevel = 0;
	} else if(mapView.region.span.latitudeDelta > 0.02 && mapView.region.span.latitudeDelta <= .05) {
		zoomLevel = 1;
	} else if(mapView.region.span.latitudeDelta > 0.05 && mapView.region.span.latitudeDelta <= 0.15) {
		zoomLevel = 2;
	} else if(mapView.region.span.latitudeDelta > 0.15 && mapView.region.span.latitudeDelta <= .25) {
		zoomLevel = 3;
	} else {
		zoomLevel = 4;
	}
	[self setNeedsDisplay];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
    // The view is configured for single touches only.
    UITouch* aTouch = [touches anyObject];
    _startLocation = [aTouch locationInView:[self superview]];
    _originalCenter = self.mapView.center;
	
    [super touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	
    UITouch* aTouch = [touches anyObject];
    CGPoint newLocation = [aTouch locationInView:[self superview]];
    CGPoint newCenter;
	
    // If the user's finger moved more than 5 pixels, begin the drag.
    if ((abs(newLocation.x - _startLocation.x) > 5.0) || (abs(newLocation.y - _startLocation.y) > 5.0)) {
        _isMoving = YES;		
    }
	
    // If dragging has begun, adjust the position of the view.
    if (_mapView && _isMoving) {
        newCenter.x = _originalCenter.x + (newLocation.x - _startLocation.x);
        newCenter.y = _originalCenter.y + (newLocation.y - _startLocation.y);
        self.center = newCenter;
    } else {
        // Let the parent class handle it.
        [super touchesMoved:touches withEvent:event];		
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
    if (_mapView && _isMoving) {				
        // Update the map coordinate to reflect the new position.
        CGPoint newCenter = self.center;
        CLLocationCoordinate2D newCoordinate = [self.mapView convertPoint:newCenter toCoordinateFromView:self.superview];
			
        // Clean up the state information.
        _startLocation = CGPointZero;
        _originalCenter = CGPointZero;
        _isMoving = NO;		
    } else {
        [super touchesEnded:touches withEvent:event];		
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	
    if (_mapView && _isMoving) {
        // Move the view back to its starting point.
        self.center = _originalCenter;
		
        // Clean up the state information.
        _startLocation = CGPointZero;
        _originalCenter = CGPointZero;
        _isMoving = NO;
    } else {
        [super touchesCancelled:touches withEvent:event];		
    }
}



/*
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
	NSLog(@"hit");
    //  Get the UIView (in this case, a UIScrollView) that
    //  would ordinarily handle the events
	UIScrollView * view = [[[[self.mapView subviews] objectAtIndex:0] subviews] objectAtIndex:0];
	view = (UIScrollView*)[super hitTest:point withEvent:event];
	
    //  But let it be known we'll handle them instead.
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
    NSLog(@"Touches Began");
    //  add your custom annotation behavior here
	UIScrollView * view = [[[[self.mapView subviews] objectAtIndex:0] subviews] objectAtIndex:0];
    [view touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	
    NSLog(@"Touches moved");
    //  add your custom annotation behavior here
	UIScrollView * view = [[[[self.mapView subviews] objectAtIndex:0] subviews] objectAtIndex:0];
    [view touchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	NSLog(@"touches ended");
	UIScrollView * view = [[[[self.mapView subviews] objectAtIndex:0] subviews] objectAtIndex:0];
	[view touchesEnded:touches withEvent:event];
}
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	NSLog(@"touches cancelled");
	UIScrollView * view = [[[[self.mapView subviews] objectAtIndex:0] subviews] objectAtIndex:0];
	[view touchesCancelled:touches withEvent:event];
}
 */


-(void) dealloc
{
	[_pointsZoom0 release];
	[_pointsZoom1 release];
	[_mapView release];
	[super dealloc];
}

@end
