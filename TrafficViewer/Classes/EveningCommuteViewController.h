//
//  EveningCommuteViewController.h
//  TrafficViewer
//
//  Created by sfernald on 10/29/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TouchXML.h"
#import "Commute.h"
#import "Congestion.h"
#import "Segment.h"
#import "User.h"
#import "AppDelegate.h"
#import "SoundEffect.h"


@interface EveningCommuteViewController : UITableViewController <UIAccelerometerDelegate>{
	IBOutlet UIBarButtonItem * altButton;
	IBOutlet UIBarButtonItem * morningButton;
	IBOutlet UIBarButtonItem * refreshButton;

	AppDelegate * appDelegate;
	NSMutableData * receivedData;
	NSURLConnection * conn;
	
	NSMutableArray * congestionInfo;
	NSMutableArray * userInfo;
	NSMutableArray * morningCommuteInfo;
	NSMutableArray * eveningCommuteInfo;
	NSMutableArray * altMorningCommuteInfo;
	NSMutableArray * altEveningCommuteInfo;
	NSMutableArray * segmentInfo;
	NSMutableArray * segmentsCounter;
	NSMutableArray * commuteCells;
	NSMutableArray * incidentInfo;
	UIAlertView * notifier;
	UIAlertView * noCommuteNotifier;
	
	BOOL histeresisExcited;
	BOOL priAlt;
	BOOL commuteInfoDone;
	UIAcceleration* lastAcceleration;

	int morningCommuteSegments,altMorningCommuteSegments,eveningCommuteSegments,altEveningCommuteSegments,segmentsTracker;
	int congestionIndex,segmentsRemaining,segmentsDone;
	
	//User contains user info
	User * user;
	Commute * tempMorning;
	Commute * tempAltMorning;
	Commute * tempEvening;
	Commute * tempAltEvening;
	Congestion * tempCong;
	Segment * tempSeg;
}

@property(nonatomic,retain) UIBarButtonItem * altButton;
@property(nonatomic,retain) UIBarButtonItem * morningButton;
@property(nonatomic,retain) UIBarButtonItem * refreshButton;
@property(nonatomic,retain) UIAcceleration * lastAcceleration;



-(void) createCommuteInfo;

-(void)prepareCommuteCells:(Commute *)c;

-(BOOL) L0AccelerationIsShaking:(UIAcceleration*)last current:(UIAcceleration*)current threshold:(double) threshold;


@end
