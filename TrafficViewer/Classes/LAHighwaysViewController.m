//
//  LAHighwaysViewController.m
//  TrafficViewer
//
//  Created by sfernald on 10/29/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "LAHighwaysViewController.h"
#import "AppDelegate.h"
#import "HighwayFlowViewController.h"


@implementation LAHighwaysViewController

/*
// Override initWithNibName:bundle: to load the view using a nib file then perform additional customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically.
- (void)loadView {
		

}
*/


// Implement viewDidLoad to do additional setup after loading the view.
- (void)viewDidLoad {
    [super viewDidLoad];
	[self setTitle:@"LA Highways"];
	self.navigationItem.hidesBackButton= YES;
	UIScrollView * scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 1000)];//(0,0,320,160)
	scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	
	[scrollView setContentSize:CGSizeMake(320,1000)];
	
	scrollView.bounces = NO;
	scrollView.scrollEnabled = YES;
	[self.view setBackgroundColor:[[UIColor colorWithRed: 249.0/255.0 green:207.0/255.0 blue:28.0/255.0 alpha:1]retain]];
	//self.view.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"TrafficAppBackground.png"]];
	[scrollView addSubview:self.view ];
	self.view = scrollView;
	[scrollView retain];
	
}

-(IBAction) highwaySelected:(id)sender{
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	appDelegate.selectedHighway = [sender tag];
	switch([sender tag]){
		case 1: {appDelegate.selectedHighwayImagePath = @"2.png"; break;}
		case 2: {appDelegate.selectedHighwayImagePath = @"2.png"; break;}
		case 3: {appDelegate.selectedHighwayImagePath = @"5.png"; break;}
		case 4: {appDelegate.selectedHighwayImagePath = @"5.png"; break;}
		case 5: {appDelegate.selectedHighwayImagePath = @"10.png"; break;}
		case 6: {appDelegate.selectedHighwayImagePath = @"10.png"; break;}
		case 7: {appDelegate.selectedHighwayImagePath = @"14.png"; break;}
		case 8: {appDelegate.selectedHighwayImagePath = @"14.png"; break;}
		case 49: {appDelegate.selectedHighwayImagePath = @"15.png"; break;}
		case 50: {appDelegate.selectedHighwayImagePath = @"15.png"; break;}
		case 9: {appDelegate.selectedHighwayImagePath = @"22.png"; break;}
		case 10: {appDelegate.selectedHighwayImagePath = @"22.png"; break;}
		case 13: {appDelegate.selectedHighwayImagePath = @"55.png"; break;}
		case 14: {appDelegate.selectedHighwayImagePath = @"55.png"; break;}
		case 15: {appDelegate.selectedHighwayImagePath = @"57.png"; break;}
		case 16: {appDelegate.selectedHighwayImagePath = @"57.png"; break;}
		case 17: {appDelegate.selectedHighwayImagePath = @"60.png"; break;}
		case 18: {appDelegate.selectedHighwayImagePath = @"60.png"; break;}
		case 51: {appDelegate.selectedHighwayImagePath = @"71.png"; break;}
		case 52: {appDelegate.selectedHighwayImagePath = @"71.png"; break;}
		case 19: {appDelegate.selectedHighwayImagePath = @"73.png"; break;}
		case 20: {appDelegate.selectedHighwayImagePath = @"73.png"; break;}
		case 21: {appDelegate.selectedHighwayImagePath = @"91.png"; break;}
		case 22: {appDelegate.selectedHighwayImagePath = @"91.png"; break;}
		case 23: {appDelegate.selectedHighwayImagePath = @"101.png"; break;}
		case 24: {appDelegate.selectedHighwayImagePath = @"101.png"; break;}
		case 25: {appDelegate.selectedHighwayImagePath = @"105.png"; break;}
		case 26: {appDelegate.selectedHighwayImagePath = @"105.png"; break;}
		case 27: {appDelegate.selectedHighwayImagePath = @"110.png"; break;}
		case 28: {appDelegate.selectedHighwayImagePath = @"110.png"; break;}
		case 29: {appDelegate.selectedHighwayImagePath = @"118.png"; break;}
		case 30: {appDelegate.selectedHighwayImagePath = @"118.png"; break;}
		case 31: {appDelegate.selectedHighwayImagePath = @"133.png"; break;}
		case 32: {appDelegate.selectedHighwayImagePath = @"133.png"; break;}
		case 33: {appDelegate.selectedHighwayImagePath = @"134.png"; break;}
		case 34: {appDelegate.selectedHighwayImagePath = @"134.png"; break;}
		case 35: {appDelegate.selectedHighwayImagePath = @"170.png"; break;}
		case 36: {appDelegate.selectedHighwayImagePath = @"170.png"; break;}
		case 37: {appDelegate.selectedHighwayImagePath = @"210.png"; break;}
		case 38: {appDelegate.selectedHighwayImagePath = @"210.png"; break;}
		case 53: {appDelegate.selectedHighwayImagePath = @"215.png"; break;}
		case 54: {appDelegate.selectedHighwayImagePath = @"215.png"; break;}
		case 39: {appDelegate.selectedHighwayImagePath = @"241.png"; break;}
		case 40: {appDelegate.selectedHighwayImagePath = @"241.png"; break;}
		case 41: {appDelegate.selectedHighwayImagePath = @"261.png"; break;}
		case 42: {appDelegate.selectedHighwayImagePath = @"261.png"; break;}
		case 43: {appDelegate.selectedHighwayImagePath = @"405.png"; break;}
		case 44: {appDelegate.selectedHighwayImagePath = @"405.png"; break;}
		case 45: {appDelegate.selectedHighwayImagePath = @"605.png"; break;}
		case 46: {appDelegate.selectedHighwayImagePath = @"605.png"; break;}
		case 47: {appDelegate.selectedHighwayImagePath = @"710.png"; break;}
		case 48: {appDelegate.selectedHighwayImagePath = @"710.png"; break;}
	}
	
	HighwayFlowViewController *view = [[HighwayFlowViewController alloc] initWithNibName:@"HighwayFlowViewController" bundle:[NSBundle mainBundle]];
	[[self navigationController] pushViewController:view animated:YES];
}

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


- (void)dealloc {
    [super dealloc];
}


@end
