//
//  AlertViewController.m
//  TrafficViewer
//
//  Created by Sam Fernald on 8/31/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import "AlertViewController.h"
#import "CJSONSerializer.h"
#import "CJSONDeserializer.h"
#import "ProfileCell.h"

@interface AlertViewController ()

@end

@implementation AlertViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    alertParts = [[NSMutableArray alloc] init];
    addParts = [[NSMutableArray alloc] init];
    isEditingAlerts = NO;
    isAdding = NO;
    
    [actionSheetType dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetType=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarType = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarType sizeToFit];
    pickerToolbarType.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsType = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnType = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedType:)];
    [barItemsType addObject:doneBtnType];
    [doneBtnType release];
    [pickerToolbarType setItems:barItemsType animated:YES];
    [actionSheetType addSubview:pickerToolbarType];
    [barItemsType release];
    [pickerToolbarType release];
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    typePicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    typePicker.showsSelectionIndicator = YES;
    typePicker.dataSource = self;
    typePicker.delegate = self;
    typePicker.tag = 0;
    typePicker.showsSelectionIndicator = YES;
    [actionSheetType addSubview:typePicker];
    
    //--------------Start ACTION SHEET
    [actionSheetStart dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetStart=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarStart = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarStart sizeToFit];
    pickerToolbarStart.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsStart = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnStart = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedStart:)];
    [barItemsStart addObject:doneBtnStart];
    [doneBtnStart release];
    [pickerToolbarStart setItems:barItemsStart animated:YES];
    [actionSheetStart addSubview:pickerToolbarStart];
    [barItemsStart release];
    [pickerToolbarStart release];
    startTimePicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    startTimePicker.showsSelectionIndicator = YES;
    startTimePicker.dataSource = self;
    startTimePicker.delegate = self;
    startTimePicker.tag = 1;
    startTimePicker.showsSelectionIndicator = YES;
    [actionSheetStart addSubview:startTimePicker];
    
    //--------------End ACTION SHEET
    [actionSheetEnd dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetEnd = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarEnd = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarEnd sizeToFit];
    pickerToolbarEnd.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsEnd = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnEnd = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedEnd:)];
    [barItemsEnd addObject:doneBtnEnd];
    [doneBtnEnd release];
    [pickerToolbarEnd setItems:barItemsEnd animated:YES];
    [actionSheetEnd addSubview:pickerToolbarEnd];
    [barItemsEnd release];
    [pickerToolbarEnd release];
    endTimePicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    endTimePicker.showsSelectionIndicator = YES;
    endTimePicker.dataSource = self;
    endTimePicker.delegate = self;
    endTimePicker.tag = 2;
    endTimePicker.showsSelectionIndicator = YES;
    [actionSheetEnd addSubview:endTimePicker];
    
    //--------------congestion ACTION SHEET
    [actionSheetCongestion dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetCongestion=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarCongestion = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarCongestion sizeToFit];
    pickerToolbarCongestion.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsCongestion = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnCongestion = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedCongestion:)];
    [barItemsCongestion addObject:doneBtnCongestion];
    [doneBtnCongestion release];
    [pickerToolbarCongestion setItems:barItemsCongestion animated:YES];
    [actionSheetCongestion addSubview:pickerToolbarCongestion];
    [barItemsCongestion release];
    [pickerToolbarCongestion release];
    congestionPicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    congestionPicker.showsSelectionIndicator = YES;
    congestionPicker.dataSource = self;
    congestionPicker.delegate = self;
    congestionPicker.tag = 3;
    congestionPicker.showsSelectionIndicator = YES;
    [actionSheetCongestion addSubview:congestionPicker];
    
    //--------------notification ACTION SHEET
    [actionSheetNotification dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetNotification=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarNotification = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarNotification sizeToFit];
    pickerToolbarNotification.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsNotification = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnNotification = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedNotification:)];
    [barItemsNotification addObject:doneBtnNotification];
    [doneBtnNotification release];
    [pickerToolbarNotification setItems:barItemsNotification animated:YES];
    [actionSheetNotification addSubview:pickerToolbarNotification];
    [barItemsNotification release];
    [pickerToolbarNotification release];
    notificationPicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    notificationPicker.showsSelectionIndicator = YES;
    notificationPicker.dataSource = self;
    notificationPicker.delegate = self;
    notificationPicker.tag = 4;
    notificationPicker.showsSelectionIndicator = YES;
    [actionSheetNotification addSubview:notificationPicker];
    
    typeList = [[NSMutableArray alloc] init];
    amTimeList = [[NSMutableArray alloc] init];
    pmTimeList = [[NSMutableArray alloc] init];
    startTimeList = [[NSMutableArray alloc] init];
    endTimeList = [[NSMutableArray alloc] init];
    congestionList = [[NSMutableArray alloc] init];
    notificationTypeList = [[NSMutableArray alloc] init];
    
    [amTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 5:00 AM" type:100 connection:@"05:00"]];
    [amTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 5:30 AM" type:101 connection:@"05:30"]];
    [amTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 6:00 AM" type:102 connection:@"06:00"]];
    [amTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 6:30 AM" type:103 connection:@"06:30"]];
    [amTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 7:00 AM" type:104 connection:@"07:00"]];
    [amTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 7:30 AM" type:105 connection:@"07:30"]];
    [amTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 8:00 AM" type:106 connection:@"08:00"]];
    [amTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 8:30 AM" type:107 connection:@"08:30"]];
    [amTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 9:00 AM" type:108 connection:@"09:00"]];
    [amTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 9:30 AM" type:109 connection:@"09:30"]];
    [amTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 10:00 AM" type:110 connection:@"10:00"]];
    
    [pmTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 2:00 PM" type:200 connection:@"14:00"]];
    [pmTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 2:30 PM" type:201 connection:@"14:30"]];
    [pmTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 3:00 PM" type:202 connection:@"15:00"]];
    [pmTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 3:30 PM" type:203 connection:@"15:30"]];
    [pmTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 4:00 PM" type:204 connection:@"16:00"]];
    [pmTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 4:30 PM" type:205 connection:@"16:30"]];
    [pmTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 5:00 PM" type:206 connection:@"17:00"]];
    [pmTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 5:30 PM" type:207 connection:@"17:30"]];
    [pmTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 6:00 PM" type:208 connection:@"18:00"]];
    [pmTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 6:30 PM" type:209 connection:@"18:30"]];
    [pmTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 7:00 PM" type:210 connection:@"19:00"]];
    [pmTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 7:30 PM" type:211 connection:@"19:30"]];
    [pmTimeList addObject:[[ProfileCell alloc] initWithDisplay:@" 8:00 PM" type:212 connection:@"20:00"]];

    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 25%" type:25 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 30%" type:30 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 35%" type:35 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 40%" type:40 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 45%" type:45 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 50%" type:50 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 55%" type:55 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 60%" type:60 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 65%" type:65 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 70%" type:70 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 75%" type:75 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 80%" type:80 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 85%" type:85 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 90%" type:90 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 95%" type:95 connection:NULL]];
    [congestionList addObject:[[ProfileCell alloc] initWithDisplay:@" 100%" type:100 connection:NULL]];
    
    [notificationTypeList addObject:[[ProfileCell alloc] initWithDisplay:@" SMS message" type:0 connection:NULL]];
    [notificationTypeList addObject:[[ProfileCell alloc] initWithDisplay:@" Email" type:1 connection:NULL]];
    [notificationTypeList addObject:[[ProfileCell alloc] initWithDisplay:@" SMS and Email" type:2 connection:NULL]];
    [notificationTypeList addObject:[[ProfileCell alloc] initWithDisplay:@" Notification Center" type:3 connection:NULL]];

    
    @try{
        
        NSLog(@"user id = %d",appDelegate.user.userId);
        if(appDelegate.user.userId <= 0) {
            connectionType = 1;
            NSString *URL;
            if(appDelegate.selectedCity == 1){
                URL = [NSString stringWithFormat: @"http://%@/sd/servlet/useridservlet?email=%@",appDelegate.cpuUrl,appDelegate.userName];;
            } else if(appDelegate.selectedCity == 2){
                URL = [NSString stringWithFormat: @"http://%@/la/servlet/useridservlet?email=%@",appDelegate.cpuUrl,appDelegate.userName];
            } else {
                URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/useridservlet?email=%@",appDelegate.cpuUrl,appDelegate.userName];
            }
            NSString *post = @"";
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
            //NSString *post = [NSString ];
            //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:URL]];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
            
            NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSArray *cookies = [scs cookies];
            NSEnumerator* e = [cookies objectEnumerator];
            NSHTTPCookie* cookie;
            while ((cookie = [e nextObject])) {
                NSString* name = [cookie name];
                if ([name isEqualToString:@"userId"]) {
                    [scs deleteCookie:cookie];
                }
            }
            
            conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (conn)
            {
                NSLog(@"received Data");
                receivedIDData = [[NSMutableData data] retain];
            } else {
                NSLog(@"no conn");
            }
            
        } else {
            connectionType = 2;
            appDelegate.userID = appDelegate.user.userId;
            NSString *URL;
            if(appDelegate.selectedCity == 1){
                URL = [NSString stringWithFormat: @"http://%@/sd/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.user.userId];
            } else if(appDelegate.selectedCity == 2){
                URL = [NSString stringWithFormat: @"http://%@/la/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.user.userId];
            } else {
                URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.user.userId];
            }
            NSString *post = @"";
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
            //NSString *post = [NSString ];
            //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:URL]];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
            
            NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSArray *cookies = [scs cookies];
            NSEnumerator* e = [cookies objectEnumerator];
            NSHTTPCookie* cookie;
            while ((cookie = [e nextObject])) {
                NSString* name = [cookie name];
                if ([name isEqualToString:@"userId"]) {
                    [scs deleteCookie:cookie];
                }
            }
            
            conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (conn)
            {
                NSLog(@"received Data");
                receivedData = [[NSMutableData data] retain];
            } else {
                NSLog(@"no conn");
            }
        }
    } @catch (NSException *e) {
        NSLog(@"Failed to Login");
    }

    // Do any additional setup after loading the view from its nib.
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSLog(@"append text data");
    if(connectionType == 1) {
        [receivedIDData appendData:data];
    } else if(connectionType == 2){
        [receivedData appendData:data];
    } else if(connectionType == 3){
        [receivedData appendData:data];
    } else if(connectionType == 4){
        [receivedEditedAlertData appendData:data];
    } else if(connectionType == 5){
        [receivedNewAlertData appendData:data];
    }
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"conn failed with error: %@", error);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    if(connectionType == 1){
        
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedIDData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedIDData encoding:NSASCIIStringEncoding];
        NSLog(@"returned data = %@",outputString);
        appDelegate.userID = [outputString intValue];
        connectionType = 2;
        NSString *URL;
        if(appDelegate.selectedCity == 1){
            URL = [NSString stringWithFormat: @"http://%@/sd/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.userID];
        } else if(appDelegate.selectedCity == 2){
            URL = [NSString stringWithFormat: @"http://%@/la/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.userID];
        } else {
            URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.userID];
        }
        
        NSString *post = @"";
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        //NSString *post = [NSString ];
        //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:URL]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
        
        NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray *cookies = [scs cookies];
        NSEnumerator* e = [cookies objectEnumerator];
        NSHTTPCookie* cookie;
        while ((cookie = [e nextObject])) {
            NSString* name = [cookie name];
            if ([name isEqualToString:@"userId"]) {
                [scs deleteCookie:cookie];
            }
        }
        
        conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (conn)
        {
            NSLog(@"received Data");
            receivedData = [[NSMutableData data] retain];
        } else {
            NSLog(@"no conn");
        }
        
    } else if(connectionType == 2){
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
        //NSLog(@"returned data = %@",outputString);
        
        NSDictionary *returnedActivityInfo = [[CJSONDeserializer alloc] deserializeAsDictionary:receivedData error:&error];
        // Set up the edit and add buttons.
        //NSLog(@"segs = %@", [returnedActivityInfo valueForKey:@"segs"]);
        NSMutableArray *segments = [returnedActivityInfo valueForKey:@"segs"];
        numMorning = numAltMorning = numEvening = numAltEvening = 0;
        
        for(int i = 0; i < [segments count]; i++) {
            if([[[segments objectAtIndex:i] valueForKey:@"commuteType"]intValue] == 0){
                numMorning++;
            } else if([[[segments objectAtIndex:i] valueForKey:@"commuteType"]intValue] == 1){
                numEvening++;
            } else if([[[segments objectAtIndex:i] valueForKey:@"commuteType"]intValue] == 2){
                numAltMorning++;
            } else if([[[segments objectAtIndex:i] valueForKey:@"commuteType"]intValue] == 3){
                numAltEvening++;
            }
        }
        
        if(numMorning > 0) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Morning" type:0 connection:NULL]];
        }
        if(numEvening > 0) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Evening" type:1 connection:NULL]];
        }
        if(numAltMorning > 0){
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Alternate Morning" type:2 connection:NULL]];
        }
        if(numAltEvening > 0){
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Alternate Evening" type:3 connection:NULL]];
        }
        
        [alertParts addObject:[[ProfileCell alloc] initWithDisplay:@"My Alerts" type:0 connection:NULL]];
        @try{
            NSDictionary *morningAlert = [returnedActivityInfo valueForKey:@"morningAlert"];
            if(morningAlert != NULL){
                morning = [[Alert alloc] initWithID:[[morningAlert valueForKey:@"alertID"] intValue] type:[[morningAlert valueForKey:@"commuteType"] intValue] start:[morningAlert valueForKey:@"start"] end:[morningAlert valueForKey:@"end"] congestion:[[morningAlert valueForKey:@"congestion"] intValue] notify:[[morningAlert valueForKey:@"notify"] intValue]];
                [alertParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Morning: %@ to %@",[morning start], [morning end]] type:2 connection:morning]];
                [self removePartIn:typeList forType:0];
                [typePicker reloadAllComponents];
            }
        } @catch (NSException *e) {
            
        }
        
        @try{
            NSDictionary *altMorningAlert = [returnedActivityInfo valueForKey:@"altMorningAlert"];
            if(altMorningAlert != NULL) {
                altMorning = [[Alert alloc] initWithID:[[altMorningAlert valueForKey:@"alertID"] intValue] type:[[altMorningAlert valueForKey:@"commuteType"] intValue] start:[altMorningAlert valueForKey:@"start"] end:[altMorningAlert valueForKey:@"end"] congestion:[[altMorningAlert valueForKey:@"congestion"] intValue] notify:[[altMorningAlert valueForKey:@"notify"] intValue]];
                [alertParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Alt Morning: %@ to %@",[altMorning start], [altMorning end]] type:2 connection:altMorning]];
                [self removePartIn:typeList forType:2];
                [typePicker reloadAllComponents];

            }
            
        } @catch (NSException *e) {
            
        }
        
        @try{
            NSDictionary *eveningAlert = [returnedActivityInfo valueForKey:@"eveningAlert"];
            if(eveningAlert != NULL){
                evening = [[Alert alloc] initWithID:[[eveningAlert valueForKey:@"alertID"] intValue] type:[[eveningAlert valueForKey:@"commuteType"] intValue] start:[eveningAlert valueForKey:@"start"] end:[eveningAlert valueForKey:@"end"] congestion:[[eveningAlert valueForKey:@"congestion"] intValue] notify:[[eveningAlert valueForKey:@"notify"] intValue]];
                [alertParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Evening: %@ to %@",[evening start], [evening end]] type:2 connection:evening]];
                [self removePartIn:typeList forType:1];
                [typePicker reloadAllComponents];
            }
            
        } @catch (NSException *e) {
            
        }
        @try{
            NSDictionary *altEveningAlert = [returnedActivityInfo valueForKey:@"altEveningAlert"];
            if(altEveningAlert != NULL){
                altEvening = [[Alert alloc] initWithID:[[altEveningAlert valueForKey:@"alertID"] intValue] type:[[altEveningAlert valueForKey:@"commuteType"] intValue] start:[altEveningAlert valueForKey:@"start"] end:[altEveningAlert valueForKey:@"end"] congestion:[[altEveningAlert valueForKey:@"congestion"] intValue] notify:[[altEveningAlert valueForKey:@"notify"] intValue]];
                [alertParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Alt Evening: %@ to %@",[altEvening start], [altEvening end]] type:2 connection:altEvening]];
                [self removePartIn:typeList forType:3];
                [typePicker reloadAllComponents];
            }
            
        } @catch (NSException *e) {
            
        }
        [alertProfile reloadData];
        [alertProfile reloadInputViews];
    } else if(connectionType == 3){
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
        NSLog(@"returned data = %@",outputString);
        NSDictionary *returnedActivityInfo = [[CJSONDeserializer alloc] deserializeAsDictionary:receivedData error:&error];
        
        for(int i = 0; i < [alertParts count]; i++){
            if([[[alertParts objectAtIndex:i] container] alertID] == selectedAlertID){
                [alertParts removeObjectAtIndex:i];
            }
        }
        NSLog(@"COMMUTE TYPE = %d",commuteType);
        if(commuteType == 0) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Morning" type:0 connection:NULL]];
        }else if(commuteType == 1) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Evening" type:1 connection:NULL]];
        } else if(commuteType == 2){
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Alternate Morning" type:2 connection:NULL]];
        } else if(commuteType == 3){
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Alternate Evening" type:3 connection:NULL]];
        }
        [typePicker reloadAllComponents];
        isEditingAlerts = NO;
        isAdding = NO;
        editsDisplayed = NO;
        [self removePartIn:alertParts forType:10];
        [alertProfile reloadData];
        [alertProfile reloadInputViews];
        
    } else if(connectionType == 4){
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedEditedAlertData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedEditedAlertData encoding:NSASCIIStringEncoding];
        NSLog(@"returned data = %@",outputString);
        NSDictionary *returnedActivityInfo = [[CJSONDeserializer alloc] deserializeAsDictionary:receivedEditedAlertData error:&error];
        [self removePartIn:alertParts forType:10];
        int selectedAlertIndex = -1;
        for(int i= 0; i<[alertParts count]; i++){
            if([[alertParts objectAtIndex:i] container] != NULL){
                if([[[alertParts objectAtIndex:i] container] commuteType] == commuteType){
                    NSLog(@"alert type = %d index = %d",[[[alertParts objectAtIndex:i] container] commuteType], i);
                    selectedAlertIndex = i;
                }
            }
        }
        
        
        if(commuteType == 0) {
            NSLog(@"edited alert being replaced");
            morning = [[Alert alloc] initWithID:[selectedAlert alertID] type:commuteType start:startTime end:endTime congestion:congestion notify:notificationType];
            [alertParts replaceObjectAtIndex:selectedAlertIndex withObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Morning: %@ to %@",[morning start], [morning end]] type:2 connection:morning]];
        } else if(commuteType == 1) {
            evening = [[Alert alloc] initWithID:[selectedAlert alertID] type:commuteType start:startTime end:endTime congestion:congestion notify:notificationType];
            [alertParts replaceObjectAtIndex:selectedAlertIndex withObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Evening: %@ to %@",[evening start], [evening end]] type:2 connection:evening]];
        } else if(commuteType == 2) {
            altMorning = [[Alert alloc] initWithID:[selectedAlert alertID] type:commuteType start:startTime end:endTime congestion:congestion notify:notificationType];
            [alertParts replaceObjectAtIndex:selectedAlertIndex withObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Alt Morning: %@ to %@",[altMorning start], [altMorning end]] type:2 connection:altMorning]];
        } else if(commuteType == 3) {
            altEvening = [[Alert alloc] initWithID:[selectedAlert alertID] type:commuteType start:startTime end:endTime congestion:congestion notify:notificationType];
            [alertParts replaceObjectAtIndex:selectedAlertIndex withObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Alt Evening: %@ to %@",[altEvening start], [altEvening end]] type:2 connection:altEvening]];
        }

        
        [addParts removeAllObjects];
        [self removePartIn:typeList forType:commuteType];
        [typePicker reloadAllComponents];
        isAdding = FALSE;
        isEditingAlerts = FALSE;
        editsDisplayed = FALSE;
        [alertProfile reloadData];
        [alertProfile reloadInputViews];
    } else if(connectionType == 5){
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedNewAlertData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedNewAlertData encoding:NSASCIIStringEncoding];
        NSLog(@"returned data = %@",outputString);
        NSDictionary *returnedActivityInfo = [[CJSONDeserializer alloc] deserializeAsDictionary:receivedNewAlertData error:&error];
        
        
        int returnedAlertID = [[returnedActivityInfo valueForKey:@"id"] intValue];
        
        if(commuteType == 0) {
            morning = [[Alert alloc] initWithID:returnedAlertID type:commuteType start:startTime end:endTime congestion:congestion notify:notificationType];
            [alertParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Morning: %@ to %@",[morning start], [morning end]] type:2 connection:morning]];
        } else if(commuteType == 1) {
            evening = [[Alert alloc] initWithID:returnedAlertID type:commuteType start:startTime end:endTime congestion:congestion notify:notificationType];
            [alertParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Evening: %@ to %@",[evening start], [evening end]] type:2 connection:evening]];
        } else if(commuteType == 2) {
            altMorning = [[Alert alloc] initWithID:returnedAlertID type:commuteType start:startTime end:endTime congestion:congestion notify:notificationType];
            [alertParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Alt Morning: %@ to %@",[altMorning start], [altMorning end]] type:2 connection:altMorning]];
        } else if(commuteType == 3) {
            altEvening = [[Alert alloc] initWithID:returnedAlertID type:commuteType start:startTime end:endTime congestion:congestion notify:notificationType];
            [alertParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Alt Evening: %@ to %@",[altEvening start], [altEvening end]] type:2 connection:altEvening]];
        }
        [addParts removeAllObjects];
        [self removePartIn:typeList forType:commuteType];
        [typePicker reloadAllComponents];
        isAdding = FALSE;
        isEditingAlerts = FALSE;
        [alertProfile reloadData];
        [alertProfile reloadInputViews];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        return [addParts count];
    } else if(section == 1){
        return [alertParts count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    // Configure the cell.
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cell.contentView subviews]){
        for (UIView *subview in [cell.contentView subviews]) {
            [subview removeFromSuperview];
        }
    }
    
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
    if(indexPath.section == 0){
        cell.textLabel.numberOfLines = 1;
        cell.textLabel.font = [UIFont systemFontOfSize:17.0];
        cell.textLabel.text = [[addParts objectAtIndex:indexPath.row] displayText];
        if([[addParts objectAtIndex:indexPath.row] displayType] == 1){
            if(!alertTypeSet) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 2){
            if(typeSelected) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 3){
            if(startTimeSelected) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 4){
            if(endTimeSelected) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 5){
            if(congestionSelected) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 6){
            if(notificationSelected && congestionSelected && endTimeSelected && startTimeSelected) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 7){
            if(notificationSelected && congestionSelected && endTimeSelected && startTimeSelected && endTimeEdited) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        }
        
    } else if(indexPath.section == 1){
        if([[alertParts objectAtIndex:indexPath.row] displayType] == 0){
            cell.textLabel.numberOfLines = 1;
            cell.textLabel.font = [UIFont systemFontOfSize:20.0];
            [cell.contentView setBackgroundColor: [UIColor colorWithRed:.96 green:.76 blue:.09 alpha:1]];
            cell.userInteractionEnabled = TRUE;
        } else if([[alertParts objectAtIndex:indexPath.row] displayType] == 1){
            cell.textLabel.numberOfLines = 2;
            cell.textLabel.font = [UIFont systemFontOfSize:13.0];
            //[cell.contentView setBackgroundColor: [UIColor colorWithRed:.65 green:.9 blue:1.0 alpha:1]];
        } else if([[alertParts objectAtIndex:indexPath.row] displayType] == 4){
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.numberOfLines = 1;
            cell.textLabel.font = [UIFont systemFontOfSize:18.0];
            [cell.contentView setBackgroundColor: [UIColor colorWithRed:1 green:1 blue:204.0/255.0 alpha:1]];
        } else if([[alertParts objectAtIndex:indexPath.row] displayType] == 10){
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIButton *editButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
            editButton.backgroundColor = [UIColor clearColor];
            editButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
            [editButton setTitle:@"Edit" forState:UIControlStateNormal];
            [editButton addTarget:self action:@selector(editAlert) forControlEvents:UIControlEventTouchUpInside];
            // Work out required size
            CGRect buttonFrame = CGRectMake(40, 5, 80, 35);
            [editButton setFrame:buttonFrame];
            
            [cell.contentView addSubview:editButton];
            
            UIButton *deleteButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
            deleteButton.backgroundColor = [UIColor clearColor];
            deleteButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
            [deleteButton setTitle:@"Delete" forState:UIControlStateNormal];
            [deleteButton addTarget:self action:@selector(deleteAlert) forControlEvents:UIControlEventTouchUpInside];
            // Work out required size
            CGRect deleteButtonFrame = CGRectMake(160, 5, 80, 35);
            [deleteButton setFrame:deleteButtonFrame];
            
            [cell.contentView addSubview:deleteButton];
        }
        cell.textLabel.text = [[alertParts objectAtIndex:indexPath.row] displayText];
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        if([[addParts objectAtIndex:indexPath.row] displayType] == 1){
            [self selectType];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 2){
            [self selectStart];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 3){
            [self selectEnd];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 4){
            [self selectCongestion];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 5){
            [self selectNotification];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 6){
            [self addAlert];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 7){
            //edit alert
            editsDisplayed = FALSE;
            NSLog(@"type: %d Start %@ end %@ congestion %d notification %d alert id %d",commuteType, startTime, endTime, congestion, notificationType, [selectedAlert alertID]);
            connectionType = 4;
            NSString *URL;
            
            if(appDelegate.selectedCity == 1){
                URL = [NSString stringWithFormat: @"http://%@/sd/servlet/alertservlet?type=%d&user=%d&ctype=%d&ntype=%d&threshold=%d&from=%@&to=%@&alertid=%d",appDelegate.cpuUrl, 2, appDelegate.userID, commuteType, notificationType, congestion, startTime, endTime, [selectedAlert alertID]];
                NSLog(@"URL = %@",URL);
            } else if(appDelegate.selectedCity == 2){
                URL = [NSString stringWithFormat: @"http://%@/la/servlet/alertservlet?type=%d&user=%d&ctype=%d&ntype=%d&threshold=%d&from=%@&to=%@&alertid=%d",appDelegate.cpuUrl, 2, appDelegate.userID, commuteType, notificationType, congestion, startTime, endTime, [selectedAlert alertID]];
            } else {
                URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/alertservlet?type=%d&user=%d&ctype=%d&ntype=%d&threshold=%d&from=%@&to=%@&alertid=%d",appDelegate.cpuUrl, 2, appDelegate.userID, commuteType, notificationType, congestion, startTime, endTime, [selectedAlert alertID]];
            }

            NSError *error = nil;
            @try{
                
                
                NSString *post = @"";
                NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
                NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:[NSURL URLWithString:URL]];
                [request setHTTPMethod:@"POST"];
                [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                [request setHTTPBody:postData];
                NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
                NSArray *cookies = [scs cookies];
                NSEnumerator* e = [cookies objectEnumerator];
                NSHTTPCookie* cookie;
                while ((cookie = [e nextObject])) {
                    NSString* name = [cookie name];
                    if ([name isEqualToString:@"userId"]) {
                        [scs deleteCookie:cookie];
                    }
                }
                
                conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
                if (conn)
                {
                    NSLog(@"received Data");
                    receivedEditedAlertData = [[NSMutableData data] retain];
                }
                else
                {
                    NSLog(@"no conn");
                }
            } @catch (NSException *e) {
                NSLog(@"Failed to Login");
            }    

        }
        
    } else if(indexPath.section == 1){
        if([[alertParts objectAtIndex:indexPath.row] displayType] == 0){
            if([typeList count] > 0){
            
                if(!isAdding){
                    alertTypeSet = FALSE;
                    isAdding = TRUE;
                    isEditingAlerts = TRUE;
                    typeSelected = FALSE;
                    startTimeSelected = FALSE;
                    endTimeSelected = FALSE;
                    congestionSelected = FALSE;
                    notificationSelected = FALSE;
            
                    [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Select Type" type:1 connection:NULL]];
                    [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Select Start Time" type:2 connection:NULL]];
                    [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Select End Time" type:3 connection:NULL]];
                    [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Select Congestion Level" type:4 connection:NULL]];
                    [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Select Notification Type" type:5 connection:NULL]];
                    [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Add Alert" type:6 connection:NULL]];
                    [alertProfile reloadData];
                    [alertProfile reloadInputViews];
                }
            } else {
                    //NOTIFY need more commutes / less alerts
            }
        } else if([[alertParts objectAtIndex:indexPath.row] displayType] == 2){
            if(!isEditingAlerts){
                editsDisplayed = FALSE;
                isEditingAlerts = TRUE;
                isAdding = TRUE;
                selectedAlertID = [[[alertParts objectAtIndex:indexPath.row] container] alertID];
                commuteType = [[[alertParts objectAtIndex:indexPath.row] container] commuteType];
                selectedAlert = [[alertParts objectAtIndex:indexPath.row] container];
                
                if([alertParts count] == indexPath.row-1){
                    [alertParts addObject:[[ProfileCell alloc] initWithDisplay:@"" type:10 connection:NULL]];
                } else {
                    [alertParts insertObject:[[ProfileCell alloc] initWithDisplay:@"" type:10 connection:NULL] atIndex:indexPath.row+1];
                }
            } else {
                if(!editsDisplayed){
                    isEditingAlerts = FALSE;
                    isAdding = FALSE;
                    [self removePartIn:alertParts forType:10];
                }
            }
            [alertProfile reloadData];
            [alertProfile reloadInputViews];
        }
        
    }
}

- (void) removePartIn:(NSMutableArray*)parts forType:(int) t{
    for(int i = 0; i < [parts count]; i++){
        if([[parts objectAtIndex:i] displayType] == t){
            [parts removeObjectAtIndex:i];
        }
    }
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    int tag = pickerView.tag;
    if(tag == 0) {
        return [typeList count];
    } else if(tag == 1) {
        return [startTimeList count];
    } else if(tag == 2) {
        return [endTimeList count];
    } else if(tag == 3) {
        return [congestionList count];
    } else if(tag == 4){
        return [notificationTypeList count];
    }
    return 0;
}

-(NSString *)pickerView:(UIPickerView *)pickerView
            titleForRow:(NSInteger)row
           forComponent:(NSInteger)component
{
    int tag = pickerView.tag;
    if(tag == 0) {
        return [[typeList objectAtIndex:row] displayText];
    } else if(tag == 1) {
        return [[startTimeList objectAtIndex:row] displayText];
    } else if(tag == 2){
        return [[endTimeList objectAtIndex:row] displayText];
    } else if(tag == 3){
        return [[congestionList objectAtIndex:row] displayText];
    } else if(tag == 4){
        return [[notificationTypeList objectAtIndex:row] displayText];
    }
    return 0;
}

-(void) selectType{
    [actionSheetType showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetType setBounds:CGRectMake(0, 0, 320, 485)];
}
-(void) selectStart{
    [actionSheetStart showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetStart setBounds:CGRectMake(0, 0, 320, 485)];
}
-(void) selectEnd{
    [actionSheetEnd showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetEnd setBounds:CGRectMake(0, 0, 320, 485)];
}
-(void) selectCongestion{
    [actionSheetCongestion showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetCongestion setBounds:CGRectMake(0, 0, 320, 485)];
}
-(void) selectNotification{
    [actionSheetNotification showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetNotification setBounds:CGRectMake(0, 0, 320, 485)];
}

-(void) selectedType:(id)sender{
    [actionSheetType dismissWithClickedButtonIndex:0 animated:YES];
    typeSelected = TRUE;
    startTimeSelected = FALSE;
    [[addParts objectAtIndex:1] setDisplay:@"Select Start Time"];
    [[addParts objectAtIndex:2] setDisplay:@"Select End Time"];
    //set cell display
    [[addParts objectAtIndex:0] setDisplay:[[typeList objectAtIndex:[typePicker selectedRowInComponent:0]] displayText]];
    commuteType = [[typeList objectAtIndex:[typePicker selectedRowInComponent:0]] displayType];
    [self setStartTimeList:commuteType];
    [alertProfile reloadData];
    [alertProfile reloadInputViews];
}
-(void) selectedStart:(id)sender{
    [actionSheetStart dismissWithClickedButtonIndex:0 animated:YES];
    startTimeSelected = TRUE;
    endTimeEdited = FALSE;
    [[addParts objectAtIndex:2] setDisplay:@"Select End Time"];
    [[addParts objectAtIndex:1] setDisplay:[[startTimeList objectAtIndex:[startTimePicker selectedRowInComponent:0]] displayText]];
    startTime = [NSString stringWithString:[[startTimeList objectAtIndex:[startTimePicker selectedRowInComponent:0]] container]];
    [self setEndTimeList:commuteType startTimeIndex:[startTimePicker selectedRowInComponent:0]];
    [alertProfile reloadData];
    [alertProfile reloadInputViews];
}
-(void) selectedEnd:(id)sender{
    [actionSheetEnd dismissWithClickedButtonIndex:0 animated:YES];
    endTimeSelected = TRUE;
    endTimeEdited = TRUE;
    [[addParts objectAtIndex:2] setDisplay:[[endTimeList objectAtIndex:[endTimePicker selectedRowInComponent:0]] displayText]];
    endTime = [NSString stringWithString:[[endTimeList objectAtIndex:[endTimePicker selectedRowInComponent:0]] container]];
    [alertProfile reloadData];
    [alertProfile reloadInputViews];

}
-(void) selectedCongestion:(id)sender{
    [actionSheetCongestion dismissWithClickedButtonIndex:0 animated:YES];
    congestionSelected = TRUE;
    [[addParts objectAtIndex:3] setDisplay:[[congestionList objectAtIndex:[congestionPicker selectedRowInComponent:0]] displayText]];
    congestion = [[congestionList objectAtIndex:[congestionPicker selectedRowInComponent:0]] displayType];
    [alertProfile reloadData];
    [alertProfile reloadInputViews];

}
-(void) selectedNotification:(id)sender{
    [actionSheetNotification dismissWithClickedButtonIndex:0 animated:YES];
    notificationSelected = TRUE;
    [[addParts objectAtIndex:4] setDisplay:[[notificationTypeList objectAtIndex:[notificationPicker selectedRowInComponent:0]] displayText]];
    notificationType = [[notificationTypeList objectAtIndex:[notificationPicker selectedRowInComponent:0]] displayType];
    [alertProfile reloadData];
    [alertProfile reloadInputViews];

}

-(void) addAlert{
    NSLog(@"type: %d Start %@ end %@ congestion %d notification %d",commuteType, startTime, endTime, congestion, notificationType);
    connectionType = 5;
    NSString *URL;
    
    if(appDelegate.selectedCity == 1){
        URL = [NSString stringWithFormat: @"http://%@/sd/servlet/alertservlet?type=%d&user=%d&ctype=%d&ntype=%d&threshold=%d&from=%@&to=%@",appDelegate.cpuUrl, 1, appDelegate.userID, commuteType, notificationType, congestion, startTime, endTime];
        NSLog(@"URL = %@",URL);
    } else if(appDelegate.selectedCity == 2){
        URL = [NSString stringWithFormat: @"http://%@/la/servlet/alertservlet?type=%d&user=%d&ctype=%d&ntype=%d&threshold=%d&from=%@&to=%@",appDelegate.cpuUrl, 1, appDelegate.userID, commuteType, notificationType, congestion, startTime, endTime];
    } else {
        URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/alertservlet?type=%d&user=%d&ctype=%d&ntype=%d&threshold=%d&from=%@&to=%@",appDelegate.cpuUrl, 1, appDelegate.userID, commuteType, notificationType, congestion, startTime, endTime];
    }
    
    
    
    NSError *error = nil;
    @try{
        
        
        NSString *post = @"";
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:URL]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray *cookies = [scs cookies];
        NSEnumerator* e = [cookies objectEnumerator];
        NSHTTPCookie* cookie;
        while ((cookie = [e nextObject])) {
            NSString* name = [cookie name];
            if ([name isEqualToString:@"userId"]) {
                [scs deleteCookie:cookie];
            }
        }
        
        conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (conn)
        {
            NSLog(@"received Data");
            receivedNewAlertData = [[NSMutableData data] retain];
        }
        else
        {
            NSLog(@"no conn");
        }
    } @catch (NSException *e) {
        NSLog(@"Failed to Login");
    }    
}

-(void) editAlert {
    if(!editsDisplayed){
        alertTypeSet = TRUE;
        editsDisplayed = TRUE;
        connectionType = 4;
        int rowSelection = -1;
        if(commuteType == 0) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Morning" type:0 connection:NULL]];
        }else if(commuteType == 1) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Evening" type:1 connection:NULL]];
        } else if(commuteType == 2){
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Alternate Morning" type:2 connection:NULL]];
        } else if(commuteType == 3){
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Alternate Evening" type:3 connection:NULL]];
        }
        
        for(int i = 0; i< [typeList count]; i++){
            if(commuteType == [[typeList objectAtIndex:i] displayType]){
                rowSelection = i;
            }
        }
        [typePicker selectRow:rowSelection inComponent:0 animated:NO];
        typeSelected = TRUE;
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:[[typeList objectAtIndex:rowSelection] displayText] type:1 connection:NULL]];
        
        [self setStartTimeList:commuteType];
        for(int i = 0; i < [startTimeList count]; i++){
            if([[[startTimeList objectAtIndex:i] container] isEqualToString: [selectedAlert start]]){
                NSLog(@"TIME1 = %@ time2 = %@ i = %d",[[startTimeList objectAtIndex:i] container], [selectedAlert start], i);
                rowSelection = i;
            }
        }
        [startTimePicker selectRow:rowSelection inComponent:0 animated:NO];
        startTimeSelected = TRUE;
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:[[startTimeList objectAtIndex:rowSelection] displayText] type:2 connection:NULL]];

        startTime = [NSString stringWithString:[[startTimeList objectAtIndex:rowSelection] container]];
        
        [self setEndTimeList:commuteType startTimeIndex:rowSelection];
        
        for(int i = 0; i < [endTimeList count]; i++){
            if([[[endTimeList objectAtIndex:i] container] isEqualToString:[selectedAlert end] ]){
                rowSelection = i;
            }
        }
        [endTimePicker selectRow:rowSelection inComponent:0 animated:NO];
        endTimeSelected = TRUE;
        endTimeEdited = TRUE;
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:[[endTimeList objectAtIndex:rowSelection] displayText] type:3 connection:NULL]];
        
        endTime = [NSString stringWithString:[[endTimeList objectAtIndex:rowSelection] container]];
        
        for(int i = 0; i < [congestionList count]; i++){
            if([[congestionList objectAtIndex:i] displayType] == [selectedAlert congestion]){
                rowSelection = i;
            }
        }
        [congestionPicker selectRow:rowSelection inComponent:0 animated:NO];
        congestionSelected = TRUE;
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:[[congestionList objectAtIndex:rowSelection] displayText] type:4 connection:NULL]];
        
        congestion = [[congestionList objectAtIndex:rowSelection] displayType];

        for(int i = 0; i < [notificationTypeList count]; i++){
            if([[notificationTypeList objectAtIndex:i] displayType] == [selectedAlert notify]){
                rowSelection = i;
            }
        }
        [notificationPicker selectRow:rowSelection inComponent:0 animated:NO];
        notificationSelected = TRUE;
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:[[notificationTypeList objectAtIndex:rowSelection] displayText] type:5 connection:NULL]];
        notificationType = [[notificationTypeList objectAtIndex:rowSelection] displayType];
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Submit Edit" type:7 connection:NULL]];
        
        [alertProfile reloadData];
        [alertProfile reloadInputViews];
    }
}

-(void) deleteAlert {
    if(!editsDisplayed){
        
        connectionType = 3;
        
        NSString *URL;
        if(appDelegate.selectedCity == 1){
            URL = [NSString stringWithFormat: @"http://%@/sd/servlet/alertservlet?user=%d&type=%d&alertid=%d",appDelegate.cpuUrl,appDelegate.userID,3,selectedAlertID];
        } else if(appDelegate.selectedCity == 2){
            URL = [NSString stringWithFormat: @"http://%@/la/servlet/alertservlet?user=%d&type=%d&alertid=%d",appDelegate.cpuUrl,appDelegate.userID,3,selectedAlertID];
        } else {
            URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/alertservlet?user=%d&type=%d&alertid=%d",appDelegate.cpuUrl,appDelegate.userID,3,selectedAlertID];
        }
        
        NSString *post = @"";
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        //NSString *post = [NSString ];
        //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:URL]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
        
        NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray *cookies = [scs cookies];
        NSEnumerator* e = [cookies objectEnumerator];
        NSHTTPCookie* cookie;
        while ((cookie = [e nextObject])) {
            NSString* name = [cookie name];
            if ([name isEqualToString:@"userId"]) {
                [scs deleteCookie:cookie];
            }
        }
        
        conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (conn)
        {
            NSLog(@"received Data");
            receivedData = [[NSMutableData data] retain];
        } else {
            NSLog(@"no conn");
        }
    }
}

-(void) setStartTimeList:(int) ct {
    [startTimeList removeAllObjects];
    if(ct == 0 || ct == 2){
        for (int i = 0; i<[amTimeList count] -1; i++) {
            [startTimeList addObject:[amTimeList objectAtIndex:i]];
        }
    } else {
        for (int i = 0; i<[pmTimeList count] -1; i++) {
            [startTimeList addObject:[pmTimeList objectAtIndex:i]];
        }
    }
    [startTimePicker reloadAllComponents];
}

-(void) setEndTimeList:(int) ct startTimeIndex:(int) selectedStartTime {
    [endTimeList removeAllObjects];
    if(ct == 0 || ct == 2){
        for (int i = selectedStartTime+1; i<[amTimeList count]; i++) {
            [endTimeList addObject:[amTimeList objectAtIndex:i]];
        }
    } else {
        for (int i = selectedStartTime+1; i<[pmTimeList count]; i++) {
            [endTimeList addObject:[pmTimeList objectAtIndex:i]];
        }
    }
    [endTimePicker reloadAllComponents];
}

- (void)viewDidAppear:(BOOL)animated {
    [self setTitle:@"Alert Setup"];
    //[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:NO];
}

- (void)viewDidDisappear:(BOOL) animated {
	//[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:YES];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
