//
//  DailyReportViewController.m
//  TrafficViewer
//
//  Created by Sam Fernald on 8/31/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import "DailyReportViewController.h"

#import "CJSONSerializer.h"
#import "CJSONDeserializer.h"
#import "ProfileCell.h"

@interface DailyReportViewController ()

@end

@implementation DailyReportViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    reportParts = [[NSMutableArray alloc] init];
    addParts = [[NSMutableArray alloc] init];
    isEditingReport = NO;
    isAdding = NO;
    
    [actionSheetType dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetType=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarType = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarType sizeToFit];
    pickerToolbarType.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsType = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnType = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedType:)];
    [barItemsType addObject:doneBtnType];
    [doneBtnType release];
    [pickerToolbarType setItems:barItemsType animated:YES];
    [actionSheetType addSubview:pickerToolbarType];
    [barItemsType release];
    [pickerToolbarType release];
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    typePicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    typePicker.showsSelectionIndicator = YES;
    typePicker.dataSource = self;
    typePicker.delegate = self;
    typePicker.tag = 0;
    typePicker.showsSelectionIndicator = YES;
    [actionSheetType addSubview:typePicker];
    
    //--------------weekday ACTION SHEET
    [actionSheetWeekday dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetWeekday=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarWeekday = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarWeekday sizeToFit];
    pickerToolbarWeekday.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsWeekday = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnWeekday = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedWeekday:)];
    [barItemsWeekday addObject:doneBtnWeekday];
    [doneBtnWeekday release];
    [pickerToolbarWeekday setItems:barItemsWeekday animated:YES];
    [actionSheetWeekday addSubview:pickerToolbarWeekday];
    [barItemsWeekday release];
    [pickerToolbarWeekday release];
    weekdayPicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    weekdayPicker.showsSelectionIndicator = YES;
    weekdayPicker.dataSource = self;
    weekdayPicker.delegate = self;
    weekdayPicker.tag = 1;
    weekdayPicker.showsSelectionIndicator = YES;
    [actionSheetWeekday addSubview:weekdayPicker];
    
    //--------------time ACTION SHEET
    [actionSheetTime dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetTime = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarTime = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarTime sizeToFit];
    pickerToolbarTime.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsTime = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnTime = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedTime:)];
    [barItemsTime addObject:doneBtnTime];
    [doneBtnTime release];
    [pickerToolbarTime setItems:barItemsTime animated:YES];
    [actionSheetTime addSubview:pickerToolbarTime];
    [barItemsTime release];
    [pickerToolbarTime release];
    timePicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    timePicker.showsSelectionIndicator = YES;
    timePicker.dataSource = self;
    timePicker.delegate = self;
    timePicker.tag = 2;
    timePicker.showsSelectionIndicator = YES;
    [actionSheetTime addSubview:timePicker];
    
    
    //--------------notification ACTION SHEET
    [actionSheetNotification dismissWithClickedButtonIndex:0 animated:YES];
    actionSheetNotification=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    UIToolbar *pickerToolbarNotification = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320,40)];
    [pickerToolbarNotification sizeToFit];
    pickerToolbarNotification.barStyle = UIBarStyleBlackTranslucent;
    NSMutableArray *barItemsNotification = [[NSMutableArray alloc] init];
    UIBarButtonItem *doneBtnNotification = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selectedNotification:)];
    [barItemsNotification addObject:doneBtnNotification];
    [doneBtnNotification release];
    [pickerToolbarNotification setItems:barItemsNotification animated:YES];
    [actionSheetNotification addSubview:pickerToolbarNotification];
    [barItemsNotification release];
    [pickerToolbarNotification release];
    notificationPicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    notificationPicker.showsSelectionIndicator = YES;
    notificationPicker.dataSource = self;
    notificationPicker.delegate = self;
    notificationPicker.tag = 3;
    notificationPicker.showsSelectionIndicator = YES;
    [actionSheetNotification addSubview:notificationPicker];
    
    typeList = [[NSMutableArray alloc] init];

    hourList = [[NSMutableArray alloc] init];
    minList = [[NSMutableArray alloc] init];
    ampmList = [[NSMutableArray alloc] init];
    weekdayList = [[NSMutableArray alloc] init];
    notificationTypeList = [[NSMutableArray alloc] init];
    
    [hourList addObject:[[ProfileCell alloc] initWithDisplay:@" 1" type:1 connection:NULL]];
    [hourList addObject:[[ProfileCell alloc] initWithDisplay:@" 2" type:2 connection:NULL]];
    [hourList addObject:[[ProfileCell alloc] initWithDisplay:@" 3" type:3 connection:NULL]];
    [hourList addObject:[[ProfileCell alloc] initWithDisplay:@" 4" type:4 connection:NULL]];
    [hourList addObject:[[ProfileCell alloc] initWithDisplay:@" 5" type:5 connection:NULL]];
    [hourList addObject:[[ProfileCell alloc] initWithDisplay:@" 6" type:6 connection:NULL]];
    [hourList addObject:[[ProfileCell alloc] initWithDisplay:@" 7" type:7 connection:NULL]];
    [hourList addObject:[[ProfileCell alloc] initWithDisplay:@" 8" type:8 connection:NULL]];
    [hourList addObject:[[ProfileCell alloc] initWithDisplay:@" 9" type:9 connection:NULL]];
    [hourList addObject:[[ProfileCell alloc] initWithDisplay:@" 10" type:10 connection:NULL]];
    [hourList addObject:[[ProfileCell alloc] initWithDisplay:@" 11" type:11 connection:NULL]];
    [hourList addObject:[[ProfileCell alloc] initWithDisplay:@" 12" type:12 connection:NULL]];
        
    [minList addObject:[[ProfileCell alloc] initWithDisplay:@"00" type:0 connection:NULL]];
    [minList addObject:[[ProfileCell alloc] initWithDisplay:@"15" type:15 connection:NULL]];
    [minList addObject:[[ProfileCell alloc] initWithDisplay:@"30" type:30 connection:NULL]];
    [minList addObject:[[ProfileCell alloc] initWithDisplay:@"45" type:45 connection:NULL]];
    
    [ampmList addObject:[[ProfileCell alloc] initWithDisplay:@" AM" type:0 connection:NULL]];
    [ampmList addObject:[[ProfileCell alloc] initWithDisplay:@" PM" type:1 connection:NULL]];
    
    [weekdayList addObject:[[ProfileCell alloc] initWithDisplay:@" Weekdays" type:5 connection:NULL]];
    [weekdayList addObject:[[ProfileCell alloc] initWithDisplay:@" Everyday" type:7 connection:NULL]];


    [notificationTypeList addObject:[[ProfileCell alloc] initWithDisplay:@" SMS message" type:0 connection:NULL]];
    [notificationTypeList addObject:[[ProfileCell alloc] initWithDisplay:@" Email" type:1 connection:NULL]];
    [notificationTypeList addObject:[[ProfileCell alloc] initWithDisplay:@" SMS and Email" type:2 connection:NULL]];
    [notificationTypeList addObject:[[ProfileCell alloc] initWithDisplay:@" Notification Center" type:3 connection:NULL]];
    
    
    @try{
        
        NSLog(@"user id = %d",appDelegate.user.userId);
        if(appDelegate.user.userId <= 0) {
            connectionType = 1;
            NSString *URL;
            if(appDelegate.selectedCity == 1){
                URL = [NSString stringWithFormat: @"http://%@/sd/servlet/useridservlet?email=%@",appDelegate.cpuUrl,appDelegate.userName];;
            } else if(appDelegate.selectedCity == 2){
                URL = [NSString stringWithFormat: @"http://%@/la/servlet/useridservlet?email=%@",appDelegate.cpuUrl,appDelegate.userName];
            } else {
                URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/useridservlet?email=%@",appDelegate.cpuUrl,appDelegate.userName];
            }
            NSString *post = @"";
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
            //NSString *post = [NSString ];
            //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:URL]];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
            
            NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSArray *cookies = [scs cookies];
            NSEnumerator* e = [cookies objectEnumerator];
            NSHTTPCookie* cookie;
            while ((cookie = [e nextObject])) {
                NSString* name = [cookie name];
                if ([name isEqualToString:@"userId"]) {
                    [scs deleteCookie:cookie];
                }
            }
            
            conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (conn)
            {
                NSLog(@"received Data");
                receivedIDData = [[NSMutableData data] retain];
            } else {
                NSLog(@"no conn");
            }
            
        } else {
            connectionType = 2;
            appDelegate.userID = appDelegate.user.userId;
            NSString *URL;
            if(appDelegate.selectedCity == 1){
                URL = [NSString stringWithFormat: @"http://%@/sd/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.user.userId];
            } else if(appDelegate.selectedCity == 2){
                URL = [NSString stringWithFormat: @"http://%@/la/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.user.userId];
            } else {
                URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.user.userId];
            }
            NSString *post = @"";
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
            //NSString *post = [NSString ];
            //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:URL]];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
            
            NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSArray *cookies = [scs cookies];
            NSEnumerator* e = [cookies objectEnumerator];
            NSHTTPCookie* cookie;
            while ((cookie = [e nextObject])) {
                NSString* name = [cookie name];
                if ([name isEqualToString:@"userId"]) {
                    [scs deleteCookie:cookie];
                }
            }
            
            conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (conn)
            {
                NSLog(@"received Data");
                receivedData = [[NSMutableData data] retain];
            } else {
                NSLog(@"no conn");
            }
        }
    } @catch (NSException *e) {
        NSLog(@"Failed to Login");
    }
    
    // Do any additional setup after loading the view from its nib.
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSLog(@"append text data");
    if(connectionType == 1) {
        [receivedIDData appendData:data];
    } else if(connectionType == 2){
        [receivedData appendData:data];
    } else if(connectionType == 3){
        [receivedData appendData:data];
    } else if(connectionType == 4){
        [receivedEditedReportData appendData:data];
    }else if(connectionType == 5){
        [receivedNewReportData appendData:data];
    }
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"conn failed with error: %@", error);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    if(connectionType == 1){
        
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedIDData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedIDData encoding:NSASCIIStringEncoding];
        NSLog(@"returned data = %@",outputString);
        appDelegate.userID = [outputString intValue];
        connectionType = 2;
        NSString *URL;
        if(appDelegate.selectedCity == 1){
            URL = [NSString stringWithFormat: @"http://%@/sd/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.userID];
        } else if(appDelegate.selectedCity == 2){
            URL = [NSString stringWithFormat: @"http://%@/la/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.userID];
        } else {
            URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/jsoncommuteprofile?u=%d",appDelegate.cpuUrl,appDelegate.userID];
        }
        
        NSString *post = @"";
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        //NSString *post = [NSString ];
        //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:URL]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
        
        NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray *cookies = [scs cookies];
        NSEnumerator* e = [cookies objectEnumerator];
        NSHTTPCookie* cookie;
        while ((cookie = [e nextObject])) {
            NSString* name = [cookie name];
            if ([name isEqualToString:@"userId"]) {
                [scs deleteCookie:cookie];
            }
        }
        
        conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (conn)
        {
            NSLog(@"received Data");
            receivedData = [[NSMutableData data] retain];
        } else {
            NSLog(@"no conn");
        }
        
    } else if(connectionType == 2){
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
        //NSLog(@"returned data = %@",outputString);
        
        NSDictionary *returnedActivityInfo = [[CJSONDeserializer alloc] deserializeAsDictionary:receivedData error:&error];
        // Set up the edit and add buttons.
        //NSLog(@"segs = %@", [returnedActivityInfo valueForKey:@"segs"]);
        NSMutableArray *segments = [returnedActivityInfo valueForKey:@"segs"];
        
        numMorning = numAltMorning = numEvening = numAltEvening = 0;
        
        for(int i = 0; i < [segments count]; i++) {
            if([[[segments objectAtIndex:i] valueForKey:@"commuteType"]intValue] == 0){
                numMorning++;
            } else if([[[segments objectAtIndex:i] valueForKey:@"commuteType"]intValue] == 1){
                numEvening++;
            } else if([[[segments objectAtIndex:i] valueForKey:@"commuteType"]intValue] == 2){
                numAltMorning++;
            } else if([[[segments objectAtIndex:i] valueForKey:@"commuteType"]intValue] == 3){
                numAltEvening++;
            }
        }
        
        if(numMorning > 0 || numAltMorning > 0) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Morning and Alt Morning" type:0 connection:NULL]];
        }
        if(numEvening > 0 || numAltEvening > 0) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Evening and Alt Evening" type:1 connection:NULL]];
        }

    
     
         [reportParts addObject:[[ProfileCell alloc] initWithDisplay:@"My Daily Report" type:0 connection:NULL]];
         
         @try {
             NSDictionary *morningReportInfo = [returnedActivityInfo valueForKey:@"morning"];
             if(morningReportInfo != NULL) {
                 morningReport = [[DailyReport alloc] initWithID:[[morningReportInfo valueForKey:@"reportID"] intValue] commuteType:[[morningReportInfo valueForKey:@"commuteType"] intValue] hour:[[morningReportInfo valueForKey:@"hour"] intValue] minute:[[morningReportInfo valueForKey:@"minute"] intValue] ampm:[[morningReportInfo valueForKey:@"ampm"] intValue] notify:[[morningReportInfo valueForKey:@"notify"] intValue] day:[[morningReportInfo valueForKey:@"day"] intValue]];
                 
                 if([morningReport ampm] == 0){
                     amORpm = @"AM";
                 } else {
                     amORpm = @"PM";
                 }
                 if([morningReport minute] == 0){
                     [reportParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Morning: %d:0%d %@",[morningReport hour], [morningReport minute], amORpm] type:3 connection:morningReport]];
                     [self removePartIn:typeList forType:0];
                 } else {
                     [reportParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Morning: %d:%d %@",[morningReport hour], [morningReport minute], amORpm] type:3 connection:morningReport]];
                     [self removePartIn:typeList forType:0];
                 }
                 
             }
         } @catch(NSException *e){
         
         }
        @try {
             NSDictionary *eveningReportInfo = [returnedActivityInfo valueForKey:@"evening"];
             if(eveningReportInfo != NULL){
             eveningReport = [[DailyReport alloc] initWithID:[[eveningReportInfo valueForKey:@"reportID"] intValue] commuteType:[[eveningReportInfo valueForKey:@"commuteType"] intValue] hour:[[eveningReportInfo valueForKey:@"hour"] intValue] minute:[[eveningReportInfo valueForKey:@"minute"] intValue] ampm:[[eveningReportInfo valueForKey:@"ampm"] intValue] notify:[[eveningReportInfo valueForKey:@"notify"] intValue] day:[[eveningReportInfo valueForKey:@"day"] intValue]];
                 if([eveningReport ampm] == 0){
                     amORpm = @"AM";
                 } else {
                     amORpm = @"PM";
                 }

                 if([eveningReport minute] == 0){
                     [reportParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Evening: %d:0%d %@",[eveningReport hour], [eveningReport minute], amORpm] type:3 connection:eveningReport]];
                 } else {
                     [reportParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Evening: %d:%d %@",[eveningReport hour], [eveningReport minute], amORpm] type:3 connection:eveningReport]];
                 }
                 [self removePartIn:typeList forType:1];
             }
         } @catch(NSException *e){
         
         }
        [typePicker reloadAllComponents];
        [reportProfile reloadData];
        [reportProfile reloadInputViews];
    } else if(connectionType == 3){
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
        NSLog(@"returned data = %@",outputString);
        NSDictionary *returnedActivityInfo = [[CJSONDeserializer alloc] deserializeAsDictionary:receivedData error:&error];
        
        for(int i = 0; i < [reportParts count]; i++){
            if([[[reportParts objectAtIndex:i] container] reportID] == selectedReportID){
                NSLog(@"REMOVING OBJECT AT %d for report ID: %d",i, selectedReportID);
                [reportParts removeObjectAtIndex:i];
            }
        }
        NSLog(@"COMMUTE TYPE = %d",commuteType);
        if(commuteType == 0) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Morning and Alt Morning" type:0 connection:NULL]];
        }
        if(commuteType == 1) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Evening and Alt Evening" type:1 connection:NULL]];
        }
        
        [typePicker reloadAllComponents];
        isEditingReport = NO;
        isAdding = NO;
        [self removePartIn:reportParts forType:10];
        [reportProfile reloadData];
        [reportProfile reloadInputViews];
        
        
    } else if(connectionType == 4){
        
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedEditedReportData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedEditedReportData encoding:NSASCIIStringEncoding];
        NSLog(@"returned data = %@",outputString);
        NSDictionary *returnedActivityInfo = [[CJSONDeserializer alloc] deserializeAsDictionary:receivedEditedReportData error:&error];
        [self removePartIn:reportParts forType:10];
        int selectedReportIndex = -1;
        for(int i= 0; i<[reportParts count]; i++){
            if([[reportParts objectAtIndex:i] container] != NULL){
                if([[[reportParts objectAtIndex:i] container] commuteType] == commuteType){
                    selectedReportIndex = i;
                }
            }
        }
        
        if(commuteType == 0) {
            NSLog(@"edited report being replaced");
            morningReport = [[DailyReport alloc] initWithID:[selectedReport reportID] commuteType:commuteType hour: hour minute:min ampm:ampm notify:notificationType day:weekday];
            if([morningReport ampm] == 0){
                amORpm = @"AM";
            } else {
                amORpm = @"PM";
            }
            if([morningReport minute] == 0){
                [reportParts replaceObjectAtIndex:selectedReportIndex withObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Morning: %d:0%d %@",[morningReport hour], [morningReport minute], amORpm] type:3 connection:morningReport]];
            } else {
                [reportParts replaceObjectAtIndex:selectedReportIndex withObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Morning: %d:%d %@",[morningReport hour], [morningReport minute], amORpm] type:3 connection:morningReport]];
            }
            
        } else if(commuteType == 1) {
            eveningReport = [[DailyReport alloc] initWithID:[selectedReport reportID] commuteType:commuteType hour: hour minute:min ampm:ampm notify:notificationType day:weekday];
            if([eveningReport ampm] == 0){
                amORpm = @"AM";
            } else {
                amORpm = @"PM";
            }
            if([eveningReport minute] == 0){
                [reportParts replaceObjectAtIndex:selectedReportIndex withObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Evening: %d:0%d %@",[eveningReport hour], [eveningReport minute], amORpm] type:3 connection:eveningReport]];
            } else {
                [reportParts replaceObjectAtIndex:selectedReportIndex withObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Evening: %d:%d %@",[eveningReport hour], [eveningReport minute], amORpm] type:3 connection:eveningReport]];
            }
        }
        
        [addParts removeAllObjects];
        [self removePartIn:typeList forType:commuteType];
        [typePicker reloadAllComponents];
        isAdding = FALSE;
        isEditingReport = FALSE;
        editsDisplayed = FALSE;
        
        [reportProfile reloadData];
        [reportProfile reloadInputViews];
        
    } else if(connectionType == 5){
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedNewReportData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedNewReportData encoding:NSASCIIStringEncoding];
        NSLog(@"returned data = %@",outputString);
        NSDictionary *returnedActivityInfo = [[CJSONDeserializer alloc] deserializeAsDictionary:receivedNewReportData error:&error];
        
        
        int returnedReportID = [[returnedActivityInfo valueForKey:@"id"] intValue];
        NSLog(@"report ID returned and parsed %d",returnedReportID);
        if(commuteType == 0) {
            morningReport = [[DailyReport alloc] initWithID:returnedReportID commuteType:commuteType hour:hour minute:min ampm:ampm notify:notificationType day:weekday];
            if([morningReport ampm] == 0){
                amORpm = @"AM";
            } else {
                amORpm = @"PM";
            }
            if([morningReport minute] == 0){
                [reportParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Morning: %d:0%d %@",[morningReport hour], [morningReport minute], amORpm] type:3 connection:morningReport]];
            } else {
                [reportParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Morning: %d:%d %@",[morningReport hour], [morningReport minute], amORpm] type:3 connection:morningReport]];
            }
        } else if(commuteType == 1) {
            eveningReport = [[DailyReport alloc] initWithID:returnedReportID commuteType:commuteType hour:hour minute:min ampm:ampm notify:notificationType day:weekday];
            if([eveningReport ampm] == 0){
                amORpm = @"AM";
            } else {
                amORpm = @"PM";
            }
            if([eveningReport minute] == 0){
                [reportParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Evening: %d:0%d %@",[eveningReport hour], [eveningReport minute], amORpm] type:3 connection:eveningReport]];

            } else {
                [reportParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@"Evening: %d:%d %@",[eveningReport hour], [eveningReport minute], amORpm] type:3 connection:eveningReport]];
            }
        }
        [addParts removeAllObjects];
        [self removePartIn:typeList forType:commuteType];
        [typePicker reloadAllComponents];
        isAdding = FALSE;
        isEditingReport = FALSE;
        [reportProfile reloadData];
        [reportProfile reloadInputViews];

    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        return [addParts count];
    } else if(section == 1){
        return [reportParts count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    // Configure the cell.
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cell.contentView subviews]){
        for (UIView *subview in [cell.contentView subviews]) {
            [subview removeFromSuperview];
        }
    }
    
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
    if(indexPath.section == 0){
        cell.textLabel.numberOfLines = 1;
        cell.textLabel.font = [UIFont systemFontOfSize:17.0];
        cell.textLabel.text = [[addParts objectAtIndex:indexPath.row] displayText];
        if([[addParts objectAtIndex:indexPath.row] displayType] == 1){
            if(!reportTypeSet) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 2){
            if(typeSelected) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 3){
            if(weekdaySelected) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 4){
            if(timeSelected) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 5){
            if(notificationSelected) {
                cell.userInteractionEnabled = TRUE;
            } else {
                cell.userInteractionEnabled = FALSE;
                [cell.contentView setBackgroundColor:[UIColor grayColor]];
            }
        }
        
    } else if(indexPath.section == 1){
        if([[reportParts objectAtIndex:indexPath.row] displayType] == 0){
            cell.textLabel.numberOfLines = 1;
            cell.textLabel.font = [UIFont systemFontOfSize:20.0];
            [cell.contentView setBackgroundColor: [UIColor colorWithRed:.96 green:.76 blue:.09 alpha:1]];
            cell.userInteractionEnabled = TRUE;
        } else if([[reportParts objectAtIndex:indexPath.row] displayType] == 1){
            cell.textLabel.numberOfLines = 2;
            cell.textLabel.font = [UIFont systemFontOfSize:13.0];
            //[cell.contentView setBackgroundColor: [UIColor colorWithRed:.65 green:.9 blue:1.0 alpha:1]];
        } else if([[reportParts objectAtIndex:indexPath.row] displayType] == 4){
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.numberOfLines = 1;
            cell.textLabel.font = [UIFont systemFontOfSize:18.0];
            [cell.contentView setBackgroundColor: [UIColor colorWithRed:1 green:1 blue:204.0/255.0 alpha:1]];
        } else if([[reportParts objectAtIndex:indexPath.row] displayType] == 10){
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIButton *editButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
            editButton.backgroundColor = [UIColor clearColor];
            editButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
            [editButton setTitle:@"Edit" forState:UIControlStateNormal];
            [editButton addTarget:self action:@selector(editReport) forControlEvents:UIControlEventTouchUpInside];
            // Work out required size
            CGRect buttonFrame = CGRectMake(40, 5, 80, 35);
            [editButton setFrame:buttonFrame];
            
            [cell.contentView addSubview:editButton];
            
            UIButton *deleteButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
            deleteButton.backgroundColor = [UIColor clearColor];
            deleteButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
            [deleteButton setTitle:@"Delete" forState:UIControlStateNormal];
            [deleteButton addTarget:self action:@selector(deleteReport) forControlEvents:UIControlEventTouchUpInside];
            // Work out required size
            CGRect deleteButtonFrame = CGRectMake(160, 5, 80, 35);
            [deleteButton setFrame:deleteButtonFrame];
            
            [cell.contentView addSubview:deleteButton];
        }
        cell.textLabel.text = [[reportParts objectAtIndex:indexPath.row] displayText];
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"something Selected in section %d at row %d",indexPath.section, indexPath.row);
    if(indexPath.section == 0){
        if([[addParts objectAtIndex:indexPath.row] displayType] == 1){
            [self selectType];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 2){
            [self selectWeekday];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 3){
            [self selectTime];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 4){
            [self selectNotification];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 5){
            isAdding = TRUE;
            //[addParts removeAllObjects];
            [self addReport];
        } else if([[addParts objectAtIndex:indexPath.row] displayType] == 6){
            editsDisplayed = FALSE;
            connectionType = 4;
            NSString *URL;
            
            if(appDelegate.selectedCity == 1){
                URL = [NSString stringWithFormat: @"http://%@/sd/servlet/reportservlet?type=%d&user=%d&ctype=%d&ntype=%d&hour=%d&min=%d&ampm=%d&day=%d&reportid=%d",appDelegate.cpuUrl, 2, appDelegate.userID, commuteType, notificationType, hour, min, ampm, weekday, [selectedReport reportID]];
                NSLog(@"URL = %@",URL);
            } else if(appDelegate.selectedCity == 2){
                URL = [NSString stringWithFormat: @"http://%@/la/servlet/reportservlet?type=%d&user=%d&ctype=%d&ntype=%d&hour=%d&min=%d&ampm=%d&day=%d&reportid=%d",appDelegate.cpuUrl, 2, appDelegate.userID, commuteType, notificationType, hour, min, ampm, weekday, [selectedReport reportID]];
            } else {
                URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/reportservlet?type=%d&user=%d&ctype=%d&ntype=%d&hour=%d&min=%d&ampm=%d&day=%d&reportid=%d",appDelegate.cpuUrl, 2, appDelegate.userID, commuteType, notificationType, hour, min, ampm, weekday, [selectedReport reportID]];
            }
            
            NSError *error = nil;
            @try{
                
                
                NSString *post = @"";
                NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
                NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:[NSURL URLWithString:URL]];
                [request setHTTPMethod:@"POST"];
                [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                [request setHTTPBody:postData];
                NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
                NSArray *cookies = [scs cookies];
                NSEnumerator* e = [cookies objectEnumerator];
                NSHTTPCookie* cookie;
                while ((cookie = [e nextObject])) {
                    NSString* name = [cookie name];
                    if ([name isEqualToString:@"userId"]) {
                        [scs deleteCookie:cookie];
                    }
                }
                
                conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
                if (conn)
                {
                    NSLog(@"received Data");
                    receivedEditedReportData = [[NSMutableData data] retain];
                }
                else
                {
                    NSLog(@"no conn");
                }
            } @catch (NSException *e) {
                NSLog(@"Failed to Login");
            }
        }
        
    } else if(indexPath.section == 1){
        if([[reportParts objectAtIndex:indexPath.row] displayType] == 0){
            if([typeList count] > 0){
                if(!isAdding){
                    reportTypeSet = FALSE;
                    isAdding = TRUE;
                    typeSelected = FALSE;
                    weekdaySelected = FALSE;
                    timeSelected = FALSE;
                    notificationSelected = FALSE;
            
                    [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Select Type" type:1 connection:NULL]];
                    [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Select Weekday" type:2 connection:NULL]];
                    [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Select Time" type:3 connection:NULL]];
                    [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Select Notification Type" type:4 connection:NULL]];
                    [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Add Report" type:5 connection:NULL]];
                    [reportProfile reloadData];
                    [reportProfile reloadInputViews];
                }
            } else {
                //NOTIFY NO SPOTS
            }
        } else if([[reportParts objectAtIndex:indexPath.row] displayType] == 3){
            if(!isEditingReport){
                isEditingReport = YES;
                editsDisplayed = NO;
                isAdding = YES;
                selectedReportID = [[[reportParts objectAtIndex:indexPath.row] container] reportID];
                commuteType = [[[reportParts objectAtIndex:indexPath.row] container] commuteType];
                selectedReport = [[reportParts objectAtIndex:indexPath.row] container];
                NSLog(@"ID: %d",selectedReportID);
                if([reportParts count] == indexPath.row-1){
                    [reportParts addObject:[[ProfileCell alloc] initWithDisplay:@"" type:10 connection:NULL]];
                } else {
                    [reportParts insertObject:[[ProfileCell alloc] initWithDisplay:@"" type:10 connection:NULL] atIndex:indexPath.row+1];
                }
            } else {
                if(!editsDisplayed){
                    isEditingReport = NO;
                    isAdding = NO;
                    [self removePartIn:reportParts forType:10];
                }
            }
            [reportProfile reloadData];
            [reportProfile reloadInputViews];
        }
        
    }
}

- (void) removePartIn:(NSMutableArray*)parts forType:(int) t{
    for(int i = 0; i < [parts count]; i++){
        if([[parts objectAtIndex:i] displayType] == t){
            [parts removeObjectAtIndex:i];
        }
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    int tag = pickerView.tag;
    if(tag == 2) {
        return 3;
    } else {
        return 1;
    }
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    int tag = pickerView.tag;
    if(tag == 0) {
        return [typeList count];
    } else if(tag == 1) {
        return [weekdayList count];
    } else if(tag == 2) {
        if(component == 0){
            return [hourList count];
        } else if(component == 1){
            return [minList count];
        } else if(component == 2){
            return [ampmList count];
        }
    } else if(tag == 3) {
        return [notificationTypeList count];
    }
    return 0;
}

-(NSString *)pickerView:(UIPickerView *)pickerView
            titleForRow:(NSInteger)row
           forComponent:(NSInteger)component
{
    int tag = pickerView.tag;
    if(tag == 0) {
        return [[typeList objectAtIndex:row] displayText];
    } else if(tag == 1) {
        return [[weekdayList objectAtIndex:row] displayText];
    } else if(tag == 2){
        if(component == 0){
            return [[hourList objectAtIndex:row] displayText];
        } else if(component == 1){
            return [[minList objectAtIndex:row] displayText];
        } else if(component == 2){
            return [[ampmList objectAtIndex:row] displayText];
        }
    } else if(tag == 3){
        return [[notificationTypeList objectAtIndex:row] displayText];
    }
    return 0;
}

-(void) selectType{
    [actionSheetType showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetType setBounds:CGRectMake(0, 0, 320, 485)];
}
-(void) selectWeekday{
    [actionSheetWeekday showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetWeekday setBounds:CGRectMake(0, 0, 320, 485)];
}
-(void) selectTime{
    [actionSheetTime showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetTime setBounds:CGRectMake(0, 0, 320, 485)];
}
-(void) selectNotification{
    [actionSheetNotification showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionSheetNotification setBounds:CGRectMake(0, 0, 320, 485)];
}

-(void) selectedType:(id)sender{
    [actionSheetType dismissWithClickedButtonIndex:0 animated:YES];
    typeSelected = TRUE;
    //set cell display
    [[addParts objectAtIndex:0] setDisplay:[[typeList objectAtIndex:[typePicker selectedRowInComponent:0]] displayText]];
    commuteType = [[typeList objectAtIndex:[typePicker selectedRowInComponent:0]] displayType];
    [reportProfile reloadData];
    [reportProfile reloadInputViews];
}
-(void) selectedWeekday:(id)sender{
    [actionSheetWeekday dismissWithClickedButtonIndex:0 animated:YES];
    weekdaySelected = TRUE;
    [[addParts objectAtIndex:1] setDisplay:[[weekdayList objectAtIndex:[weekdayPicker selectedRowInComponent:0]] displayText]];
    weekday = [[weekdayList objectAtIndex:[weekdayPicker selectedRowInComponent:0]] displayType];
    [reportProfile reloadData];
    [reportProfile reloadInputViews];
}
-(void) selectedTime:(id)sender{
    [actionSheetTime dismissWithClickedButtonIndex:0 animated:YES];
    timeSelected = TRUE;
    [[addParts objectAtIndex:2] setDisplay:[NSString stringWithFormat:@" %@:%@ %@",[[hourList objectAtIndex:[timePicker selectedRowInComponent:0]] displayText], [[minList objectAtIndex:[timePicker selectedRowInComponent:1]] displayText], [[ampmList objectAtIndex:[timePicker selectedRowInComponent:2]] displayText]]];
    hour = [[hourList objectAtIndex:[timePicker selectedRowInComponent:0]] displayType];
    min = [[minList objectAtIndex:[timePicker selectedRowInComponent:1]] displayType];
    ampm = [[ampmList objectAtIndex:[timePicker selectedRowInComponent:2]] displayType];

    [reportProfile reloadData];
    [reportProfile reloadInputViews];
    
}

-(void) selectedNotification:(id)sender{
    [actionSheetNotification dismissWithClickedButtonIndex:0 animated:YES];
    notificationSelected = TRUE;
    [[addParts objectAtIndex:3] setDisplay:[[notificationTypeList objectAtIndex:[notificationPicker selectedRowInComponent:0]] displayText]];
    notificationType = [[notificationTypeList objectAtIndex:[notificationPicker selectedRowInComponent:0]] displayType];
    [reportProfile reloadData];
    [reportProfile reloadInputViews];
}

-(void) addReport{
    NSLog(@"type: %d Weekday %d hour %d min %d ampm %d notification %d",commuteType, weekday, hour, min, ampm, notificationType);
    connectionType = 5;
    NSString *URL;
    
    if(appDelegate.selectedCity == 1){
        URL = [NSString stringWithFormat: @"http://%@/sd/servlet/reportservlet?type=%d&user=%d&ctype=%d&ntype=%d&hour=%d&min=%d&ampm=%d&day=%d",appDelegate.cpuUrl, 1, appDelegate.userID, commuteType, notificationType, hour, min, ampm, weekday];
        NSLog(@"URL = %@",URL);
    } else if(appDelegate.selectedCity == 2){
        URL = [NSString stringWithFormat: @"http://%@/la/servlet/reportservlet?type=%d&user=%d&ctype=%d&ntype=%d&hour=%d&min=%d&ampm=%d&day=%d",appDelegate.cpuUrl, 1, appDelegate.userID, commuteType, notificationType, hour, min, ampm, weekday];
    } else {
        URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/reportservlet?type=%d&user=%d&ctype=%d&ntype=%d&hour=%d&min=%d&ampm=%d&day=%d",appDelegate.cpuUrl, 1, appDelegate.userID, commuteType, notificationType, hour, min, ampm, weekday];
    }
    
    
    
    NSError *error = nil;
    @try{
        
        
        NSString *post = @"";
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:URL]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray *cookies = [scs cookies];
        NSEnumerator* e = [cookies objectEnumerator];
        NSHTTPCookie* cookie;
        while ((cookie = [e nextObject])) {
            NSString* name = [cookie name];
            if ([name isEqualToString:@"userId"]) {
                [scs deleteCookie:cookie];
            }
        }
        
        conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (conn)
        {
            NSLog(@"received Data");
            receivedNewReportData = [[NSMutableData data] retain];
        }
        else
        {
            NSLog(@"no conn");
        }
    } @catch (NSException *e) {
        NSLog(@"Failed to Login");
    }
}

-(void) editReport {
    NSLog(@"select Report id: %d",[selectedReport reportID]);
    if(!editsDisplayed){
        reportTypeSet = TRUE;
        editsDisplayed = TRUE;
        connectionType = 4;
        int rowSelection = -1;
        if(commuteType == 0) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Morning and Alt Morning" type:0 connection:NULL]];
        } else if(commuteType == 1) {
            [typeList addObject:[[ProfileCell alloc] initWithDisplay:@" Evening and Alt Evening" type:1 connection:NULL]];
        }
        
        for(int i = 0; i< [typeList count]; i++){
            if(commuteType == [[typeList objectAtIndex:i] displayType]){
                rowSelection = i;
            }
        }
        [typePicker selectRow:rowSelection inComponent:0 animated:NO];
        typeSelected = TRUE;
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:[[typeList objectAtIndex:rowSelection] displayText] type:1 connection:NULL]];
        
        
        for(int i = 0; i < [weekdayList count]; i++){
            if([[weekdayList objectAtIndex:i] displayType] == [selectedReport day]){
                rowSelection = i;
            }
        }
        [weekdayPicker selectRow:rowSelection inComponent:0 animated:NO];
        weekdaySelected = TRUE;
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:[[weekdayList objectAtIndex:rowSelection] displayText] type:2 connection:NULL]];
        weekday = [[weekdayList objectAtIndex:[weekdayPicker selectedRowInComponent:0]] displayType];
        
        NSLog(@"hour");
        for(int i = 0; i < [hourList count]; i++){
            if([[hourList objectAtIndex:i] displayType] == [selectedReport hour]){
                rowSelection = i;
            }
        }

        [timePicker selectRow:rowSelection inComponent:0 animated:NO];
        NSLog(@"min");

        for(int i = 0; i < [minList count]; i++){
            if([[minList objectAtIndex:i] displayType] == [selectedReport minute]){
                rowSelection = i;
            }
        }
        [timePicker selectRow:rowSelection inComponent:1 animated:NO];
        NSLog(@"ampm");

        for(int i = 0; i < [ampmList count]; i++){
            if([[ampmList objectAtIndex:i] displayType] == [selectedReport ampm]){
                rowSelection = i;
            }
        }
        [timePicker selectRow:rowSelection inComponent:2 animated:NO];
        
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:[NSString stringWithFormat:@" %@:%@ %@",[[hourList objectAtIndex:[timePicker selectedRowInComponent:0]] displayText], [[minList objectAtIndex:[timePicker selectedRowInComponent:1]] displayText], [[ampmList objectAtIndex:[timePicker selectedRowInComponent:2]] displayText]] type:3 connection:NULL]];
        hour = [[hourList objectAtIndex:[timePicker selectedRowInComponent:0]] displayType];
        min = [[minList objectAtIndex:[timePicker selectedRowInComponent:1]] displayType];
        ampm = [[ampmList objectAtIndex:[timePicker selectedRowInComponent:2]] displayType];
        
        timeSelected = TRUE;

        
        for(int i = 0; i < [notificationTypeList count]; i++){
            if([[notificationTypeList objectAtIndex:i] displayType] == [selectedReport notify]){
                rowSelection = i;
            }
        }
        [notificationPicker selectRow:rowSelection inComponent:0 animated:NO];
        notificationSelected = TRUE;
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:[[notificationTypeList objectAtIndex:[notificationPicker selectedRowInComponent:0]] displayText] type:4 connection:NULL]];
        notificationType = [[notificationTypeList objectAtIndex:[notificationPicker selectedRowInComponent:0]] displayType];
        [addParts addObject:[[ProfileCell alloc] initWithDisplay:@"Submit Edit" type:6 connection:NULL]];
        
        [reportProfile reloadData];
        [reportProfile reloadInputViews];
    }
}

-(void) deleteReport {
    if(!editsDisplayed){

        connectionType = 3;
        
        NSString *URL;
        if(appDelegate.selectedCity == 1){
            URL = [NSString stringWithFormat: @"http://%@/sd/servlet/reportservlet?user=%d&type=%d&reportid=%d",appDelegate.cpuUrl,appDelegate.userID,3,selectedReportID];
        } else if(appDelegate.selectedCity == 2){
            URL = [NSString stringWithFormat: @"http://%@/la/servlet/reportservlet?user=%d&type=%d&reporttid=%d",appDelegate.cpuUrl,appDelegate.userID,3,selectedReportID];
        } else {
            URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/reportservlet?user=%d&type=%d&reportid=%d",appDelegate.cpuUrl,appDelegate.userID,3,selectedReportID];
        }
        
        NSString *post = @"";
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        //NSString *post = [NSString ];
        //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:URL]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
        
        NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray *cookies = [scs cookies];
        NSEnumerator* e = [cookies objectEnumerator];
        NSHTTPCookie* cookie;
        while ((cookie = [e nextObject])) {
            NSString* name = [cookie name];
            if ([name isEqualToString:@"userId"]) {
                [scs deleteCookie:cookie];
            }
        }
        
        conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (conn)
        {
            NSLog(@"received Data");
            receivedData = [[NSMutableData data] retain];
        } else {
            NSLog(@"no conn");
        }
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [self setTitle:@"Daily Report Setup"];
    //[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:NO];
}

- (void)viewDidDisappear:(BOOL) animated {
	//[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:YES];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
