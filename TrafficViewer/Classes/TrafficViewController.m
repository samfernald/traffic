//
//  TrafficViewController.m
//  TrafficViewer
//
//  Created by sfernald on 11/12/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "TrafficViewController.h"
#import "RootViewController.h"
#import "GAI.h"


@implementation TrafficViewController

@synthesize sdhighways, lahighways, bahighways, highwayFlowViewController;

/*
// Override initWithNibName:bundle: to load the view using a nib file then perform additional customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement loadView to create a view hierarchy programmatically.
- (void) viewDidLoad {
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[self setTitle:@"Speeds"];
    NSLog(@"TrafficView Loaded");
	//self.trackedViewName = @"Traffic view";
	self.navigationItem.hidesBackButton = YES;
	//id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-33122525-1"];
    //[tracker sendView:@"Traffic view"];

	
	//self.view.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"TrafficAppBackground.png"]];
	
	@try{
		if(appDelegate.selectedCity == 1){
            UIScrollView * scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 600)];//(0,0,320,160)
            scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
            NSLog(@"loading SD HIGHWAYS");
			[scrollView setContentSize:CGSizeMake(320,600)];
			scrollView.bounces = NO;
			scrollView.scrollEnabled = YES;
			[sdhighways setBackgroundColor:[[UIColor colorWithRed: 249.0/255.0 green:207.0/255.0 blue:28.0/255.0 alpha:1]retain]];
            //[self.view addSubview:sdhighways];
            
			[scrollView addSubview:sdhighways];
            self.view = scrollView;
			//[self.view addSubview:scrollView];
             
			[scrollView retain];
		} else if(appDelegate.selectedCity == 2){
            UIScrollView * scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 1000)];//(0,0,320,160)
            scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
			[scrollView setContentSize:CGSizeMake(320,1000)];	
			scrollView.bounces = NO;
			scrollView.scrollEnabled = YES;
			[lahighways setBackgroundColor:[[UIColor colorWithRed: 249.0/255.0 green:207.0/255.0 blue:28.0/255.0 alpha:1]retain]];
			[scrollView addSubview:lahighways];
			self.view = scrollView;
			[scrollView retain];
		} else {
            UIScrollView * scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 1000)];//(0,0,320,160)
            scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
			[scrollView setContentSize:CGSizeMake(320,1000)];
			scrollView.bounces = NO;
			scrollView.scrollEnabled = YES;
			[bahighways setBackgroundColor:[[UIColor colorWithRed: 249.0/255.0 green:207.0/255.0 blue:28.0/255.0 alpha:1]retain]];
			[scrollView addSubview:bahighways];
			self.view = scrollView;
			[scrollView retain];
		}
		NSLog(@"got to the end");
		
		
	} @catch (NSException * e) {
		NSLog(@"Exception %@: %@", [e name], [e  reason]);
	}
		
}


-(IBAction) highwaySelected:(id)sender{
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	appDelegate.selectedHighway = [sender tag];
	switch([sender tag]){
		case 1: {appDelegate.selectedHighwayImagePath = @"5.png"; break;}
		case 2: {appDelegate.selectedHighwayImagePath = @"5.png"; break;}
		case 3: {appDelegate.selectedHighwayImagePath = @"8.png"; break;}
		case 4: {appDelegate.selectedHighwayImagePath = @"8.png"; break;}
		case 5: {appDelegate.selectedHighwayImagePath = @"15.png"; break;}
		case 6: {appDelegate.selectedHighwayImagePath = @"15.png"; break;}
		case 7: {appDelegate.selectedHighwayImagePath = @"52.png"; break;}
		case 8: {appDelegate.selectedHighwayImagePath = @"52.png"; break;}
		case 9: {appDelegate.selectedHighwayImagePath = @"54.png"; break;}
		case 10: {appDelegate.selectedHighwayImagePath = @"54.png"; break;}
		case 11: {appDelegate.selectedHighwayImagePath = @"56.png"; break;}
		case 12: {appDelegate.selectedHighwayImagePath = @"56.png"; break;}
		case 13: {appDelegate.selectedHighwayImagePath = @"78.png"; break;}
		case 14: {appDelegate.selectedHighwayImagePath = @"78.png"; break;}
		case 15: {appDelegate.selectedHighwayImagePath = @"94.png"; break;}
		case 16: {appDelegate.selectedHighwayImagePath = @"94.png"; break;}
		case 17: {appDelegate.selectedHighwayImagePath = @"125.png"; break;}
		case 18: {appDelegate.selectedHighwayImagePath = @"125.png"; break;}
		case 19: {appDelegate.selectedHighwayImagePath = @"163.png"; break;}
		case 20: {appDelegate.selectedHighwayImagePath = @"163.png"; break;}
		case 21: {appDelegate.selectedHighwayImagePath = @"805.png"; break;}
		case 22: {appDelegate.selectedHighwayImagePath = @"805.png"; break;}
	}
	HighwayFlowViewController *speedsView = [[HighwayFlowViewController alloc] initWithNibName:@"HighwayFlowViewController" bundle:[NSBundle mainBundle]];
	[[self navigationController] pushViewController:speedsView animated:YES];
}

	
	/*
	SDHighwaysViewController *SDview = [[SDHighwaysViewController alloc] initWithNibName:@"SDHighwaysViewController" bundle:[NSBundle mainBundle]];
	
	UINavigationController *localNavController = [[UINavigationController alloc] initWithRootViewController:SDview];
	localNavController.tabBarItem.enabled = YES;
	[SDview release];   // This is now managed by the navigation controller 
	
	[[appDelegate.tabBarController viewControllers] replaceObjectAtIndex:2 withObject:localNavController];
	[localNavController release];
	
	[appDelegate.tabBarController setSelectedIndex:2];
	*/
	
	//!!!!!!ADD check for page already loaded
	/*
	 
	 
-(void)viewWillAppear:(BOOL) animated {
	[super viewWillAppear:animated];
	notifier = [[UIAlertView alloc] initWithTitle:@"" message:@"Obtaining map data from http://traffic.calit2.net." delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
	[notifier show];
}
*/
/*
// Implement viewDidLoad to do additional setup after loading the view.
- (void)viewDidLoad {
    [super viewDidLoad];
}
 */


/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


- (void)dealloc {
    [super dealloc];
}


@end
