//
//  Commute.m
//  SimpleCommuteParser
//
//  Created by sfernald on 9/18/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "Commute.h"


@implementation Commute
@synthesize morning;
@synthesize alternate;
@synthesize AM_PM; 
@synthesize numSegments;
@synthesize currentTime;
@synthesize averageTime; 
@synthesize difference;
@synthesize segments;
@synthesize timeStamp;

-(id)initWithType:(bool)morn 
		alternate:(bool)alt 
	  numSegments:(int)numSegs 
	  currentTime:(int)currTime 
	  averageTime:(int)ave
	   difference:(float)diff{
	self.morning = morn;
	self.alternate = alt;
	self.numSegments = numSegs;
	self.currentTime = currTime;
	self.averageTime = ave;
	self.difference = diff;
	segments = [[NSMutableArray alloc] init];
	return self;
}

-(id) initWithCommute:(Commute *)c{
	self.morning = c.morning;
	self.alternate = c.alternate;
	self.numSegments = c.numSegments;
	self.currentTime = c.currentTime;
	self.averageTime = c.averageTime;
	self.difference = c.difference;
	self.segments = c.segments;
	self.timeStamp = c.timeStamp;
	return self;
}

-(void)addSegment:(Segment *)seg{
	[segments addObject:[[Segment alloc] initWithSegment:seg]];
	numSegments = [segments count];
}

@end
