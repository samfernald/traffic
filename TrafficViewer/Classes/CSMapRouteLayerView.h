//
//  pointInfo.h
//  mapLines
//
//  Created by sfernald on 2/17/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "AppDelegate.h"
#import "MyCLController.h"
#import "pointInfo.h"
#import "testPin.h"


@interface CSMapRouteLayerView : UIView <MKMapViewDelegate,MyCLControllerDelegate,UIScrollViewDelegate>
{
	MKMapView* _mapView;
	NSArray* _pointsZoom0;
	NSArray* _pointsZoom1;
	NSArray* _pointsZoom2;
	NSArray* _pointsZoom3;
	NSArray* _pointsZoom4;
	NSArray* _pointDetails;
	NSMutableArray* pointsInSegment;
	CGPoint _startLocation;
	CGPoint _originalCenter;
	UIColor* _lineColor;
	int zoomLevel;
	BOOL initMapLoad, pointsAdded;
	MyCLController *locationController;
	BOOL locationHasUpdated;
	BOOL _isMoving;
}

-(id) initWithMapView:(MKMapView*)mapView;

-(id) initWithPoints:(NSArray*)routePointsZoom0
		 pointsZoom1:(NSArray*)routePointsZoom1
		 pointsZoom2:(NSArray*)routePointsZoom2
		 pointsZoom3:(NSArray*)routePointsZoom3 
		 pointsZoom4:(NSArray*)routePointsZoom4
		pointDetails:(NSArray*)pointDets
			 mapView:(MKMapView*)mapView;

-(void) addPoints:(NSArray*)routePointsZoom0
	  pointsZoom1:(NSArray*)routePointsZoom1
	  pointsZoom2:(NSArray*)routePointsZoom2
	  pointsZoom3:(NSArray*)routePointsZoom3 
	  pointsZoom4:(NSArray*)routePointsZoom4
	 pointDetails:(NSArray*)pointDets;

-(void) addIncidents:(NSMutableArray*)incidents;

-(void) addLineSegment:(NSArray*)pointsToAdd speedRange:(int)speed;

- (void)locationUpdate:(CLLocation *)location; 
- (void)locationError:(NSError *)error;


@property (nonatomic, retain) NSArray* pointsZoom0;
@property (nonatomic, retain) NSArray* pointsZoom1;
@property (nonatomic, retain) NSArray* pointsZoom2;
@property (nonatomic, retain) NSArray* pointsZoom3;
@property (nonatomic, retain) NSArray* pointsZoom4;
@property (nonatomic, retain) NSArray* pointDetails;
@property (nonatomic, retain) NSMutableArray* pointsInSegment;
@property (nonatomic, retain) MKMapView* mapView;
@property (nonatomic, retain) UIColor* lineColor; 
@property int zoomLevel;
@property BOOL initMapLoad;
@property BOOL pointsAdded;

@end
