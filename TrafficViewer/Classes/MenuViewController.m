//
//  MenuViewController.m
//  TrafficViewer
//
//  Created by sfernald on 11/19/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MenuViewController.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "FAQViewController.h"
#import "PreferencesViewController.h"
#import "ProfileCell.h"
#import "GAI.h"


@implementation MenuViewController


// Implement viewDidLoad to do additional setup after loading the view.
- (void)viewDidLoad {
	NSLog(@"Menu view loaded");
    [super viewDidLoad];
    //self.trackedViewName = @"Menu View";
    //id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-33122525-1"];
    //[tracker sendView:@"Menu view"];
	appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.menuNavController = self.navigationController;
    chartObjects = [[NSMutableArray alloc] init];
    menuObjects = [[NSMutableArray alloc] init];
    [menuObjects addObject:[[ProfileCell alloc] initWithDisplay:@"      FAQ" type:1 connection:[UIImage imageNamed:@"faq.png"]]];
    [menuObjects addObject:[[ProfileCell alloc] initWithDisplay:@"      Feedback" type:2 connection:[UIImage imageNamed:@"feedback.png"]]];
    [menuObjects addObject:[[ProfileCell alloc] initWithDisplay:@"      Change City / Logout" type:3 connection:[UIImage imageNamed:@"logout.png"]]];
    [menuObjects addObject:[[ProfileCell alloc] initWithDisplay:@"      Preferences" type:4 connection:[UIImage imageNamed:@"prefs.png"]]];

    
	self.navigationItem.hidesBackButton = TRUE;
	[self setTitle:@"Menu"];
	self.navigationController.tabBarItem.title = @"Menu";
	[self.view setBackgroundColor:[[UIColor colorWithRed: 249.0/255.0 green:207.0/255.0 blue:28.0/255.0 alpha:1] retain]];
    

    appDelegate.tabBarController = (UITabBarController *)[appDelegate.storyBoard instantiateViewControllerWithIdentifier:@"TabBarController"];
    NSLog(@"selected index = %d",[appDelegate.tabBarController selectedIndex]);
	//self.view.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"TrafficAppBackground.png"]];
    
    
	if(!appDelegate.autoLoadedGuest) {
		//[self.navigationController.tabBarItem setEnabled:NO];
		appDelegate.autoLoadedGuest = FALSE;
	}
    
    if(appDelegate.screenToLoad == 1){
        [self.tabBarController setSelectedIndex:1];
    } else if(appDelegate.screenToLoad == 2){
        [self.tabBarController setSelectedIndex:2];
    } else if(appDelegate.screenToLoad == 3){
        [self.tabBarController setSelectedIndex:3];
    }
}


-(void)viewDidAppear:(BOOL) animated {
	NSLog(@"menu VIEW appeared");
    NSLog(@"selected index = %d and total VCs = %d",[appDelegate.tabBarController selectedIndex],[appDelegate.tabBarController.viewControllers count]);
	[super viewDidAppear:animated];
    //[UIApplication sharedApplication].statusBarHidden = FALSE;

    if(appDelegate.userName != nil) {
        if(chartObjects.count < 1){
            //[menuObjects addObject:[[ProfileCell alloc] initWithDisplay:@"Edit Commute" type:5 connection:NULL]];
            //[menuObjects addObject:[[ProfileCell alloc] initWithDisplay:@"Edit Alerts" type:6 connection:NULL]];
            //[menuObjects addObject:[[ProfileCell alloc] initWithDisplay:@"Edit Daily Report" type:7 connection:NULL]];
            //[menuObjects addObject:[[ProfileCell alloc] initWithDisplay:@"Edit Account" type:8 connection:NULL]];
            if(appDelegate.morningExists){
                [chartObjects addObject:[[ProfileCell alloc] initWithDisplay:@"      Morning" type:10 connection:[UIImage imageNamed:@"graphicon.png"]]];
            }
            if(appDelegate.altMorningExists){
                [chartObjects addObject:[[ProfileCell alloc] initWithDisplay:@"      Alternate Morning" type:14 connection:[UIImage imageNamed:@"graphicon.png"]]];
            }
            if(appDelegate.eveningExists){
                [chartObjects addObject:[[ProfileCell alloc] initWithDisplay:@"      Evening" type:11 connection:[UIImage imageNamed:@"graphicon.png"]]];
            }
            if(appDelegate.altEveningExists){
                [chartObjects addObject:[[ProfileCell alloc] initWithDisplay:@"      Alternate Evening" type:15 connection:[UIImage imageNamed:@"graphicon.png"]]];
            }
            if(appDelegate.morningExists && appDelegate.altMorningExists){
                [chartObjects addObject:[[ProfileCell alloc] initWithDisplay:@"      Morning vs Alternate Mor" type:12 connection:[UIImage imageNamed:@"graphicon.png"]]];
            }
            if(appDelegate.eveningExists && appDelegate.altEveningExists){
                [chartObjects addObject:[[ProfileCell alloc] initWithDisplay:@"      Evening vs Alternate Eve" type:13 connection:[UIImage imageNamed:@"graphicon.png"]]];
            }
        }
    } else {
        /*
        if(menuObjects.count < 5){
            [menuObjects addObject:[[ProfileCell alloc] initWithDisplay:@"Create Account" type:9 connection:NULL]];
        }
         */
    }
    [menuTable reloadData];
    [menuTable reloadInputViews];
	if(!appDelegate.autoLoadedGuest) {
		NSLog(@"guest was not auto loaded");
		//[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:NO];
	}
	appDelegate.autoLoadedGuest = FALSE;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 1){
        return @"Best Time To Commute Graphs";
    }
    return NULL;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if(section == 1){
        return @"Graphs are based on your commute's average over the last 3 months.";
    }
    return NULL;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        return menuObjects.count;
    } else {
        return chartObjects.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    // Configure the cell.
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        cell.textLabel.numberOfLines = 1;
        cell.textLabel.font = [UIFont systemFontOfSize:20.0];
        cell.textLabel.text = [[menuObjects objectAtIndex:indexPath.row] displayText];
        UIImageView *cellIcon = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 32, 32)];
        cellIcon.image = [[menuObjects objectAtIndex:indexPath.row] container];
        [cell.contentView addSubview:cellIcon];
    } else if(indexPath.section == 1){
        cell.textLabel.numberOfLines = 1;
        cell.textLabel.font = [UIFont systemFontOfSize:20.0];
        cell.textLabel.text = [[chartObjects objectAtIndex:indexPath.row] displayText];
        UIImageView *cellIcon = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 32, 32)];
        cellIcon.image = [[chartObjects objectAtIndex:indexPath.row] container];
        [cell.contentView addSubview:cellIcon];

    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        if([[menuObjects objectAtIndex:indexPath.row] displayType] == 1){
            [self viewFAQ];
        } else if([[menuObjects objectAtIndex:indexPath.row] displayType] == 2){
            [self feedBack];
        } else if([[menuObjects objectAtIndex:indexPath.row] displayType] == 3){
            [self logOut];
        } else if([[menuObjects objectAtIndex:indexPath.row] displayType] == 4){
            [self viewPreferences];
        }
    }/*else if([[menuObjects objectAtIndex:indexPath.row] displayType] == 5){
        [self editCommute];
    } else if([[menuObjects objectAtIndex:indexPath.row] displayType] == 6){
        [self editAlerts];
    }else if([[menuObjects objectAtIndex:indexPath.row] displayType] == 7){
        [self editDailyReport];
    }else if([[menuObjects objectAtIndex:indexPath.row] displayType] == 8){
        [self editAccount];
    }else if([[menuObjects objectAtIndex:indexPath.row] displayType] == 9){
        [self createAccount];
    } */
    else {
        if([[chartObjects objectAtIndex:indexPath.row] displayType] == 10){
            [self viewM5DGraphs];
        } else if([[chartObjects objectAtIndex:indexPath.row] displayType] == 11){
            [self viewE5DGraphs];
        } else if([[chartObjects objectAtIndex:indexPath.row] displayType] == 12){
            [self viewMCGraphs];
        } else if([[chartObjects objectAtIndex:indexPath.row] displayType] == 13){
            [self viewECGraphs];
        }else if([[chartObjects objectAtIndex:indexPath.row] displayType] == 14){
            [self viewAM5DGraphs];
        } else if([[chartObjects objectAtIndex:indexPath.row] displayType] == 15){
            [self viewAE5DGraphs];
        }
    }
}

- (void)viewDidDisappear:(BOOL) animated {
	//[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:YES];
}

-(void) logOut{
	[appDelegate resetApplication];
}

-(void) viewPreferences {
	NSLog(@"about to load prefs");
	PreferencesViewController *prefView = [[PreferencesViewController alloc] initWithNibName:@"PreferencesViewController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:prefView animated:YES];
	NSLog(@"loaded");
}

-(void) viewFAQ {
	FAQViewController *faqView = [[FAQViewController alloc] initWithNibName:@"FAQViewController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:faqView animated:YES];
}


-(void) editCommute{
    UIWebView* webView = [[UIWebView alloc] initWithFrame:self.view.frame];
    
    self.title = @"Edit Commute";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(closeWebview:)];
    
    [self.view addSubview:webView];
    
    NSString* url = @"http://trafficdev.calit2.net/sd/mycommute.jsp";
    
    NSURL* nsUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    
    NSDictionary *properties1 = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"trafficdev.calit2.net", NSHTTPCookieDomain,
                                @"/", NSHTTPCookiePath,  // IMPORTANT!
                                @"userId", NSHTTPCookieName,
                                @"1", NSHTTPCookieValue,
                                nil];
    NSHTTPCookie *cookie1 = [NSHTTPCookie cookieWithProperties:properties1];
    
    NSDictionary *properties2 = [NSDictionary dictionaryWithObjectsAndKeys:
                                 @"trafficdev.calit2.net", NSHTTPCookieDomain,
                                 @"/", NSHTTPCookiePath,  // IMPORTANT!
                                 @"sessionId", NSHTTPCookieName,
                                 @"1", NSHTTPCookieValue,
                                 nil];
    NSHTTPCookie *cookie2 = [NSHTTPCookie cookieWithProperties:properties2];

    
    NSArray* cookies = [NSArray arrayWithObjects: cookie1, cookie2, nil];
    
    NSDictionary * headers = [NSHTTPCookie requestHeaderFieldsWithCookies:cookies];
    
    [request setAllHTTPHeaderFields:headers];
    
    [webView loadRequest:request];
    /*
    CommuteCreationViewController *ccvc = [[CommuteCreationViewController alloc] initWithNibName:@"CommuteCreationViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:ccvc animated:YES];
    [ccvc release];
     */
}

-(void) editAlerts{
    UIWebView* webView = [[UIWebView alloc] initWithFrame:self.view.frame];
    
    self.title = @"Edit Alerts";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(closeWebview:)];
    
    [self.view addSubview:webView];
    /*
    AlertViewController *avc = [[AlertViewController alloc] initWithNibName:@"AlertViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:avc animated:YES];
    [avc release];
     */
}

-(void) editDailyReport{
    UIWebView* webView = [[UIWebView alloc] initWithFrame:self.view.frame];
    
    self.title = @"Edit Daily Report";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(closeWebview:)];
    
    [self.view addSubview:webView];
    /*
    DailyReportViewController *drvc = [[DailyReportViewController alloc] initWithNibName:@"DailyReportViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:drvc animated:YES];
    [drvc release];
     */
}

-(void) editAccount{
    UIWebView* webView = [[UIWebView alloc] initWithFrame:self.view.frame];
    
    self.title = @"Edit Account";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(closeWebview:)];
    
    [self.view addSubview:webView];
}

-(void) createAccount{
    UIWebView* webView = [[UIWebView alloc] initWithFrame:self.view.frame];
    
    [self.view addSubview:webView];
    
    NSString* url = @"http://trafficdev.calit2.net/sd/signup.jsp";
    
    NSURL* nsUrl = [NSURL URLWithString:url];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    
    [webView loadRequest:request];
}

-(void) closeWebview:(id)sender {
    self.navigationItem.leftBarButtonItem = nil;
    self.title = @"Menu";
    [[self.view.subviews objectAtIndex:self.view.subviews.count-1] removeFromSuperview];
}


-(void) feedBack {
	if(appDelegate.selectedCity == 1) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:sd-feedback@traffic.calit2.net?subject=iphone"]];
	} else if(appDelegate.selectedCity == 2) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:la-feedback@traffic.calit2.net?subject=iphone"]];
	} else if(appDelegate.selectedCity == 3) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:ba-feedback@traffic.calit2.net?subject=iphone"]];
	}
}

-(void) viewM5DGraphs {
    MorningFiveDayViewController *m5dvc = [[MorningFiveDayViewController alloc] initWithNibName:@"MorningFiveDayViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController presentModalViewController:m5dvc animated:YES];
    //[self.navigationController pushViewController:m5dvc animated:YES];

}

-(void) viewE5DGraphs {
    EveningFiveDayViewController *e5dvc = [[EveningFiveDayViewController alloc] initWithNibName:@"EveningFiveDayViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController presentModalViewController:e5dvc animated:YES];
    //[self.navigationController pushViewController:e5dvc animated:YES];
    
}

-(void) viewAM5DGraphs {
    AlternateMorningFiveDayViewController *am5dvc = [[AlternateMorningFiveDayViewController alloc] initWithNibName:@"AlternateMorningFiveDayViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController presentModalViewController:am5dvc animated:YES];
    
}

-(void) viewAE5DGraphs {
    AlternateEveningFiveDayViewController *ae5dvc = [[AlternateEveningFiveDayViewController alloc] initWithNibName:@"AlternateEveningFiveDayViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController presentModalViewController:ae5dvc animated:YES];
    
}


-(void) viewMCGraphs {
    MorningVSViewController *mvsvc = [[MorningVSViewController alloc] initWithNibName:@"MorningVSViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController presentModalViewController:mvsvc animated:YES];
    //[self.navigationController pushViewController:m5dvc animated:YES];
    
}

-(void) viewECGraphs {
    EveningVSViewController *e5dvc = [[EveningVSViewController alloc] initWithNibName:@"EveningVSViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController presentModalViewController:e5dvc animated:YES];
    //[self.navigationController pushViewController:m5dvc animated:YES];
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;// for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
	NSLog(@"menu did receive mem warning");
}


- (void)dealloc {
    [super dealloc];
}




@end
