//
//  MenuViewController.h
//  TrafficViewer
//
//  Created by sfernald on 11/19/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommuteCreationViewController.h"
#import "DailyReportViewController.h"
#import "AlertViewController.h"
#import "MorningFiveDayViewController.h"
#import "EveningFiveDayViewController.h"
#import "AlternateMorningFiveDayViewController.h"
#import "AlternateEveningFiveDayViewController.h"
#import "MorningVSViewController.h"
#import "EveningVSViewController.h"
//#import "GAITrackedViewController.h"


@interface MenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {

    IBOutlet UIButton *editCommute;
    IBOutlet UIButton *viewGraphs;
    IBOutlet UITableView *menuTable;
    NSMutableArray *menuObjects;
    NSMutableArray *chartObjects;
    AppDelegate *appDelegate;

}

-(void)closeWebview;

@end
