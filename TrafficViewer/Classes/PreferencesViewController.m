//
//  PreferencesViewController.m
//  TrafficViewer
//
//  Created by sfernald on 7/31/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "PreferencesViewController.h"

@implementation PreferencesViewController

@synthesize startScreen, startMap, cityLabel, loadMapFaster, trafficMapOps;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	NSLog(@"pref screen did load");
    [super viewDidLoad];
	appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[self setTitle:@"Preferences"];
	//natural_t freemem =[AppDelegate get_free_memory];
	//NSLog(@"(menu) freemem = %d",freemem);
	//NSString *freeMemMessage = [[NSString alloc] initWithFormat:@"freemem = %d",freemem];
	[self.view setBackgroundColor:[[UIColor colorWithRed: 249.0/255.0 green:207.0/255.0 blue:28.0/255.0 alpha:1] retain]];
	NSNumber * screenToLoad = (NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"screenToLoad", kCFPreferencesCurrentApplication);
	[startScreen setSelectedSegmentIndex:[screenToLoad intValue]-1];
	NSLog(@"selected index = %d",[screenToLoad intValue]);
	NSNumber * mapToLoad = (NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"initMap", kCFPreferencesCurrentApplication);
	[startMap setSelectedSegmentIndex:[mapToLoad intValue]];
    
    NSNumber * googleAna = (NSNumber*)CFPreferencesCopyAppValue((CFStringRef) @"googleAnalytics", kCFPreferencesCurrentApplication);
    if([googleAna intValue] == 0){
        [googleAnalyticsSwitch setOn:TRUE];
    } else {
        [googleAnalyticsSwitch setOn:FALSE];
    }
    

    
	NSLog(@"selected map index = %d",[mapToLoad intValue]);
	//[startMap setHidden:YES];
    /*
	if(appDelegate.selectedCity == 1) {
		[startMap setTitle:@"SD only" forSegmentAtIndex:0];
		[cityLabel setText:@"Choosing San Diego only will"];
	} else if(appDelegate.selectedCity == 2) {
		[startMap setTitle:@"LA only" forSegmentAtIndex:0];
		[cityLabel setText:@"Choosing Los Angeles only will"];
	} else {
		[startMap setHidden:YES];
		[cityLabel setHidden:YES];
		[trafficMapOps setHidden:YES];
		[loadMapFaster setHidden:YES];
	}
     */
	if(appDelegate.userName == nil) {
		
		[startScreen setEnabled:NO forSegmentAtIndex:0];
	}
	
}

-(void)viewDidAppear:(BOOL) animated {
	[super viewDidAppear:animated];
	
	//[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:NO];
}

- (void)viewDidDisappear:(BOOL)animated {
	
	
	//[[[appDelegate.tabBarController.viewControllers objectAtIndex:0] tabBarItem] setEnabled:YES];
}

-(void) setLoadScreen:(id) sender {
	UISegmentedControl *segCtrl = sender;
	NSLog(@"selected index = %d",[segCtrl selectedSegmentIndex]);
	NSNumber * screen2Load = [NSNumber numberWithInt: [segCtrl selectedSegmentIndex]+1];
	if([screen2Load intValue]>=0) {
		CFPreferencesSetAppValue((CFStringRef)@"screenToLoad",screen2Load , kCFPreferencesCurrentApplication);
	}
	//CFPreferencesAppSynchronize(kCFPreferencesCurrentApplication);
	
}


-(void) setLoadMap:(id) sender {
	UISegmentedControl *segCtrl = sender;
	NSLog(@"selected index = %d",[segCtrl selectedSegmentIndex]);
		NSNumber * map2Load = [NSNumber numberWithInt:[segCtrl selectedSegmentIndex]];
	CFPreferencesSetAppValue((CFStringRef)@"initMap", map2Load , kCFPreferencesCurrentApplication);
    appDelegate.mapToLoad = [map2Load intValue];
	//CFPreferencesAppSynchronize(kCFPreferencesCurrentApplication);
}


-(IBAction) setGoogleAnalyticsState:(id)sender {
    UISwitch *segCtrl = sender;
    NSNumber * googleAna;
    if([segCtrl isOn]){
        googleAna = [NSNumber numberWithInt:0];
    } else {
        googleAna = [NSNumber numberWithInt:1];
    }
	CFPreferencesSetAppValue((CFStringRef)@"googleAnalytics", googleAna , kCFPreferencesCurrentApplication);
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
