//
//  CommuteType.h
//  TrafficViewer
//
//  Created by sfernald on 7/17/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CommuteType : NSObject {
	UIImage *icon;
	NSString *alternate;

}

@property(nonatomic,retain) UIImage *icon;
@property(nonatomic,retain) NSString *alternate;

-(id) initWithImg:(UIImage*)cIcon title:(NSString*)alt;

@end
