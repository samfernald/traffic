//
//  ExitListing.m
//  TrafficViewer
//
//  Created by Sam Fernald on 9/21/12.
//  Copyright (c) 2012 Calit2. All rights reserved.
//

#import "ExitListing.h"

@implementation ExitListing

@synthesize exitID, exitName, highwayID;

-(id) initWithName:(NSString*)n exitId:(int)e highwayId:(int)h {
    self.exitName = n;
    self.exitID = e;
    self.highwayID = h;
    return self;
}

@end
