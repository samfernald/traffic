//
//  CPDScatterPlotViewController.m
//  CorePlotDemo
//
//  Created by Fahim Farook on 19/5/12.
//  Copyright 2012 RookSoft Pte. Ltd. All rights reserved.
//

#import "MorningFiveDayViewController.h"
#import "CJSONSerializer.h"
#import "CJSONDeserializer.h"


@interface MorningFiveDayViewController ()



@property (nonatomic, strong) CPTGraphHostingView *hostView;
@property (nonatomic, strong) UIButton *buttonDisplay;




@end

@implementation MorningFiveDayViewController


CPTGraph *graph;

@synthesize hostView = hostView_;
@synthesize buttonDisplay = buttonDisplay_;
//@synthesize day;
@synthesize chosenLaneType;

@synthesize graphData;

@synthesize infoLabel, navBar;
@synthesize morningOrEvening;


//max value for axis tick labels.

//int xLabelMaxValue should be dynamically bound to our data.


-(void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - UIViewController lifecycle methods
-(void)viewDidLoad{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [UIApplication sharedApplication].statusBarHidden = TRUE;
    //max scroll values
    xGlobalOrigin = -0.25;
    xGlobalLength = 65;
    
    yGlobalOrigin = 50;
    yGlobalLength = 20;
    
    //plot area value range that is showing constantly
    xShowAreaOrigin = 1.2;
    xShowAreaLength = 65;
    
    yShowAreaOrigin = 50;
    yShowAreaLength = 20;
    
    //spacing - stretching factor of each axis
    xShowAreaScaleFactor = 0.4f;
    yShowAreaScaleFactor = 1.0f;
    
    //yLabelMaxValue = 500;
    
    scaleFactor = 1.15;
        
    //self.navigationController.title = @"Morning 5 Day";
    //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(backToMenu:)];
    
    @try{
        
        NSLog(@"user id = %d",appDelegate.user.userId);
        if(appDelegate.user.userId <= 0) {
            connectionType = 1;
            NSString *URL;
            if(appDelegate.selectedCity == 1){
                URL = [NSString stringWithFormat: @"http://%@/sd/servlet/useridservlet?email=%@",appDelegate.cpuUrl,appDelegate.userName];;
            } else if(appDelegate.selectedCity == 2){
                URL = [NSString stringWithFormat: @"http://%@/la/servlet/useridservlet?email=%@",appDelegate.cpuUrl,appDelegate.userName];
            } else {
                URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/useridservlet?email=%@",appDelegate.cpuUrl,appDelegate.userName];
            }
            NSString *post = @"";
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
            //NSString *post = [NSString ];
            //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:URL]];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
            
            NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSArray *cookies = [scs cookies];
            NSEnumerator* e = [cookies objectEnumerator];
            NSHTTPCookie* cookie;
            while ((cookie = [e nextObject])) {
                NSString* name = [cookie name];
                if ([name isEqualToString:@"userId"]) {
                    [scs deleteCookie:cookie];
                }
            }
            
            conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (conn)
            {
                NSLog(@"received Data");
                receivedIDData = [[NSMutableData data] retain];
            } else {
                NSLog(@"no conn");
            }
            
        } else {
            connectionType = 2;
            appDelegate.userID = appDelegate.user.userId;
            NSString *URL;
            if(appDelegate.selectedCity == 1){
                URL = [NSString stringWithFormat: @"http://%@/sd/servlet/jsongraphdata?u=%d",appDelegate.cpuUrl,appDelegate.user.userId];
            } else if(appDelegate.selectedCity == 2){
                URL = [NSString stringWithFormat: @"http://%@/la/servlet/jsongraphdata?u=%d",appDelegate.cpuUrl,appDelegate.user.userId];
            } else {
                URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/jsongraphdata?u=%d",appDelegate.cpuUrl,appDelegate.user.userId];
            }
            NSString *post = @"";
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
            //NSString *post = [NSString ];
            //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:URL]];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
            
            NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSArray *cookies = [scs cookies];
            NSEnumerator* e = [cookies objectEnumerator];
            NSHTTPCookie* cookie;
            while ((cookie = [e nextObject])) {
                NSString* name = [cookie name];
                if ([name isEqualToString:@"userId"]) {
                    [scs deleteCookie:cookie];
                }
            }
            
            conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (conn)
            {
                NSLog(@"received Data");
                receivedData = [[NSMutableData data] retain];
            } else {
                NSLog(@"no conn");
            }
        }
    } @catch (NSException *e) {
        NSLog(@"Failed to Login");
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSLog(@"append text data");
    if(connectionType == 1) {
        [receivedIDData appendData:data];
    } else if(connectionType == 2){
        [receivedData appendData:data];
    }
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"conn failed with error: %@", error);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    if(connectionType == 1){
        
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedIDData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedIDData encoding:NSASCIIStringEncoding];
        NSLog(@"returned data = %@",outputString);
        appDelegate.userID = [outputString intValue];
        connectionType = 2;
        NSString *URL;
        if(appDelegate.selectedCity == 1){
            URL = [NSString stringWithFormat: @"http://%@/sd/servlet/jsongraphdata?u=%d",appDelegate.cpuUrl,appDelegate.userID];
        } else if(appDelegate.selectedCity == 2){
            URL = [NSString stringWithFormat: @"http://%@/la/servlet/jsongraphdata?u=%d",appDelegate.cpuUrl,appDelegate.userID];
        } else {
            URL = [NSString stringWithFormat: @"http://%@/bayarea/servlet/jsongraphdata?u=%d",appDelegate.cpuUrl,appDelegate.userID];
        }
        
        NSString *post = @"";
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        //NSString *post = [NSString ];
        //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSLog([[NSString alloc] initWithData:postData encoding:NSASCIIStringEncoding]);
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:URL]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"ec2-23-22-235-216.compute-1.amazonaws.com"];
        
        NSHTTPCookieStorage *scs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray *cookies = [scs cookies];
        NSEnumerator* e = [cookies objectEnumerator];
        NSHTTPCookie* cookie;
        while ((cookie = [e nextObject])) {
            NSString* name = [cookie name];
            if ([name isEqualToString:@"userId"]) {
                [scs deleteCookie:cookie];
            }
        }
        
        conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (conn)
        {
            NSLog(@"received Data");
            receivedData = [[NSMutableData data] retain];
        } else {
            NSLog(@"no conn");
        }
        
    } else if(connectionType == 2){
        NSError *error = nil;
        NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
        NSString * outputString = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
        //NSLog(@"returned data = %@",outputString);
        
        NSDictionary *returnedActivityInfo = [[CJSONDeserializer alloc] deserializeAsDictionary:receivedData error:&error];
        // Set up the edit and add buttons.
        //NSLog(@"morning5day = %@", [returnedActivityInfo valueForKey:@"morning5day"]);
        morning5Day = [returnedActivityInfo valueForKey:@"morning5day"];
        //evening5Day = [returnedActivityInfo valueForKey:@"evening5day"];
        NSLog(@"count = %d and %d", [morning5Day count],  [[morning5Day objectAtIndex:0] count]);
        
        NSNumber *highestNumber = [[NSNumber alloc] initWithFloat:0.0f];
        NSNumber *highestNumberTotal = [[NSNumber alloc] initWithFloat:0.0f];
        NSNumber *smallestNumber = [[NSNumber alloc] initWithFloat:10000.0f];
        NSNumber *smallestNumberTotal = [[NSNumber alloc] initWithFloat:10000.0f];
        
        for (NSNumber *theNumber in [morning5Day objectAtIndex:0])
        {
            if([theNumber floatValue] > 0){
                if ([theNumber floatValue] > [highestNumber floatValue]) {
                    highestNumber = [NSNumber numberWithFloat:[theNumber floatValue]];
                }
                if ([theNumber floatValue] < [smallestNumber floatValue]) {
                    smallestNumber = [NSNumber numberWithFloat:[theNumber floatValue]];
                }
            }
            
        }
        if ([highestNumber floatValue] > [highestNumberTotal floatValue]) {
            highestNumberTotal = [NSNumber numberWithFloat:[highestNumber floatValue]];
        }
        if ([smallestNumber floatValue] < [smallestNumberTotal floatValue]) {
            smallestNumberTotal = [NSNumber numberWithFloat:[smallestNumber floatValue]];
        }
        
        for (NSNumber *theNumber in [morning5Day objectAtIndex:1])
        {
            if([theNumber floatValue] > 0){
                if ([theNumber floatValue] > [highestNumber floatValue]) {
                    highestNumber = [NSNumber numberWithFloat:[theNumber floatValue]];
                }
                if ([theNumber floatValue] < [smallestNumber floatValue]) {
                    smallestNumber = [NSNumber numberWithFloat:[theNumber floatValue]];
                }
            }
        }
        if ([highestNumber floatValue] > [highestNumberTotal floatValue]) {
            highestNumberTotal = [NSNumber numberWithFloat:[highestNumber floatValue]];
        }
        if ([smallestNumber floatValue] < [smallestNumberTotal floatValue]) {
            smallestNumberTotal = [NSNumber numberWithFloat:[smallestNumber floatValue]];
        }
        
        for (NSNumber *theNumber in [morning5Day objectAtIndex:2])
        {
            if([theNumber floatValue] > 0){
                if ([theNumber floatValue] > [highestNumber floatValue]) {
                    highestNumber = [NSNumber numberWithFloat:[theNumber floatValue]];
                }
                if ([theNumber floatValue] < [smallestNumber floatValue]) {
                    smallestNumber = [NSNumber numberWithFloat:[theNumber floatValue]];
                }
            }
        }
        if ([highestNumber floatValue] > [highestNumberTotal floatValue]) {
            highestNumberTotal = [NSNumber numberWithFloat:[highestNumber floatValue]];
        }
        if ([smallestNumber floatValue] < [smallestNumberTotal floatValue]) {
            smallestNumberTotal = [NSNumber numberWithFloat:[smallestNumber floatValue]];
        }
        
        for (NSNumber *theNumber in [morning5Day objectAtIndex:3])
        {
            if([theNumber floatValue] > 0){
                if ([theNumber floatValue] > [highestNumber floatValue]) {
                    highestNumber = [NSNumber numberWithFloat:[theNumber floatValue]];
                }
                if ([theNumber floatValue] < [smallestNumber floatValue]) {
                    smallestNumber = [NSNumber numberWithFloat:[theNumber floatValue]];
                }
            }
        }
        if ([highestNumber floatValue] > [highestNumberTotal floatValue]) {
            highestNumberTotal = [NSNumber numberWithFloat:[highestNumber floatValue]];
        }
        if ([smallestNumber floatValue] < [smallestNumberTotal floatValue]) {
            smallestNumberTotal = [NSNumber numberWithFloat:[smallestNumber floatValue]];
        }
        
        for (NSNumber *theNumber in [morning5Day objectAtIndex:4])
        {
            if([theNumber floatValue] > 0){
                if ([theNumber floatValue] > [highestNumber floatValue]) {
                    highestNumber = [NSNumber numberWithFloat:[theNumber floatValue]];
                }
                if ([theNumber floatValue] < [smallestNumber floatValue]) {
                    smallestNumber = [NSNumber numberWithFloat:[theNumber floatValue]];
                }
            }
        }
        if ([highestNumber floatValue] > [highestNumberTotal floatValue]) {
            highestNumberTotal = [NSNumber numberWithFloat:[highestNumber floatValue]];
        }
        if ([smallestNumber floatValue] < [smallestNumberTotal floatValue]) {
            smallestNumberTotal = [NSNumber numberWithFloat:[smallestNumber floatValue]];
        }
        
        
        NSLog(@"greatest = %f and smallest = %f",[highestNumberTotal floatValue], [smallestNumberTotal floatValue]);
        
        yGlobalLength = [highestNumberTotal floatValue] - [smallestNumberTotal floatValue]+10;
        yShowAreaLength = yGlobalLength;
        NSLog(@"y global length %f",yGlobalLength);
        yGlobalOrigin = [smallestNumberTotal floatValue]-5;
      
        
        timesOfDay = [[NSArray alloc] initWithObjects:
                      @"5:00",@"",@"",@"",@"",@"",@"5:30",@"",@"",@"",@"",@"",
                      @"6:00",@"",@"",@"",@"",@"",@"6:30",@"",@"",@"",@"",@"",
                      @"7:00",@"",@"",@"",@"",@"",@"7:30",@"",@"",@"",@"",@"",
                      @"8:00",@"",@"",@"",@"",@"",@"8:30",@"",@"",@"",@"",@"",
                      @"9:00",@"",@"",@"",@"",@"",@"9:30",@"",@"",@"",@"",@"",
                      @"10:00", nil];

         
        self.graphData = morning5Day;
        
        
        if(self.graphData == nil){
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"There was an error downloading the graph data" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            [self dismissModalViewControllerAnimated:YES];
        }
        [self initPlot];

    }
}



-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Reset" style:UIBarButtonItemStylePlain target:self action:@selector(resetGraph:)];
    
    //self.navBar.title = self.title;
    
    //int maxYValue = ([[graphData objectAt: @"MAX"] intValue]) * scaleFactor;
    
    
    
    //THESE LINES CAUSE THE BLANK SCREEN BUG... weird.
//    yGlobalLength = maxYValue;
//    yShowAreaLength = maxYValue;
//    
    
    
    
}


#pragma mark - Chart behavior
-(void)initPlot {
    [self configureHost];
    [self configureGraph];
    [self configurePlots];
    [self configureAxes];    
}

-(void)configureHost {  
    //first graph
    
    self.hostView = nil;
    self.hostView = [(CPTGraphHostingView *) [CPTGraphHostingView alloc] initWithFrame:CGRectMake(0, 44, self.view.bounds.size.width, self.view.bounds.size.height-44)];
    
    self.view.backgroundColor = [UIColor blackColor];
    self.hostView.allowPinchScaling = YES;
    //self.hostView.
    [self.view addSubview:self.hostView];

}


-(IBAction)resetGraph:(id)sender{
    NSLog(@"button pressed to reset graph");
    [self.hostView setNeedsDisplay];
    [self configureGraph];
    [self configurePlots];
    [self configureAxes];
    
}


-(void)configureGraph {
    // 1 - Create the graph
    self.hostView.hostedGraph = [[CPTXYGraph alloc] initWithFrame:self.hostView.bounds];
    
    //aesthetics:
    [self.hostView.hostedGraph applyTheme:[CPTTheme themeNamed:kCPTPlainBlackTheme]];
    
    CPTMutableLineStyle *borderLineStyle = [CPTMutableLineStyle lineStyle];
    borderLineStyle.lineColor = [CPTColor clearColor];
    borderLineStyle.lineWidth = 2.0f;
    
    
    self.hostView.hostedGraph.borderLineStyle = borderLineStyle;
    //    self.hostView.hostedGraph.cornerRadius = 7.0f;
    
    
    //-3 set paddings for graph?
    self.hostView.hostedGraph.paddingBottom = 0.0f;
    self.hostView.hostedGraph.paddingTop = 0.0f;
    self.hostView.hostedGraph.paddingLeft = 0.0f;
    self.hostView.hostedGraph.paddingRight = 0.0f;
    
    
    // 4 - Set padding for plot area
    [self.hostView.hostedGraph.plotAreaFrame setPaddingLeft:38.0f];
    [self.hostView.hostedGraph.plotAreaFrame setPaddingBottom:34.0f];
    self.hostView.hostedGraph.plotAreaFrame.borderLineStyle = nil;
    
    // 5 - Enable user interactions for plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) self.hostView.hostedGraph.defaultPlotSpace;
    plotSpace.allowsUserInteraction = YES;
    
    if(self.infoLabel){ [self.infoLabel removeFromSuperview]; }
    self.infoLabel = nil;
    
    self.infoLabel = [[UILabel alloc] init];
    [self.infoLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0f]];
    [self.infoLabel setText:chosenLaneType];
    [self.infoLabel sizeToFit];
    [self.infoLabel setTextAlignment:NSTextAlignmentCenter];
    [self.infoLabel setTextColor:[UIColor whiteColor]];
    [self.infoLabel setBackgroundColor:[UIColor clearColor]];
    
    float viewWidth = self.hostView.frame.size.width;
    float labelWidth = self.infoLabel.frame.size.width;
    float labelHeight = self.infoLabel.frame.size.height;
    
    float xpos = (viewWidth/2.0f) - (labelWidth/2.0f);
    
    [self.infoLabel setFrame:CGRectMake(xpos,44,labelWidth,labelHeight)];
    
    [self.view addSubview:self.infoLabel];
}

-(void)configurePlots { 
	// 1 - Get graph and plot space
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    
    
    // 2 - Create the seven plots
    CPTScatterPlot *mondayPlot = [[CPTScatterPlot alloc] init];
    mondayPlot.dataSource = self;
    mondayPlot.identifier = @"Monday";
    CPTColor *mondayColor = [CPTColor redColor];
    mondayPlot.title = @"Mon";
    [graph addPlot:mondayPlot toPlotSpace:plotSpace];
    
    
    CPTScatterPlot *tuesdayPlot = [[CPTScatterPlot alloc] init];
    tuesdayPlot.dataSource = self;
    tuesdayPlot.identifier = @"Tuesday";
    CPTColor *tuesdayColor = [CPTColor greenColor];
    tuesdayPlot.title = @"Tue";
    [graph addPlot:tuesdayPlot toPlotSpace:plotSpace];
    
    
    
    CPTScatterPlot *wednesdayPlot = [[CPTScatterPlot alloc] init];
    wednesdayPlot.dataSource = self;
    wednesdayPlot.identifier = @"Wednesday";
    CPTColor *wednesdayColor = [CPTColor blueColor];
    wednesdayPlot.title = @"Wed";
    [graph addPlot:wednesdayPlot toPlotSpace:plotSpace];
    
    
    CPTScatterPlot *thursdayPlot = [[CPTScatterPlot alloc] init];
    thursdayPlot.dataSource = self;
    thursdayPlot.identifier = @"Thursday";
    CPTColor *thursdayColor = [CPTColor orangeColor];
    thursdayPlot.title = @"Thu";
    [graph addPlot:thursdayPlot toPlotSpace:plotSpace];
    
    
    
    CPTScatterPlot *fridayPlot = [[CPTScatterPlot alloc] init];
    fridayPlot.dataSource = self;
    fridayPlot.identifier = @"Friday";
    CPTColor *fridayColor = [CPTColor whiteColor];
    fridayPlot.title = @"Fri";
    [graph addPlot:fridayPlot toPlotSpace:plotSpace];
    
    
    
    // 3 - Set up plot space
    [plotSpace scaleToFitPlots:[NSArray arrayWithObjects:mondayPlot, tuesdayPlot,
                                wednesdayPlot, thursdayPlot, fridayPlot, nil]];
    
    
    CPTMutablePlotRange *newXRange  =
    [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(xShowAreaOrigin)length:CPTDecimalFromFloat(xShowAreaLength)];
    
    CPTMutablePlotRange *newYRange  =
    [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(yShowAreaOrigin)length:CPTDecimalFromFloat(yShowAreaLength)];
    
    
    CPTMutablePlotRange *xRange = [plotSpace.xRange mutableCopy];
    [xRange expandRangeByFactor:CPTDecimalFromCGFloat(xShowAreaScaleFactor)];
    plotSpace.xRange = newXRange ;
    
    CPTMutablePlotRange *yRange = [plotSpace.yRange mutableCopy];
    [yRange expandRangeByFactor:CPTDecimalFromCGFloat(yShowAreaScaleFactor)];
    plotSpace.yRange = newYRange;
    
    
    
#pragma mark - Global Ranges
    ///////set up max scroll values
    plotSpace.globalXRange =[CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(xGlobalOrigin) length:CPTDecimalFromFloat(xGlobalLength)];
    plotSpace.globalYRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(yGlobalOrigin) length:CPTDecimalFromFloat(yGlobalLength)];
    
    
#pragma mark - Styles and Symbols
    
    // 4 - Create styles and symbols
    CPTMutableLineStyle *mondayLineStyle = [mondayPlot.dataLineStyle mutableCopy];
    mondayLineStyle.lineWidth = 2.5;
    mondayLineStyle.lineColor = mondayColor;
    mondayPlot.dataLineStyle = mondayLineStyle;
    CPTMutableLineStyle * mondaySymbolLineStyle = [CPTMutableLineStyle lineStyle];
    mondaySymbolLineStyle.lineColor = mondayColor;
    CPTPlotSymbol *mondaySymbol = [CPTPlotSymbol ellipsePlotSymbol];
    mondaySymbol.fill = [CPTFill fillWithColor:mondayColor];
    mondaySymbol.lineStyle =  mondaySymbolLineStyle;
    mondaySymbol.size = CGSizeMake(6.0f, 6.0f);
    mondayPlot.plotSymbol = mondaySymbol;
    
    
    CPTMutableLineStyle *tuesdayLineStyle = [tuesdayPlot.dataLineStyle mutableCopy];
    tuesdayLineStyle.lineWidth = 1.0;
    tuesdayLineStyle.lineColor = tuesdayColor;
    tuesdayPlot.dataLineStyle = tuesdayLineStyle;
    CPTMutableLineStyle *tuesdaySymbolLineStyle = [CPTMutableLineStyle lineStyle];
    tuesdaySymbolLineStyle.lineColor = tuesdayColor;
    CPTPlotSymbol *tuesdaySymbol = [CPTPlotSymbol starPlotSymbol];
    tuesdaySymbol.fill = [CPTFill fillWithColor:tuesdayColor];
    tuesdaySymbol.lineStyle = tuesdaySymbolLineStyle;
    tuesdaySymbol.size = CGSizeMake(6.0f, 6.0f);
    tuesdayPlot.plotSymbol = tuesdaySymbol;
    
    
    CPTMutableLineStyle *wednesdayLineStyle = [wednesdayPlot.dataLineStyle mutableCopy];
    wednesdayLineStyle.lineWidth = 2.0;
    wednesdayLineStyle.lineColor = wednesdayColor;
    wednesdayPlot.dataLineStyle = wednesdayLineStyle;
    CPTMutableLineStyle *wednesdaySymbolLineStyle = [CPTMutableLineStyle lineStyle];
    wednesdaySymbolLineStyle.lineColor = wednesdayColor;
    CPTPlotSymbol *wednesdaySymbol = [CPTPlotSymbol diamondPlotSymbol];
    wednesdaySymbol.fill = [CPTFill fillWithColor:wednesdayColor];
    wednesdaySymbol.lineStyle = wednesdaySymbolLineStyle;
    wednesdaySymbol.size = CGSizeMake(6.0f, 6.0f);
    wednesdayPlot.plotSymbol = wednesdaySymbol;
    
    CPTMutableLineStyle *thursdayLineStyle = [thursdayPlot.dataLineStyle mutableCopy];
    thursdayLineStyle.lineWidth = 2.0;
    thursdayLineStyle.lineColor = thursdayColor;
    thursdayPlot.dataLineStyle = thursdayLineStyle;
    CPTMutableLineStyle *thursdaySymbolLineStyle = [CPTMutableLineStyle lineStyle];
    thursdaySymbolLineStyle.lineColor = thursdayColor;
    CPTPlotSymbol *thursdaySymbol = [CPTPlotSymbol diamondPlotSymbol];
    thursdaySymbol.fill = [CPTFill fillWithColor:thursdayColor];
    thursdaySymbol.lineStyle = thursdaySymbolLineStyle;
    thursdaySymbol.size = CGSizeMake(6.0f, 6.0f);
    thursdayPlot.plotSymbol = thursdaySymbol;
    
    CPTMutableLineStyle *fridayLineStyle = [fridayPlot.dataLineStyle mutableCopy];
    fridayLineStyle.lineWidth = 2.0;
    fridayLineStyle.lineColor = fridayColor;
    fridayPlot.dataLineStyle = fridayLineStyle;
    CPTMutableLineStyle *fridaySymbolLineStyle = [CPTMutableLineStyle lineStyle];
    fridaySymbolLineStyle.lineColor = fridayColor;
    CPTPlotSymbol *fridaySymbol = [CPTPlotSymbol diamondPlotSymbol];
    fridaySymbol.fill = [CPTFill fillWithColor:fridayColor];
    fridaySymbol.lineStyle = fridaySymbolLineStyle;
    fridaySymbol.size = CGSizeMake(6.0f, 6.0f);
    fridayPlot.plotSymbol = fridaySymbol;
    
    
    
    ///////////LEGEND/////////////////
    
    graph.legend = [CPTLegend legendWithGraph:graph];
    graph.legend.fill = [CPTFill fillWithColor:[CPTColor clearColor]];
    //  graph.legend.cornerRadius = 5.0;
    graph.legend.swatchSize =  CGSizeMake(7.0, 2.0);
    graph.legendAnchor = CPTRectAnchorTopRight;
    graph.legendDisplacement = CGPointMake(0, 0);
    
    graph.legend.numberOfRows = 1;
    
    CPTMutableTextStyle *legendTextStyle = [CPTMutableTextStyle textStyle];
    legendTextStyle.color = [CPTColor whiteColor];
    legendTextStyle.fontName = @"Helvetica-Bold";
    legendTextStyle.fontSize = 12.0f;
    
    graph.legend.textStyle = legendTextStyle;
    
    //CPTMutableLineStyle *legendLineStyle = [CPTMutableLineStyle lineStyle];
    //legendLineStyle.lineColor = [CPTColor whiteColor];
    //legendLineStyle.lineWidth = 4.0f;
    
    //graph.legend.swatchBorderLineStyle = legendLineStyle;
    
    // graph.legend.numberOfColumns = 3;
    
    //sublayermargin left right top botttom !!!

    
 
}

-(void)configureAxes {
	
    // 1 - Create styles
    CPTMutableTextStyle *axisTitleStyle = [CPTMutableTextStyle textStyle];
    axisTitleStyle.color = [CPTColor whiteColor];
    axisTitleStyle.fontName = @"Helvetica-Bold";
    axisTitleStyle.fontSize = 12.0f;
    
    CPTMutableLineStyle *axisLineStyle = [CPTMutableLineStyle lineStyle];
    axisLineStyle.lineWidth = 2.0f;
    axisLineStyle.lineColor = [CPTColor whiteColor];
    
    CPTMutableLineStyle *axisTickLineStyle = [CPTMutableLineStyle lineStyle];
    axisTickLineStyle.lineWidth = 1.0f;
    axisTickLineStyle.lineColor = [CPTColor whiteColor];
    
    
    CPTMutableTextStyle *axisTextStyle = [[CPTMutableTextStyle alloc] init];
    axisTextStyle.color = [CPTColor whiteColor];
    axisTextStyle.fontName = @"Helvetica-Bold";
    axisTextStyle.fontSize = 9.0f;
    
    CPTMutableLineStyle *tickLineStyle = [CPTMutableLineStyle lineStyle];
    tickLineStyle.lineColor = [CPTColor whiteColor];
    tickLineStyle.lineWidth = 2.0f;
    
    
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorGridLineStyle.lineWidth = 0.5f;
    majorGridLineStyle.lineColor = [[CPTColor grayColor] colorWithAlphaComponent:.4f] ;
    
    
    
    
    // 2 - Get axis set
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostView.hostedGraph.axisSet;
    
    
    
    //MAKE AXES STICK TO THE EDGES/////////
    ///////////////////////////////////////
    CPTXYAxis *xAxis = axisSet.xAxis;
    CPTConstraints *xAxisConstraints = [CPTConstraints constraintWithLowerOffset:0.0f];
    xAxis.axisConstraints = xAxisConstraints;
    
    CPTXYAxis *yAxis = axisSet.yAxis;
    CPTConstraints *yAxisConstraints = [CPTConstraints constraintWithRelativeOffset:0.0f];
    yAxis.axisConstraints = yAxisConstraints;
    /////////////////////////////////////
    /////////////////////////////////////
    
    // 3 - Configure x-axis
    CPTXYAxis *x = axisSet.xAxis;
    x.title = @"Hour (AM)";
    x.titleTextStyle = axisTitleStyle;
    x.titleOffset = 14.0f;
    x.axisLineStyle = axisLineStyle;
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    x.labelTextStyle = axisTextStyle;
    
    x.majorTickLineStyle = axisTickLineStyle;
    x.majorTickLength = 5.0f;
    x.minorTickLineStyle = axisTickLineStyle;
    x.minorTickLength = 2.0f;
    x.tickDirection = CPTSignNegative;
    
    BOOL switch12 = YES;        
        
    //sets the labels array in the x axis
    CGFloat timeCount = [timesOfDay count];
    NSMutableSet *xLabels = [NSMutableSet setWithCapacity:timeCount];
    NSMutableSet *xLocations = [NSMutableSet setWithCapacity:timeCount];
    NSInteger i = 0;
    
    for (NSString *times in timesOfDay) {
        
        CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:times  textStyle:x.labelTextStyle];
        
        if([times isEqualToString:@"12"] && switch12 == YES){
            label = [[CPTAxisLabel alloc] initWithText:@"12 am"  textStyle:x.labelTextStyle];
            switch12 = NO;
        }else if([times isEqualToString:@"12"] && switch12 == NO){
            label = [[CPTAxisLabel alloc] initWithText:@"12 pm"  textStyle:x.labelTextStyle];
            switch12 = YES;
        }
        
        CGFloat location = i++;
        label.tickLocation = CPTDecimalFromCGFloat(location);
        label.offset = x.majorTickLength + 1;
        if (label) {
            [xLabels addObject:label];
            [xLocations addObject:[NSNumber numberWithFloat:location]];
        }
    }
    
    x.axisLabels = xLabels;
    x.majorTickLocations = xLocations;
    
	// 4 - Configure y-axis
    CPTXYAxis *y = axisSet.yAxis;
    y.title = @"Commute Time (in mins)";
    y.titleTextStyle = axisTitleStyle;
    y.titleOffset = -34.0f;
    y.axisLineStyle = axisLineStyle;
    
    
    y.labelingPolicy = CPTAxisLabelingPolicyNone;
    y.labelTextStyle = axisTextStyle;
    y.labelOffset = 16.0f;
    y.majorTickLineStyle = axisTickLineStyle;
    y.majorTickLength = 4.0f;
    y.tickDirection = CPTSignPositive;
    y.majorGridLineStyle = majorGridLineStyle;
    
    
    
    
    // calculates the value so that there is 10 ticks that increment in multiples of 5
    int incrementSize;
    int tempIncrementSize = (yGlobalLength / 11);
    NSLog(@"temp increment = %d",tempIncrementSize);
    int remainder = tempIncrementSize % 5;
    if (remainder == 0){
        incrementSize = tempIncrementSize;
    }else{
        incrementSize = tempIncrementSize + 5 - remainder;
    }
    NSLog(@"inc size = %d",incrementSize);
    
    if(incrementSize == 0){
        incrementSize = 1;
    } else {
        int globalRemainder = (int)yGlobalOrigin%5;
        if(globalRemainder != 0){
            yGlobalOrigin+= (5-globalRemainder);
        }
    }
    //sets the labels array in the y axis
    NSInteger majorIncrement = incrementSize;
    CGFloat yMax;  // should determine dynamically based on max price
    if(yGlobalLength < 6){
        yMax = 6;  // should determine dynamically based on max price
    }else{
        yMax = yGlobalLength;  // should determine dynamically based on max price
    }
    
    NSMutableSet *yLabels = [NSMutableSet set];
    NSMutableSet *yMajorLocations = [NSMutableSet set];
    for (NSInteger j = majorIncrement; j <= yMax; j += majorIncrement) {
        
        CPTAxisLabel *label = [[CPTAxisLabel alloc]
                               initWithText:[NSString stringWithFormat:@"%i", j+(int)yGlobalOrigin]
                               textStyle:y.labelTextStyle];
        NSDecimal location = CPTDecimalFromInteger(j+yGlobalOrigin);
        label.tickLocation = location;
        label.offset = -y.majorTickLength - y.labelOffset;
        if (label) {
            [yLabels addObject:label];
        }
        
        [yMajorLocations addObject:
         [NSDecimalNumber decimalNumberWithDecimal:location]];
        
        
    }
    y.axisLabels = yLabels;
    y.majorTickLocations = yMajorLocations;
 
}

#pragma mark - Rotation
/*
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [self willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}
*/
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

#pragma mark - CPTPlotDataSource methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
	return [timesOfDay count];
}



-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
    NSInteger valueCount = [timesOfDay count];
    
    //NSLog(@"plot id = %@", plot.identifier);
    
    //NSString *indexString = [NSString stringWithFormat:@"%d", index];
    
    switch (fieldEnum) {
            
        case CPTScatterPlotFieldX:
            if (index < valueCount) {
                return [NSNumber numberWithUnsignedInteger:index];
            }
            break;
            
            //each plot item has an identifier that you can give it
        case CPTScatterPlotFieldY:
             if ([plot.identifier isEqual:@"Monday"] == YES) {
                return [[graphData objectAtIndex:0] objectAtIndex:index];
            }
            else if ([plot.identifier isEqual:@"Tuesday"] == YES) {
                return [[graphData objectAtIndex:1] objectAtIndex:index];
            }
            else if ([plot.identifier isEqual:@"Wednesday"] == YES) {
                return [[graphData objectAtIndex:2] objectAtIndex:index];
            }
            else if ([plot.identifier isEqual:@"Thursday"] == YES) {
                return [[graphData objectAtIndex:3] objectAtIndex:index];
            }
            else if ([plot.identifier isEqual:@"Friday"] == YES) {
                return [[graphData objectAtIndex:4] objectAtIndex:index];
            }
            break;
    }
    return [NSDecimalNumber zero];
    //NSLog(@"found no value for plot %@, at field: %@, at index %@");
    
    
}

-(IBAction) backToMenu:(id)sender{
    NSLog(@"trying to leave graphs");
    [appDelegate.menuNavController dismissModalViewControllerAnimated:YES];
}



#pragma mark - generate Data methods








@end
